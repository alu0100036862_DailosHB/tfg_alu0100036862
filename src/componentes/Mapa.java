package componentes;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.ImageObserver;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Properties;

import javax.swing.*;
import javax.swing.JFormattedTextField.AbstractFormatter;
import javax.swing.border.BevelBorder;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

import formularios.frmAyuda;
import formularios.frmInfoVuelo;
import formularios.frmNuevoVuelo;

/**
 * Clase Mapa
 * Clase encargada del funcionamiento de la app
 * @author Dailos
 *
 */
@SuppressWarnings("serial")
public class Mapa extends JFrame implements ActionListener, MouseListener {
  
	// Variables para pintar la pantalla
	private JPanel jpanel, jpnlSuperior, jpnlCarga, jpnlSeleccion, jpnlInfFich, jpnlEdicion, jpnlInferior, jpnlInfoIzq, jpnlInfoDer;
	private JLabel jlblInfoIzq, jlblInfoDer, jlblInfoRetraso, jlblInfoAdelanto, jlblInfoCancelacion, jlblInfoNuevo, lblArchivoSolucion, lblArchivoConfiguracion;
	private JButton jbtnCargar, jbtnAppBinter, jbtnEdicion, jbtnAbrirSol, jbtnAbrirConf, jbtnGuardar, jbtnGenerarSol, jbtnAyuda;
	private JTextArea jtxtADatos;
	JDatePickerImpl datePicker;
	JSpinner jspnValor;
	
	int preX, preY, valorSolucion, numVuelos, numAviones, numPilotos, numCambios, vueloEnEdicion = -1;
  
	String _RutaFicheros = "", _FichSol = "", _FichConf = "", _FichVuelos = "";
	private ArrayList<Ediciones> Edit = new ArrayList<Ediciones>();
	private ArrayList<Vuelo> EditNuevosVuelos = new ArrayList<Vuelo>();
  private ArrayList<ArrayList<Vuelo>> info = new ArrayList<ArrayList<Vuelo>>();
  private ArrayList<ArrayList<VuelosPilotos>> infoPilotos = new ArrayList<ArrayList<VuelosPilotos>>();
	FicherosInfo fich;
  PintarVuelos mapaVuelos;
  Graphics2D g2;
  
  String PATH = System.getProperty("user.dir");
	String RUTA = PATH + "/code2016/dati";
	String EJECUTABLE = PATH + "/code2016/Routing/x64/Release/Binter.exe";
	int RANGOMIN = 360; // 06:00 horas en minutos
	int RANGOMAX = 1380; // 23:00 horas en minutos
	int TRAMO = 5; // tramos de horario
	int HORA = 60; // valor de la hora
	int EDICION = 0;
	
  /**
   * Obtenemos el n�mero de cambios
   * @return
   */
  public int getNumCambios() {
  	
  	int cont = 0;
  	
		// Tratamos los vuelos de los pilotos
		for (int i = 0; i < infoPilotos.size(); i++) {
			int avion = -1;
			for (int j = 0; j < infoPilotos.get(i).size(); j++) {
				
				// Buscamos el vuelo
				String vuelo = infoPilotos.get(i).get(j).getNumVuelo();
				for (int a = 0; a < info.size(); a++) {
					for (int b = 0; b < info.get(a).size(); b++) {
						if (info.get(a).get(b).getVuelo().equals(vuelo)) {
							if (avion != a) {
								if (avion != -1)
									cont++;
								avion = a;
							}
						}							
					}
				}
			}			
		}
  	
  	return cont;
  	
  }
  
  /**
   * Cargamos los componentes del menu superior, parte izquierda
   */
  private void CargarMenuSuperiorIzquierda() {
  	
  	GridLayout grlCarga = new GridLayout(2,0); 	        
  	jpnlCarga = new JPanel();
  	jpnlCarga.setLayout(grlCarga);
    
  	// Fecha
    UtilDateModel model = new UtilDateModel();
    //Calendar fechaHoy = Calendar.getInstance();
    //model.setDate(fechaHoy.get(Calendar.YEAR), fechaHoy.get(Calendar.MONTH), fechaHoy.get(Calendar.DAY_OF_MONTH));
    model.setDate(2012, 8, 1);
    model.setSelected(true);
    
	  Properties prop = new Properties();
	  prop.put("text.today", "D�a");
	  prop.put("text.month", "Mes");
	  prop.put("text.year", "A�o");
	  JDatePanelImpl datePanel = new JDatePanelImpl(model, prop);
	  datePicker = new JDatePickerImpl(datePanel, new DateLabelFormatter());

		//jbtnCargar = new JButton(new ImageIcon("./others/imagenes/botones/cargar.jpg"));
	  ImageIcon icon = new ImageIcon("./others/imagenes/iconos/play-button-1.png");
		Image img = icon.getImage().getScaledInstance(20,20,java.awt.Image.SCALE_SMOOTH);
		jbtnCargar = new JButton(new ImageIcon(img));
		jbtnCargar.addActionListener(this);
		jbtnCargar.setToolTipText("Cargar soluci�n");		
		
		//jbtnGenerarSol = new JButton(new ImageIcon("./others/imagenes/botones/generar.jpg"));
		icon = new ImageIcon("./others/imagenes/iconos/success.png");
		img = icon.getImage().getScaledInstance(20,20,java.awt.Image.SCALE_SMOOTH);
		jbtnGenerarSol = new JButton(new ImageIcon(img));
		jbtnGenerarSol.addActionListener(this);
		jbtnGenerarSol.setToolTipText("Generar soluci�n");
		
		//jbtnAbrirSol = new JButton(new ImageIcon("./others/imagenes/botones/abrirSol.jpg"));
		icon = new ImageIcon("./others/imagenes/iconos/folder-13.png");
		img = icon.getImage().getScaledInstance(20,20,java.awt.Image.SCALE_SMOOTH);
		jbtnAbrirSol = new JButton(new ImageIcon(img));
		jbtnAbrirSol.addActionListener(this);
		jbtnAbrirSol.setToolTipText("Abrir fichero de Soluci�n ya existente");
		
		//jbtnAbrirConf = new JButton(new ImageIcon("./others/imagenes/botones/abrirConf.jpg"));
		icon = new ImageIcon("./others/imagenes/iconos/settings-5.png");
		img = icon.getImage().getScaledInstance(20,20,java.awt.Image.SCALE_SMOOTH);
		jbtnAbrirConf = new JButton(new ImageIcon(img));
		jbtnAbrirConf.addActionListener(this);
		jbtnAbrirConf.setToolTipText("Abrir fichero de Configuraci�n");
		
		//jbtnGuardar = new JButton(new ImageIcon("./others/imagenes/botones/guardar.jpg"));
		icon = new ImageIcon("./others/imagenes/iconos/save.png");
		img = icon.getImage().getScaledInstance(20,20,java.awt.Image.SCALE_SMOOTH);
		jbtnGuardar = new JButton(new ImageIcon(img));
		jbtnGuardar.addActionListener(this);
		jbtnGuardar.setToolTipText("Guardar soluci�n");
		
		//jbtnAyuda = new JButton(new ImageIcon("./others/imagenes/botones/question.jpg"));
		icon = new ImageIcon("./others/imagenes/iconos/info.png");
		img = icon.getImage().getScaledInstance(20,20,java.awt.Image.SCALE_SMOOTH);
		jbtnAyuda = new JButton(new ImageIcon(img));
		jbtnAyuda.addActionListener(this);
		jbtnAyuda.setToolTipText("Ayuda");		

    jbtnEdicion = new JButton();
    jbtnEdicion.setText("Modo Edici�n");
    jbtnEdicion.setBackground(new Color(0,100,00));
    jbtnEdicion.setForeground(Color.WHITE);
    jbtnEdicion.addActionListener(this);
    
    lblArchivoSolucion = new JLabel("", JLabel.LEFT);
    lblArchivoConfiguracion = new JLabel("", JLabel.LEFT);

    jpnlSeleccion = new JPanel();
    jpnlSeleccion.setLayout(new FlowLayout(FlowLayout.LEFT)); 

    jpnlSeleccion.add(jbtnAyuda);
    jpnlSeleccion.add(jbtnGuardar);
    //jpnlSeleccion.add(datePicker);
    jpnlSeleccion.add(jbtnAbrirConf);
    jpnlSeleccion.add(jbtnAbrirSol);
    jpnlSeleccion.add(jbtnGenerarSol);
    jpnlSeleccion.add(jbtnCargar);
    jpnlSeleccion.add(jbtnEdicion);
    jpnlCarga.add(jpnlSeleccion);
    
    GridLayout grlInfFich = new GridLayout(0,2); 	        
    jpnlInfFich = new JPanel();
    jpnlInfFich.setLayout(grlInfFich);
  	
    jpnlInfFich.add(lblArchivoConfiguracion);
    jpnlInfFich.add(lblArchivoSolucion);
    jpnlCarga.add(jpnlInfFich);
    
  }
  
  /**
   * Cargamos los componentes del menu superior, parte derecha
   */
  private void CargarMenuSuperiorDerecha() {
  	
  	SpinnerNumberModel modelSpinner = new SpinnerNumberModel(5, 5, 600, 5); 
    jspnValor = new JSpinner(modelSpinner);
    JFormattedTextField jtxtValor = ((JSpinner.DefaultEditor) jspnValor.getEditor()).getTextField();
    jtxtValor.setEditable(false);
    
		jbtnAppBinter = new JButton();
		jbtnAppBinter.setText("Generar soluci�n");
		jbtnAppBinter.setBackground(new Color(170, 28, 71));
		jbtnAppBinter.setForeground(Color.WHITE);
		jbtnAppBinter.addActionListener(this);
    
    jpnlEdicion = new JPanel();
    jpnlEdicion.setLayout(new FlowLayout(FlowLayout.RIGHT));

    jpnlEdicion.add(new JLabel("Aplicar: ", JLabel.RIGHT));
    jpnlEdicion.add(jspnValor);
    jpnlEdicion.add(new JLabel("minutos", JLabel.LEFT));
    jpnlEdicion.add(jbtnAppBinter);
    
    jpnlEdicion.setVisible(false);

  }
    
  /**
   * Carga el men� superior
   */
  private void CargarMenuSuperior() {
  	
  	CargarMenuSuperiorIzquierda();
  	CargarMenuSuperiorDerecha();

		// A�adimos al panel
    jpnlSuperior = new JPanel();
    jpnlSuperior.setLayout(new GridLayout(1, 2)); 
    jpnlSuperior.add(jpnlCarga);
    jpnlSuperior.add(jpnlEdicion);
 
		Toolkit tk = Toolkit.getDefaultToolkit();
		int xSize = (int)tk.getScreenSize().getWidth();
		
		jpnlSuperior.setMinimumSize(new Dimension(xSize,70));
		jpnlSuperior.setPreferredSize(new Dimension(xSize, 70));
		jpnlSuperior.setMaximumSize(new Dimension(xSize,70));
		
		jpanel = new JPanel();
    jpanel.setLayout(new BoxLayout(jpanel, BoxLayout.PAGE_AXIS));
		jpanel.add(jpnlSuperior).setLocation(0, 0);
		
  }
  
  /**
   * Carga el men� inferior
   */
  private void CargarMenuInferior() {
  	
  	jpnlInferior = new JPanel();
		jpnlInferior.setLayout(new GridLayout(1, 2));
		
		jpnlInfoIzq = new JPanel();
		jpnlInfoIzq.setLayout(new FlowLayout(FlowLayout.LEFT));
		
		jpnlInfoDer = new JPanel();
		jpnlInfoDer.setLayout(new FlowLayout(FlowLayout.RIGHT));
		
		jpnlInferior.add(jpnlInfoIzq);
		jpnlInferior.add(jpnlInfoDer);

		Toolkit tk = Toolkit.getDefaultToolkit();
		int xSize = (int)tk.getScreenSize().getWidth();
		
		jpnlInferior.setMinimumSize(new Dimension(xSize,40));
		jpnlInferior.setPreferredSize(new Dimension(xSize, 40));
		jpnlInferior.setMaximumSize(new Dimension(xSize,40));

		jpanel.add(jpnlInferior).setLocation((int)tk.getScreenSize().getHeight()+40, 0);
		
  }
  
	/**
	 * Constructor
	 */
	public Mapa() {
	
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		CargarMenuSuperior();
		CargarMenuInferior();
		
		add(jpanel);
		
		//setSize(750,500);
		setTitle("TFG alu0100036862");
		setLocationRelativeTo(null);
		setExtendedState(JFrame.MAXIMIZED_BOTH); 
		Toolkit kit = Toolkit.getDefaultToolkit();
		Image img = kit.createImage("./others/imagenes/logos/logoULL.jpg");
		setIconImage(img);		
		setVisible(true);
		
	}
	
	/**
	 * Mostramos el mapa de vuelos
	 */
	private void mostrarMapa (int isEdicion) {
    
		int adaptedWidth = (int) (this.getWidth() * 0.8);

		jpanel.removeAll();
		jpanel.add(jpnlSuperior).setLocation(0, 0);		
    
		mapaVuelos = new PintarVuelos(info, infoPilotos, adaptedWidth, isEdicion);
		jpanel.add(mapaVuelos, BorderLayout.CENTER);
		
		mapaVuelos.addMouseListener(this);
	  addMouseListener(this);
		
		jpnlInferior.removeAll();
		jpnlInfoIzq.removeAll();
		jpnlInfoDer.removeAll();
		Toolkit tk = Toolkit.getDefaultToolkit();

		valorSolucion = fich.getValorSolucion();
		numVuelos = fich.getNumVuelos();
		numAviones = fich.getNumAviones();
		numPilotos = fich.getNumPilotos();
		numCambios = getNumCambios();
		
		String strAux = "Valor de la soluci�n: " + String.valueOf(valorSolucion);		
		
		if (EDICION == 1) {
			
			jlblInfoRetraso	= new JLabel("Retraso", JLabel.CENTER);
			jlblInfoRetraso.setBackground(Color.ORANGE);
			jlblInfoRetraso.setOpaque(true);
			jlblInfoAdelanto = new JLabel("Adelanto", JLabel.CENTER);
			jlblInfoAdelanto.setBackground(Color.GREEN);
			jlblInfoAdelanto.setOpaque(true);
			jlblInfoCancelacion	= new JLabel("Cancelaci�n", JLabel.CENTER);
			jlblInfoCancelacion.setBackground(Color.RED);
			jlblInfoCancelacion.setOpaque(true);
			jlblInfoNuevo = new JLabel("Nuevo", JLabel.CENTER);
			jlblInfoNuevo.setBackground(Color.CYAN);
			jlblInfoNuevo.setOpaque(true);

			jpnlInfoIzq.add(new JLabel("Leyenda: "));
			jpnlInfoIzq.add(jlblInfoRetraso);
			jpnlInfoIzq.add(jlblInfoAdelanto);
			jpnlInfoIzq.add(jlblInfoCancelacion);
			jpnlInfoIzq.add(jlblInfoNuevo);
			strAux = String.valueOf(numVuelos) + " vuelos y " + String.valueOf(numAviones) + " aviones";
			
		} else {
			
			jlblInfoIzq	= new JLabel(strAux, JLabel.LEFT);
			jpnlInfoIzq.add(jlblInfoIzq);
			strAux = String.valueOf(numVuelos) + " vuelos, " + String.valueOf(numAviones) + " aviones, " + String.valueOf(numPilotos) + " pilotos y " + String.valueOf(numCambios) + " cambios";
			
		}
		
		jlblInfoDer	= new JLabel(strAux, JLabel.RIGHT);	
		jpnlInfoDer.add(jlblInfoDer);
		
		jpnlInferior.add(jpnlInfoIzq);
		jpnlInferior.add(jpnlInfoDer);
		jpanel.add(jpnlInferior).setLocation((int)tk.getScreenSize().getHeight(), 0);
		
		this.add(jpanel);
		this.invalidate();
		this.validate();
		this.repaint();
		
	}
	
	/**
	 * Obtenemos el nombre del fichero de vuelos
	 * @param fichconf
	 * @return
	 * @throws IOException 
	 */
	@SuppressWarnings("resource")
	public String ObtenerFicheroVuelos (String fichconf, String fecha) throws IOException {
		
		String fich = "";
		
		// Obtenemos el fichero de vuelos desde el de configuracion
		String cadena;
		FileReader fsalida = new FileReader(RUTA + "/" + fichconf);
		BufferedReader buffer = new BufferedReader(fsalida);
		
    // Copiamos el fichero en el nuevo
		boolean encontrado = false;
    while (((cadena = buffer.readLine()) != null) && (encontrado == false)) {

    	//System.out.println(cadena);
  		String[] parts = cadena.split(" ");
  		if (parts[0].equals("<ficheroVuelos>")) {
  			fich = parts[1];
  			encontrado = true;
  		}
  		
  	}
    
    // El fichero de vuelos es DDMMAAAA_ficheroVuelos.txt
    String[] parts = fecha.split("/"); 
    fich = parts[0] + parts[1] + parts[2] + "_" + fich; 
	  
		return fich;
		
	}
		
	/**
	 * Muestra los datos cargados en la estructura
	 */
	@SuppressWarnings("unused")
	private void mostarDatosCargados () {
		
		String datos = "";
		// mostramos el contenido		
		for (int i = 0; i < info.size(); i++) {
			
			datos += "L�nea a�rea " + Integer.toString(i + 1) + "\n";
			datos += "---------------------------------------------------------" + "\n";
			
			for (int j = 0; j < info.get(i).size(); j++) {				
				datos += Integer.toString(info.get(i).get(j).getOrden()) + " " + info.get(i).get(j).getCompa�ia() + " " + info.get(i).get(j).getVuelo() + " " + info.get(i).get(j).getAvion() + " " + info.get(i).get(j).getPiloto() + " " + info.get(i).get(j).getOrigen() + " " + info.get(i).get(j).getDestino() + " " + info.get(i).get(j).getHoraInicio() + " " + info.get(i).get(j).getHoraFin() + "\n";
			}
			 
			datos += "\n";
			 
		}
		
		jtxtADatos.setText(datos);
		
	}
	
	/**
	 * Crea el texto del fichero de Solucion
	 * @return
	 */
	private String CrearTextoFicheroSolucion(String fecha) {
		
		String Separador = ";", SaltoLinea = "\r";
		String TextoFichero = fecha + SaltoLinea + String.valueOf(valorSolucion) + SaltoLinea;
		
		// Tratamos los vuelos de los aviones
		boolean bolPrimerVuelo;
		for (int i = 0; i < info.size(); i++) {			
			
			bolPrimerVuelo = true;
			for (int j = 0; j < info.get(i).size(); j++) {

				// Cogemos la compa��a del primer vuelo
				if (bolPrimerVuelo == true) {
					TextoFichero += info.get(i).get(j).getCompa�ia() + Separador + info.get(i).get(j).getVuelo();
					bolPrimerVuelo = false;
				} else {				
					// No a�adimos el separador en el ultimo vuelo
					if (j <= (info.get(i).size() - 1))
						TextoFichero += Separador;
					
					TextoFichero += info.get(i).get(j).getVuelo();
				}
			}
			 
			TextoFichero += SaltoLinea;
			
		}
		
		// Separaci�n entre aviones y pilotos
		TextoFichero += SaltoLinea + "---" + SaltoLinea;
		
		// Tratamos los pilotos de los aviones
		for (int i = 0; i < infoPilotos.size(); i++) {
			for (int j = 0; j < infoPilotos.get(i).size(); j++) {
				TextoFichero += infoPilotos.get(i).get(j).getNumVuelo() + Separador;
			}			 
			TextoFichero += SaltoLinea;			
		}
		
		return TextoFichero;
		
	}
	
	/**
	 * Cargar / Editar 
	 */
	private void CargarEditar(String fecha, boolean opc) {
		
		/*
		// Comprobamos la elecci�n de la fecha
		if ((fecha.length() < 10) || (fecha.isEmpty() == true)) {
  		JOptionPane.showMessageDialog(null, "Debe seleccionar una fecha!");
  		return;
		}
		*/
		
		// Comprobamos la elecci�n de un fichero de configuraci�n
		if ((_FichConf.length() < 3) || (_FichConf.isEmpty() == true) || (_RutaFicheros.length() < 3) || (_RutaFicheros.isEmpty() == true)) {
  		JOptionPane.showMessageDialog(null, "Debe seleccionar un fichero con la configuraci�n!");
  		return;
		}
		
		// Comprobamos la elecci�n de un fichero de soluci�n
		if ((_FichSol.length() < 3) || (_FichSol.isEmpty() == true) || (_RutaFicheros.length() < 3) || (_RutaFicheros.isEmpty() == true)) {
  		JOptionPane.showMessageDialog(null, "Debe seleccionar un fichero con la soluci�n!");
  		return;
		}
		
		try {
			
			// Fecha para la carga
	    fich = new FicherosInfo(fecha, _FichVuelos, _FichSol);
	    info.clear();
	    infoPilotos.clear();
			try { 
    		info = fich.cargarInformacion(RUTA + "/");
		    infoPilotos = fich.ObtenerVuelosPilotos();
			} catch (java.text.ParseException e1) {
				e1.printStackTrace();
			}	
			
			EDICION = 0;
	    jpnlEdicion.setVisible(false);
	    jbtnGuardar.setVisible(true);
	    jbtnAbrirSol.setVisible(true);
	    jbtnAbrirConf.setVisible(true);
	    jbtnGenerarSol.setVisible(true);
	    jbtnAyuda.setVisible(true);
			if (opc == true) {
				EDICION = 1;			    
		    jpnlEdicion.setVisible(true);
		    jbtnGuardar.setVisible(false);
		    jbtnAbrirSol.setVisible(false);
		    jbtnAbrirConf.setVisible(false);
		    jbtnGenerarSol.setVisible(false);
		    jbtnAyuda.setVisible(false);
				Edit.clear();
			}
			this.mostrarMapa(EDICION);
	    
		} catch (IOException e1) {
			e1.printStackTrace();
			return;
		}
		
	}
	
	/**
	 * Llamada al programa binter
	 * @param fecha
	 * @param parts
	 */
	private void LlamadaBinter(String fecha, String[] parts) {
		
		if (JOptionPane.showConfirmDialog(null, "Este proceso durar� varios minutos (30 como m�ximo)\n�Desea continuar?", "Mensaje", JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION)
			return;
			
		try {

  		String fichSol = _FichConf; //"config" + parts[0] + "_" + parts[1] + "_" + parts[2] + ".txt";
  		
	    FicherosInfo fich = new FicherosInfo(fecha, _FichVuelos, _FichSol);

	    String strNombreFicheroVuelosNewAux = "NEW_" + _FichVuelos;
	    String strNombreFicheroVuelosNew = strNombreFicheroVuelosNewAux.replace(parts[0] + parts[1] + parts[2] + "_", "");
	    
	    // Backup del fichero de vuelos diarios
	    fich.BackupFichero(RUTA + "/" + _FichVuelos, RUTA + "/BACKUP_" + _FichVuelos);
	    // Backup del fichero de vuelos
	    fich.BackupFichero(RUTA + "/" + _FichVuelos.replace(parts[0] + parts[1] + parts[2] + "_", ""), RUTA + "/BACKUP_" + _FichVuelos.replace(parts[0] + parts[1] + parts[2] + "_", ""));
	    
	    // Crear nuevo fichero de vuelos con las modificaciones
	    fich.CrearFicheroVuelos(strNombreFicheroVuelosNew.replace("NEW_", ""), strNombreFicheroVuelosNew, strNombreFicheroVuelosNewAux, Edit, EditNuevosVuelos);
  		
	    File f1 = new File(RUTA + "/" + _FichVuelos.replace(parts[0] + parts[1] + parts[2] + "_", ""));
	    File f2 = new File(RUTA + "/" + strNombreFicheroVuelosNew);
	    
	    // Borramos el fichero de configuracion y renombramos el nuevo
	    if (!f1.delete())
	    	JOptionPane.showMessageDialog(null, "El fichero de vuelos general no puede ser borrado");
	    
	    boolean correcto = f2.renameTo(f1);
	    if (!correcto)
	    	JOptionPane.showMessageDialog(null, "El fichero de vuelos general no se ha podido renombrar");
	    
	    File fvueloDiario = new File(RUTA + "/" + _FichVuelos);
	    File fvueloDiarioNew = new File(RUTA + "/" + strNombreFicheroVuelosNewAux);
	    
	    // Borramos el fichero de configuracion y renombramos el nuevo
	    if (!fvueloDiario.delete())
	    	JOptionPane.showMessageDialog(null, "El fichero de vuelos diarios no puede ser borrado");
	    
	    correcto = fvueloDiarioNew.renameTo(fvueloDiario);
	    if (!correcto)
	    	JOptionPane.showMessageDialog(null, "El fichero de vuelos diarios no se ha podido renombrar");
	    
	    //System.out.println("Vamos a BINTER");
	    
  		File fichero = new File(RUTA + "/" + fichSol);
  		if (!fichero.exists()) {
    		JOptionPane.showMessageDialog(null, "No existe el fichero de configuraci�n!");
    		return;
  		}
  		Process p = Runtime.getRuntime().exec("cmd.exe /c cd " + RUTA + " && " + EJECUTABLE + " " + fichSol);
      BufferedReader in = new BufferedReader(  
                          new InputStreamReader(p.getInputStream()));  
      
      // Devoluci�n de la ejecuci�n del programa Binter.exe
      String line = null;
			boolean error = false, sinSolucion = false;
			while ((line = in.readLine()) != null) {  
				System.out.println(line);  
				if (line.equals("Cannot proceed with execution"))
					sinSolucion = true;			  
				if (line.equals("durata IP 1800.000000 secondi"))
					error = true;
			}
			
			if (sinSolucion) {
				JOptionPane.showMessageDialog(null, "No se ha podido encontrar una soluci�n para la configuraci�n elegida!", "Mensaje", JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			if (error) {
				JOptionPane.showMessageDialog(null, "Se ha superado el tiempo tope de proceso.\nLa soluci�n puede no ser �ptima ni contener todas las incidencias informadas!", "Mensaje", JOptionPane.ERROR_MESSAGE);
			}
      
      _FichSol = parts[0] + parts[1] + parts[2] + "_salida.txt";
      
      CargarEditar(fecha, false);
      
		} catch (IOException e1) {
			e1.printStackTrace();
			return;
		}
		
	}
	
	/**
	 * Abrir soluci�n
	 * @param fecha
	 * @param parts
	 */	
	private void AbrirSolucion() {
		
		JFileChooser fc = new JFileChooser(RUTA);
    FileFilter filter = new FileNameExtensionFilter("Texto","txt", "text");
    fc.setAcceptAllFileFilterUsed(false);
    fc.setDialogTitle("Abrir archivo de soluci�n");
    fc.setFileFilter(filter);
    int respuesta = fc.showOpenDialog(this);
    
    // Comprobar si se ha pulsado Aceptar
    if (respuesta == JFileChooser.APPROVE_OPTION) {
      File archivoElegido = fc.getSelectedFile();
      try {
      	_FichSol = archivoElegido.getName();
      	lblArchivoSolucion.setText("Fichero Soluci�n: " + _FichSol);
      	_RutaFicheros = archivoElegido.getCanonicalPath().toString().substring(0, (archivoElegido.getCanonicalPath().toString()).indexOf(_FichSol));
			} catch (IOException e1) {
				e1.printStackTrace();
			}
    }
    
	}
	
	
/**
	 * Generar soluci�n llamando al programa Binter
	 * @param fecha
	 * @param parts
	 */
	private void GenerarSolucion(String fecha, String[] parts) {
		
		// Comprobamos la elecci�n de un fichero de configuraci�n
		if ((_FichConf.length() < 3) || (_FichConf.isEmpty() == true) || (_RutaFicheros.length() < 3) || (_RutaFicheros.isEmpty() == true)) {
  		JOptionPane.showMessageDialog(null, "Debe seleccionar un fichero con la configuraci�n!");
  		return;
		}
		
		if (JOptionPane.showConfirmDialog(null, "Este proceso durar� varios minutos (30 como m�ximo)\n�Desea continuar?", "Mensaje", JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION)
			return;
			
		try {
			
			Process p = Runtime.getRuntime().exec("cmd.exe /c cd " + RUTA + " && " + EJECUTABLE + " " + _FichConf);				
			BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream())); 
			
			String line = null;
			boolean error = false, sinSolucion = false;
			while ((line = in.readLine()) != null) {  
				System.out.println(line);  
				if (line.equals("Cannot proceed with execution"))
					sinSolucion = true;			  
				if (line.equals("durata IP 1800.000000 secondi"))
					error = true;
			}
			
			if (sinSolucion) {
				JOptionPane.showMessageDialog(null, "No se ha podido encontrar una soluci�n para la configuraci�n elegida!", "Mensaje", JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			if (error) {
				JOptionPane.showMessageDialog(null, "Se ha superado el tiempo tope de proceso.\nLa soluci�n puede no ser �ptima ni contener todas las incidencias informadas!", "Mensaje", JOptionPane.ERROR_MESSAGE);
			}
			
			_FichSol = parts[0] + parts[1] + parts[2] + "_salida.txt";
			
			try {
        
				// Fecha para la carga
		    fich = new FicherosInfo(fecha, _FichVuelos, _FichSol);
		    info.clear();
		    infoPilotos.clear();
		    Edit.clear();
		    EditNuevosVuelos.clear();
				try { 
					//info = fich.cargarInformacion("./planificacion/");
	    		info = fich.cargarInformacion(RUTA + "/");
			    infoPilotos = fich.ObtenerVuelosPilotos();
				} catch (java.text.ParseException e1) {
					e1.printStackTrace();
				}
				
				EDICION = 0;
		    jpnlEdicion.setVisible(false);
		    jbtnGuardar.setVisible(true);
		    jbtnAbrirSol.setVisible(true);
		    jbtnAbrirConf.setVisible(true);
		    jbtnGenerarSol.setVisible(true);
		    jbtnAyuda.setVisible(true);

				this.mostrarMapa(EDICION);
				
		    jpnlInferior.add(jpnlInfoIzq);
				jpnlInferior.add(jpnlInfoDer);
		    jpnlInferior.invalidate();
		    jpnlInferior.validate();
		    jpnlInferior.repaint();
		    
		    
			} catch (IOException e1) {
				e1.printStackTrace();
				return;
			}
			
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
	}
		
	/**
	 * Abrir archivo de configuraci�n
	 */
	private void AbrirConfiguracion() {
		
		JFileChooser fc = new JFileChooser(RUTA);
    FileFilter filter = new FileNameExtensionFilter("Texto","txt", "text");
    fc.setAcceptAllFileFilterUsed(false);
    fc.setDialogTitle("Abrir archivo de configuraci�n");
    fc.setFileFilter(filter);
    int respuesta = fc.showOpenDialog(this);
    
    // Comprobar si se ha pulsado Aceptar
    if (respuesta == JFileChooser.APPROVE_OPTION) {
      File archivoElegido = fc.getSelectedFile();
      try {
      	_FichConf = archivoElegido.getName();
      	String fecha = _FichConf.replace("config", "").replace(".txt", "").replace("_", "/");
      	lblArchivoConfiguracion.setText("Fichero Configuraci�n: " + _FichConf);
      	_FichVuelos = ObtenerFicheroVuelos(_FichConf, fecha);	      	
      	_RutaFicheros = archivoElegido.getCanonicalPath().toString().substring(0, (archivoElegido.getCanonicalPath().toString()).indexOf(_FichConf));
			} catch (IOException e1) {
				e1.printStackTrace();
			}
    }
    
	}
	
	/**
	 * Guardamos la solucion
	 * @param fecha
	 */
	private void GuardarSolucion(String fecha) {
		
		// Modo edici�n
		if (EDICION == 1) {
			JOptionPane.showMessageDialog(null, "En modo EDICI�N, no hay soluci�n generada");
			return;
		}
		
		// Comprobamos si existe soluci�n cargada
		if ((info.size() == 0)) {
  		JOptionPane.showMessageDialog(null, "Debe abrir o generar una soluci�n!");
  		return;
		}
		
		try {
			 
			JFileChooser file = new JFileChooser(RUTA);
      FileFilter filter = new FileNameExtensionFilter("Texto","txt", "text");
      file.setAcceptAllFileFilterUsed(false);
      file.setFileFilter(filter);
			file.showSaveDialog(this);
			File fichero = file.getSelectedFile();
			 
			if (fichero != null)  {

				String ext = ".txt";
				if (fichero.getName().indexOf(ext) > 0)
					ext = "";
				
				FileWriter save = new FileWriter(fichero + ext);
				save.write(CrearTextoFicheroSolucion(fecha));
				save.close();
				JOptionPane.showMessageDialog(null, "La soluci�n se ha guardado correctamente", "Informaci�n", JOptionPane.INFORMATION_MESSAGE);
			    
			}
		  
		} catch(IOException ex) {
			JOptionPane.showMessageDialog(null, "La soluci�n no se ha podido guardar", "Advertencia", JOptionPane.WARNING_MESSAGE);
		}
	}
	
	/**
	 * Mostramos la ayuda del programa
	 */
	private void VerAyuda() {
		
		frmAyuda ayuda = new frmAyuda();

		Dimension pantalla = getSize();
		final JDialog frameAyuda = new JDialog(ayuda, "Ayuda", true);
		frameAyuda.getContentPane().add(ayuda.contenedor);
		frameAyuda.pack();
		frameAyuda.setLocation((pantalla.width - ImageObserver.WIDTH ) / 18 , (pantalla.height - ImageObserver.HEIGHT) / 6); 
		frameAyuda.setResizable(false); 
		frameAyuda.setVisible(true);		
  	
	}
	
	/**
	 * Carga del mapa
	 */
	public void actionPerformed(ActionEvent e) {

		// Obtenemos la fecha
		/*
    String fecha = String.valueOf(datePicker.getModel().getYear());
    fecha = String.valueOf(datePicker.getModel().getMonth() + 1) + "/" + fecha;
    if ((datePicker.getModel().getMonth() + 1) < 10)
    	fecha = "0" + fecha;
    fecha = String.valueOf(datePicker.getModel().getDay()) + "/" + fecha;
    if (datePicker.getModel().getDay() < 10)
    	fecha = "0" + fecha;
		 */
		String fecha = _FichConf.replace("config", "").replace(".txt", "").replace("_", "/");
        
    String[] parts = fecha.split("/");
    
    // Cargar soluci�n o Modo Edici�n
		if ((e.getSource() == jbtnCargar) || (e.getSource() == jbtnEdicion)) {			
			CargarEditar(fecha, (e.getSource() == jbtnEdicion));			
		}
		
		// Tratamos las modificaciones y buscamos la soluci�n
		if (e.getSource() == jbtnAppBinter) {			
			LlamadaBinter(fecha, parts);					
		}
		
		// Buscamos el fichero de la solucion
		if (e.getSource() == jbtnAbrirSol) {
			AbrirSolucion();
		}
		
		// Generamos la soluci�n
		if (e.getSource() == jbtnGenerarSol) {
			GenerarSolucion(fecha, parts);
		}
		
		// Buscamos el fichero de la configuraci�n
		if (e.getSource() == jbtnAbrirConf) {
			AbrirConfiguracion();
		}
		
		// Guardar soluci�n
		if (e.getSource() == jbtnGuardar) {
			GuardarSolucion(fecha);
		}	
		
		// Ayuda
		if (e.getSource() == jbtnAyuda) {
			VerAyuda();
		}
		
	}
	
	/**
	 * Getter datepicker
	 * @return
	 */
	public JDatePickerImpl getDatePicker() {
		return datePicker;
	}
	
	/**
	 * Setter datepikcer
	 * @param datePicker
	 */
	public void setDatePicker(JDatePickerImpl datePicker) {
		this.datePicker = datePicker;
	}
	
	/**
	 * A�adimos Edicion
	 * @param strTipo
	 * @param col
	 * @param intVal
	 * @param intVueloEdit
	 * @param vuelo
	 * @param posNuevo
	 */
	public void A�adirEdicion (String strTipo, Color color, int intVal, int intVueloEdit,  String vuelo, int posNuevo) {		
		Edit.add(new Ediciones(strTipo, color, intVal, intVueloEdit, vuelo, posNuevo));		
	}
	
	/**
	 * A�adimos un nuevo vuelo de edici�n
	 * @param oVuelo
	 */
	public void A�adirEdicionNuevosVuelos(Vuelo oVuelo) {
		
		int pos = EditNuevosVuelos.size();
		EditNuevosVuelos.add(oVuelo);
		A�adirEdicion("N", Color.CYAN, 0, numVuelos + EditNuevosVuelos.size() - 1, oVuelo.getVuelo(), pos);
		
	}
	
	/**
	 * Comprueba si existe el vuelo nuevo
	 * @param numVuelo
	 * @return
	 */
	public boolean ExisteVueloNuevo(String numVuelo) {
		
		boolean existe = false;
		
		for (int i = 0; i < Edit.size(); i++) {			
			if (Edit.get(i).getTipo().equals("N"))
				if (EditNuevosVuelos.get(Edit.get(i).getPosicionNuevo()).getVuelo().equals(numVuelo))
    			return true;
  	}
    	
		return existe;
		
	}
	
	/**
	 * Clase que da formato de fecha.
	 */
	public class DateLabelFormatter extends AbstractFormatter {

    	private String datePattern = "dd/MM/yyyy";
    	private SimpleDateFormat dateFormatter = new SimpleDateFormat(datePattern);

    	public Object stringToValue(String text) throws ParseException, java.text.ParseException {
	        return dateFormatter.parseObject(text);
	    }

    	public String valueToString(Object value) throws ParseException {
	        if (value != null) {
	            Calendar cal = (Calendar) value;
	            return dateFormatter.format(cal.getTime());
	        }

	        return "";
	    }

	}
	
	/**
	 * Devuelve el n�mero del vuelo seg�n la posici�n
	 * @return
	 */
	public String BuscarNumVueloPos(int pos) {
		
		String vuelo = "";
		int cont = 0;
		for (int i = 0; i < info.size(); i++) {  		
			for (int j = 0; j < info.get(i).size(); j++) {
				if (cont == pos)
					vuelo = info.get(i).get(j).getVuelo();
				cont++;
			}			
		}
		return vuelo;
		
	}
	
	/**
	 * Adelantar vuelo
	 */
	private void AdelantarVuelo(int intValor) {
		
		boolean encontrado = false;
  	int pos = 0;
  	
  	// Recorremos las ediciones
  	while ((encontrado == false) && (Edit.size() > 0) && (pos < Edit.size())) {
  		
			if (Edit.get(pos).getPosicion() == vueloEnEdicion) {							
				
				Edit.get(pos).setColor(Color.GREEN);
				
				if (Edit.get(pos).getTipo() == "A") {
					Edit.get(pos).setTiempo(Edit.get(pos).getTiempo() + intValor);
				}
				
				if (Edit.get(pos).getTipo() == "C") {
					Edit.get(pos).setTiempo(intValor);
					Edit.get(pos).setTipo("A");			
				}
				
				if (Edit.get(pos).getTipo() == "R") {
					intValor = Edit.get(pos).getTiempo() - intValor;
					if (intValor < 0) {
						Edit.get(pos).setTipo("A");
					}
					Edit.get(pos).setTiempo(Math.abs(intValor));
					if (intValor == 0)
						Edit.remove(pos);
				}
				
				encontrado = true;
				
			}
			pos++;
		}
		
  	// A�adimos el vuelo a la tabla de ediciones
  	if (encontrado == false)
  		A�adirEdicion("A", Color.GREEN, intValor, vueloEnEdicion, BuscarNumVueloPos(vueloEnEdicion),-1);
  		//Edit.add(new Ediciones("A", Color.GREEN, intValor, vueloEnEdicion, BuscarNumVueloPos(vueloEnEdicion),-1));
  	
	}
	
	/**
	 * Retrasar vuelo
	 * @param intValor
	 */
	private void RetrasarVuelo(int intValor) {
		
		boolean encontrado = false;
  	int pos = 0;
		
		// Recorremos las ediciones
  	while ((encontrado == false) && (Edit.size() > 0) && (pos < Edit.size())) {
  		
			if (Edit.get(pos).getPosicion() == vueloEnEdicion) {

				Edit.get(pos).setColor(Color.ORANGE);
				
				if (Edit.get(pos).getTipo() == "R") {
					Edit.get(pos).setTiempo(Edit.get(pos).getTiempo() + intValor);
				}
				
				if (Edit.get(pos).getTipo() == "C") {
					Edit.get(pos).setTiempo(intValor);
					Edit.get(pos).setTipo("R");							
				}
				
				if (Edit.get(pos).getTipo() == "A") {
					intValor = Edit.get(pos).getTiempo() - intValor;
					if (intValor < 0) {
						Edit.get(pos).setTipo("R");
					}
					Edit.get(pos).setTiempo(Math.abs(intValor));
					if (intValor == 0)
						Edit.remove(pos);
				}
				
				encontrado = true;
				
			}
			pos++;
		}
		
  	// A�adimos el vuelo a la tabla de ediciones
  	if (encontrado == false)
  		A�adirEdicion("R", Color.ORANGE, intValor, vueloEnEdicion, BuscarNumVueloPos(vueloEnEdicion), -1);
  		//Edit.add(new Ediciones("R", Color.ORANGE, intValor, vueloEnEdicion, BuscarNumVueloPos(vueloEnEdicion), -1)); 
  	
	}
	
	/**
	 * Cancelar vuelo
	 */
	private void CancelarVuelo() {
		
		boolean encontrado = false;
  	int pos = 0;
		
		// Recorremos las ediciones
  	while ((encontrado == false) && (Edit.size() > 0) && (pos < Edit.size())) {
  		
			if (Edit.get(pos).getPosicion() == vueloEnEdicion) {
				
				Edit.get(pos).setColor(Color.RED);
				Edit.get(pos).setTiempo(0);
				Edit.get(pos).setTipo("C");	
				encontrado = true;
				
			}
			
			pos++;
			
		}
		
  	// A�adimos el vuelo a la tabla de ediciones
  	if (encontrado == false)
  		A�adirEdicion("C", Color.RED, 0, vueloEnEdicion, BuscarNumVueloPos(vueloEnEdicion),-1);
  		//Edit.add(new Ediciones("C", Color.RED, 0, vueloEnEdicion, BuscarNumVueloPos(vueloEnEdicion),-1));
  	
	}
	
	/**
	 * A�adir vuelo
	 */
	private void A�adirVuelo() {

		// Obtenemos la fecha
		/*
    String fecha = String.valueOf(datePicker.getModel().getYear());
    fecha = String.valueOf(datePicker.getModel().getMonth() + 1) + "/" + fecha;
    if ((datePicker.getModel().getMonth() + 1) < 10)
    	fecha = "0" + fecha;
    fecha = String.valueOf(datePicker.getModel().getDay()) + "/" + fecha;
    if (datePicker.getModel().getDay() < 10)
    	fecha = "0" + fecha;
    */    
		String fecha = _FichConf.replace("config", "").replace(".txt", "").replace("_", "/");
		
		frmNuevoVuelo nuevoVuelo = new frmNuevoVuelo(fecha, info, this);

		Dimension pantalla = getSize();
		final JDialog frameNuevoVuelo = new JDialog(nuevoVuelo, "Nuevo vuelo", true);
		frameNuevoVuelo.getContentPane().add(nuevoVuelo.contentPane);
		frameNuevoVuelo.pack();
		frameNuevoVuelo.setLocation((pantalla.width - ImageObserver.WIDTH ) / 3 , (pantalla.height - ImageObserver.HEIGHT) / 6); 
		frameNuevoVuelo.setResizable(false); 
		frameNuevoVuelo.setVisible(true);
		
		//JOptionPane.showMessageDialog(null, "vuelo: " + nuevoVuelo.getVuelo());
		
  	// A�adimos el vuelo a la tabla de ediciones
  	//Edit.add(new Ediciones("N", Color.CYAN, 0, -1, "", posNuevo));
  	
	}
	
	/**
	 * Eliminar edici�n
	 */
	private void EliminarEdicion() {
		
		boolean encontrado = false;
  	int pos = 0;
		
		// Recorremos las ediciones
  	int posBorrar = -1, posNuevoBorrar = -1;
  	while ((encontrado == false) && (Edit.size() > 0) && (pos < Edit.size())) {
  		
			if (Edit.get(pos).getPosicion() == vueloEnEdicion) {
				
				if (Edit.get(pos).getTipo().equals("N")) {
					posNuevoBorrar = Edit.get(pos).getPosicionNuevo();
					posBorrar = Edit.get(pos).getPosicion();
					EditNuevosVuelos.remove(posNuevoBorrar);
				}				
				
				Edit.remove(pos);
				encontrado = true;
				
			}
			
			pos++;
			
		}
  	
  	// Se elimin� un vuelo nuevo, actualizamos el resto
  	if (posNuevoBorrar >= 0) {
  		for (int i = 0; i < Edit.size(); i++) {
  			
  			if (Edit.get(i).getTipo().equals("N")) {
  				
					if (Edit.get(i).getPosicionNuevo() > posNuevoBorrar)
						Edit.get(i).setPosicionNuevo(Edit.get(i).getPosicionNuevo() - 1);
					
					if (Edit.get(i).getPosicion() > posBorrar)
						Edit.get(i).setPosicion(Edit.get(i).getPosicion() - 1);
  						
  			}
  			
  		}  		
  	}
		
	}
	
	// Listener del menu contextual
	ActionListener menuListener = new ActionListener() {
		
		/**
		 * Acciones del men� contextual
		 */
    public void actionPerformed(ActionEvent event) {
      
    	int intValor = Integer.parseInt(((SpinnerNumberModel)jspnValor.getModel()).getNumber().toString());    	

      // Acciones del menu popup
      switch (event.getActionCommand()) {
      
	      case "Adelantar vuelo":	    
	      	AdelantarVuelo(intValor);	      	
		      break;
	
	      case "Retrasar vuelo":
	      	RetrasarVuelo(intValor);
	      	break;
	      	
	      case "A�adir vuelo":
	      	A�adirVuelo();
		      break;

	      case "Cancelar vuelo":
	      	CancelarVuelo();
		      break;

	      case "Eliminar edici�n":
	      	EliminarEdicion();
		      break;
	
      }
      
      mapaVuelos.ActualizarEdiciones(Edit, EditNuevosVuelos);
    	jpanel.invalidate();
    	jpanel.validate();
    	jpanel.repaint();
    	
      // Seteamos el vuelo en edici�n
      vueloEnEdicion = -1;
      
    }
    
  };
	
	@Override
	public void mouseClicked(MouseEvent e) {
		
		/*
		// S�lo si es edici�n
		if (EDICION == 0)
			return;
		*/
		
    // Recorremos los vuelos pintados
    int i = 0;
    boolean encontrado = false;
    while ((encontrado == false) && (i < mapaVuelos.rects.size())) {
    	
    	if (mapaVuelos.rects.get(i).contains(e.getX(), e.getY())) {
    	
    		// Comprobamos si es un vuelo nuevo
				boolean nuevo = false, editado = false;
		  	int pos = 0;
		  	while ((nuevo == false) && (Edit.size() > 0) && (pos < Edit.size())) {			
		  		//System.out.println("pos: " + String.valueOf(pos) + " - i: " + String.valueOf(i) + "posEdit: " + String.valueOf(Edit.get(pos).getPosicion()) + " - tipo: " + Edit.get(pos).getTipo());
					if ((Edit.get(pos).getPosicion() == i) && (Edit.get(pos).getTipo().equals("N"))) 
						nuevo = true;
					if ((Edit.get(pos).getPosicion() == i) && (!Edit.get(pos).getTipo().equals("N"))) 
						editado = true;		
					pos++;						
				}
		  	
    		// Bot�n derecho
    		if ((SwingUtilities.isRightMouseButton(e)) && (EDICION == 1)) {
    			
					JPopupMenu popup = new JPopupMenu();
					JMenuItem item;
					
					// Si es distinto de nuevo vuelo
					if (nuevo == false) {
						
						popup.add(item = new JMenuItem("Adelantar vuelo"));
				    item.addActionListener(menuListener);
						popup.add(item = new JMenuItem("Retrasar vuelo"));
				    item.addActionListener(menuListener);
				    popup.addSeparator();
						popup.add(item = new JMenuItem("Cancelar vuelo"));
				    item.addActionListener(menuListener);
				    popup.addSeparator();
				    
					}
					
					if ((nuevo == true) || (editado == true)) {
						popup.add(item = new JMenuItem("Eliminar edici�n"));
				    item.addActionListener(menuListener);
					}
					
					popup.show(e.getComponent(), e.getX(), e.getY());
			    popup.setLabel("Justification");
			    popup.setBorder(new BevelBorder(BevelBorder.RAISED));
			    
			    vueloEnEdicion = i;
			    
				} 
    		
    		// Bot�n izquierdo
    		if (SwingUtilities.isLeftMouseButton(e)) {

    			frmInfoVuelo infoVuelo = null;
    			
    			if (nuevo) {    				
    				infoVuelo = new frmInfoVuelo(EditNuevosVuelos.get(Edit.get(pos-1).getPosicionNuevo()));    				
    			} else {
    				
    				int cont = 0;
    				for (int a = 0; a < info.size(); a++) {  		
    					for (int b = 0; b < info.get(a).size(); b++) {
    						if (cont == i)
    	    				infoVuelo = new frmInfoVuelo(info.get(a).get(b));    						
    						cont++;
    					}			
    				}
    				
    			}

    			Dimension pantalla = getSize();
    			final JDialog frameInfoVuelo = new JDialog(infoVuelo, "Informaci�n vuelo", true);
    			frameInfoVuelo.getContentPane().add(infoVuelo.contenedor);
    			frameInfoVuelo.pack();
    			frameInfoVuelo.setLocation((pantalla.width - ImageObserver.WIDTH ) / 3 , (pantalla.height - ImageObserver.HEIGHT) / 6); 
    			frameInfoVuelo.setResizable(false); 
    			frameInfoVuelo.setVisible(true);
    			
    		}
    		
    		encontrado = true;
    		
    	} else {
    		
    		// Bot�n derecho
    		if ((SwingUtilities.isRightMouseButton(e)) && (EDICION == 1)) {
    			
    			JPopupMenu popup = new JPopupMenu();
					JMenuItem item;
					popup.add(item = new JMenuItem("A�adir vuelo"));
			    item.addActionListener(menuListener);
					popup.show(e.getComponent(), e.getX(), e.getY());

			    popup.setLabel("Justification");
			    popup.setBorder(new BevelBorder(BevelBorder.RAISED));
			    
    		}
    		
    	}
    	
    	i++;
    	
    }
    
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}