package componentes;

import java.awt.*;
import java.awt.font.*;
import java.util.ArrayList;
import javax.swing.*;
 
/**
 * Clase PintarVuelos
 * Clase encargada de pintar los vuelos en el panel
 */
@SuppressWarnings("serial")
public class PintarVuelos extends JPanel {
  
	// Variables
	public ArrayList<Rectangle> rects = new ArrayList<Rectangle>();
	ArrayList<ArrayList<Vuelo>> datos;
	ArrayList<ArrayList<VuelosPilotos>> datosPilotos;		
	ArrayList<Ediciones> ediciones = new ArrayList<Ediciones>();
	ArrayList<Vuelo> edicionesVuelos = new ArrayList<Vuelo>();
	Colores colores = new Colores();

	int WIDTH = 0;
	int ALTO_VUELOS = 20;
	int SEPARA = 25;
	int POS_MAPA = 40;
	int RANGOMIN = 360; // 06:00 horas en minutos
	int RANGOMAX = 1380; // 23:00 horas en minutos
	int HORA = 60; // valor de la hora
	int MARGEN_SUP = 40; // valor de la hora
	int EDICION = 0;
 
  /**
   * Construnctor
   * @param info
   */
	@SuppressWarnings("unchecked")
	public PintarVuelos(ArrayList<ArrayList<Vuelo>> info, ArrayList<ArrayList<VuelosPilotos>> infoPilotos, int width, int isEdicion) {

		datos = (ArrayList<ArrayList<Vuelo>>) info.clone();
		datosPilotos = (ArrayList<ArrayList<VuelosPilotos>>) infoPilotos.clone();
		WIDTH = width;
		EDICION = isEdicion;
		
	}
	
	/**
	 * Devuelve la letra del destino
	 * @param dest
	 * @return
	 */
	public String ObtenerDestinos(String dest, int opc) {
		
		String destino = "";
		
		switch (dest) {
		 
			case "LPA":
				if (opc == 1)
					destino = "L";
				if (opc == 2)
					destino = "Aeropuerto de Gran Canaria, Espa�a";
				break;
	
			case "TFN":
				if (opc == 1)
					destino = "T";
				if (opc == 2)
					destino = "Aeropuerto de Tenerife-Norte, Espa�a";
				break;
	
			case "TFS":
				if (opc == 1)
					destino = "t";
				if (opc == 2)
					destino = "Aeropuerto de Tenerife-Sur, Espa�a";
				break;
	
			case "ACE":
				if (opc == 1)
					destino = "A";
				if (opc == 2)
					destino = "Aeropuerto de Lanzarote, Espa�a";
				break;
	
			case "SPC":
				if (opc == 1)
					destino = "S";
				if (opc == 2)
					destino = "Aeropuerto de La Palma, Espa�a";
				break;
	
			case "GMZ":
				if (opc == 1)
					destino = "G";
				if (opc == 2)
					destino = "Aeropuerto de La Gomera, Espa�a";
				break;
	
			case "VDE":
				if (opc == 1)
					destino = "V";
				if (opc == 2)
					destino = "Aeropuerto de El Hierro, Espa�a";
				break;
	
			case "FUE":
				if (opc == 1)
					destino = "F";
				if (opc == 2)
					destino = "Aeropuerto de Fuerteventura, Espa�a";
				break;
	
			case "EUN":
				if (opc == 1)
					destino = "E";
				if (opc == 2)
					destino = "Aeropuerto de Laayoune, Sahara Occidental";
				break;
	
			case "RAK":
				if (opc == 1)
					destino = "R";
				if (opc == 2)
					destino = "Aeropuerto de Marrakech-Menara, Marruecos";
				break;
	
			case "FNC":
				if (opc == 1)
					destino = "f";
				if (opc == 2)
					destino = "Aeropuerto de Madeira, Portugal";
				break;
	
			case "NDB":
				if (opc == 1)
					destino = "N";
				if (opc == 2)
					destino = "Aeropuerto Internacional de Nouadhibou, Mauritania";
				break;
	
			case "CMN":
				if (opc == 1)
					destino = "C";
				if (opc == 2)
					destino = "Aeropuerto Internacional Moh�mmed V, Marruecos";
				break;
	
			case "AGA":
				if (opc == 1)
					destino = "a";
				if (opc == 2)
					destino = "Aeropuerto de Agadir-Al Massira, Marruecos";
				break;
	    
	    default:
	    	break;

		}
		
		return destino;
	}
	
	/**
	 * Pintamos la union de los vuelos de los pilotos
	 */
	private void PintarUnionVuelosPiloto(Graphics2D g2) {
		
    // Recorremos los vuelos de los pilotos
		Color col = Color.WHITE;
    for (int a = 0; a < datosPilotos.size(); a++) {

  		// Tratamos los vuelos a partir del segundo de cada piloto para unirlo con el anterior
    	for (int b = 1; b < datosPilotos.get(a).size(); b++) {

  			String vueloActual = datosPilotos.get(a).get(b).getNumVuelo();     // Vuelo actual
  			String vueloAnterior = datosPilotos.get(a).get(b-1).getNumVuelo(); // Vuelo anterior
  			int iniAnt, finAnt, posVueloIni = 0, posVueloFinAnt = 0, posLineaIni = 0, posLineaFin = 0;
  			for (int i = 0; i < datos.size(); i++) {
  	    	for (int j = 0; j < datos.get(i).size(); j++) {
  	    		
  	    		// Buscamos el vuelo actual
  	    		if (datos.get(i).get(j).getVuelo().equals(vueloActual)) {
  	    			
		  				String[] horaIni = datos.get(i).get(j).getHoraInicio().split(":");
		      		iniAnt = Integer.parseInt(horaIni[0]) * HORA + Integer.parseInt(horaIni[1]);
		  				posVueloIni = (((iniAnt - RANGOMIN) * RANGOMAX) / (int)(RANGOMIN * 3.02)) + POS_MAPA + (ALTO_VUELOS / 2);
		  				posLineaIni = (i * ((POS_MAPA + SEPARA) / 2)) + MARGEN_SUP + (ALTO_VUELOS / 2);
							col = colores.colores[Integer.parseInt(datos.get(i).get(j).getPiloto())];
		  				
  	    		}
  	    		
  	    		// Buscamos el vuelo anterior
  	    		if (datos.get(i).get(j).getVuelo().equals(vueloAnterior)) {
  	    			
  	    			String[] horafinAnt = datos.get(i).get(j).getHoraFin().split(":");
		      		finAnt = Integer.parseInt(horafinAnt[0]) * HORA + Integer.parseInt(horafinAnt[1]);
		  				posVueloFinAnt = (((finAnt - RANGOMIN) * RANGOMAX) / (int)(RANGOMIN * 3.02)) + POS_MAPA + (ALTO_VUELOS / 2);
		  				posLineaFin = (i * ((POS_MAPA + SEPARA) / 2)) + MARGEN_SUP + (ALTO_VUELOS / 2);
		  				
  	    		}
  	    			
  	    	}

  				g2.setPaint(col);
  	    	
  	    	if (posVueloIni > 0 && posVueloFinAnt > 0)
  	    		g2.drawLine (posVueloFinAnt, posLineaFin, posVueloIni - (ALTO_VUELOS), posLineaIni);
  	    	
  			}
    		
    	}
    	
    }
		
	}
	
	/**
	 * Metodo que se encarga de pintar los vuelos
	 */
  protected void paintComponent(Graphics g) {
  	
  	super.paintComponent(g);
    Graphics2D g2 = (Graphics2D)g;
    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    FontRenderContext frc = g2.getFontRenderContext();
    Font font = g2.getFont().deriveFont(11f);
    g2.setFont(font);
    
  	// Recorremos los vuelos de la linea aerea
    rects.clear();
  	int i, ini, fin, posVueloIni, posVueloFin, cont = 0;
  	for (i = 0; i < datos.size(); i++) {
  		
			for (int j = 0; j < datos.get(i).size(); j++) {
				
				// Obtenemos la informaci�n del vuelo
				//System.out.println( datos.get(i).get(j).getVuelo());
				String[] horainicio = datos.get(i).get(j).getHoraInicio().split(":");
				ini = Integer.parseInt(horainicio[0]) * HORA + Integer.parseInt(horainicio[1]);
				String[] horafin = datos.get(i).get(j).getHoraFin().split(":");
				fin = Integer.parseInt(horafin[0]) * HORA + Integer.parseInt(horafin[1]);
				String vuelo = datos.get(i).get(j).getVuelo();
				int piloto = Integer.parseInt(datos.get(i).get(j).getPiloto());
				String origen = datos.get(i).get(j).getOrigen();
				String destino = datos.get(i).get(j).getDestino();
				String company = datos.get(i).get(j).getCompa�ia();
	
				float sw = (float)font.getStringBounds(vuelo, frc).getWidth();
		    LineMetrics lm = font.getLineMetrics(vuelo, frc);
		    float sh = lm.getAscent() + lm.getDescent();
		    
				// Crear vuelo
		    posVueloIni = (((ini - RANGOMIN) * RANGOMAX) / (int)(RANGOMIN * 3.02)) + POS_MAPA;
				posVueloFin = (((fin - RANGOMIN) * RANGOMAX) / (int)(RANGOMIN * 3.02)) + POS_MAPA;

		    // Recorremos las ediciones
				for (int a = 0; a < ediciones.size(); a++) {
					
					if (ediciones.get(a).getPosicion() == cont) {
						
						// Adelanto
						if (ediciones.get(a).getTipo() == "A") {
							ini -= ediciones.get(a).getTiempo();
							fin -= ediciones.get(a).getTiempo();
						}
						
						// Retraso
						if (ediciones.get(a).getTipo() == "R") {
							ini += ediciones.get(a).getTiempo();
							fin += ediciones.get(a).getTiempo();
						}
						
				    posVueloIni = (((ini - RANGOMIN) * RANGOMAX) / (int)(RANGOMIN * 3.02)) + POS_MAPA;
						posVueloFin = (((fin - RANGOMIN) * RANGOMAX) / (int)(RANGOMIN * 3.02)) + POS_MAPA;
						
					}

				}
				
				// En el primer vuelo de cada linea pintamos la linea de separaci�n, compa��a y l�nea aerea
				if (j == 0) {
					
					// L�nea aerea
					g2.drawString("R." + Integer.toString(i + 1), (ALTO_VUELOS / 4), (i * ((POS_MAPA + SEPARA) / 2)) + (ALTO_VUELOS * 3/4) + MARGEN_SUP);
					// Compa�ia
					g2.drawString(company, (((RANGOMAX - RANGOMIN) * RANGOMAX) / (int)(RANGOMIN  * 3.02)) + POS_MAPA + ALTO_VUELOS, (i * ((POS_MAPA + SEPARA) / 2)) + (ALTO_VUELOS * 3/4) + MARGEN_SUP);
					// Linea separaci�n de l�nea aerea
					g2.setPaint(Color.lightGray);
					g2.drawLine (POS_MAPA, (i * ((POS_MAPA + SEPARA) / 2)) + ALTO_VUELOS + MARGEN_SUP, ((((RANGOMAX - RANGOMIN) * RANGOMAX) / (RANGOMIN * 3)) + POS_MAPA), (i * ((POS_MAPA + SEPARA) / 2)) + ALTO_VUELOS + MARGEN_SUP);
					g2.setPaint(Color.BLACK);
				}
				
				// Pintamos el Origen y Destino
				g2.drawString(ObtenerDestinos(origen,1), posVueloIni - (ALTO_VUELOS / 2) + 1, (i * ((POS_MAPA + SEPARA) / 2)) + ALTO_VUELOS - 5 + MARGEN_SUP);
				g2.drawString(ObtenerDestinos(destino,1), posVueloFin + (ALTO_VUELOS / 4), (i * ((POS_MAPA + SEPARA) / 2)) + ALTO_VUELOS - 5 + MARGEN_SUP);

				// A�adimos el rectangulo que representa al vuelo
				rects.add(new Rectangle(posVueloIni, (i * ((POS_MAPA + SEPARA) / 2)) + MARGEN_SUP, (posVueloFin - posVueloIni), ALTO_VUELOS));
				
				Color col = Color.WHITE;
				if (EDICION == 0) 
					col = colores.colores[piloto];
				
				// Recorremos las ediciones para buscar el color (atraso, adelanto, cancelaci�n, nuevo vuelo)
				for (int a = 0; a < ediciones.size(); a++) {
					if (ediciones.get(a).getPosicion() == cont)
						col = ediciones.get(a).getColor();
				}
				
				// Pintamos el vuelo
				g2.setPaint(col);
	      g2.fill(rects.get(cont));
	      
	      if (EDICION == 1)
	      	g2.setPaint(Color.BLACK);
	      
	      g2.setPaint(Color.black);
	      g2.draw(rects.get(cont));
	      
	      // Pintamos el n�mero del vuelo
	      float sx = rects.get(cont).x + (rects.get(cont).width - sw)/2;
        float sy = rects.get(cont).y + (rects.get(cont).height + sh)/2 - lm.getDescent();
        g2.drawString(vuelo, sx, sy);
	      
	      cont++;
				
			}			
			
		}
  	
  	// Pintamos los vuelos nuevos en edicion
		if (EDICION == 1) {
		
			for (int a = 0; a < ediciones.size(); a++) {
				
				if (ediciones.get(a).getTipo().equals("N")) {
					
		      i = edicionesVuelos.get(ediciones.get(a).getPosicionNuevo()).getLinea() - 1;
		      
					String[] horainicio = edicionesVuelos.get(ediciones.get(a).getPosicionNuevo()).getHoraInicio().split(":");
					ini = Integer.parseInt(horainicio[0]) * HORA + Integer.parseInt(horainicio[1]);
					String[] horafin = edicionesVuelos.get(ediciones.get(a).getPosicionNuevo()).getHoraFin().split(":");
					fin = Integer.parseInt(horafin[0]) * HORA + Integer.parseInt(horafin[1]);

					posVueloIni = (((ini - RANGOMIN) * RANGOMAX) / (int)(RANGOMIN * 3.02)) + POS_MAPA;
					posVueloFin = (((fin - RANGOMIN) * RANGOMAX) / (int)(RANGOMIN * 3.02)) + POS_MAPA;
					
					rects.add(new Rectangle(posVueloIni, (i * ((POS_MAPA + SEPARA) / 2)) + MARGEN_SUP, (posVueloFin - posVueloIni), ALTO_VUELOS));
					g2.setPaint(ediciones.get(a).getColor());
		      g2.fill(rects.get(cont));
		      g2.setPaint(Color.black);
		      g2.draw(rects.get(cont));
		      
		      g2.drawString(ObtenerDestinos(edicionesVuelos.get(ediciones.get(a).getPosicionNuevo()).getOrigen(),1), posVueloIni - (ALTO_VUELOS / 2) + 1, (i * ((POS_MAPA + SEPARA) / 2)) + ALTO_VUELOS - 5 + MARGEN_SUP);
					g2.drawString(ObtenerDestinos(edicionesVuelos.get(ediciones.get(a).getPosicionNuevo()).getDestino(),1), posVueloFin + (ALTO_VUELOS / 4), (i * ((POS_MAPA + SEPARA) / 2)) + ALTO_VUELOS - 5 + MARGEN_SUP);
					
		      // N�mero de vuelo nuevo
		      String vuelo = edicionesVuelos.get(ediciones.get(a).getPosicionNuevo()).getVuelo();
		      float sw = (float)font.getStringBounds(vuelo, frc).getWidth();
			    LineMetrics lm = font.getLineMetrics(vuelo, frc);			
			    float sh = lm.getAscent() + lm.getDescent();	    
		      float sx = rects.get(cont).x + (rects.get(cont).width - sw)/2;
	        float sy = rects.get(cont).y + (rects.get(cont).height + sh)/2 - lm.getDescent();				    
	        g2.drawString(vuelo, sx, sy);
		      
		      cont++;
					
				}
				
			}
			
		}
  	
  	// Pintamos las horas
  	cont = 0;
  	for (i = RANGOMIN; i <= RANGOMAX; i++) {
  		
  		if ((i - (cont * HORA)) == RANGOMIN) {
  	  	g2.setPaint(Color.lightGray);
  			g2.drawLine ((((i - RANGOMIN) * RANGOMAX) / (int)(RANGOMIN  * 3.02)) + POS_MAPA, 0, (((i - RANGOMIN) * RANGOMAX) / (int)(RANGOMIN  * 3.02)) + POS_MAPA, 600);
  	  	g2.setPaint(Color.black);
  	  	if ((i / HORA) < 10) {
  	  		g2.drawString("0" + Integer.toString(i / HORA) + ":00",(((i - RANGOMIN) * RANGOMAX) / (int)(RANGOMIN  * 3.02)) + POS_MAPA + (ALTO_VUELOS / 4), (ALTO_VUELOS / 2));
  	  	} else {
  	  		g2.drawString(Integer.toString(i / HORA) + ":00",(((i - RANGOMIN) * RANGOMAX) / (int)(RANGOMIN  * 3.02)) + POS_MAPA + (ALTO_VUELOS / 4), (ALTO_VUELOS / 2));
  	  	}
  			cont++;
  		}
  		
  	}
  	
  	// Repintamos si no es edici�n  	
		if (EDICION == 0) {
			PintarUnionVuelosPiloto(g2);			
			this.invalidate();
			this.validate();
			this.repaint();
		}
  
  }
  
  /**
   * Cancelamos el vuelo indicado
   * @param pos
   */
  @SuppressWarnings("unchecked")
	public void CancelaVuelo(int pos, ArrayList<Ediciones> vuelos, Graphics g) {
  	
  	ediciones.clear();
  	ediciones = (ArrayList<Ediciones>) vuelos.clone();
  	
  }
  	
  /**
   * Actualizamos las ediciones
   * @param vuelos
   */
  @SuppressWarnings("unchecked")
	public void ActualizarEdiciones(ArrayList<Ediciones> edit, ArrayList<Vuelo> vuelos) {
  	
  	ediciones.clear();
  	ediciones = (ArrayList<Ediciones>) edit.clone();
  	
  	edicionesVuelos.clear();
  	edicionesVuelos = (ArrayList<Vuelo>) vuelos.clone();
  
  } 	
  
}