package componentes;

import java.awt.Color;

/**
 * Clase Colores
 * Clase que almacena los diferentes colores que representan los pilotos y sus vuelos
 * @author Dailos
 *
 */
public class Colores {

	// Variables
	int TOTAL_COLORES = 60;
	Color[] colores = new Color[TOTAL_COLORES];
	
	/**
	 *  Constructor
	 */
	public Colores() {
		
		// Colores
		colores[0] = new Color(255,0,0);
		colores[1] = new Color(255,20,147);
		colores[2] = new Color(255,69,0);
		colores[3] = new Color(255,215,0);
		colores[4] = new Color(230,230,250);
		colores[5] = new Color(221,160,221);
		colores[6] = new Color(148,0,211);
		colores[7] = new Color(128,0,128);
		colores[8] = new Color(0,250,154);
		colores[9] = new Color(0,100,00);
		colores[10] = new Color(0,255,255);
		colores[11] = new Color(30,144,255);
		colores[12] = new Color(0,0,139);
		colores[13] = new Color(139,69,19);
		colores[14] = new Color(128,0,0);
		colores[15] = new Color(128,128,128);
		colores[16] = new Color(47,79,79);
		colores[17] = new Color(255,160,122);
		colores[18] = new Color(220,20,60);
		colores[19] = new Color(255,192,203);
		colores[20] = new Color(255,140,0);
		colores[21] = new Color(255,228,181);
		colores[22] = new Color(189,183,107);
		colores[23] = new Color(186,85,211);
		colores[24] = new Color(106,90,205);
		colores[25] = new Color(0,255,0);
		colores[26] = new Color(0,255,127);
		colores[27] = new Color(0,139,139);
		colores[28] = new Color(70,130,180);
		colores[29] = new Color(0,191,255);
		colores[30] = new Color(0,0,255);
		colores[31] = new Color(210,180,140);
		colores[32] = new Color(210,105,30);
		colores[33] = new Color(165,42,42);
		colores[34] = new Color(169,169,169);
		colores[35] = new Color(105,105,105);
		colores[36] = new Color(255,160,122);
		colores[37] = new Color(199,21,133);
		colores[38] = new Color(255,255,0);
		colores[39] = new Color(238,232,170);
		colores[40] = new Color(75,0,130);
		colores[41] = new Color(50,205,50);
		colores[42] = new Color(34,139,34);
		colores[43] = new Color(102,205,170);
		colores[44] = new Color(135,206,250);
		colores[45] = new Color(255,222,173);
		colores[46] = new Color(184,134,11);
		colores[47] = new Color(240,255,240);
		colores[48] = new Color(219,112,147);
		colores[49] = new Color(216,191,216);
		colores[50] = new Color(72,61,139);
		colores[51] = new Color(154,205,50);
		colores[52] = new Color(128,128,0);
		colores[53] = new Color(175,238,238);
		colores[54] = new Color(127,255,212);
		colores[55] = new Color(100,149,237);
		colores[56] = new Color(188,143,143);
		colores[57] = new Color(218,165,32);
		colores[58] = new Color(205,133,63);
		colores[59] = new Color(112,128,144);
		
	}
	
}
