package componentes;

import java.awt.Color;

/**
 * Clase Ediciones
 * Esta clase guarda la información de las ediciones sobre el mapa
 * @author Dailos
 *
 */
public class Ediciones {
	
	// Variables
	String _Tipo; // (R)etraso, (A)delanto, (C)ancelacion, (N)uevo vuelo
	Color color;
	int _Tiempo;
	int _Pos;
	String _Vuelo;
	int _PosNuevo;
	
	/**
	 * Constructor
	 * @param tipos
	 * @param col
	 * @param time
	 */
	public Ediciones (String tipo, Color col, int time, int pos, String vuelo, int posnuevo) {
		
		// Asignacion de valores
		_Tipo = tipo;
		color = col;
		_Tiempo = time;
		_Pos = pos;
		_Vuelo = vuelo;
		_PosNuevo = posnuevo;

	}

	// getters
	public String getTipo(){return _Tipo;};
	public Color getColor(){return color;};
	public int getTiempo(){return _Tiempo;};
	public int getPosicion(){return _Pos;};
	public String getVuelo(){return _Vuelo;};
	public int getPosicionNuevo(){return _PosNuevo;};
	
	// setters
	public void setTipo(String tipo){_Tipo = tipo;};
	public void setColor(Color col){color = col;};
	public void setTiempo(int time){_Tiempo = time;};
	public void setPosicion(int pos){_Pos = pos;};
	public void setVuelo(String vuelo){_Vuelo = vuelo;};
	public void setPosicionNuevo(int posnuevo){_PosNuevo = posnuevo;};

}
