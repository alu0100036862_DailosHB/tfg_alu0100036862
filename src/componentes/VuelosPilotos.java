package componentes;

/**
 * Clase VuelosPilotos
 * Esta clase guarda la información de los vuelos y pilotos
 * @author Dailos
 *
 */
public class VuelosPilotos {
	
	//Variables
	int _NumPiloto;
	String _NumVuelo;
	
	/**
	 * Constructor
	 * @param numPiloto
	 * @param numVuelo
	 */
	public VuelosPilotos (int numPiloto, String numVuelo) {
		
		// Asignacion de valores
		_NumPiloto = numPiloto;
		_NumVuelo = numVuelo;
		
	}
	
	// getters
	public int getNumPiloto(){return _NumPiloto;};
	public String getNumVuelo(){return _NumVuelo;};
	
	// setters
	public void setNumPiloto(int numPiloto){_NumPiloto = numPiloto;};
	public void setNumVuelo(String numVuelo){_NumVuelo = numVuelo;};
	
	/**
	 * Imprime los datos
	 */
	public void Print() {
		
		System.out.println("Datos de los pilotos - vuelos:");
		System.out.println("-----------------------:");
		System.out.println("Piloto: " + _NumVuelo);
		System.out.println("Orden: " + Integer.toString(_NumPiloto));
		
	}

}
