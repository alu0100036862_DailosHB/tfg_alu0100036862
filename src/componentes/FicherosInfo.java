package componentes;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Clase FicherosInfo
 * Clase que se encarga de las acciones sobre los ficheros de datos de los vuelos
 * @author Dailos
 *
 */
public class FicherosInfo {

	String _Fecha;
	String _FicheroVuelos; // DDMMAAAA_XXXX.txt (XXXX leido de config)
  String _FicheroSalida; // DDMMAAAA_salida.txt
	int EsHorarioVerano = 0, _ValorSolucion = 0, _NumVuelos = 0, _NumAviones = 0, _NumPilotos = 0;
	
	ArrayList<ArrayList<VuelosPilotos>> datosPilotos = new ArrayList<ArrayList<VuelosPilotos>>();	
	
	int HORA = 60; // valor de la hora;
	String PATH = System.getProperty("user.dir");
	String RUTA = PATH + "/code2016/dati";
	
	//getters
	public String getFicheroVuelos(){return _FicheroVuelos;};
	public String getFicheroSalidas(){return _FicheroSalida;};
	public int getValorSolucion(){return _ValorSolucion;};
	public int getNumVuelos(){return _NumVuelos;};
	public int getNumAviones(){return _NumAviones;};
	public int getNumPilotos(){return _NumPilotos;};
	
	//setters
	public void setFicheroVuelos(String ficheroVuelos){_FicheroVuelos = ficheroVuelos;};
	public void setFicheroSalida(String ficheroSalida){_FicheroSalida = ficheroSalida;};
	public void setValorSolucion(int valorsolucion){_ValorSolucion = valorsolucion;};
	public void setNumVuelos(int numvuelos){_NumVuelos = numvuelos;};
	public void setNumAviones(int numaviones){_NumAviones = numaviones;};
	public void setNumPilotos(int numpilotos){_NumPilotos = numpilotos;};
	
	/**
	 * Constructor
	 * @param fecha
	 * @throws IOException 
	 */
	public FicherosInfo(String fecha, String fichvuelos, String fichsol) throws IOException {
		
	  _FicheroVuelos = fichvuelos; //"vuelossept12.txt"; //parts[2] + parts[1] + ".txt";
	  _FicheroSalida = fichsol; //parts[0] + parts[1] + parts[2] + "_salida.txt";
	  _Fecha = fecha;
        
	}
	
	/**
	 * Comprueba si la fecha que se le pasa est� en horario de verano
	 * @param fecha
	 * @return
	 * @throws ParseException
	 */
	public int EsHorarioVerano(String fecha) throws ParseException {

	  SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");
	  
	  Date fechaActual = formateador.parse(fecha);
	  Date fechaMinVerano = formateador.parse("31/03/" + fecha.substring(6));
	  Date fechaMaxVerano = formateador.parse("31/10/" + fecha.substring(6));
	  
		Calendar calendarFECHA = Calendar.getInstance();
		calendarFECHA.setTime(fechaActual);
		fechaActual = calendarFECHA.getTime();
		Calendar calendarMAR = Calendar.getInstance();
		calendarMAR.setTime(fechaMinVerano);
		Calendar calendarOCT = Calendar.getInstance();
		calendarOCT.setTime(fechaMaxVerano);
 
		// Buscamos que sea el ultimo Domingo de Marzo
		while(true) {
			if (calendarMAR.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
				calendarMAR.add(Calendar.DAY_OF_MONTH, -1);
				continue;
			}
			break;
		}
		fechaMinVerano = calendarMAR.getTime();

		// Buscamos que sea el ultimo Domingo de Octubre
		while(true) {
			if (calendarOCT.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
				calendarOCT.add(Calendar.DAY_OF_MONTH, -1);
				continue;
			}
			break;
		}
		fechaMaxVerano = calendarOCT.getTime();
		
		// Horario de verano		
		if ((fechaMinVerano.compareTo(fechaActual) <= 0) && (fechaMaxVerano.compareTo(fechaActual) > 0))
			return 1;
		
		return 0;
	
	}
	
	/**
	 * Muestra el contenido del fichero de vuelos por completo
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
  public void mostrarContenidoFicheroVuelos() throws FileNotFoundException, IOException {
  	
    String cadena;
    FileReader f = new FileReader("./planificacion/" + _FicheroVuelos);
    BufferedReader b = new BufferedReader(f);
    
    while((cadena = b.readLine())!=null) {
        System.out.println(cadena);
    }
    
    b.close();
      
  }
    
	/**
	 * Muestra el contenido del fichero de salida
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
  public void mostrarContenidoFicheroSalida() throws FileNotFoundException, IOException {
  	
    String cadena;
    FileReader f = new FileReader("./planificacion/" + _FicheroSalida);
    BufferedReader b = new BufferedReader(f);
    
    while((cadena = b.readLine())!=null) {
        System.out.println(cadena);
    }
    
    b.close();
      
  }

	/**
	 * Muestra el contenido del fichero por fecha
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public void mostrarContenidoFicheroVuelosPorFecha() throws FileNotFoundException, IOException {
		
		String cadena;
		FileReader f = new FileReader("./planificacion/" +_FicheroVuelos);
		BufferedReader b = new BufferedReader(f);
		
		while((cadena = b.readLine())!=null) {
			if (cadena.indexOf(_Fecha) >= 0) 
				System.out.println(cadena);
		}
		
		b.close();
	    
	}
	
	/**
	 * Carga las lineas con n�mero de vuelos y pilotos
	 * @return
	 * @throws IOException 
	 * @throws ParseException 
	 */
	private ArrayList<ArrayList<Vuelo>> cargaLineasPilotos(String path) throws IOException, ParseException {
		
		String cadena;
		FileReader fsalida = new FileReader(path + _FicheroSalida);
		BufferedReader buffer = new BufferedReader(fsalida);
		
		ArrayList<ArrayList<Vuelo>> result = new ArrayList<ArrayList<Vuelo>>();
		
    // Cargamos las l�neas con sus vuelos
    int lineasDeSobra = 1, contLinea = 0, contPiloto = 1, opcion = -1; // opcion -1 son las lineas, 0 separador y 1 vuelos de pilotos
    String separador = "---";
    while((cadena = buffer.readLine())!=null) {
    		
    	if (contLinea == 0)
    		EsHorarioVerano = EsHorarioVerano(cadena);
    	
    	if (contLinea == 1)
    		_ValorSolucion = Integer.parseInt(cadena);
    	
    	// Las primeras 2 l�neas no son significativas
  		if ((contLinea > lineasDeSobra) && (cadena.length() > 2)) {
  			  
  			// Llegamos al separador de lineas y pilotos
  			if ((cadena.equals(separador)) || (opcion == 0))
  				opcion++;
  			
  			// Cargamos las l�neas
  			if (opcion < 0) {
  				
       		String[] parts = cadena.split(";");
       		
       		// Nueva l�nea de vuelos
       		ArrayList<Vuelo> lineaAerea = new ArrayList<Vuelo>();
      		_NumAviones++;
       		
       		for (int i = 1; i < parts.length; i++) {
       			Vuelo oVuelo = new Vuelo(_Fecha, i, (contLinea - lineasDeSobra), parts[0], parts[i]);
	       		lineaAerea.add(oVuelo);
	      		_NumVuelos++;
       		}
       		
       		result.add(lineaAerea);
       	
  			}
  			
				// Cargamos los pilotos
				if (opcion > 0) {

					String[] parts = cadena.split(";");
					
				// Nueva l�nea de vuelos
       		ArrayList<VuelosPilotos> lineaPiloto = new ArrayList<VuelosPilotos>();
      		_NumPilotos++;
					
					// Recorremos los vuelos de los pilotos
					for (int k = 0; k < parts.length; k++) {
						
						VuelosPilotos oVuelosPilotos = new VuelosPilotos(contPiloto,parts[k]);
						lineaPiloto.add(oVuelosPilotos);
						
						int i = 0;
						boolean encontrado = false;

						while ((i < result.size()) && (encontrado == false)) {

							int j = 0;
							while ((j < result.get(i).size()) && (encontrado == false)) {

								if (result.get(i).get(j)._Vuelo.equals(parts[k])) {
									
									result.get(i).get(j)._Piloto = Integer.toString(contPiloto);
									encontrado = true;
									
								}

								j++;
								 
							}
							
							i++;
							
						}
						
					}
       		
       		datosPilotos.add(lineaPiloto);
  				
					contPiloto++;
					
  			}
    		
  		}
  		
  		contLinea++;
    	
    }      

    buffer.close();
    return result;
    
	}

	/**
	 * Carga los datos de los vuelos
	 * @param result
	 * @return
	 * @throws IOException
	 */
	private ArrayList<ArrayList<Vuelo>> cargaVuelos(ArrayList<ArrayList<Vuelo>> result, String path) throws IOException {

		String cadena;
		FileReader fsalida = new FileReader(path + _FicheroVuelos);
		BufferedReader buffer = new BufferedReader(fsalida);
		
    // Cargamos los vuelos
    while((cadena = buffer.readLine())!=null) {
    	
    	// S�lo los vuelos de la fecha
    	if (cadena.indexOf(_Fecha) >= 0) {

    		String[] parts = cadena.split("\t");
					
				int i = 0;
				boolean encontrado = false;
				while ((i < result.size()) && (encontrado == false)) {
					int j = 0;
					while ((j < result.get(i).size()) && (encontrado == false)) {

							if ((result.get(i).get(j)._Fecha.equals(parts[0])) && (result.get(i).get(j)._Vuelo.equals(parts[2]))) {
								
								result.get(i).get(j)._Avion = result.get(i).get(j)._Vuelo;
								result.get(i).get(j)._Origen = parts[3];
								result.get(i).get(j)._Destino = parts[4];
								
								// Vemos si es horario de verano para sumar 1 hora
								String[] HIni = parts[5].split(":");																	
								String strAux = ":" + HIni[1];
								int hora = Integer.parseInt(HIni[0]) + EsHorarioVerano;
								strAux = String.valueOf(hora) + strAux;
								if (hora < 10)
									strAux = "0" + strAux;						
								result.get(i).get(j)._HoraInicio = strAux;
								
								String[] HFin = parts[6].split(":");																	
								strAux = ":" + HFin[1];
								hora = Integer.parseInt(HFin[0]) + EsHorarioVerano;
								strAux = String.valueOf(hora) + strAux;
								if (hora < 10)
									strAux = "0" + strAux;
								result.get(i).get(j)._HoraFin = strAux;
								
								encontrado = true;
								
							}
			
							j++;
							 
						}
						
						i++;
						
					}
					
				}
  		
  	}
    	
    buffer.close();
    
    /*
    // Eliminamos los vuelos err�neos
    for (int i = 0; i < result.size(); i++) {  		
			for (int j = 0; j < result.get(i).size(); j++) { 
				if (result.get(i).get(j).getHoraFin() == null) {
					result.get(i).remove(j);
					_NumVuelos--;
					for (int p = 0; p < datosPilotos.get(i).size(); p++) {
						datosPilotos.get(i).remove(p);
						_NumPilotos--;
					}
				}
			}
		}
    */
    
    return result;
    
	}
	
	/**
	 * Carga los datos de los ficheros
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ParseException 
	 */
	public ArrayList<ArrayList<Vuelo>> cargarInformacion(String path) throws FileNotFoundException, IOException, ParseException {
   
     ArrayList<ArrayList<Vuelo>> result = new ArrayList<ArrayList<Vuelo>>();
     
     // Cargamos los datos
     result = cargaLineasPilotos(path);
     result = cargaVuelos(result,path);

     return result;
       
   }
	
	/**
	 * Devolver los vuelos de los pilotos
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<ArrayList<VuelosPilotos>> ObtenerVuelosPilotos() {
		
		ArrayList<ArrayList<VuelosPilotos>> resultado = new ArrayList<ArrayList<VuelosPilotos>>();
		resultado = (ArrayList<ArrayList<VuelosPilotos>>) datosPilotos.clone();

		return resultado;
		
	}
	
	/**
	 * Hace backup de un fichero
	 * @param fichoOrigen
	 * @param ficheroDestino
	 * @return 
	 */
	public void BackupFichero(String ficheroOrigen, String ficheroDestino)  throws IOException {
		
      Path FROM = Paths.get(ficheroOrigen);
      Path TO = Paths.get(ficheroDestino);

      CopyOption[] options = new CopyOption[]{
        StandardCopyOption.REPLACE_EXISTING,
        StandardCopyOption.COPY_ATTRIBUTES
      }; 
      Files.copy(FROM, TO, options);
		
	}
	
	/**
	 * Crear fichero Configuracion
	 * @param fich
	 * @return
	 * @throws IOException 
	 */
	public void CrearFicheroConfiguracion(String fichConf, String fichConfNew) throws IOException {

		String cadena;
		FileReader fsalida = new FileReader(RUTA + "/" + fichConf);
		BufferedReader buffer = new BufferedReader(fsalida);
		BufferedWriter writer = new BufferedWriter(new FileWriter(RUTA + "/" + fichConfNew));
		
    // Copiamos el fichero en el nuevo
		boolean primeraLinea = true;
    while((cadena = buffer.readLine()) != null) {

    	// Se crea nueva linea a partir de la primera
    	if (primeraLinea == false) {
        writer.newLine();
    	}
    	
    	primeraLinea = false;    	
  		String[] parts = cadena.split(" ");
  		if (parts[0].equals("<ficheroVuelos>")) {
  			cadena = parts[0] + " " + fichConfNew;
  		}
  		
  		writer.write(cadena);
  		
  	}
    	
    buffer.close();
    writer.close();			
		
	}
	
	/**
	 * Crear el nuevo fichero de vuelos con las modificaciones
	 * @throws IOException 
	 * @throws ParseException 
	 */
	public void CrearFicheroVuelos (String fichVuelos, String fichVuelosNew, String fichVuelosDiarioNew, ArrayList<Ediciones> Ediciones, ArrayList<Vuelo> EdicionesNuevosVuelos) throws IOException {
	
		String cadena, cadenaAux = "", cadenaLog = "", cadenaDiarioAux = "", cadenaNew = "";
		FileReader fsalida = new FileReader(RUTA + "/" + fichVuelos);
		BufferedReader buffer = new BufferedReader(fsalida);
		BufferedWriter writer = new BufferedWriter(new FileWriter(RUTA + "/" + fichVuelosNew));		
		BufferedWriter writerDiario = new BufferedWriter(new FileWriter(RUTA + "/" + fichVuelosDiarioNew));
		
		try {
			EsHorarioVerano = EsHorarioVerano(_Fecha);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
    // Cargamos los vuelos
		boolean primeraLinea = false;
    while((cadena = buffer.readLine()) != null) {
    	
    	// Revisamos los vuelos de la fecha
    	if (cadena.indexOf(_Fecha) >= 0) {
    		
    		String[] parts = cadena.split("\t");
    		primeraLinea = true;
    		
    		boolean encontrado = false, pintar = true;
    		int pos = 0, valor = 0, valorIni = 0, valorFin = 0, aux;
    		String strHoraIni, strHoraFin;
    		while ((encontrado == false) && (pos < Ediciones.size())) {

    			if (Ediciones.get(pos).getVuelo().equals(parts[2])) {

    				// Cancelaciones
    				if (Ediciones.get(pos).getTipo().equals("C")) {
    					pintar = false;
    				}
    				
    				// Atrasos / Adelantos
    				if ((Ediciones.get(pos).getTipo().equals("A")) || (Ediciones.get(pos).getTipo().equals("R"))) {
    					
    					valor = Ediciones.get(pos).getTiempo();
    					if (Ediciones.get(pos).getTipo().equals("A"))
    						valor = valor * -1;
    					
    					// Hora Origen
    					String[] HIni = parts[5].split(":");
    					valorIni = Integer.parseInt(HIni[0]) * HORA + Integer.parseInt(HIni[1]) + valor;
    					
    					aux = (valorIni % HORA);
    					strHoraIni = String.valueOf(aux);

    					if (Integer.parseInt(strHoraIni) < 10)
    						strHoraIni = "0" + strHoraIni;
    					aux = (valorIni - aux) / HORA;
    					strHoraIni = String.valueOf(aux) + ":" + strHoraIni;
    					if (aux < 10)
    						strHoraIni = "0" + strHoraIni;
    					
    					// Hora Destino
    					String[] HFin = parts[6].split(":");
    					valorFin = Integer.parseInt(HFin[0]) * HORA + Integer.parseInt(HFin[1]) + valor;
    					
    					aux = (valorFin % HORA);
    					strHoraFin = String.valueOf(aux);
    					
    					if (Integer.parseInt(strHoraFin) < 10)
    						strHoraFin = "0" + strHoraFin;
    					aux = (valorFin - aux) / HORA;
    					strHoraFin = String.valueOf(aux) + ":" + strHoraFin;
    					if (aux < 10)
    						strHoraFin = "0" + strHoraFin;
    					
    					cadena = parts[0] + "\t" + parts[1] + "\t" + parts[2] + "\t" + parts[3] + "\t" + parts[4] + "\t" + strHoraIni + "\t" + strHoraFin;
    					    					
    				}

    				encontrado = true;
    				cadenaLog += Ediciones.get(pos).getVuelo() + "\t" + Ediciones.get(pos).getTipo() + "\t" + String.valueOf(Ediciones.get(pos).getTiempo()) + "\n";
    				
    			}
    			
    			pos++;
    			
    		}
    		
    		// Las cancelaciones no las pintamos
    		if (pintar) {
    			cadenaAux += cadena + "\n";
    			cadenaDiarioAux += cadena + "\n";
    		}
    		
    	} else {
    		
    		// Ya se han pintado todos los vuelos de la fecha menos los nuevos
    		if (primeraLinea == true) {
    			
    			primeraLinea = false;
    			for (int a = 0; a < Ediciones.size(); a++) {
    				
	    			if (Ediciones.get(a).getTipo().equals("N")) {
	    				
	    				cadenaNew = _Fecha + "\t" + "AT" + EdicionesNuevosVuelos.get(Ediciones.get(a).getPosicionNuevo()).getCompa�ia() + "\t" + EdicionesNuevosVuelos.get(Ediciones.get(a).getPosicionNuevo()).getVuelo() + "\t";
	    				cadenaNew += EdicionesNuevosVuelos.get(Ediciones.get(a).getPosicionNuevo()).getOrigen() + "\t" + EdicionesNuevosVuelos.get(Ediciones.get(a).getPosicionNuevo()).getDestino() + "\t";
	    				//cadenaNew += EdicionesNuevosVuelos.get(Ediciones.get(a).getPosicionNuevo()).getHoraInicio() + "\t" + EdicionesNuevosVuelos.get(Ediciones.get(a).getPosicionNuevo()).getHoraFin() + "\n";
	    				
	    				String[] parts = EdicionesNuevosVuelos.get(Ediciones.get(a).getPosicionNuevo()).getHoraInicio().split(":");
	    				String hora = ":" + parts[1];
	    				int horaTratada = Integer.parseInt(parts[0]) - EsHorarioVerano;
	    				hora = String.valueOf(horaTratada) + hora;
	    				if (horaTratada < 10)
	    					hora = "0" + hora;
	    				
	    				cadenaNew += hora + "\t";
	    				
	    				parts = EdicionesNuevosVuelos.get(Ediciones.get(a).getPosicionNuevo()).getHoraFin().split(":");
	    				hora = ":" + parts[1];
	    				horaTratada = Integer.parseInt(parts[0]) - EsHorarioVerano;
	    				hora = String.valueOf(horaTratada) + hora;
	    				if (horaTratada < 10)
	    					hora = "0" + hora;
	    						
	    				cadenaNew += hora + "\n";

	    				cadenaDiarioAux += cadenaNew;
	    				cadenaAux += cadenaNew;
	    				
	    				cadenaLog += EdicionesNuevosVuelos.get(Ediciones.get(a).getPosicionNuevo()).getVuelo() + "\t" + Ediciones.get(a).getTipo() + "\t" + String.valueOf(Ediciones.get(a).getTiempo()) + "\t" + EdicionesNuevosVuelos.get(Ediciones.get(a).getPosicionNuevo()).getHoraInicio() + "\t" + EdicionesNuevosVuelos.get(Ediciones.get(a).getPosicionNuevo()).getHoraFin() + "\n";
	    				
	    			}
	    			
    			}
    			
    		}
    		
  			cadenaAux += cadena + "\n";
    	}
    	
    }
    
    // Ya se han pintado todos los vuelos de la fecha menos los nuevos o solol hay vuelos de la fecha
		if (primeraLinea == true) {
			
			primeraLinea = false;
			for (int a = 0; a < Ediciones.size(); a++) {
				
  			if (Ediciones.get(a).getTipo().equals("N")) {
  				
  				cadenaNew = _Fecha + "\t" + "AT" + EdicionesNuevosVuelos.get(Ediciones.get(a).getPosicionNuevo()).getCompa�ia() + "\t" + EdicionesNuevosVuelos.get(Ediciones.get(a).getPosicionNuevo()).getVuelo() + "\t";
  				cadenaNew += EdicionesNuevosVuelos.get(Ediciones.get(a).getPosicionNuevo()).getOrigen() + "\t" + EdicionesNuevosVuelos.get(Ediciones.get(a).getPosicionNuevo()).getDestino() + "\t";
  				//cadenaNew += EdicionesNuevosVuelos.get(Ediciones.get(a).getPosicionNuevo()).getHoraInicio() + "\t" + EdicionesNuevosVuelos.get(Ediciones.get(a).getPosicionNuevo()).getHoraFin() + "\n";

  				String[] parts = EdicionesNuevosVuelos.get(Ediciones.get(a).getPosicionNuevo()).getHoraInicio().split(":");
  				String hora = ":" + parts[1];
  				int horaTratada = Integer.parseInt(parts[0]) - EsHorarioVerano;
  				hora = String.valueOf(horaTratada) + hora;
  				if (horaTratada < 10)
  					hora = "0" + hora;
  				
  				cadenaNew += hora + "\t";
  				
  				parts = EdicionesNuevosVuelos.get(Ediciones.get(a).getPosicionNuevo()).getHoraFin().split(":");
  				hora = ":" + parts[1];
  				horaTratada = Integer.parseInt(parts[0]) - EsHorarioVerano;
  				hora = String.valueOf(horaTratada) + hora;
  				if (horaTratada < 10)
  					hora = "0" + hora;
  						
  				cadenaNew += hora + "\n";
  				
  				cadenaDiarioAux += cadenaNew;
  				cadenaAux += cadenaNew;
  				
  				cadenaLog += EdicionesNuevosVuelos.get(Ediciones.get(a).getPosicionNuevo()).getVuelo() + "\t" + Ediciones.get(a).getTipo() + "\t" + String.valueOf(Ediciones.get(a).getTiempo()) + "\t" + EdicionesNuevosVuelos.get(Ediciones.get(a).getPosicionNuevo()).getHoraInicio() + "\t" + EdicionesNuevosVuelos.get(Ediciones.get(a).getPosicionNuevo()).getHoraFin() + "\n";
  				
  			}
  			
			}
			
		}
    	
    buffer.close();
    writer.write(cadenaAux);
    writer.close();
    writerDiario.write(cadenaDiarioAux);
    writerDiario.close();
    
    // Si hay ediciones se crea un fichero de log
    if (cadenaLog.length() > 3) {
    	String fichLog = _Fecha.split("/")[0] + _Fecha.split("/")[1] + _Fecha.split("/")[2] + "_salida.log";
  		BufferedWriter writerLog = new BufferedWriter(new FileWriter(RUTA + "/" + fichLog));
      writerLog.write(cadenaLog);
      writerLog.close();
    }

	}
	
}