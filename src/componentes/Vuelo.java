package componentes;

/**
 * Clase Vuelo
 * Esta clase guarda la informaci�n de los vuelos
 * @author Dailos
 *
 */
public class Vuelo {
	
	// Variables
	String _Avion;
	String _Vuelo;
	String _Compa�ia;
	String _Origen;
	String _Destino;
	String _Fecha;
	String _HoraInicio;
	String _HoraFin;
	String _Piloto;
	int _Linea;
	int _Orden;
	
	/**
	 * Constructor
	 * @param orden
	 * @param linea
	 * @param compa�ia
	 * @param vuelo
	 */
	public Vuelo (String fecha, int orden, int linea, String compa�ia, String vuelo) {
		
		// Asignacion de valores
		_Fecha = fecha;
		_Orden = orden;
		_Linea = linea;
		_Compa�ia = compa�ia;
		_Vuelo = vuelo;

	}
	
	/**
	 * Constructor
	 * @param avion
	 * @param vuelo
	 * @param compa�ia
	 * @param origen
	 * @param destino
	 * @param fecha
	 * @param horaInicio
	 * @param horaFin
	 * @param piloto
	 * @param linea
	 * @param orden
	 */
	public Vuelo (String avion, String vuelo, String compa�ia, String origen, String destino, String fecha, String horaInicio, String horaFin, String piloto, int linea, int orden) {
		
		// Asignacion de valores
		_Avion = avion;
		_Vuelo = vuelo;
		_Compa�ia = compa�ia;
		_Origen = origen;
		_Destino = destino;
		_Fecha = fecha;
		_HoraInicio = horaInicio;
		_HoraFin = horaFin;
		_Piloto = piloto;
		_Linea = linea;
		_Orden = orden;
		
	}
	
	// getters
	public String getAvion(){return _Avion;};
	public String getVuelo(){return _Vuelo;};
	public String getCompa�ia(){return _Compa�ia;};
	public String getOrigen(){return _Origen;};
	public String getDestino(){return _Destino;};
	public String getFecha(){return _Fecha;};
	public String getHoraInicio(){return _HoraInicio;};
	public String getHoraFin(){return _HoraFin;};
	public String getPiloto(){return _Piloto;};
	public int getLinea(){return _Linea;};
	public int getOrden(){return _Orden;};
	
	// setters
	public void setAvion(String avion){_Avion = avion;};
	public void setVuelo(String vuelo){_Vuelo = vuelo;};
	public void setCompa�ia(String compa�ia){_Compa�ia = compa�ia;};
	public void setOrigen(String origen){_Origen = origen;};
	public void setDestino(String destino){_Destino = destino;};
	public void setFecha(String fecha){_Fecha = fecha;};
	public void setHoraInicio(String horaInicio){_HoraInicio = horaInicio;};
	public void setHoraFin(String horaFin){_HoraFin = horaFin;};
	public void setPiloto(String piloto){_Piloto = piloto;};
	public void setLinea(int linea){_Linea = linea;};
	public void setOrden(int orden){_Orden = orden;};
	
	/**
	 * Imprime los datos del vuelo
	 */
	public void Print() {
		
		System.out.println("Datos del vuelo:");
		System.out.println("-----------------------:");
		System.out.println("Piloto: " + _Piloto);
		System.out.println("Avi�n: " + _Avion);
		System.out.println("Vuelo: " + _Vuelo);
		System.out.println("Compa�ia: " + _Compa�ia);
		System.out.println("Origen: " + _Origen);
		System.out.println("Destino: " + _Destino);
		System.out.println("Fecha: " + _Fecha);
		System.out.println("Hora Inicio: " + _HoraInicio);
		System.out.println("Hora Fin: " + _HoraFin);
		System.out.println("L�nea: " + Integer.toString(_Linea));
		System.out.println("Orden: " + Integer.toString(_Orden));
		
	}

}
