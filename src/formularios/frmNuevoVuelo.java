package formularios;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;

import componentes.Mapa;
import componentes.Vuelo;

/**
 * Clase frmNuevoVuelo
 * Formulario mediante el cual se crean los nuevos vuelos
 * @author Dailos
 *
 */
@SuppressWarnings("serial")
public class frmNuevoVuelo extends JFrame {

	// Variables
	public JPanel contentPane;
	JSpinner jspnHoraIni, jspnHoraFin, jspnMinIni, jspnMinFin;
	String txtHoraIni, txtHoraFin;
	JTextField txtVuelo;
	JComboBox<String> cmbAvion, cmbCompany, cmbOrigen, cmbDestino;
	
	ArrayList<ArrayList<Vuelo>> datos = new ArrayList<ArrayList<Vuelo>>();
	Mapa mapa;
	
	String _Vuelo;
	String _Avion;
	String _HorarioIni;
	String _HorarioFin;
	String _Origen;
	String _Destino;
	String _Company;
	String _Fecha;
	
	//getters
	public String getVuelo(){return _Vuelo;};
	public String getAvion(){return _Avion;};
	public String getHorarioIni(){return _HorarioIni;};
	public String getHorarioFin(){return _HorarioFin;};
	public String getOrigen(){return _Origen;};
	public String getDestino(){return _Destino;};
	public String getCompany(){return _Company;};
	public String getFecha(){return _Fecha;};
	
	// setters
	public void setVuelo(String vuelo){_Vuelo = vuelo;};
	public void setAvion(String avion){_Avion = avion;};
	public void setHorarioIni(String horarioini){_HorarioIni = horarioini;};
	public void setHorarioFin(String horariofin){_HorarioFin = horariofin;};
	public void setOrigen(String origen){_Origen = origen;};
	public void setDestino(String destino){_Destino = destino;};
	public void setCompany(String company){_Company = company;};
	public void setFecha(String fecha){_Fecha = fecha;};

	/*
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frmNuevoVuelo frame = new frmNuevoVuelo();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	*/
	
	/**
	 * Devuelve el combo de aeropuertos
	 * @return
	 */
	private JComboBox<String> ComboAeropuertos() {
		
		JComboBox<String> combo = new JComboBox<String>();
		
		combo.addItem("LPA - Aeropuerto de Gran Canaria, Espa�a");
		combo.addItem("TFN - Aeropuerto de Tenerife-Norte, Espa�a");
		combo.addItem("TFS - Aeropuerto de Tenerife-Sur, Espa�a");
		combo.addItem("ACE - Aeropuerto de Lanzarote, Espa�a");
		combo.addItem("SPC - Aeropuerto de La Palma, Espa�a");
		combo.addItem("GMZ - Aeropuerto de La Gomera, Espa�a");
		combo.addItem("VDE - Aeropuerto de El Hierro, Espa�a");
		combo.addItem("FUE - Aeropuerto de Fuerteventura, Espa�a");
		combo.addItem("EUN - Aeropuerto de Laayoune, Sahara Occidental");
		combo.addItem("RAK - Aeropuerto de Marrakech-Menara, Marruecos");
		combo.addItem("FNC - Aeropuerto de Madeira, Portugal");
		combo.addItem("NDB - Aeropuerto Internacional de Nouadhibou, Mauritania");
		combo.addItem("CMN - Aeropuerto Internacional Moh�mmed V, Marruecos");
		combo.addItem("AGA - Aeropuerto de Agadir-Al Massira, Marruecos");
		
		return combo;
		
	}
	
	/**
	 * Configura la pantalla
	 */
	private void ConfigurarPantalla() {
		
		setTitle("Nuevo Vuelo");
		Toolkit kit = Toolkit.getDefaultToolkit();
		Image img = kit.createImage("./others/imagenes/logos/logoULL.jpg");
		setIconImage(img);
		
	}
	
	/**
	 * Constructor
	 * @param fecha
	 * @param info
	 * @param map
	 */
	@SuppressWarnings("unchecked")
	public frmNuevoVuelo(String fecha, ArrayList<ArrayList<Vuelo>> info, Mapa map) {
		
		datos = (ArrayList<ArrayList<Vuelo>>) info.clone();
		mapa = map;
		setFecha(fecha);
		mostrarNuevoVuelo();
		
	}
	
	/**
	 * Metodo que muestra el formulario
	 */
	public void mostrarNuevoVuelo() {
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new GridLayout(9,1));
		setContentPane(contentPane);
		
		ConfigurarPantalla();
		
		// Fecha
		JPanel pnlFecha = new JPanel();
		pnlFecha.setLayout(new FlowLayout(FlowLayout.RIGHT));

		JLabel lblFecha = new JLabel(getFecha());
		lblFecha.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 20));
		pnlFecha.add(lblFecha);
		
		ImageIcon imgLogo = new ImageIcon("C:/Users/Dailos/workspace/TFG_alu0100036862/others/imagenes/logos/nanlogoBinter.png");
		JLabel lblLogo = new JLabel(imgLogo);
		lblLogo.setLayout(new FlowLayout(FlowLayout.RIGHT));
		pnlFecha.add(lblLogo);
		
		
		// N�mero de vuelo
		JPanel pnlVuelo = new JPanel();
		pnlVuelo.setLayout(new FlowLayout(FlowLayout.LEFT));
		
		txtVuelo = new JTextField(10);
		
		pnlVuelo.add(new JLabel("N�m. de vuelo: "));
		pnlVuelo.add(txtVuelo);
		
		
		// Avi�n
		JPanel pnlAvion = new JPanel();
		pnlAvion.setLayout(new FlowLayout(FlowLayout.LEFT));
		
		cmbAvion = new JComboBox<String>();
		for (int i = 0; i < datos.size(); i++)
			cmbAvion.addItem("R. " + String.valueOf(i+1));
		
		pnlAvion.add(new JLabel("Avi�n: "));
		pnlAvion.add(cmbAvion);
		
		
		// Compa��a
		JPanel pnlCompany = new JPanel();
		pnlCompany.setLayout(new FlowLayout(FlowLayout.LEFT));
		
		cmbCompany = new JComboBox<String>();
		cmbCompany.addItem("Naysa");
		cmbCompany.addItem("Binter Canarias");
		cmbCompany.addItem("Canair");
		
		pnlCompany.add(new JLabel("Compa��a: "));
		pnlCompany.add(cmbCompany);
				
		
		// Hora salida
		JPanel pnlHoraInicio = new JPanel();
		pnlHoraInicio.setLayout(new FlowLayout(FlowLayout.LEFT));
		
		SpinnerNumberModel modelSpinnerHoraIni = new SpinnerNumberModel(6, 6, 23, 1); 
		jspnHoraIni = new JSpinner(modelSpinnerHoraIni);
    JFormattedTextField jtxtHoraIni = ((JSpinner.DefaultEditor) jspnHoraIni.getEditor()).getTextField();
    jtxtHoraIni.setEditable(false);
    
    SpinnerNumberModel modelSpinnerMinIni = new SpinnerNumberModel(0, 0, 59, 1); 
		jspnMinIni = new JSpinner(modelSpinnerMinIni);
    JFormattedTextField jtxtMinIni = ((JSpinner.DefaultEditor) jspnMinIni.getEditor()).getTextField();
    jtxtMinIni.setEditable(false);
    
		pnlHoraInicio.add(new JLabel("Hora inicio: "));
		pnlHoraInicio.add(jspnHoraIni);
		pnlHoraInicio.add(jspnMinIni);
		
		
		// Hora llegada
		JPanel pnlHoraFin = new JPanel();
		pnlHoraFin.setLayout(new FlowLayout(FlowLayout.LEFT));
		
		SpinnerNumberModel modelSpinnerHoraFin = new SpinnerNumberModel(6, 6, 23, 1);
		jspnHoraFin = new JSpinner(modelSpinnerHoraFin);
    JFormattedTextField jtxtHoraFin = ((JSpinner.DefaultEditor) jspnHoraFin.getEditor()).getTextField();
    jtxtHoraFin.setEditable(false);

    SpinnerNumberModel modelSpinnerMinFin = new SpinnerNumberModel(0, 0, 59, 1);
    jspnMinFin = new JSpinner(modelSpinnerMinFin);
    JFormattedTextField jtxtMinFin = ((JSpinner.DefaultEditor) jspnMinFin.getEditor()).getTextField();
    jtxtMinFin.setEditable(false);
    
		pnlHoraFin.add(new JLabel("Hora fin: "));
		pnlHoraFin.add(jspnHoraFin);
		pnlHoraFin.add(jspnMinFin);


		// Origen
		JPanel pnlOrigen = new JPanel();
		pnlOrigen.setLayout(new FlowLayout(FlowLayout.LEFT));
		
		cmbOrigen = new JComboBox<String>();
		cmbOrigen = ComboAeropuertos();
		
		pnlOrigen.add(new JLabel("Origen: "));
		pnlOrigen.add(cmbOrigen);
		
		
		// Destino
		JPanel pnlDestino = new JPanel();
		pnlDestino.setLayout(new FlowLayout(FlowLayout.LEFT));
		
		cmbDestino = new JComboBox<String>();
		cmbDestino = ComboAeropuertos();
		
		pnlDestino.add(new JLabel("Destino: "));
		pnlDestino.add(cmbDestino);
		
		
		// Botones
		JPanel pnlBotones = new JPanel();
		pnlBotones.setLayout(new FlowLayout(FlowLayout.CENTER));
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(buttonListener);
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(buttonListener);
		
		pnlBotones.add(btnAceptar);
		pnlBotones.add(btnCancelar);
		
		// Cargamos el formulario
		contentPane.add(pnlFecha);
		contentPane.add(pnlVuelo);
		contentPane.add(pnlAvion);
		contentPane.add(pnlCompany);
		contentPane.add(pnlHoraInicio);
		contentPane.add(pnlHoraFin);
		contentPane.add(pnlOrigen);
		contentPane.add(pnlDestino);
		contentPane.add(pnlBotones);
		
	}
	
	/**
	 * Comprueba si existe el vuelo
	 * @param numVuelo
	 * @return
	 */
	private boolean ExisteVuelo(String numVuelo) {
		
		boolean existe = false;
		
		for (int i = 0; i < datos.size(); i++) {
    	for (int j = 1; j < datos.get(i).size(); j++) {
    		if (datos.get(i).get(j).getVuelo().equals(numVuelo))
    			return true;
    	}
		}
    	
		return existe;
		
	}
	
	/**
	 * Comprobaci�n de los datos
	 * @return
	 */
	private boolean DatosCorrectos() {
		
		boolean salida = true;
		
		// N�mero de vuelo
		if (txtVuelo.getText().length() < 3) {
			JOptionPane.showMessageDialog(null, "El n�mero de vuelo no es correcto!");
			return false;
		}
		
		if ((ExisteVuelo(txtVuelo.getText())) || (mapa.ExisteVueloNuevo(txtVuelo.getText()))) {
			JOptionPane.showMessageDialog(null, "El n�mero de vuelo ya existe!");
			return false;
		}
		
		// Avion
		if (cmbAvion.getSelectedItem().toString().length() < 2) {
			JOptionPane.showMessageDialog(null, "Debe seleccionar un avi�n!");
			return false;
		}
		
		// Compa��a
		if (cmbCompany.getSelectedItem().toString().length() < 2) {
			JOptionPane.showMessageDialog(null, "Debe seleccionar una compa��a!");
			return false;
		}		
		
		// Horario
		int HoraIni = (Integer.parseInt(((SpinnerNumberModel)jspnHoraIni.getModel()).getNumber().toString()) * 60) + Integer.parseInt(((SpinnerNumberModel)jspnMinIni.getModel()).getNumber().toString());
		int HoraFin = (Integer.parseInt(((SpinnerNumberModel)jspnHoraFin.getModel()).getNumber().toString()) * 60) + Integer.parseInt(((SpinnerNumberModel)jspnMinFin.getModel()).getNumber().toString());
		
		if (HoraFin <= HoraIni) {
			JOptionPane.showMessageDialog(null, "El horario no es correcto!");
			return false;
		}		
		
		// Origen
		if (cmbOrigen.getSelectedItem().toString().length() < 2) {
			JOptionPane.showMessageDialog(null, "Debe seleccionar un origen!");
			return false;
		}
		
		// Destino
		if (cmbDestino.getSelectedItem().toString().length() < 2) {
			JOptionPane.showMessageDialog(null, "Debe seleccionar un destino!");
			return false;
		}		
		
		// Origen
		if (cmbOrigen.getSelectedItem().toString().equals(cmbDestino.getSelectedItem().toString())) {
			JOptionPane.showMessageDialog(null, "Debe seleccionar un origen y destino diferentes!");
			return false;
		}
		
		return salida;
		
	}
	
	/**
	 * Obtenemos el horario
	 * @return
	 */
	private String ObtenerHorario(JSpinner jspnHora, JSpinner jspnMin) {
		
		String hora = ((SpinnerNumberModel)jspnHora.getModel()).getNumber().toString();
		if (Integer.parseInt(((SpinnerNumberModel)jspnHora.getModel()).getNumber().toString()) < 10)
			hora = "0" + hora;
		hora += ":";
		if (Integer.parseInt(((SpinnerNumberModel)jspnMin.getModel()).getNumber().toString()) < 10)
			hora += "0";
		hora += ((SpinnerNumberModel)jspnMin.getModel()).getNumber().toString();
		
		return hora;
		
	}
	
	// Listener de los botones
	ActionListener buttonListener = new ActionListener() {
		
		/**
		 * Acciones de los botones
		 */
    public void actionPerformed(ActionEvent event) {
    	
      switch (event.getActionCommand()) {
      
	      case "Aceptar":
	      	
	      	setVuelo("");
	      	setAvion("");
	      	setHorarioIni("");
	      	setHorarioFin("");
	      	setOrigen("");
	      	setDestino("");
	      	setCompany("");
	      	
	      	// Comprobamos los datos antes de guardarlos
	      	if (DatosCorrectos()) {
	      		
	      		setVuelo(txtVuelo.getText());
		      	setAvion(cmbAvion.getSelectedItem().toString().replace("R. ", ""));
		      	setHorarioIni(ObtenerHorario(jspnHoraIni, jspnMinIni));
		      	setHorarioFin(ObtenerHorario(jspnHoraFin, jspnMinFin));
		      	setOrigen(cmbOrigen.getSelectedItem().toString().substring(0,3));
		      	setDestino(cmbDestino.getSelectedItem().toString().substring(0,3));
		      	setCompany(cmbCompany.getSelectedItem().toString().substring(0,1));
		      	
		      	Vuelo oVuelo = new Vuelo(getVuelo(), getVuelo(), getCompany(), getOrigen(), getDestino(), getFecha(), getHorarioIni(), getHorarioFin(), "", Integer.parseInt(getAvion()), Integer.parseInt(getAvion()));
		      	mapa.A�adirEdicionNuevosVuelos(oVuelo);
		      	
		      	dispose();
		      	
	      	}

		      break;
		      
	      case "Cancelar":
	      	dispose();
		      break;
		      
      }
      
    }
   
	};

}
