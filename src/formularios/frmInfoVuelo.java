package formularios;

import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import componentes.Vuelo;

import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

/**
 * Clase frmInfoVuelo
 * Formulario que muestra la informaci�n de un vuelo
 * @author Dailos
 *
 */
@SuppressWarnings("serial")
public class frmInfoVuelo extends JFrame {

	// Variables	
	public JPanel contenedor;
	
	String PATH = System.getProperty("user.dir");
	
	String _Vuelo;
	String _Piloto;
	String _Horario;
	String _Origen;
	String _Destino;
	String _Company;
	
	//getters
	public String getVuelo(){return _Vuelo;};
	public String getPiloto(){return _Piloto;};
	public String getHorario(){return _Horario;};
	public String getOrigen(){return _Origen;};
	public String getDestino(){return _Destino;};
	public String getCompany(){return _Company;};
	
	// setters
	public void setVuelo(String vuelo){_Vuelo = vuelo;};
	public void setPiloto(String piloto){_Piloto = piloto;};
	public void setHorario(String horario){_Horario = horario;};
	public void setOrigen(String origen){_Origen = origen;};
	public void setDestino(String destino){_Destino = destino;};
	public void setCompany(String company){_Company = company;};

	/**
	 * Devuelve la letra del destino
	 * @param dest: 3 caracteres (AEROPUERTO)
	 * @param opc: 1 - Inicial, 2 - Nombre aeropuerto, 3 - Nombre fichero imagen bandera
	 * @return
	 */
	public String ObtenerDestinos(String dest, int opc) {
		
		String destino = "";
		
		switch (dest) {
		 
			case "LPA":
				if (opc == 1)
					destino = "L";
				if (opc == 2)
					destino = "Aeropuerto de Gran Canaria, Espa�a";
				if (opc == 3)
					destino = "ESP";
				break;
	
			case "TFN":
				if (opc == 1)
					destino = "T";
				if (opc == 2)
					destino = "Aeropuerto de Tenerife-Norte, Espa�a";
				if (opc == 3)
					destino = "ESP";
				break;
	
			case "TFS":
				if (opc == 1)
					destino = "t";
				if (opc == 2)
					destino = "Aeropuerto de Tenerife-Sur, Espa�a";
				if (opc == 3)
					destino = "ESP";
				break;
	
			case "ACE":
				if (opc == 1)
					if (opc == 1)
						destino = "A";
				if (opc == 2)
					destino = "Aeropuerto de Lanzarote, Espa�a";
				if (opc == 3)
					destino = "ESP";
				break;
	
			case "SPC":
				if (opc == 1)
					destino = "S";
				if (opc == 2)
					destino = "Aeropuerto de La Palma, Espa�a";
				if (opc == 3)
					destino = "ESP";
				break;
	
			case "GMZ":
				destino = "G";
				if (opc == 2)
					destino = "Aeropuerto de La Gomera, Espa�a";
				if (opc == 3)
					destino = "ESP";
				break;
	
			case "VDE":
				if (opc == 1)
					destino = "V";
				if (opc == 2)
					destino = "Aeropuerto de El Hierro, Espa�a";
				if (opc == 3)
					destino = "ESP";
				break;
	
			case "FUE":
				if (opc == 1)
					destino = "F";
				if (opc == 2)
					destino = "Aeropuerto de Fuerteventura, Espa�a";
				if (opc == 3)
					destino = "ESP";
				break;
	
			case "EUN":
				if (opc == 1)
					destino = "E";
				if (opc == 2)
					destino = "Aeropuerto de Laayoune, Sahara Occidental";
				if (opc == 3)
					destino = "SAH";
				break;
	
			case "RAK":
				if (opc == 1)
					destino = "R";
				if (opc == 2)
					destino = "Aeropuerto de Marrakech-Menara, Marruecos";
				if (opc == 3)
					destino = "MAR";
				break;
	
			case "FNC":
				if (opc == 1)
					destino = "f";
				if (opc == 2)
					destino = "Aeropuerto de Madeira, Portugal";
				if (opc == 3)
					destino = "POR";
				break;
	
			case "NDB":
				if (opc == 1)
					destino = "N";
				if (opc == 2)
					destino = "Aeropuerto Internacional de Nouadhibou, Mauritania";
				if (opc == 3)
					destino = "MAU";
				break;
	
			case "CMN":
				if (opc == 1)
					destino = "C";
				if (opc == 2)
					destino = "Aeropuerto Internacional Moh�mmed V, Marruecos";
				if (opc == 3)
					destino = "MAR";
				break;
	
			case "AGA":
				if (opc == 1)
					destino = "a";
				if (opc == 2)
					destino = "Aeropuerto de Agadir-Al Massira, Marruecos";
				if (opc == 3)
					destino = "MAR";
				break;
	    
	    default:
	    	if (opc == 1)
	    		destino = "X";
	    	if (opc == 2)
	    		destino = "** Desconocido **";
				if (opc == 3)
					destino = "DES";
	    	break;

		}
		
		return destino;
	}

	/**
	 * Obtenemos la compa�ia
	 * @param comp
	 * @return
	 */
	public String ObtenerCompany(String comp) {
		
		String company = "";
		
		switch (comp) {
		 
			case "N":
				company = "Naysa";
				break;
	
			case "C":
				company = "CanAir";
				break;
	
			case "B":
				company = "Binter Canarias";
				break;
				
			default:
				company = "** Desconocida **";				
				break;
		
		}
		
		return company;
		
	}
	
	/**
	 * Constructor
	 */
	public frmInfoVuelo(Vuelo info) {
		
		_Vuelo = info.getVuelo();
		_Piloto = info.getPiloto();
		_Horario = info.getHoraInicio() + " - " + info.getHoraFin();
		_Origen = info.getOrigen();
		_Destino = info.getDestino();
		_Company = ObtenerCompany(info.getCompa�ia());
		
		mostrarInfoVuelo();
			
	}
	
	/**
	 * Configura la pantalla
	 */
	private void ConfigurarPantalla() {
		
		setTitle("Info Vuelo");
		Toolkit kit = Toolkit.getDefaultToolkit();
		Image img = kit.createImage("./others/imagenes/logos/logoULL.jpg");
		setIconImage(img);
		
	}

	/*
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frmInfoVuelo frame = new frmInfoVuelo();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	*/

	/**
	 * Metodo para mostrar la informaci�n del vuelo
	 */
	public void mostrarInfoVuelo() {

		setAlwaysOnTop(true);
		setBounds(100, 100, 410, 360);
		contenedor = new JPanel();
		contenedor.setBorder(new EmptyBorder(5, 5, 5, 5));
		contenedor.setLayout(new GridBagLayout());
		setContentPane(contenedor);
		
		ConfigurarPantalla();
		
		GridBagConstraints c = new GridBagConstraints();

		JPanel pnlVuelo = new JPanel();
		pnlVuelo.setLayout(new FlowLayout(FlowLayout.LEFT));
		
		JLabel lblNumVuelo = new JLabel(_Vuelo);
		lblNumVuelo.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 32));
		pnlVuelo.add(lblNumVuelo);
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 0.5;
		c.gridx = 0;
		c.gridy = 0;
		contenedor.add(pnlVuelo, c);

		JPanel pnlLogo = new JPanel();
		pnlLogo.setLayout(new FlowLayout(FlowLayout.RIGHT));
		
		ImageIcon imgLogo = null;
		/*
		switch (_Company.substring(0, 1)) {
		 
			case "N":
				imgLogo = new ImageIcon("C:/Users/Dailos/workspace/TFG_alu0100036862/others/imagenes/logos/NAY.jpg");
	    	break;

			case "B":
				imgLogo = new ImageIcon("C:/Users/Dailos/workspace/TFG_alu0100036862/others/imagenes/logos/BIN.jpg");
	    	break;
				
			case "C":
				imgLogo = new ImageIcon("C:/Users/Dailos/workspace/TFG_alu0100036862/others/imagenes/logos/CAN.jpg");
	    	break;
				
			default:
				imgLogo = new ImageIcon("C:/Users/Dailos/workspace/TFG_alu0100036862/others/imagenes/logos/minlogoBinter.png");
	    	break;

		}
		*/
		
		imgLogo = new ImageIcon("C:/Users/Dailos/workspace/TFG_alu0100036862/others/imagenes/logos/minlogoBinter.png");
		JLabel lblLogo = new JLabel(imgLogo);
		lblLogo.setLayout(new FlowLayout(FlowLayout.RIGHT));
		pnlLogo.add(lblLogo); 

		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 0.5;
		c.gridx = 2;
		c.gridy = 0;
		contenedor.add(pnlLogo, c);
		
		JPanel pnlInfo = new JPanel();
		pnlInfo.setLayout(new GridLayout(1,0));		
		
		JPanel pnlOrigenDestino = new JPanel();
		pnlOrigenDestino.setLayout(new GridLayout(6,1));		

		JPanel pnlHorario = new JPanel();
		pnlHorario.setLayout(new GridLayout(2,1));
		
		pnlHorario.add(new JLabel("Hora Inicio - Fin:"));
		JLabel lblHorario = new JLabel(_Horario);
		lblHorario.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 14));
		lblHorario.setForeground(new Color(0,100,00));
		pnlHorario.add(lblHorario);
		
		JPanel pnlOrigen = new JPanel();
		pnlOrigen.setLayout(new FlowLayout(FlowLayout.LEFT));
		ImageIcon imgOri = new ImageIcon("C:/Users/Dailos/workspace/TFG_alu0100036862/others/imagenes/banderas/" + ObtenerDestinos(_Origen,3) + ".png");
		JLabel lblImgOri = new JLabel(imgOri);
		lblImgOri.setLayout(new FlowLayout(FlowLayout.RIGHT));
		pnlOrigen.add(lblImgOri); 
		JLabel lblOri = new JLabel(ObtenerDestinos(_Origen,2));
		lblOri.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 14));
		lblOri.setForeground(new Color(0,100,00));
		pnlOrigen.add(lblOri);
		
		JPanel pnlDestino = new JPanel();
		pnlDestino.setLayout(new FlowLayout(FlowLayout.LEFT));
		ImageIcon imgDes = new ImageIcon("C:/Users/Dailos/workspace/TFG_alu0100036862/others/imagenes/banderas/" + ObtenerDestinos(_Destino,3) + ".png");
		JLabel lblImgDes = new JLabel(imgDes);
		lblImgDes.setLayout(new FlowLayout(FlowLayout.RIGHT));
		pnlDestino.add(lblImgDes); 
		JLabel lblDes = new JLabel(ObtenerDestinos(_Destino,2));
		lblDes.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 14));
		lblDes.setForeground(new Color(0,100,00));
		pnlDestino.add(lblDes);
		
		JPanel pnlPiloto = new JPanel();
		pnlPiloto.setLayout(new GridLayout(2,1));
		
		pnlPiloto.add(new JLabel("Piloto:"));
		JLabel lblPiloto = new JLabel(_Piloto);
		lblPiloto.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 14));
		lblPiloto.setForeground(new Color(0,100,00));
		pnlPiloto.add(lblPiloto);
		//pnlInfo.add(pnlHorario);
		
		JPanel pnlCompany = new JPanel();
		pnlCompany.setLayout(new GridLayout(2,1));
		
		pnlCompany.add(new JLabel("Compa��a:"));
		JLabel lblCompany = new JLabel(_Company);
		lblCompany.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 14));
		lblCompany.setForeground(new Color(0,100,00));
		pnlCompany.add(lblCompany);
		//pnlInfo.add(pnlHorario);

		pnlOrigenDestino.add(pnlPiloto);
		pnlOrigenDestino.add(pnlCompany);
		pnlOrigenDestino.add(pnlHorario);
		pnlOrigenDestino.add(new JLabel("Origen - Destino:"));
		pnlOrigenDestino.add(pnlOrigen);
		pnlOrigenDestino.add(pnlDestino);
		pnlInfo.add(pnlOrigenDestino);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipady = 40;
		c.weightx = 0.0;
		c.gridwidth = 3;
		c.gridx = 0;
		c.gridy = 1;
		contenedor.add(pnlInfo, c);		
		
	}

}
