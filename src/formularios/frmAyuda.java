package formularios;

import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import componentes.Vuelo;

import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

/**
 * Clase frmAyuda
 * Formulario que muestra la información de la herramienta
 * @author Dailos
 *
 */
@SuppressWarnings("serial")
public class frmAyuda extends JFrame {

	// Variables	
	public JPanel contenedor;
	
	/**
	 * Constructor
	 */	
	public frmAyuda() {
		
		mostrarInfoAyuda();
		
	}

	/**
	 * Configura la pantalla
	 */
	private void ConfigurarPantalla() {
		
		setTitle("Ayuda");
		Toolkit kit = Toolkit.getDefaultToolkit();
		Image img = kit.createImage("./others/imagenes/logos/logoULL.jpg");
		setIconImage(img);
		
	}

	/*
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frmAyuda frame = new frmAyuda();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	*/

	/**
	 * Metodo para mostrar la información de la herramienta
	 */
	public void mostrarInfoAyuda() {

		setAlwaysOnTop(true);
		setBounds(100, 100, 410, 360);
		contenedor = new JPanel();
		contenedor.setBorder(new EmptyBorder(5, 5, 5, 5));
		contenedor.setLayout(new GridLayout(8,0));
		setContentPane(contenedor);
		
		ConfigurarPantalla();
		
		ImageIcon aux = new ImageIcon("./others/imagenes/iconos/save.png");
		Image img = aux.getImage().getScaledInstance(20,20,java.awt.Image.SCALE_SMOOTH);
		ImageIcon icon = new ImageIcon(img);
		JLabel lblGuardar = new JLabel("Este botón permite guardar la solución cargada en el formulario (solución inicial o resultante de generar solución editada)", icon, javax.swing.SwingConstants.LEFT);
		
		aux = new ImageIcon("./others/imagenes/iconos/settings-5.png");
		img = aux.getImage().getScaledInstance(20,20,java.awt.Image.SCALE_SMOOTH);
		icon = new ImageIcon(img);
		JLabel lblConfig = new JLabel("Este botón permite seleccionar el fichero de configuración. Este fichero es indispensable para realizar cualquier proceso en la herramienta", icon, javax.swing.SwingConstants.LEFT);

		aux = new ImageIcon("./others/imagenes/iconos/folder-13.png");
		img = aux.getImage().getScaledInstance(20,20,java.awt.Image.SCALE_SMOOTH);
		icon = new ImageIcon(img);
		JLabel lblAbrirSol = new JLabel("Este botón permite seleccionar el fichero que contiene la solución con la planificación a cargar", icon, javax.swing.SwingConstants.LEFT);
		
		aux = new ImageIcon("./others/imagenes/iconos/success.png");
		img = aux.getImage().getScaledInstance(20,20,java.awt.Image.SCALE_SMOOTH);
		icon = new ImageIcon(img);
		JLabel lblGenSol = new JLabel("Este botón permite generar una nueva solución basándose en la configuración del fichero elegido previamente", icon, javax.swing.SwingConstants.LEFT);

		aux = new ImageIcon("./others/imagenes/iconos/play-button-1.png");
		img = aux.getImage().getScaledInstance(20,20,java.awt.Image.SCALE_SMOOTH);
		icon = new ImageIcon(img);
		JLabel lblCargar = new JLabel("Este botón carga la solución o bien desde un fichero o la generada", icon, javax.swing.SwingConstants.LEFT);
		
		aux = new ImageIcon("./others/imagenes/iconos/ModoEdicion.png");
		img = aux.getImage().getScaledInstance(109,26,java.awt.Image.SCALE_SMOOTH);
		icon = new ImageIcon(img);
		JLabel lblEdicion = new JLabel("Este botón permite entrar en el modo edición de la solución para informar las incidencias que se crean oportunas sobre la misma", icon, javax.swing.SwingConstants.LEFT);

		aux = new ImageIcon("./others/imagenes/iconos/MinutosAplicar.png");
		img = aux.getImage().getScaledInstance(53,20,java.awt.Image.SCALE_SMOOTH);
		icon = new ImageIcon(img);
		JLabel lblAplicar = new JLabel("Este control permite seleccionar los minutos a aplicar en la incidencia, si corresponde", icon, javax.swing.SwingConstants.LEFT);
		
		aux = new ImageIcon("./others/imagenes/iconos/GenerarSol.png");
		img = aux.getImage().getScaledInstance(131,26,java.awt.Image.SCALE_SMOOTH);
		icon = new ImageIcon(img);
		JLabel lblNuevaSol = new JLabel("Este botón es el encargado de realizar la llamada al optimizador para generar una nueva solución con la planificación que contenga las incidencias informadas si son viables en la misma", icon, javax.swing.SwingConstants.LEFT);
		
		contenedor.add(lblGuardar);
		contenedor.add(lblConfig);
		contenedor.add(lblAbrirSol);
		contenedor.add(lblGenSol);
		contenedor.add(lblCargar);
		contenedor.add(lblEdicion);
		contenedor.add(lblAplicar);
		contenedor.add(lblNuevaSol);
		
	}

}
