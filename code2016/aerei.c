#include "file_strutt.h"
#include <time.h>


void main(int argc, char *argv[])
{

	
	time_t start,end;
	char* name_config_file;
	
#ifdef EVAL_MANUAL_SOL
	double cost;
#endif

	time(&start);

	if(argc<2) {
		//stop_error("call NameInputFile\n");
		stop_error("call NameConfigFile\n");
	}
	name_config_file=argv[1];

	set_airports();

	set_main_airports();

	set_crews();

	set_aircrafts();

	read_config_file(name_config_file);
	
	read_input_file(name_input_file);

	read_duration_table(name_duration_file);
	
	build_crew_graph();

	build_aircraft_graph();

	build_starting_ending_arcs();


#ifdef EVAL_MANUAL_SOL
	//cost=eval_sol("salida01_09_2012.txt");
	//cost=eval_sol("salida02_09_2012.txt");
	//cost=eval_sol("salida03_09_2012.txt");
	//cost=eval_sol("salida04_09_2012.txt");
	//cost=eval_sol("salida05_09_2012.txt");
	//cost=eval_sol("salida06_09_2012.txt");
	//cost=eval_sol("salida07_09_2012.txt");
	//cost=eval_sol("salida01_04_2012.txt");
	//cost=eval_sol("salida02_04_2012.txt");
	//cost=eval_sol("salida03_04_2012.txt");
	//cost=eval_sol("salida04_04_2012.txt");
	//cost=eval_sol("salida05_04_2012.txt");
	//cost=eval_sol("salida06_04_2012.txt");
	//cost=eval_sol("salida07_04_2012.txt");

	cost=eval_sol("soluzione_07_09_2012_30cols_1800.txt");
	//printf("manual sol cost %f\n",cost);
	printf("costo della soluzione %f\n",cost);
	exit(0);
#endif

	init_master();

	init_branch_and_price();

	//LB=solve_master();

	//CPXwriteprob(env,lp,"col_gen_crew.lp",NULL);
    
	time(&end);
	printf("durata totale programma %f secondi\n",difftime(end,start));
	exit(0);
	
}


void alloc_error(char *s)
{
  printf("Warning: Not enough memory to allocate %s",s);
  printf("Cannot proceed with execution");
  exit(-1);
}

void stop_error(char *s)
{
  printf("%s",s);
  printf("Cannot proceed with execution\n");
  exit(-1);
}


void free_memory()
{
	int i,j;

	for(i=0;i<n_of_flights;i++) {
#ifdef RETIME
		free(flight_array[i]->copies);
#endif
		free(flight_array[i]);
	}
	free(flight_array);

	for(i=0;i<N_OF_AIRPORTS;i++) {
		free(airports[i]);
	}
	free(airports);

	free(crews);

	
	for(i=0;i<n_of_flights;i++) {
		free(successori[i]->ind_succ);
		free(successori[i]->arcs);
		free(successori[i]->x_values);
		free(successori[i]->forb);
		free(successori[i]);
	}
	free(successori);

	for(i=0;i<n_of_flights;i++) {
		free(predecessori[i]->ind_pred);
		free(predecessori[i]->arcs);
		free(predecessori[i]);
	}
	free(predecessori);

	for(i=0;i<n_of_flights;i++) {
		free(successori_a[i]->ind_succ);
		free(successori_a[i]->arcs);
		free(successori_a[i]->x_values);
		free(successori_a[i]->forb);
		free(successori_a[i]);
	}
	free(successori_a);

	for(i=0;i<n_of_flights;i++) {
		free(predecessori_a[i]->ind_pred);
		free(predecessori_a[i]->arcs);
		free(predecessori_a[i]);
	}
	free(predecessori_a);


	for(i=0;i<N_MAIN_AIRPORTS;i++) {
		free(starting_arcs[i]->ind_succ);
		free(starting_arcs[i]);
		free(ending_arcs[i]->ind_succ);
		free(ending_arcs[i]);
	}
	free(starting_arcs);
	free(ending_arcs);

	free(dual_vars);
	free(alpha);
	free(beta);
	for(i=0;i<N_COMPANIES;i++) {
		free(gamma_[i]);
	}
	free(gamma_);

	for(i=0;i<n_of_flights;i++) {
		free(eta[i]);
	}
	free(eta);

	for(i=0;i<N_COMPANIES;i++) {
		for(j=0;j<n_of_flights;j++) {
			free(delta[i][j]);
		}
		free(delta[i]);
	}
	free(delta);

	for(i=0;i<n_of_flights;i++) {
		free(indices_short_arcs[i]);
	}
	free(indices_short_arcs);

	
	for(i=0;i<n_of_cols;i++) {
		free(crew_day_pool[i]->flights);
		free(crew_day_pool[i]);
	}
	free(crew_day_pool);

	free(intervals);

	for(i=0;i<8;i++) {
		free(duration_table[i]);
	}
	free(duration_table);

	free(aircrafts);

	free(main_airports);

	
}

void set_airports()
{
	int i;
	
	airports=(char**) calloc(N_OF_AIRPORTS,sizeof(char*));
	if(airports==NULL) alloc_error("airports\n");

	for(i=0;i<N_OF_AIRPORTS;i++) {
		airports[i]=(char*) calloc(4,sizeof(char));
		if(airports[i]==NULL) alloc_error("airports[i]\n");
	}

	strcpy(airports[0],"LPA");
	strcpy(airports[1],"TFN");
	strcpy(airports[2],"TFS");
	strcpy(airports[3],"SPC");
	strcpy(airports[4],"FUE");
	strcpy(airports[5],"VDE");
	strcpy(airports[6],"GMZ");
	strcpy(airports[7],"ACE");
	strcpy(airports[8],"CMN");
	strcpy(airports[9],"RAK");
	strcpy(airports[10],"EUN");
	strcpy(airports[11],"FNC");
	strcpy(airports[12],"AGA");

}

void set_main_airports()
{
	int i;
	
	main_airports=(int*) calloc(N_MAIN_AIRPORTS,sizeof(int));
	if(main_airports==NULL) alloc_error("main_airports\n");

	main_airports[0]=1; //TFN
	main_airports[1]=0; //LPA
	main_airports[2]=3; //SPC
	main_airports[3]=7; //ACE
}

void set_crews()
{
	int i;

	n_of_crew_types=14;
	crews=(crew_type*) calloc(n_of_crew_types,sizeof(crew_type));
	if(crews==NULL) alloc_error("crews\n");

	for(i=0;i<n_of_crew_types;i++) {
		crews[i].n_of_crews=20;
	}
	//L-L
	crews[0].dep_airport=0;
	crews[0].arr_airport=0;
	crews[0].max_flights=8;
	crews[0].company=1; //N

#ifdef ONLY_PENAL_COST
	crews[0].n_of_crews=7;
#endif

	//L-L
	crews[1].dep_airport=0;
	crews[1].arr_airport=0;
	crews[1].max_flights=8;
	crews[1].company=2; //C

#ifdef ONLY_PENAL_COST
	crews[1].n_of_crews=4;
#endif

	//L-L
	crews[2].dep_airport=0;
	crews[2].arr_airport=0;
	crews[2].max_flights=6;
	crews[2].company=0; //B

#ifdef ONLY_PENAL_COST
	crews[2].n_of_crews=0;
#endif

	//T-T
	crews[3].dep_airport=1;
	crews[3].arr_airport=1;
	crews[3].max_flights=8;
	crews[3].company=1; //N

#ifdef ONLY_PENAL_COST
	crews[3].n_of_crews=3;
#endif

	//T-T
	crews[4].dep_airport=1;
	crews[4].arr_airport=1;
	crews[4].max_flights=8;
	crews[4].company=2; //C
#ifdef ONLY_PENAL_COST
	crews[4].n_of_crews=6;
#endif

	//T-T
	crews[5].dep_airport=1;
	crews[5].arr_airport=1;
	crews[5].max_flights=6;
	crews[5].company=0; //B
#ifdef ONLY_PENAL_COST
	crews[5].n_of_crews=0;
#endif

	//S-T
	crews[6].dep_airport=3;
	crews[6].arr_airport=1;
	crews[6].max_flights=8;
	crews[6].company=1; //N
	crews[6].n_of_crews=1;
#ifdef ONLY_PENAL_COST
	crews[6].n_of_crews=1;
#endif

	
	//S-T
	crews[7].dep_airport=3;
	crews[7].arr_airport=1;
	crews[7].max_flights=8;
	crews[7].company=2; //C
	crews[7].n_of_crews=0;


	
	//A-L
	crews[8].dep_airport=7;
	crews[8].arr_airport=0;
	crews[8].max_flights=8;
	crews[8].company=1; //N
	crews[8].n_of_crews=1;
#ifdef ONLY_PENAL_COST
	crews[8].n_of_crews=1;
#endif

	
	//A-L
	crews[9].dep_airport=7;
	crews[9].arr_airport=0;
	crews[9].max_flights=8;
	crews[9].company=2; //C
	crews[9].n_of_crews=0;

	

	//T-S
	crews[10].dep_airport=1;
	crews[10].arr_airport=3;
	crews[10].max_flights=8;
	crews[10].company=1; //N
	crews[10].n_of_crews=1;
#ifdef ONLY_PENAL_COST
	crews[10].n_of_crews=0;
#endif

	
	//T-S
	crews[11].dep_airport=1;
	crews[11].arr_airport=3;
	crews[11].max_flights=8;
	crews[11].company=2; //C
	crews[11].n_of_crews=0;

	
	//L-A
	crews[12].dep_airport=0;
	crews[12].arr_airport=7;
	crews[12].max_flights=8;
	crews[12].company=1; //N
	crews[12].n_of_crews=1;
#ifdef ONLY_PENAL_COST
	crews[12].n_of_crews=0;
#endif

	//L-A
	crews[13].dep_airport=0;
	crews[13].arr_airport=7;
	crews[13].max_flights=8;
	crews[13].company=2; //C
	crews[13].n_of_crews=0;

}

void set_aircrafts()
{
	int i;

	n_of_aircraft_types=10;
	aircrafts=(aircraft_type*) calloc(n_of_aircraft_types,sizeof(aircraft_type));
	if(aircrafts==NULL) alloc_error("aircrafts\n");

	for(i=0;i<n_of_aircraft_types;i++) {
		//lettura da config
		aircrafts[i].n_of_aircrafts=0;
	}
	//T 
	aircrafts[0].dep_airport=1;
	aircrafts[0].company=0; //B
	//aircrafts[0].n_of_aircrafts=0;

	//T
	aircrafts[1].dep_airport=1;
	aircrafts[1].company=1; //N
	//aircrafts[1].n_of_aircrafts=4;

	//T
	aircrafts[2].dep_airport=1;
	aircrafts[2].company=2; //C
	//aircrafts[2].n_of_aircrafts=3;

	//L
	aircrafts[3].dep_airport=0;
	aircrafts[3].company=0; //B
	//aircrafts[3].n_of_aircrafts=1;

	//L
	aircrafts[4].dep_airport=0;
	aircrafts[4].company=1; //N
	//aircrafts[4].n_of_aircrafts=6;

	//L
	aircrafts[5].dep_airport=0;
	aircrafts[5].company=2; //C
	//aircrafts[5].n_of_aircrafts=2;

	//A
	aircrafts[6].dep_airport=7;
	aircrafts[6].company=1; //N
	//aircrafts[6].n_of_aircrafts=1;

	//A
	aircrafts[7].dep_airport=7;
	aircrafts[7].company=2; //C
	//aircrafts[7].n_of_aircrafts=0;

	//S
	aircrafts[8].dep_airport=3;
	aircrafts[8].company=1; //N
	//aircrafts[8].n_of_aircrafts=1;

	//S
	aircrafts[9].dep_airport=3;
	aircrafts[9].company=2; //C
	//aircrafts[9].n_of_aircrafts=0;

}


void read_input_file(char* name_input_file) 
{
	FILE *input_file;
	char buf[DIM];
	char app[DIM];
	int iapp;
	int err;
	int i;
	int ora, min;
#ifdef RETIME
	int j;
	int* flight_array_ord;
	flight** flight_array_temp;
	int found;
	int index;
	int max_code;
	int count;
	int code_copia1, code_copia2;
#endif

	input_file=fopen(name_input_file, "r");
	if(input_file==NULL) stop_error("main cannot open input file\n");

	n_of_flights=0;

	while(fgets(buf,DIM,input_file)!=NULL) {
		n_of_flights++;
	}

	printf("number of flights %d\n",n_of_flights);

#ifdef RETIME
	n_of_flights=n_of_flights*RETIME_INTERV;
#endif

	flight_array=(flight**) calloc(n_of_flights,sizeof(flight*));
	if(flight_array==NULL) alloc_error("flight_array\n");

	for(i=0;i<n_of_flights;i++) {
		flight_array[i]=(flight*) calloc(1,sizeof(flight));
		if(flight_array[i]==NULL) alloc_error("flight_array[i]\n");
#ifdef RETIME
		flight_array[i]->copies=(int*) calloc(RETIME_INTERV-1,sizeof(int));
		if(flight_array[i]->copies==NULL) alloc_error("flight_array[i]->copies\n");
#endif
	}

	err=fseek(input_file,0,0);

	if(err<0)
	{
		printf("Error in reading input file");
		exit(-1);
	}

#ifdef RETIME
	//cerco il volo con codice massimo per poi dare alle copie un codice che non sia doppione di un altro volo
	max_code=-1;
	i=0;
	while(fscanf(input_file,"%s %s %d ",app,app,&iapp)!=EOF) {
		if(iapp>max_code) {
			max_code=iapp;
		}
		fgets(buf,DIM,input_file);
	}
	err=fseek(input_file,0,0);

	if(err<0)
	{
		printf("Error in reading input file");
		exit(-1);
	}
	count=1;
#endif

	i=0;
	while(fscanf(input_file,"%s %s %d ",app,app,&iapp)!=EOF) {
		flight_array[i]->code=iapp;
		fscanf(input_file,"%s ",app);
		flight_array[i]->dep=find_airport(app);
		if(flight_array[i]->dep==-1) {
			stop_error("wrong index departure airport\n");
		}
		fscanf(input_file,"%s ",app);
		flight_array[i]->arr=find_airport(app);
		if(flight_array[i]->arr==-1) {
			stop_error("wrong index arrival airport\n");
		}
		fscanf(input_file,"%d:%d ",&ora,&min);

		flight_array[i]->dep_t=ora*60+min;

		fscanf(input_file,"%d:%d\n",&ora,&min);

		flight_array[i]->arr_t=ora*60+min;
#ifdef RETIME
		flight_array[i]->copies[0]=max_code+count;
		code_copia1=max_code+count;
		count++;
		flight_array[i]->copies[1]=max_code+count;
		code_copia2=max_code+count;
		count++;
		flight_array[i]->is_earliest_copy=0;
		flight_array[i]->is_latest_copy=0;
		flight_array[i]->is_original_flight=1;
#ifdef RETIME_5
		flight_array[i]->copies[2]=flight_array[i]->code*1000;
		flight_array[i]->copies[3]=flight_array[i]->code*10000;
#endif


#endif


		i++;
#ifdef RETIME
		//define the other copies
#ifdef RETIME_5
		//attenzione: i codici vanno cambiati!
		flight_array[i]->code=flight_array[i-1]->code*10;
		flight_array[i]->dep=flight_array[i-1]->dep;
		flight_array[i]->arr=flight_array[i-1]->arr;
		flight_array[i]->dep_t=flight_array[i-1]->dep_t-5;
		flight_array[i]->arr_t=flight_array[i-1]->arr_t-5;
		flight_array[i]->copies[0]=flight_array[i-1]->code;
		flight_array[i]->copies[1]=flight_array[i-1]->code*100;
		flight_array[i]->copies[2]=flight_array[i-1]->code*1000;
		flight_array[i]->copies[3]=flight_array[i-1]->code*10000;
		flight_array[i]->is_earliest_copy=0;
		flight_array[i]->is_latest_copy=0;
		i++;
		flight_array[i]->code=flight_array[i-2]->code*100;
		flight_array[i]->dep=flight_array[i-2]->dep;
		flight_array[i]->arr=flight_array[i-2]->arr;
		flight_array[i]->dep_t=flight_array[i-2]->dep_t+5;
		flight_array[i]->arr_t=flight_array[i-2]->arr_t+5;
		flight_array[i]->copies[0]=flight_array[i-2]->code;
		flight_array[i]->copies[1]=flight_array[i-2]->code*10;
		flight_array[i]->copies[2]=flight_array[i-2]->code*1000;
		flight_array[i]->copies[3]=flight_array[i-2]->code*10000;
		flight_array[i]->is_earliest_copy=0;
		flight_array[i]->is_latest_copy=0;
		i++;
		flight_array[i]->code=flight_array[i-3]->code*1000;
		flight_array[i]->dep=flight_array[i-3]->dep;
		flight_array[i]->arr=flight_array[i-3]->arr;
		flight_array[i]->dep_t=flight_array[i-3]->dep_t-10;
		flight_array[i]->arr_t=flight_array[i-3]->arr_t-10;
		flight_array[i]->copies[0]=flight_array[i-3]->code;
		flight_array[i]->copies[1]=flight_array[i-3]->code*10;
		flight_array[i]->copies[2]=flight_array[i-3]->code*100;
		flight_array[i]->copies[3]=flight_array[i-3]->code*10000;
		flight_array[i]->is_earliest_copy=1;
		flight_array[i]->is_latest_copy=0;
		i++;
		flight_array[i]->code=flight_array[i-4]->code*10000;
		flight_array[i]->dep=flight_array[i-4]->dep;
		flight_array[i]->arr=flight_array[i-4]->arr;
		flight_array[i]->dep_t=flight_array[i-4]->dep_t+10;
		flight_array[i]->arr_t=flight_array[i-4]->arr_t+10;
		flight_array[i]->copies[0]=flight_array[i-4]->code;
		flight_array[i]->copies[1]=flight_array[i-4]->code*10;
		flight_array[i]->copies[2]=flight_array[i-4]->code*100;
		flight_array[i]->copies[3]=flight_array[i-4]->code*1000;
		flight_array[i]->is_earliest_copy=0;
		flight_array[i]->is_latest_copy=1;
		i++;

#else
		flight_array[i]->code=code_copia1;
		flight_array[i]->dep=flight_array[i-1]->dep;
		flight_array[i]->arr=flight_array[i-1]->arr;
		flight_array[i]->dep_t=flight_array[i-1]->dep_t-5;
		flight_array[i]->arr_t=flight_array[i-1]->arr_t-5;
		flight_array[i]->copies[0]=flight_array[i-1]->code;
		flight_array[i]->copies[1]=code_copia2;
		flight_array[i]->is_earliest_copy=1;
		flight_array[i]->is_latest_copy=0;
		flight_array[i]->is_original_flight=0;
		i++;
		flight_array[i]->code=code_copia2;
		flight_array[i]->dep=flight_array[i-2]->dep;
		flight_array[i]->arr=flight_array[i-2]->arr;
		flight_array[i]->dep_t=flight_array[i-2]->dep_t+5;
		flight_array[i]->arr_t=flight_array[i-2]->arr_t+5;
		flight_array[i]->copies[0]=flight_array[i-2]->code;
		flight_array[i]->copies[1]=code_copia1;
		flight_array[i]->is_earliest_copy=0;
		flight_array[i]->is_latest_copy=1;
		flight_array[i]->is_original_flight=0;
		i++;
#endif
#endif
	}

	fclose(input_file);


#ifdef RETIME
	//flights need to be ordered by dep time
	flight_array_ord=(int*) calloc(n_of_flights,sizeof(int));
	if(flight_array_ord==NULL) alloc_error("flight_array_ord\n");
	for(i=0;i<n_of_flights;i++) {
		flight_array_ord[i]=i;
	}
        qsort((void *) flight_array_ord,(size_t) n_of_flights, sizeof(int),
		(int(*) (const void *,const void *)) cmp_dep_times);

	flight_array_temp=(flight**) calloc(n_of_flights,sizeof(flight*));
	if(flight_array_temp==NULL) alloc_error("flight_array_temp\n");

	for(i=0;i<n_of_flights;i++) {
		flight_array_temp[i]=(flight*) calloc(1,sizeof(flight));
		if(flight_array_temp[i]==NULL) alloc_error("flight_array_temp[i]\n");
		flight_array_temp[i]->copies=(int*) calloc(RETIME_INTERV-1,sizeof(int));
		if(flight_array_temp[i]->copies==NULL) alloc_error("flight_array_temp[i]->copies\n");
	}

	for(i=0;i<n_of_flights;i++) {
		flight_array_temp[i]->dep=flight_array[flight_array_ord[i]]->dep;
		flight_array_temp[i]->arr=flight_array[flight_array_ord[i]]->arr;
		flight_array_temp[i]->dep_t=flight_array[flight_array_ord[i]]->dep_t;
		flight_array_temp[i]->arr_t=flight_array[flight_array_ord[i]]->arr_t;
		flight_array_temp[i]->code=flight_array[flight_array_ord[i]]->code;
		flight_array_temp[i]->is_earliest_copy=flight_array[flight_array_ord[i]]->is_earliest_copy;
		flight_array_temp[i]->is_latest_copy=flight_array[flight_array_ord[i]]->is_latest_copy;
		flight_array_temp[i]->is_original_flight=flight_array[flight_array_ord[i]]->is_original_flight;
		for(j=0;j<RETIME_INTERV-1;j++) {
			flight_array_temp[i]->copies[j]=flight_array[flight_array_ord[i]]->copies[j];
		}
	}
	for(i=0;i<n_of_flights;i++) {
		flight_array[i]->dep=flight_array_temp[i]->dep;
		flight_array[i]->arr=flight_array_temp[i]->arr;
		flight_array[i]->dep_t=flight_array_temp[i]->dep_t;
		flight_array[i]->arr_t=flight_array_temp[i]->arr_t;
		flight_array[i]->code=flight_array_temp[i]->code;
		flight_array[i]->is_earliest_copy=flight_array_temp[i]->is_earliest_copy;
		flight_array[i]->is_latest_copy=flight_array_temp[i]->is_latest_copy;
		flight_array[i]->is_original_flight=flight_array_temp[i]->is_original_flight;
		for(j=0;j<RETIME_INTERV-1;j++) {
			flight_array[i]->copies[j]=flight_array_temp[i]->copies[j];
		}
	}
	for(i=0;i<n_of_flights;i++) {
		free(flight_array_temp[i]->copies);
		free(flight_array_temp[i]);
	}
	free(flight_array_temp);
	//memorizza per ogni volo l'indice rispetto ai voli veri (scartando le copie): serve per i vincoli
	index=0;
	for(i=0;i<n_of_flights;i++) {
		found=FALSE;
		for(j=0;j<i;j++) {
			if(are_copies(i,j)) {
				found=TRUE;
				flight_array[i]->its_real_flight_index=flight_array[j]->its_real_flight_index;
			}
		}
		if(!found) {
			flight_array[i]->its_real_flight_index=index;
			index++;
		}		
	}
#endif

	/*printf("max code %d\n",max_code);
	for(i=0;i<n_of_flights;i++) {
		printf("flight %d code %d %s %d %s %d\n", i, flight_array[i]->code, airports[flight_array[i]->dep],
			flight_array[i]->dep_t, airports[flight_array[i]->arr],
			flight_array[i]->arr_t);
#ifdef RETIME
		printf("copies:\n");
		for(j=0;j<RETIME_INTERV-1;j++) {
			printf("code %d\n",flight_array[i]->copies[j]);
		}
#endif
	}
	exit(0);*/
	
}

#ifdef RETIME
int cmp_dep_times(n1,n2)
int* n1;
int* n2;
{
	int t1,t2;

	t1=flight_array[(*n1)]->dep_t;
	t2=flight_array[(*n2)]->dep_t;
	if(t1<=t2) return (-1);
    else return (1);
}
#endif

void read_duration_table(char* name_duration_file)
{
	FILE *input_file;
	char buf[DIM];
	char app[DIM];
	int iora, imin, fora, fmin;
	int err;
	int i;
	int ora, min;
	int dur;
	int n_flights;
	int k;
	int sora, smin, eora, emin;

	input_file=fopen(name_duration_file, "r");
	if(input_file==NULL) stop_error("main cannot open duration file\n");

	n_of_intervals=0;

	iora=0;
	imin=0;
	fora=0;
	fmin=0;
	while(fscanf(input_file,"%d:%d %d:%d",&sora,&smin,&eora,&emin)!=EOF) {
		if(sora!=iora || smin!=imin || eora!=fora || emin!=fmin) {
			n_of_intervals++; 
			iora=sora;
			imin=smin;
			fora=eora;
			fmin=emin;
		}
		fgets(buf,DIM,input_file);
	}

	intervals=(interval*) calloc(n_of_intervals,sizeof(interval));
	if(intervals==NULL) alloc_error("intervals\n");

	//initializing duration table
	duration_table=(int**) calloc(n_of_intervals,sizeof(int*));
	if(duration_table==NULL) alloc_error("duration_table\n");

	for(i=0;i<n_of_intervals;i++) {
		duration_table[i]=(int*) calloc(8,sizeof(int)); //8 max number of flights
		if(duration_table[i]==NULL) alloc_error("duration_table[i]\n");
	}


	err=fseek(input_file,0,0);

	if(err<0)
	{
		printf("Error in reading input file");
		exit(-1);
	}

	i=0;
	iora=0;
	imin=0;
	fora=0;
	fmin=0;
	while(fscanf(input_file,"%d:%d %d:%d",&sora,&smin,&eora,&emin)!=EOF) {
		if(sora!=iora || smin!=imin || eora!=fora || emin!=fmin) {
			iora=sora;
			imin=smin;
			fora=eora;
			fmin=emin;
			intervals[i].start=sora*60+smin;
			intervals[i].end=eora*60+emin;
			i++;
		}
		fscanf(input_file,"%d ",&n_flights);
		fscanf(input_file,"%d:%d\n",&ora,&min);
		dur=ora*60+min;
		duration_table[i-1][n_flights-1]=dur;
	}

	//initializing the empty entries for the case of 0 or 1 flights
	for(i=0;i<n_of_intervals;i++) {
		for(k=7;k>=0;k--) {
			if(duration_table[i][7]==0) {
				stop_error("duration table non defined for the case of 8 flights\n");
			}
			if(duration_table[i][k]==0) {
				duration_table[i][k]=duration_table[i][k+1];
			}
		}
	}

	fclose(input_file);
}


void read_config_file(char* name_config_file) 
{
	FILE *input_file;
	char app[DIM];
	int iapp;
	int i,j;
	char c1,c2;
	int company,dep_airport;
	int numberSPC,numberACE;
	char day[DIM];
	char name_aviones_file[DIM];
	char name_mantenimiento_file[DIM];
	FILE* aviones_file;
	FILE* mantenimiento_file;
	char aircr[30];
	char aircrm[30];
	char begin[30];
	char end[30];
	int beg, en, da;
	char comp[5];
	

	input_file=fopen(name_config_file, "r");
	if(input_file==NULL) stop_error("main cannot open config file\n");

	name_input_file=(char*) calloc(300,sizeof(char));
	if(name_input_file==NULL) alloc_error("name_input_file\n");
	name_duration_file=(char*) calloc(300,sizeof(char));
	if(name_duration_file==NULL) alloc_error("name_duration_file\n");
	name_solution_file=(char*) calloc(300,sizeof(char));
	if(name_solution_file==NULL) alloc_error("name_solution_file\n");

	
	fscanf(input_file,"%s %s\n",app,app);
	strcpy(our_day,app);
	j=0;
	for(i=0;i<(signed)strlen(app);i++) {
		if(app[i]=='/') continue;
		day[j]=app[i];
		j++;
	}
	day[j]='_';
	j++;
	day[j]='\0';

	for(i=0;i<6;i++) {
		fscanf(input_file,"<%c%c> %d\n",&c1,&c2,&iapp);
		if(c2=='B') {
			company=0;
		}
		else if(c2=='N') {
			company=1;
		}
		else if(c2=='C') {
			company=2;
		}
		if(c1=='T') {
			dep_airport=1;
		}
		else if(c1=='L') {
			dep_airport=0;
		}
		for(j=0;j<n_of_aircraft_types;j++) {
			if(aircrafts[j].company==company && aircrafts[j].dep_airport==dep_airport) {
				aircrafts[j].n_of_aircrafts=iapp;
				break;
			}
		}
	}

	numberSPC=0;
	fscanf(input_file,"<amanece%s %d\n",app,&iapp);
	numberSPC=numberSPC+iapp;
	fscanf(input_file,"<amanece%s %d\n",app,&iapp);
	numberSPC=numberSPC+iapp;
	


	numberACE=0;

	fscanf(input_file,"<amanece%s %d\n",app,&iapp);
	numberACE=numberACE+iapp;
	fscanf(input_file,"<amanece%s %d\n",app,&iapp);
	numberACE=numberACE+iapp;
	

	for(i=0;i<4;i++) {
		fscanf(input_file,"<duerme%s %d\n",app,&iapp);
	}

	fscanf(input_file,"<pernoctaACEoperador> %s\n",app);
	if(strcmp(app,"-")==0 || strcmp(app,"N")==0) {
		company=1; //N
	}
	else if(strcmp(app,"B")==0) {
		company=0;
	}   
	else if(strcmp(app,"C")==0) {
		company=2;
	}

	for(i=0;i<n_of_aircraft_types;i++) {
		if(aircrafts[i].company==company && aircrafts[i].dep_airport==7) {
			aircrafts[i].n_of_aircrafts=numberACE;
		}
		
		if(aircrafts[i].company==company && aircrafts[i].dep_airport==0) {
			aircrafts[i].n_of_aircrafts=aircrafts[i].n_of_aircrafts-numberACE;
		}
		
	}


	fscanf(input_file,"<pernoctaSPCoperador> %s\n",app);
	if(strcmp(app,"-")==0 || strcmp(app,"N")==0) {
		company=1; //N
	}
	else if(strcmp(app,"B")==0) {
		company=0;
	}   
	else if(strcmp(app,"C")==0) {
		company=2;
	}

	for(i=0;i<n_of_aircraft_types;i++) {
		
		if(aircrafts[i].company==company && aircrafts[i].dep_airport==3) {
			aircrafts[i].n_of_aircrafts=numberSPC;
		}
		
		if(aircrafts[i].company==company && aircrafts[i].dep_airport==1) {
			aircrafts[i].n_of_aircrafts=aircrafts[i].n_of_aircrafts-numberSPC;
		}
	}


	//reading the name of the input file which is the month and must be combined with the day
	fscanf(input_file,"%s %s\n",app,app);
	strcpy(name_input_file,day);
	strcat(name_input_file,app);

	//reading the name of the solution file
	fscanf(input_file,"%s %s\n",app,app);
	strcpy(name_solution_file,day);
	strcat(name_solution_file,app);
	strcat(name_solution_file,".txt");
	
	//skipping lines
	fscanf(input_file,"%s %s\n",app,app);

	//reading the name of the duration file
	fscanf(input_file,"%s %s\n",app,name_duration_file);

	//reading the names of the aviones and mantenimiento files
	fscanf(input_file,"%s %s\n",app,name_aviones_file);
	fscanf(input_file,"%s %s\n",app,name_mantenimiento_file);

	//checking which aircrafts are under maintenance
	aviones_file=fopen(name_aviones_file, "r");
	if(aviones_file==NULL) stop_error("main cannot open aviones file\n");
	mantenimiento_file=fopen(name_mantenimiento_file, "r");
	if(mantenimiento_file==NULL) stop_error("main cannot open mantenimiento file\n");


	while(fscanf(mantenimiento_file,"%s %s %s",aircrm,begin,end)!=EOF) {

        comp[2]='\0';

        comp[0]=begin[0]; comp[1]=begin[1];
        beg=atoi(comp);

        comp[0]=end[0];   comp[1]=end[1];
        en = atoi(comp);

        comp[0]=day[0];   comp[1]=day[1];
        da = atoi(comp);

		if(da < beg || da > en) continue;

		while(fscanf(aviones_file,"%s %s\n",aircr,comp)!=EOF) {
			if(strcmp(aircrm,aircr)==0) {
				if(strcmp(comp,"B")==0) {
					aircrafts[3].n_of_aircrafts--; //aerei che partono da L di B
				}
				else if(strcmp(comp,"N")==0) {
					aircrafts[4].n_of_aircrafts--; //aerei che partono da L di N
				}
				else if(strcmp(comp,"C")==0) {
					aircrafts[5].n_of_aircrafts--; //aerei che partono da L di C
				}
			}
		}
	}
	

	fclose(aviones_file);
	fclose(mantenimiento_file);

	//skipping lines
	fscanf(input_file,"%s %s\n",app,app);
	fscanf(input_file,"%s %s\n",app,app);
	fscanf(input_file,"%s %s\n",app,app);
	fscanf(input_file,"%s %s\n",app,app);
	fscanf(input_file,"%s %s\n",app,app);
	fscanf(input_file,"%s %s\n",app,app);
	fscanf(input_file,"%s %s\n",app,app);
	fscanf(input_file,"%s %s\n",app,app);

	//reading penalties for the waiting time of the crew
	/*PENAL1 2 //attesa tra 20' e 30'
	PENAL2 1 //attesa tra 30' e 60'
	PENAL3 3 //attesa tra 60' e 120'
	PENAL4 5 //attesa tra 120' e 180'*/
	fscanf(input_file,"%s %s\n",app,app);
	PENAL1=atoi(app);
	fscanf(input_file,"%s %s\n",app,app);
	PENAL2=atoi(app);
	fscanf(input_file,"%s %s\n",app,app);
	PENAL3=atoi(app);
	fscanf(input_file,"%s %s\n",app,app);
	PENAL4=atoi(app);

	//reading the corresponding time intervals for the penalties
	fscanf(input_file,"%s %s\n",app,app);
	TIME1=atoi(app);
	fscanf(input_file,"%s %s\n",app,app);
	TIME2=atoi(app);
	fscanf(input_file,"%s %s\n",app,app);
	TIME3=atoi(app);
	fscanf(input_file,"%s %s\n",app,app);
	TIME4=atoi(app);
	
#ifdef MAGGIO
	TIME4=240;
#endif

    //reading the maximum number of aircrafts doing TT or LL
	/*MAX_TT 2 //max number of aircrafts that depart in T and arrive in T
    MAX_LL 2 //max number of aircrafts that depart in L and arrive in L*/
	fscanf(input_file,"%s %s\n",app,app);
	MAX_TT=atoi(app);
	//reading the cost
	fscanf(input_file,"%s %d\n",app,&y_cost_TT);
	y_cost_TT=y_cost_TT*INCR_COST;
	fscanf(input_file,"%s %s\n",app,app);
	MAX_LL=atoi(app);

	//reading the cost
	fscanf(input_file,"%s %d\n",app,&y_cost_LL);
	y_cost_LL=y_cost_LL*INCR_COST;
	fscanf(input_file,"%s %d\n",app,&z_cost);
	z_cost=z_cost*INCR_COST;

	fscanf(input_file,"%s %d\n",app,&y_cost_T);
	y_cost_T=y_cost_T*INCR_COST;
	fscanf(input_file,"%s %d\n",app,&y_cost_L);
	y_cost_L=y_cost_L*INCR_COST;

	//skipping lines
	fscanf(input_file,"%s %s\n",app,app);
	fscanf(input_file,"%s %s\n",app,app);
	fscanf(input_file,"%s %s\n",app,app);
	fscanf(input_file,"%s %s\n",app,app);
	fscanf(input_file,"%s %s\n",app,app);
	fscanf(input_file,"%s %s\n",app,app);
	fscanf(input_file,"%s %s\n",app,app);
	fscanf(input_file,"%s %s\n",app,app);
	fscanf(input_file,"%s %s\n",app,app);
	fscanf(input_file,"%s %s\n",app,app);

	//reading cost
	fscanf(input_file,"%s %d\n",app,&x_cost_B);
	x_cost_B=x_cost_B*INCR_COST;
	fscanf(input_file,"%s %d\n",app,&x_cost_N);
	x_cost_N=x_cost_N*INCR_COST;
	fscanf(input_file,"%s %d\n",app,&x_cost_C);
	x_cost_C=x_cost_C*INCR_COST;

	//skipping lines
	fscanf(input_file,"%s %s\n",app,app);
	fscanf(input_file,"%s %s\n",app,app);
	fscanf(input_file,"%s %s\n",app,app);

	//reading cost
	fscanf(input_file,"%s %d\n",app,&x_cost_pernocta_N);
	x_cost_pernocta_N=x_cost_pernocta_N*INCR_COST;
	fscanf(input_file,"%s %d\n",app,&y_cost_intern_N);
	y_cost_intern_N=y_cost_intern_N*INCR_COST;
	fscanf(input_file,"%s %s\n",app,app);
	fscanf(input_file,"%s %d\n",app,&x_cost_pernocta_C);
	x_cost_pernocta_C=x_cost_pernocta_C*INCR_COST;
	fscanf(input_file,"%s %d\n",app,&y_cost_intern_C);
	y_cost_intern_C=y_cost_intern_C*INCR_COST;
	

	y_cost_T=1000;
	y_cost_L=1000;
	y_cost_TT=1000;
	y_cost_LL=1000;
	
#ifdef COST_JJ
	y_cost_T=10;
	y_cost_L=10;
	y_cost_TT=10;
	y_cost_LL=10;
#endif

	/*y_cost_T=0;
	y_cost_L=0;
	y_cost_TT=0;
	y_cost_LL=0;*/

	x_cost_B=1000;
	x_cost_N=1000;
	x_cost_C=1000;
	
#ifdef COST_JJ
	x_cost_B=1000;
	x_cost_N=950;
	x_cost_C=910;
#endif

	/*x_cost_B=0;
	x_cost_N=0;
	x_cost_C=0;*/


	x_cost_pernocta_N=0;
	x_cost_pernocta_C=0;
	y_cost_intern_N=0;
	y_cost_intern_C=0;
	//z_cost=1000;
	//z_cost=100;
	z_cost=10;
	
#ifdef COST_JJ
	z_cost=100;
#endif
	
#ifdef COST_0_CHANGES
	z_cost=0;
#endif

#ifdef ONLY_PENAL_COST
	x_cost_B=0;
	x_cost_N=0;
	x_cost_C=0;
	y_cost_T=0;
	y_cost_L=0;
	y_cost_TT=0;
	y_cost_LL=0;
	//T 
	aircrafts[0].dep_airport=1;
	aircrafts[0].company=0; //B
	aircrafts[0].n_of_aircrafts=0;

	//T
	aircrafts[1].dep_airport=1;
	aircrafts[1].company=1; //N
	aircrafts[1].n_of_aircrafts=2;

	//T
	aircrafts[2].dep_airport=1;
	aircrafts[2].company=2; //C
	aircrafts[2].n_of_aircrafts=3;

	//L
	aircrafts[3].dep_airport=0;
	aircrafts[3].company=0; //B
	aircrafts[3].n_of_aircrafts=0;

	//L
	aircrafts[4].dep_airport=0;
	aircrafts[4].company=1; //N
	aircrafts[4].n_of_aircrafts=3;

	//L
	aircrafts[5].dep_airport=0;
	aircrafts[5].company=2; //C
	aircrafts[5].n_of_aircrafts=2;

	//A
	aircrafts[6].dep_airport=7;
	aircrafts[6].company=1; //N
	aircrafts[6].n_of_aircrafts=1;

	//A
	aircrafts[7].dep_airport=7;
	aircrafts[7].company=2; //C
	aircrafts[7].n_of_aircrafts=0;

	//S
	aircrafts[8].dep_airport=3;
	aircrafts[8].company=1; //N
	aircrafts[8].n_of_aircrafts=1;

	//S
	aircrafts[9].dep_airport=3;
	aircrafts[9].company=2; //C
	aircrafts[9].n_of_aircrafts=0;
#endif
	

	/*PENAL1=0;
	PENAL2=0;
	PENAL3=0;
	PENAL4=0;*/

	//MAX_TT=0;
	//MAX_TT=1;
	//MAX_LL=0;
	//MAX_LL=1;


	fclose(input_file);

	/*for(i=0;i<n_of_flights;i++) {
		printf("flight %d %s %d %s %d\n", i, airports[flight_array[i]->dep],
			flight_array[i]->dep_t, airports[flight_array[i]->arr],
			flight_array[i]->arr_t);
	}*/
	
}


int find_airport(char* name_airport)
{
	int i;

	
	for(i=0;i<N_OF_AIRPORTS;i++) {
		if(strcmp(airports[i],name_airport)==0) {
			return i;
		}
	}
	return -1;
}

void build_crew_graph()
{
	int i,j;
	int dep_airport, arr_airport;
	int dep_time, arr_time;

int n_of_short_connections;
int n_of_restricted_connections;
int n_of_other_connections;
		
	n_of_arcs=0;

n_of_short_connections=0;
n_of_restricted_connections=0;
n_of_other_connections=0;

	successori=(succ**) calloc(n_of_flights,sizeof(succ*));
	if(successori==NULL) alloc_error("successori\n");

	for(i=0;i<n_of_flights;i++) {
		//unknown number of successors for each flight before computing it
		successori[i]=(succ*) calloc(n_of_flights,sizeof(succ));
		if(successori[i]==NULL) alloc_error("successori[i]\n");
		successori[i]->ind_succ=(int*) calloc(n_of_flights,sizeof(int));
		if(successori[i]->ind_succ==NULL) alloc_error("successori[i]->ind_succ\n");
		successori[i]->arcs=(int*) calloc(n_of_flights,sizeof(int));
		if(successori[i]->arcs==NULL) alloc_error("successori[i]->arcs\n");
		successori[i]->x_values=(double*) calloc(n_of_flights,sizeof(double));
		if(successori[i]->x_values==NULL) alloc_error("successori[i]->x_values\n");
		successori[i]->forb=(int*) calloc(n_of_flights,sizeof(int));
		if(successori[i]->forb==NULL) alloc_error("successori[i]->forb\n");
		successori[i]->n_of_succ=0;
	}


	predecessori=(pred**) calloc(n_of_flights,sizeof(pred*));
	if(predecessori==NULL) alloc_error("predecessori\n");

	for(i=0;i<n_of_flights;i++) {
		//unknown number of predessors for each flight before computing it
		predecessori[i]=(pred*) calloc(n_of_flights,sizeof(pred));
		if(predecessori[i]==NULL) alloc_error("predecessori[i]\n");
		predecessori[i]->ind_pred=(int*) calloc(n_of_flights,sizeof(int));
		if(predecessori[i]->ind_pred==NULL) alloc_error("predecessori[i]->ind_pred\n");
		predecessori[i]->arcs=(int*) calloc(n_of_flights,sizeof(int));
		if(predecessori[i]->arcs==NULL) alloc_error("predecessori[i]->arcs\n");
		predecessori[i]->n_of_pred=0;
	}

	for(i=0;i<n_of_flights;i++) {
		arr_airport=flight_array[i]->arr;
		arr_time=flight_array[i]->arr_t;
		for(j=0;j<n_of_flights;j++) {
			if(i==j) continue;
			dep_airport=flight_array[j]->dep;
			dep_time=flight_array[j]->dep_t;
			if(arr_airport!=dep_airport) continue;
			if(dep_time<arr_time+20) continue;
if(dep_time-arr_time>=20 && dep_time-arr_time<30) {
	n_of_short_connections++;
}
else if(dep_time-arr_time>=30 && dep_time-arr_time<=3*60) {
	if(arr_airport==0 || arr_airport==1) {
		n_of_restricted_connections++;
	}
	else {
		n_of_other_connections++;
	}
}
else { n_of_other_connections++;
//printf("%s %d - %s %d diff=%d\n",airports[arr_airport],arr_time,airports[dep_airport],dep_time,dep_time-arr_time);
}

#ifndef MAGGIO
			if(dep_time>arr_time+3*60) continue;
#else
			if(dep_time>arr_time+4*60) continue;
#endif
			successori[i]->ind_succ[successori[i]->n_of_succ]=j;
			successori[i]->arcs[successori[i]->n_of_succ]=n_of_arcs;
			successori[i]->n_of_succ++;
			predecessori[j]->ind_pred[predecessori[j]->n_of_pred]=i;
			predecessori[j]->arcs[predecessori[j]->n_of_pred]=n_of_arcs;
			predecessori[j]->n_of_pred++;
			n_of_arcs++;
		
		}
			
	}

printf("number of arcs in the crew graph %d\n",n_of_arcs);
printf("number of short connections %d\n",n_of_short_connections);
printf("number of restricted connections %d\n",n_of_restricted_connections);
printf("number of other connections %d\n",n_of_other_connections);
printf("total number of connections %d\n",n_of_short_connections+n_of_restricted_connections+n_of_other_connections);



	/*for(i=0;i<n_of_flights;i++) {
		printf("flight code %d %s %d successori:\n",flight_array[i]->code,
			airports[flight_array[i]->arr],flight_array[i]->arr_t);
		for(j=0;j<successori[i]->n_of_succ;j++) {
			printf("%s %d arc %d\n",airports[flight_array[successori[i]->ind_succ[j]]->dep],flight_array[successori[i]->ind_succ[j]]->dep_t,
				successori[i]->arcs[j]);
			if(flight_array[successori[i]->ind_succ[j]]->dep!=flight_array[i]->arr) {
				stop_error("different airport\n");
			}
		}
	}
	
	for(i=0;i<n_of_flights;i++) {
		printf("flight code %d %s %d predecessori:\n",flight_array[i]->code,
			airports[flight_array[i]->dep],flight_array[i]->dep_t);
		for(j=0;j<predecessori[i]->n_of_pred;j++) {
			printf("%s %d arc %d\n",airports[flight_array[predecessori[i]->ind_pred[j]]->arr],flight_array[predecessori[i]->ind_pred[j]]->arr_t,
				predecessori[i]->arcs[j]);
			if(flight_array[predecessori[i]->ind_pred[j]]->arr!=flight_array[i]->dep) {
				stop_error("different airport\n");
			}
		}
	}
	exit(0);*/
}


void build_aircraft_graph()
{
	int i,j;
	int dep_airport, arr_airport;
	int dep_time, arr_time;
#ifdef CALCOLA_MIN_CAMBI
	int cont_TFNd, cont_LPAd, cont_ACEd, cont_SPCd, cont_TFNa, cont_LPAa, cont_ACEa, cont_SPCa;
	int* partenze_TFN;
	int sum_dep_TFN, max_dep_TFN, max_i_TFN;
	int* partenze_LPA;
	int sum_dep_LPA, max_dep_LPA, max_i_LPA;
	int* arrivi_TFN;
	int sum_arr_TFN, max_arr_TFN, max_ia_TFN;
	int* arrivi_LPA;
	int sum_arr_LPA, max_arr_LPA, max_ia_LPA;
#ifdef RETIME
	int l,c;
	int found;
	int* flights_TFNd;
	int* flights_LPAd;
	int* flights_TFNa;
	int* flights_LPAa;
	int* flights_ACEd;
	int* flights_SPCd;
	int* flights_SPCa;
	int* flights_ACEa;
#endif
#endif

		
	n_of_arcs_a=0;

	successori_a=(succ**) calloc(n_of_flights,sizeof(succ*));
	if(successori_a==NULL) alloc_error("successori_a\n");

	for(i=0;i<n_of_flights;i++) {
		//unknown number of successors for each flight before computing it
		successori_a[i]=(succ*) calloc(n_of_flights,sizeof(succ));
		if(successori_a[i]==NULL) alloc_error("successori_a[i]\n");
		successori_a[i]->ind_succ=(int*) calloc(n_of_flights,sizeof(int));
		if(successori_a[i]->ind_succ==NULL) alloc_error("successori_a[i]->ind_succ\n");
		successori_a[i]->arcs=(int*) calloc(n_of_flights,sizeof(int));
		if(successori_a[i]->arcs==NULL) alloc_error("successori_a[i]->arcs\n");
		successori_a[i]->x_values=(double*) calloc(n_of_flights,sizeof(double));
		if(successori_a[i]->x_values==NULL) alloc_error("successori_a[i]->x_values\n");
		successori_a[i]->forb=(int*) calloc(n_of_flights,sizeof(int));
		if(successori_a[i]->forb==NULL) alloc_error("successori_a[i]->forb\n");
		successori_a[i]->n_of_succ=0;
	}


	predecessori_a=(pred**) calloc(n_of_flights,sizeof(pred*));
	if(predecessori_a==NULL) alloc_error("predecessori_a\n");

	for(i=0;i<n_of_flights;i++) {
		//unknown number of predessors for each flight before computing it
		predecessori_a[i]=(pred*) calloc(n_of_flights,sizeof(pred));
		if(predecessori_a[i]==NULL) alloc_error("predecessori_a[i]\n");
		predecessori_a[i]->ind_pred=(int*) calloc(n_of_flights,sizeof(int));
		if(predecessori_a[i]->ind_pred==NULL) alloc_error("predecessori_a[i]->ind_pred\n");
		predecessori_a[i]->arcs=(int*) calloc(n_of_flights,sizeof(int));
		if(predecessori_a[i]->arcs==NULL) alloc_error("predecessori_a[i]->arcs\n");
		predecessori_a[i]->n_of_pred=0;
	}

	for(i=0;i<n_of_flights;i++) {
		arr_airport=flight_array[i]->arr;
		arr_time=flight_array[i]->arr_t;
		for(j=0;j<n_of_flights;j++) {
			if(i==j) continue;
			dep_airport=flight_array[j]->dep;
			dep_time=flight_array[j]->dep_t;
			if(arr_airport!=dep_airport) continue;
			if(dep_time<arr_time+20) continue;
			//if(dep_time>arr_time+3*60) continue;

			successori_a[i]->ind_succ[successori_a[i]->n_of_succ]=j;
			successori_a[i]->arcs[successori_a[i]->n_of_succ]=n_of_arcs_a;
			successori_a[i]->n_of_succ++;
			predecessori_a[j]->ind_pred[predecessori_a[j]->n_of_pred]=i;
			predecessori_a[j]->arcs[predecessori_a[j]->n_of_pred]=n_of_arcs_a;
			predecessori_a[j]->n_of_pred++;
			n_of_arcs_a++;
		
		}
			
	}

	
	printf("number of arcs in the aircraft graph %d\n",n_of_arcs_a);


	/*for(i=0;i<n_of_flights;i++) {
		printf("flight code %d %s %d successori:\n",flight_array[i]->code,
			airports[flight_array[i]->arr],flight_array[i]->arr_t);
		for(j=0;j<successori_a[i]->n_of_succ;j++) {
			printf("%s %d arc %d\n",airports[flight_array[successori_a[i]->ind_succ[j]]->dep],flight_array[successori_a[i]->ind_succ[j]]->dep_t,
				successori_a[i]->arcs[j]);
			if(flight_array[successori_a[i]->ind_succ[j]]->dep!=flight_array[i]->arr) {
				stop_error("different airport\n");
			}
		}
	}
	
	for(i=0;i<n_of_flights;i++) {
		printf("flight code %d %s %d predecessori:\n",flight_array[i]->code,
			airports[flight_array[i]->dep],flight_array[i]->dep_t);
		for(j=0;j<predecessori_a[i]->n_of_pred;j++) {
			printf("%s %d arc %d\n",airports[flight_array[predecessori_a[i]->ind_pred[j]]->arr],flight_array[predecessori_a[i]->ind_pred[j]]->arr_t,
				predecessori_a[i]->arcs[j]);
			if(flight_array[predecessori_a[i]->ind_pred[j]]->arr!=flight_array[i]->dep) {
				stop_error("different airport\n");
			}
		}
	}
	exit(0);*/



#ifdef CALCOLA_MIN_CAMBI
	//calcolo del numero minimo di cambi
	cont_TFNd=0;
	cont_LPAd=0;
	cont_ACEd=0;
	cont_SPCd=0;
	cont_TFNa=0;
	cont_LPAa=0;
	cont_ACEa=0;
	cont_SPCa=0;

#ifdef RETIME
	flights_TFNd=(int*) calloc(n_of_flights,sizeof(int));
	if(flights_TFNd==NULL) alloc_error("flights_TFNd\n");
	flights_LPAd=(int*) calloc(n_of_flights,sizeof(int));
	if(flights_LPAd==NULL) alloc_error("flights_LPAd\n");
	flights_SPCd=(int*) calloc(n_of_flights,sizeof(int));
	if(flights_SPCd==NULL) alloc_error("flights_SPCd\n");
	flights_ACEd=(int*) calloc(n_of_flights,sizeof(int));
	if(flights_ACEd==NULL) alloc_error("flights_ACEd\n");

	flights_TFNa=(int*) calloc(n_of_flights,sizeof(int));
	if(flights_TFNa==NULL) alloc_error("flights_TFNa\n");
	flights_LPAa=(int*) calloc(n_of_flights,sizeof(int));
	if(flights_LPAa==NULL) alloc_error("flights_LPAa\n");
	flights_SPCa=(int*) calloc(n_of_flights,sizeof(int));
	if(flights_SPCa==NULL) alloc_error("flights_SPCa\n");
	flights_ACEa=(int*) calloc(n_of_flights,sizeof(int));
	if(flights_ACEa==NULL) alloc_error("flights_ACEa\n");
	
	for(i=0;i<n_of_flights;i++) {
		flights_TFNd[i]=-1;
		flights_LPAd[i]=-1;
		flights_SPCd[i]=-1;
		flights_ACEd[i]=-1;
		flights_TFNa[i]=-1;
		flights_LPAa[i]=-1;
		flights_SPCa[i]=-1;
		flights_ACEa[i]=-1;

	}
#endif

	printf("\n\nDEPARTURES\n\n");
	for(i=0;i<n_of_flights;i++) {
		if(flight_array[i]->dep==0 && flight_array[i]->dep_t<=540) {
			if(predecessori_a[i]->n_of_pred<=2) {
				printf("volo %d %s num_pred %d\n",flight_array[i]->code,
				airports[flight_array[i]->dep],predecessori_a[i]->n_of_pred);
				for(j=0;j<predecessori_a[i]->n_of_pred;j++) {
					printf("%s %d\n",airports[flight_array[predecessori_a[i]->ind_pred[j]]->arr],flight_array[predecessori_a[i]->ind_pred[j]]->code);
				}
				printf("\n");
				if(predecessori_a[i]->n_of_pred==0) {
#ifdef RETIME
					found=FALSE;
					for(l=0;l<cont_LPAd;l++) {
						found=are_copies(i,flights_LPAd[l]);
						if(found) break;
					}
					if(!found) {
						flights_LPAd[cont_LPAd]=i;
						cont_LPAd++;
					}
#else
					cont_LPAd++;
#endif
				}
			}
		}
		if(flight_array[i]->dep==1 && flight_array[i]->dep_t<=540) {
			if(predecessori_a[i]->n_of_pred<=2) {
				printf("volo %d %s num_pred %d\n",flight_array[i]->code,
				airports[flight_array[i]->dep],predecessori_a[i]->n_of_pred);
				for(j=0;j<predecessori_a[i]->n_of_pred;j++) {
					printf("%s %d\n",airports[flight_array[predecessori_a[i]->ind_pred[j]]->arr],flight_array[predecessori_a[i]->ind_pred[j]]->code);
				}
				printf("\n");
				if(predecessori_a[i]->n_of_pred==0) {
#ifdef RETIME
					found=FALSE;
					for(l=0;l<cont_TFNd;l++) {
						found=are_copies(i,flights_TFNd[l]);
						if(found) break;
					}
					if(!found) {
						flights_TFNd[cont_TFNd]=i;
						cont_TFNd++;
					}
#else
					cont_TFNd++;
#endif
				}

			}
		}
		if(flight_array[i]->dep==3 && flight_array[i]->dep_t<=540) {
			if(predecessori_a[i]->n_of_pred<=2) {
				printf("volo %d %s num_pred %d\n",flight_array[i]->code,
				airports[flight_array[i]->dep],predecessori_a[i]->n_of_pred);
				for(j=0;j<predecessori_a[i]->n_of_pred;j++) {
					printf("%s %d\n",airports[flight_array[predecessori_a[i]->ind_pred[j]]->arr],flight_array[predecessori_a[i]->ind_pred[j]]->code);
				}
				printf("\n");
				if(predecessori_a[i]->n_of_pred==0) {
#ifdef RETIME
					found=FALSE;
					for(l=0;l<cont_SPCd;l++) {
						found=are_copies(i,flights_SPCd[l]);
						if(found) break;
					}
					if(!found) {
						flights_SPCd[cont_SPCd]=i;
						cont_SPCd++;
					}
#else
					cont_SPCd++;
#endif
				}

			}
		}
		if(flight_array[i]->dep==7 && flight_array[i]->dep_t<=540) {
			if(predecessori_a[i]->n_of_pred<=2) {
				printf("volo %d %s num_pred %d\n",flight_array[i]->code,
				airports[flight_array[i]->dep],predecessori_a[i]->n_of_pred);
				for(j=0;j<predecessori_a[i]->n_of_pred;j++) {
					printf("%s %d\n",airports[flight_array[predecessori_a[i]->ind_pred[j]]->arr],flight_array[predecessori_a[i]->ind_pred[j]]->code);
				}
				printf("\n");
				if(predecessori_a[i]->n_of_pred==0) {
#ifdef RETIME
					found=FALSE;
					for(l=0;l<cont_ACEd;l++) {
						found=are_copies(i,flights_ACEd[l]);
						if(found) break;
					}
					if(!found) {
						flights_ACEd[cont_ACEd]=i;
						cont_ACEd++;
					}
#else
					cont_ACEd++;
#endif
				}
			}
			if(flight_array[i]->arr==0 && first_flight_A(i)) {
#ifdef RETIME
					found=FALSE;
					for(l=0;l<cont_ACEd;l++) {
						found=are_copies(i,flights_ACEd[l]);
						if(found) break;
					}
					if(!found) {
						flights_ACEd[cont_ACEd]=i;
						cont_ACEd++;
					}
#else
					cont_ACEd++;
#endif
			}
		}


	}

	printf("\n\nARRIVALS\n\n");
	for(i=0;i<n_of_flights;i++) {
		if(flight_array[i]->arr==0 && flight_array[i]->arr_t>=1080) {
			if(successori_a[i]->n_of_succ<=2) {
				printf("volo %d %s num_succ %d\n",flight_array[i]->code,
				airports[flight_array[i]->arr],successori_a[i]->n_of_succ);
				for(j=0;j<successori_a[i]->n_of_succ;j++) {
					printf("%s %d\n",airports[flight_array[successori_a[i]->ind_succ[j]]->dep],flight_array[successori_a[i]->ind_succ[j]]->code);
				}
				printf("\n");
				if(successori_a[i]->n_of_succ==0) {
#ifdef RETIME
					found=FALSE;
					for(l=0;l<cont_LPAa;l++) {
						found=are_copies(i,flights_LPAa[l]);
						if(found) break;
					}
					if(!found) {
						flights_LPAa[cont_LPAa]=i;
						cont_LPAa++;
					}
#else
					cont_LPAa++;
#endif
				}
			}
		}
		if(flight_array[i]->arr==1 && flight_array[i]->arr_t>=1080) {
			if(successori_a[i]->n_of_succ<=2) {
				printf("volo %d %s num_succ %d\n",flight_array[i]->code,
				airports[flight_array[i]->arr],successori_a[i]->n_of_succ);
				for(j=0;j<successori_a[i]->n_of_succ;j++) {
					printf("%s %d\n",airports[flight_array[successori_a[i]->ind_succ[j]]->dep],flight_array[successori_a[i]->ind_succ[j]]->code);
				}
				printf("\n");
				if(successori_a[i]->n_of_succ==0) {
#ifdef RETIME
					found=FALSE;
					for(l=0;l<cont_TFNa;l++) {
						found=are_copies(i,flights_TFNa[l]);
						if(found) break;
					}
					if(!found) {
						flights_TFNa[cont_TFNa]=i;
						cont_TFNa++;
					}
#else
					cont_TFNa++;
#endif
				}

			}
		}
		if(flight_array[i]->arr==3 && flight_array[i]->arr_t>=1080) {
			if(successori_a[i]->n_of_succ<=2) {
				printf("volo %d %s num_succ %d\n",flight_array[i]->code,
				airports[flight_array[i]->arr],successori_a[i]->n_of_succ);
				for(j=0;j<successori_a[i]->n_of_succ;j++) {
					printf("%s %d\n",airports[flight_array[successori_a[i]->ind_succ[j]]->dep],flight_array[successori_a[i]->ind_succ[j]]->code);
				}
				printf("\n");
				if(successori_a[i]->n_of_succ==0) {
#ifdef RETIME
					found=FALSE;
					for(l=0;l<cont_SPCa;l++) {
						found=are_copies(i,flights_SPCa[l]);
						if(found) break;
					}
					if(!found) {
						flights_SPCa[cont_SPCa]=i;
						cont_SPCa++;
					}
#else
					cont_SPCa++;
#endif
				}

			}
		}
		if(flight_array[i]->arr==7 && flight_array[i]->arr_t>=1080) {
			if(successori_a[i]->n_of_succ<=2) {
				printf("volo %d %s num_succ %d\n",flight_array[i]->code,
				airports[flight_array[i]->arr],successori_a[i]->n_of_succ);
				for(j=0;j<successori_a[i]->n_of_succ;j++) {
					printf("%s %d\n",airports[flight_array[successori_a[i]->ind_succ[j]]->dep],flight_array[successori_a[i]->ind_succ[j]]->code);
				}
				printf("\n");
				if(successori_a[i]->n_of_succ==0) {
#ifdef RETIME
					found=FALSE;
					for(l=0;l<cont_ACEa;l++) {
						found=are_copies(i,flights_ACEa[l]);
						if(found) break;
					}
					if(!found) {
						flights_ACEa[cont_ACEa]=i;
						cont_ACEa++;
					}
#else
					cont_ACEa++;
#endif
				}

			}
			if(flight_array[i]->dep==0 && last_flight_A(i)) {
#ifdef RETIME
					found=FALSE;
					for(l=0;l<cont_ACEa;l++) {
						found=are_copies(i,flights_ACEa[l]);
						if(found) break;
					}
					if(!found) {
						flights_ACEa[cont_ACEa]=i;
						cont_ACEa++;
					}
#else
					cont_ACEa++;
#endif
			}

		}



	}

	printf("cont_LPAd %d cont_TFNd %d cont_SPCd %d cont_ACEd %d\n",
	cont_LPAd, cont_TFNd, cont_SPCd, cont_ACEd);
	printf("cont_LPAa %d cont_TFNa %d cont_SPCa %d cont_ACEa %d\n",
	cont_LPAa, cont_TFNa, cont_SPCa, cont_ACEa);
#ifdef RETIME
	printf("ealry dep from LPA (LPAd):\n");
	for(i=0;i<cont_LPAd;i++) {
		printf("flight %d\n",flight_array[flights_LPAd[i]]->code);
	}
	printf("ealry dep from TFN (TFNd):\n");
	for(i=0;i<cont_TFNd;i++) {
		printf("flight %d\n",flight_array[flights_TFNd[i]]->code);
	}
	printf("ealry dep from SPC (SPCd):\n");
	for(i=0;i<cont_SPCd;i++) {
		printf("flight %d\n",flight_array[flights_SPCd[i]]->code);
	}
	printf("ealry dep from ACE (ACEd):\n");
	for(i=0;i<cont_ACEd;i++) {
		printf("flight %d\n",flight_array[flights_ACEd[i]]->code);
	}

	printf("late arr at LPA (LPAa):\n");
	for(i=0;i<cont_LPAa;i++) {
		printf("flight %d\n",flight_array[flights_LPAa[i]]->code);
	}
	printf("late arr at TFN (TFNa):\n");
	for(i=0;i<cont_TFNa;i++) {
		printf("flight %d\n",flight_array[flights_TFNa[i]]->code);
	}
	printf("late arr at SPC (SPCa):\n");
	for(i=0;i<cont_SPCa;i++) {
		printf("flight %d\n",flight_array[flights_SPCa[i]]->code);
	}
	printf("late arr at ACE (ACEa):\n");
	for(i=0;i<cont_ACEa;i++) {
		printf("flight %d\n",flight_array[flights_ACEa[i]]->code);
	}

	free(flights_TFNd);
	free(flights_LPAd);
	free(flights_SPCd);
	free(flights_ACEd);
	free(flights_TFNa);
	free(flights_LPAa);
	free(flights_SPCa);
	free(flights_ACEa);
	//exit(0);
#endif

	partenze_TFN=(int*) calloc(1440,sizeof(int));
	if(partenze_TFN==NULL) alloc_error("partenze_TFN\n");
	partenze_LPA=(int*) calloc(1440,sizeof(int));
	if(partenze_LPA==NULL) alloc_error("partenze_LPA\n");

	arrivi_TFN=(int*) calloc(1440,sizeof(int));
	if(arrivi_TFN==NULL) alloc_error("arrivi_TFN\n");
	arrivi_LPA=(int*) calloc(1440,sizeof(int));
	if(arrivi_LPA==NULL) alloc_error("arrivi_LPA\n");

	for(i=0;i<n_of_flights;i++) {
		if(flight_array[i]->dep==1) {
#ifdef RETIME
			if(flight_array[i]->is_latest_copy) {
				dep_time=flight_array[i]->dep_t;
				partenze_TFN[dep_time]++;
			}
#else
			dep_time=flight_array[i]->dep_t;
			partenze_TFN[dep_time]++;
#endif
		}
		if(flight_array[i]->arr==1) {
#ifdef RETIME
			if(flight_array[i]->is_earliest_copy) {
				arr_time=flight_array[i]->arr_t;
				partenze_TFN[arr_time+20]--;
			}
#else
			arr_time=flight_array[i]->arr_t;
			partenze_TFN[arr_time+20]--;
#endif
		}
		if(flight_array[i]->dep==0) {
#ifdef RETIME
			if(flight_array[i]->is_latest_copy) {
				dep_time=flight_array[i]->dep_t;
				partenze_LPA[dep_time]++;
			}
#else
			dep_time=flight_array[i]->dep_t;
			partenze_LPA[dep_time]++;
#endif
		}
		if(flight_array[i]->arr==0) {
#ifdef RETIME
			if(flight_array[i]->is_earliest_copy) {
				arr_time=flight_array[i]->arr_t;
				partenze_LPA[arr_time+20]--;
			}
#else
			arr_time=flight_array[i]->arr_t;
			partenze_LPA[arr_time+20]--;
#endif
		}


		if(flight_array[i]->arr==1) {
#ifdef RETIME
			if(flight_array[i]->is_earliest_copy) {
				arr_time=flight_array[i]->arr_t;
				arrivi_TFN[arr_time]++;
			}
#else
			arr_time=flight_array[i]->arr_t;
			arrivi_TFN[arr_time]++;
#endif
		}
		if(flight_array[i]->dep==1) {
#ifdef RETIME
			if(flight_array[i]->is_latest_copy) {
				dep_time=flight_array[i]->dep_t;
				arrivi_TFN[dep_time-20]--;
			}
#else
			dep_time=flight_array[i]->dep_t;
			arrivi_TFN[dep_time-20]--;
#endif
		}
		if(flight_array[i]->arr==0) {
#ifdef RETIME
			if(flight_array[i]->is_earliest_copy) {
				arr_time=flight_array[i]->arr_t;
				arrivi_LPA[arr_time]++;
			}
#else
			arr_time=flight_array[i]->arr_t;
			arrivi_LPA[arr_time]++;
#endif
		}
		if(flight_array[i]->arr==0) {
#ifdef RETIME
			if(flight_array[i]->is_earliest_copy) {
				dep_time=flight_array[i]->dep_t;
				arrivi_LPA[dep_time-20]--;
			}
#else
			dep_time=flight_array[i]->dep_t;
			arrivi_LPA[dep_time-20]--;
#endif
		}


	}
	sum_dep_TFN=0;
	max_dep_TFN=0;
	sum_dep_LPA=0;
	max_dep_LPA=0;

	max_i_TFN=0;
	max_i_LPA=0;

	sum_arr_TFN=0;
	max_arr_TFN=0;
	sum_arr_LPA=0;
	max_arr_LPA=0;

	max_ia_TFN=0;
	max_ia_LPA=0;


	for(i=0;i<1440;i++) {
		if(partenze_TFN[i]!=0) {
			printf("partenze_TFN[%d] %d\n",i,partenze_TFN[i]);
			sum_dep_TFN=sum_dep_TFN+partenze_TFN[i];
			if(sum_dep_TFN>max_dep_TFN) {
				max_dep_TFN=sum_dep_TFN;
				max_i_TFN=i;
			}
		}
		if(partenze_LPA[i]!=0) {
			printf("partenze_LPA[%d] %d\n",i,partenze_LPA[i]);
			sum_dep_LPA=sum_dep_LPA+partenze_LPA[i];
			if(sum_dep_LPA>max_dep_LPA) {
				max_dep_LPA=sum_dep_LPA;
				max_i_LPA=i;
			}
		}
	}

	for(i=1439;i>=0;i--) {

		if(arrivi_TFN[i]!=0) {
			printf("arrivi_TFN[%d] %d\n",i,arrivi_TFN[i]);
			sum_arr_TFN=sum_arr_TFN+arrivi_TFN[i];
			if(sum_arr_TFN>max_arr_TFN) {
				max_arr_TFN=sum_arr_TFN;
				max_ia_TFN=i;
			}
		}
		if(arrivi_LPA[i]!=0) {
			printf("arrivi_LPA[%d] %d\n",i,arrivi_LPA[i]);
			sum_arr_LPA=sum_arr_LPA+arrivi_LPA[i];
			if(sum_arr_LPA>max_arr_LPA) {
				max_arr_LPA=sum_arr_LPA;
				max_ia_LPA=i;
			}
		}


	}
	printf("max_dep_TFN %d max_i_TFN %d\n",max_dep_TFN,max_i_TFN);
	printf("max_dep_LPA %d max_i_LPA %d\n",max_dep_LPA,max_i_LPA);
	printf("max_arr_TFN %d max_ia_TFN %d\n",max_arr_TFN,max_ia_TFN);
	printf("max_arr_LPA %d max_ia_LPA %d\n",max_arr_LPA,max_ia_LPA);


	free(partenze_TFN);
	free(partenze_LPA);
	free(arrivi_TFN);
	free(arrivi_LPA);

	exit(0);

#endif //fine calcola cambi
}

void build_starting_ending_arcs()
{
	int i,j;
	int dep_airport, arr_airport;
	

	n_of_starting_arcs=0;
	n_of_ending_arcs=0;

	starting_arcs=(arc**) calloc(N_MAIN_AIRPORTS,sizeof(arc*));
	if(starting_arcs==NULL) alloc_error("starting_arcs\n");

	ending_arcs=(arc**) calloc(N_MAIN_AIRPORTS,sizeof(arc*));
	if(ending_arcs==NULL) alloc_error("ending_arcs\n");

	

	for(i=0;i<N_MAIN_AIRPORTS;i++) {
		//unknown number of starting arcs from N_MAIN_AIRPORTS before computing it
		starting_arcs[i]=(arc*) calloc(n_of_flights,sizeof(arc));
		if(starting_arcs[i]==NULL) alloc_error("starting_arcs[i]\n");
		starting_arcs[i]->ind_succ=(int*) calloc(n_of_flights,sizeof(int));
		if(starting_arcs[i]->ind_succ==NULL) alloc_error("starting_arcs[i]->ind_succ\n");
		starting_arcs[i]->n_of_succ=0;

		//unknown number of ending arcs to N_MAIN_AIRPORTS before computing it
		ending_arcs[i]=(arc*) calloc(n_of_flights,sizeof(arc));
		if(ending_arcs[i]==NULL) alloc_error("ending_arcs[i]\n");
		ending_arcs[i]->ind_succ=(int*) calloc(n_of_flights,sizeof(int));
		if(ending_arcs[i]->ind_succ==NULL) alloc_error("ending_arcs[i]->ind_succ\n");
		ending_arcs[i]->n_of_succ=0;
	}


	for(i=0;i<n_of_flights;i++) {
		dep_airport=flight_array[i]->dep;
		arr_airport=flight_array[i]->arr;
		for(j=0;j<N_MAIN_AIRPORTS;j++) {
			if(dep_airport==main_airports[j]) {
				//starting arc
				starting_arcs[j]->ind_succ[starting_arcs[j]->n_of_succ]=i;
				starting_arcs[j]->airport=dep_airport;
				starting_arcs[j]->flag=0;
				starting_arcs[j]->n_of_succ++;
				n_of_starting_arcs++;
			}
			if(arr_airport==main_airports[j]) {
				//ending arc
				ending_arcs[j]->ind_succ[ending_arcs[j]->n_of_succ]=i;
				ending_arcs[j]->airport=arr_airport;
				ending_arcs[j]->flag=1;
				ending_arcs[j]->n_of_succ++;
				n_of_ending_arcs++;
			}		
		}
			
	}

printf("number of starting arcs %d\n",n_of_starting_arcs);
printf("number of ending arcs %d\n",n_of_ending_arcs);
printf("total number of arcs in the aircraft graph %d\n",n_of_starting_arcs+n_of_arcs_a+n_of_ending_arcs);
printf("total number of arcs in the crew graph %d\n",n_of_starting_arcs+n_of_arcs+n_of_ending_arcs);
//exit(0);


}

void init_master()
{
	int status;
	int ccnt;
	double* obj;
	double* lb;
	double* ub;
	int i;
	int rcnt;
	int nzcnt;
	double* rhs;
	char* sense;
	int* rmatbeg;
	int* rmatind;
	double* rmatval;
	char** constr_name;
	char** col_name;
	int ind,j,k,pos;
	int arr_airport, dep_airport, company;
	int index,l,count1, count2, count3,h;
	int pos1,pos2;
	int succ;
#ifdef RETIME
	int c;
#endif
	
	
	durata_cplex=0.0;


	env=CPXopenCPLEX(&status);
	if(status) {
		stop_error("cannot open cplex environment\n");
	}

	CPXsetintparam(env, CPX_PARAM_THREADS, 1);
	
	/*status = CPXsetintparam (env, CPX_PARAM_FRACCUTS,-1);
	if(status) {
		stop_error("cannot eliminate Gomory cuts");
	}*/

	lp=CPXcreateprob(env,&status,"voli");
	if(status) {
		stop_error("cannot create problem");
	}
	
	CPXchgobjsen(env,lp,CPX_MIN);

	/*creating auxiliary variables*/
	ccnt=n_of_flights;
#ifdef RETIME
	//consider here only the real flights (no copies)
	ccnt=n_of_flights/RETIME_INTERV;
#endif

#ifdef ITER_MIN_CAMBI
	ccnt=ccnt+2;
#endif

	obj=(double*) calloc(ccnt,sizeof(double));
	if(obj==NULL) alloc_error("obj\n");
	lb=(double*) calloc(ccnt,sizeof(double));
	if(lb==NULL) alloc_error("lb\n");
	ub=(double*) calloc(ccnt,sizeof(double));
	if(ub==NULL) alloc_error("ub\n");
	for(i=0;i<ccnt;i++) {
		obj[i]=(double) 10000;
#ifdef MULTI_OBJ
		obj[i]=(double) 10000;
#endif
		lb[i]=0.0;
		ub[i]=1.0;
	}
	
	status=CPXnewcols(env,lp,ccnt,obj,lb,ub,NULL,NULL);
	if(status) {
		stop_error("CPXnewcols\n");
	}

	free(obj);
	free(lb);
	free(ub);

	/*adding constraints on the maximum number of available crews for each type*/
	rcnt=n_of_crew_types;
	nzcnt=0;
	rhs=(double*) calloc(rcnt,sizeof(double));
	if(rhs==NULL) alloc_error("rhs\n");
	sense=(char*) calloc(rcnt,sizeof(char));
	if(sense==NULL) alloc_error("sense\n");
	for(i=0;i<rcnt;i++) {
		rhs[i]=crews[i].n_of_crews;
		sense[i]='L';
	}
    	rmatbeg=NULL;
	rmatval=NULL;
	rmatind=NULL;

	status=CPXaddrows(env,lp,0,rcnt,nzcnt,rhs,sense,rmatbeg,rmatind,rmatval,NULL,NULL);
	if(status) {
		stop_error("CPXaddrows\n");
	}
	free(rhs);
	free(sense);

	/*adding constraints on the covering of the flights*/
	rcnt=n_of_flights;
#ifdef RETIME
	//consider only the real flights 
	rcnt=n_of_flights/RETIME_INTERV;
#endif
	constr_name=(char**) calloc(rcnt,sizeof(char*));
	if(constr_name==NULL) alloc_error("constr_name\n");
	for(i=0;i<rcnt;i++) {
		constr_name[i]=(char*)calloc(12,sizeof(char));
		if(constr_name[i]==NULL) alloc_error("constr_name[i]\n");
	}
	nzcnt=n_of_flights;
#ifdef RETIME
	//consider only the real flights 
	nzcnt=n_of_flights/RETIME_INTERV;
#endif
	rhs=(double*) calloc(rcnt,sizeof(double));
	if(rhs==NULL) alloc_error("rhs\n");
	sense=(char*) calloc(rcnt,sizeof(char));
	if(sense==NULL) alloc_error("sense\n");
	rmatbeg=(int*) calloc(rcnt,sizeof(int));
	if(rmatbeg==NULL) alloc_error("rmatbeg covering constraints\n");
	rmatind=(int*) calloc(nzcnt,sizeof(int));
	if(rmatind==NULL) alloc_error("rmatind covering constraints\n");
	rmatval=(double*) calloc(nzcnt,sizeof(double));
	if(rmatval==NULL) alloc_error("rmatval covering constraints\n");

#ifndef RETIME	
	for(i=0;i<rcnt;i++) {
		rhs[i]=1.0;
		sense[i]='E';
		rmatbeg[i]=i;
		rmatind[i]=i;
		rmatval[i]=1.0;
		sprintf(constr_name[i],"c%d",flight_array[i]->code);
	}
#endif

#ifdef RETIME
	//per avere i nomi dei vincoli giusti
	c=0;
	for(i=0;i<n_of_flights;i++) {
		if(flight_array[i]->is_earliest_copy) {
			rhs[c]=1.0;
			sense[c]='E';
			rmatbeg[c]=c;
			rmatind[c]=c;
			rmatval[c]=1.0;
			sprintf(constr_name[c],"c%d",flight_array[i]->code);
			c++;
		}
	}
#endif

	status=CPXaddrows(env,lp,0,rcnt,nzcnt,rhs,sense,rmatbeg,rmatind,rmatval,NULL,constr_name);
	if(status) {
		stop_error("CPXaddrows\n");
	}
	free(rhs);
	free(sense);
	free(rmatbeg);
	free(rmatind);
	free(rmatval);
	for(i=0;i<rcnt;i++) {
		free(constr_name[i]);
	}
	free(constr_name);

	//CPXwriteprob(env,lp,"voli.lp",NULL);
	
	
	n_of_cols=0;
	x=NULL;
	offset=n_of_flights;
	n_auxil=n_of_flights;
#ifdef RETIME
	//consider only the real flights 
	offset=n_of_flights/RETIME_INTERV;
	n_auxil=n_of_flights/RETIME_INTERV;
#endif
#ifdef ITER_MIN_CAMBI
	offset=offset+2;
	n_auxil=n_auxil+2;
#endif


#ifdef PRICING_ARCHI
	conta_vincoli=0;
	numero_vincoli_y=n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1+N_COMPANIES*n_of_flights+n_of_arcs+n_of_short_arcs*N_COMPANIES;
#ifdef MIN_NUM_Z
	numero_vincoli_y=numero_vincoli_y+1;
#endif
	pricing_yvars=(info_y*) calloc(n_of_arcs+n_of_starting_arcs+n_of_ending_arcs,sizeof(info_y));
	if(pricing_yvars==NULL) alloc_error("pricing_yvars\n");
	for(i=0;i<n_of_arcs+n_of_starting_arcs+n_of_ending_arcs;i++) {
		pricing_yvars[i].cont=0;
		pricing_yvars[i].indici_vincoli=(int*) calloc(numero_vincoli_y,sizeof(int));
		if(pricing_yvars[i].indici_vincoli==NULL) alloc_error("pricing_yvars[i].indici_vincoli\n");
		pricing_yvars[i].coeff_vincoli=(double*) calloc(numero_vincoli_y,sizeof(double));
		if(pricing_yvars[i].coeff_vincoli==NULL) alloc_error("pricing_yvars[i].coeff_vincoli\n");
	}
#endif

	//creating y vars for the full model
	ccnt=(n_of_arcs_a+n_of_starting_arcs+n_of_ending_arcs)*n_of_aircraft_types;
	obj=(double*) calloc(ccnt,sizeof(double));
	if(obj==NULL) alloc_error("obj\n");
	lb=(double*) calloc(ccnt,sizeof(double));
	if(lb==NULL) alloc_error("lb\n");
	ub=(double*) calloc(ccnt,sizeof(double));
	if(ub==NULL) alloc_error("ub\n");
	col_name=(char**)calloc(ccnt,sizeof(char*));
	ind=0;
	for(j=0;j<n_of_aircraft_types;j++) {
		for(k=0;k<N_MAIN_AIRPORTS;k++) {
			dep_airport=starting_arcs[k]->airport;
			for(i=0;i<starting_arcs[k]->n_of_succ;i++) {
				if(dep_airport==0) {
					obj[ind]=(double)y_cost_L; 
				}
				else if(dep_airport==1) {
					obj[ind]=(double)y_cost_T;
				}
				else {
					obj[ind]=(double)y_cost_L; //we use the same cost also for this case
				}
				lb[ind]=0.0;
				company=aircrafts[j].company;		
				//if the company is B and the airport is SPC or ACE it is not allowed
				if(company==0 && (dep_airport==7 || dep_airport==3)) {
					ub[ind]=0.0;
				}
				else {
					ub[ind]=1.0;
				}
				//in base al tipo setto a zero l'ub degli archi uscenti da aereporti diversi da quello del tipo
				if(dep_airport!=aircrafts[j].dep_airport) {
					ub[ind]=0.0;
				}
#ifdef PRICING_ARCHI_BOUND
				//metto a zero l'ub degli starting arcs delle copie dei voli (tengo solo i voli originali)
				if(flight_array[starting_arcs[k]->ind_succ[i]]->is_original_flight==0) {
					ub[ind]=0.0;
				}
#endif
				col_name[ind]=(char*)calloc(20,sizeof(char));
				sprintf(col_name[ind],"ys%d_%s_%d_%d",
					flight_array[starting_arcs[k]->ind_succ[i]]->code,airports[dep_airport],company,j);
				ind++;
			}
		}
	}

	for(j=0;j<n_of_aircraft_types;j++) {
		for(i=0;i<n_of_arcs_a;i++) {		
			obj[ind]=(double) 0.0;
			lb[ind]=0.0;
			ub[ind]=1.0;
			col_name[ind]=(char*)calloc(20,sizeof(char));
			/*sprintf(col_name[ind],"y%s_%d_%d",
				airports[aircrafts[j].dep_airport],aircrafts[j].companys,j);*/
			ind++;
		}
	}

	ind=n_of_starting_arcs*n_of_aircraft_types;
	for(j=0;j<n_of_aircraft_types;j++) {
		for(i=0;i<n_of_flights;i++) {	
			for(k=0;k<successori_a[i]->n_of_succ;k++) {
				sprintf(col_name[ind],"y%d_%d_%s_%d_%d",
					flight_array[i]->code, flight_array[successori_a[i]->ind_succ[k]]->code,
					airports[aircrafts[j].dep_airport],aircrafts[j].company,j);
				//if the arr airport of flight i is international, then for company B it is not allowed
				if(aircrafts[j].company==0) {
					if(flight_array[i]->arr==6 || flight_array[i]->arr==8 || flight_array[i]->arr==9
						|| flight_array[i]->arr==10 || flight_array[i]->arr==11 || flight_array[i]->arr==12) {
						ub[ind]=0.0;
					}
				}
				//for N and for C we have a cost for doing the international flight
				else if(aircrafts[j].company==1) {
					if(flight_array[i]->arr==6 || flight_array[i]->arr==8 || flight_array[i]->arr==9
						|| flight_array[i]->arr==10 || flight_array[i]->arr==11 || flight_array[i]->arr==12) {
						obj[ind]=(double)y_cost_intern_N;
					}
				}
				else if(aircrafts[j].company==2) {
					if(flight_array[i]->arr==6 || flight_array[i]->arr==8 || flight_array[i]->arr==9
						|| flight_array[i]->arr==10 || flight_array[i]->arr==11 || flight_array[i]->arr==12) {
						obj[ind]=(double)y_cost_intern_C;
					}
				}
#ifdef PRICING_ARCHI_BOUND
				//metto a zero l'ub degli archi tra voli che non siano entrambi originali
				if(flight_array[i]->is_original_flight==0 || flight_array[successori_a[i]->ind_succ[k]]->is_original_flight==0) {
					ub[ind]=0.0;
				}
#endif
			    ind++;
			}
		}
	}

	for(j=0;j<n_of_aircraft_types;j++) {
		for(k=0;k<N_MAIN_AIRPORTS;k++) {
			arr_airport=ending_arcs[k]->airport;
			for(i=0;i<ending_arcs[k]->n_of_succ;i++) {
				obj[ind]=(double) 0.0;
				if(arr_airport==1 && aircrafts[j].dep_airport==1) {
					obj[ind]=(double) y_cost_TT;
				}
				if(arr_airport==0 && aircrafts[j].dep_airport==0) {
					obj[ind]=(double) y_cost_LL;
				}
				lb[ind]=0.0;
				company=aircrafts[j].company;				
				//if the company is B and the airport is SPC or ACE it is not allowed
				if(company==0 && (arr_airport==7 || arr_airport==3)) {
					ub[ind]=0.0;
				}
				else {
					ub[ind]=1.0;
				}
				//if the dep airport is S or A, then the arrival airport cannot be T or S or A
				dep_airport=aircrafts[j].dep_airport;
				if(dep_airport==3 || dep_airport==7) {
					if(arr_airport==1 || arr_airport==3 || arr_airport==7) {
						ub[ind]=0.0;
					}
				}
				//if the dep airport is T, then the arrival airport cannot be S or A
				if(dep_airport==1) {
					if(arr_airport==3 || arr_airport==7) {
						ub[ind]=0.0;
					}
				}
#ifdef PRICING_ARCHI_BOUND
				//metto a zero l'ub degli ending arcs per voli non originali
				if(flight_array[ending_arcs[k]->ind_succ[i]]->is_original_flight==0) {
					ub[ind]=0.0;
				}
#endif
				col_name[ind]=(char*)calloc(20,sizeof(char));
				sprintf(col_name[ind],"ye%d_%s_%d_%d",flight_array[ending_arcs[k]->ind_succ[i]]->code,
					airports[aircrafts[j].dep_airport],company,j);
				ind++;
			}
		}
	}
	
	status=CPXnewcols(env,lp,ccnt,obj,lb,ub,NULL,col_name);
	if(status) {
		stop_error("CPXnewcols\n");
	}

	free(obj);
	free(lb);
	free(ub);
	for(i=0;i<ccnt;i++) {
		free(col_name[i]);
	}
	free(col_name);

	n_of_y=ccnt;

	offset=offset+n_of_y;

	/*CPXwriteprob(env,lp,"prova.lp",NULL);
	exit(0);*/

	
	//adding constraints on the max number of aircrafts for each aircraft type 
	rcnt=n_of_aircraft_types;
	nzcnt=0;
	for(k=0;k<n_of_aircraft_types;k++) {
		dep_airport=aircrafts[k].dep_airport;
		for(i=0;i<N_MAIN_AIRPORTS;i++) {
			if(main_airports[i]!=dep_airport) {
				continue;
			}
			for(j=0;j<starting_arcs[i]->n_of_succ;j++) {
				nzcnt++;
			}
		}
	}
	rhs=(double*) calloc(rcnt,sizeof(double));
	if(rhs==NULL) alloc_error("rhs\n");
	sense=(char*) calloc(rcnt,sizeof(char));
	if(sense==NULL) alloc_error("sense\n");
	rmatbeg=(int*) calloc(rcnt,sizeof(int));
	if(rmatbeg==NULL) alloc_error("rmatbeg aircrafts constraints\n");
	rmatind=(int*) calloc(nzcnt,sizeof(int));
	if(rmatind==NULL) alloc_error("rmatind aircrafts constraints\n");
	rmatval=(double*) calloc(nzcnt,sizeof(double));
	if(rmatval==NULL) alloc_error("rmatval aircrafts constraints\n");
	constr_name=(char**) calloc(rcnt,sizeof(char*));
	if(constr_name==NULL) alloc_error("constr_name\n");
	for(i=0;i<rcnt;i++) {
		constr_name[i]=(char*)calloc(12,sizeof(char));
		if(constr_name[i]==NULL) alloc_error("constr_name[i]\n");
	}
	for(i=0;i<rcnt;i++) {
		rhs[i]=aircrafts[i].n_of_aircrafts;
		sense[i]='L';
	}

#ifdef PRICING_ARCHI
	//usato per sapere l'indice del vincolo
	conta_vincoli=n_of_crew_types+n_of_flights;
#endif

    	ind=0;
	pos=0;
	for(k=0;k<n_of_aircraft_types;k++) {
		rmatbeg[k]=ind;
		dep_airport=aircrafts[k].dep_airport;
		sprintf(constr_name[k],"c%s_%d",airports[dep_airport],aircrafts[k].company);
		for(i=0;i<N_MAIN_AIRPORTS;i++) {
			for(j=0;j<starting_arcs[i]->n_of_succ;j++) {
				if(main_airports[i]!=dep_airport) {
					pos++;
				    continue;
				} 
				rmatind[ind]=pos+n_auxil;
				rmatval[ind]=1.0;
#ifdef PRICING_ARCHI
				pricing_yvars[pos+n_auxil].coeff_vincoli[pricing_yvars[pos+n_auxil].cont]=1.0;
				pricing_yvars[pos+n_auxil].indici_vincoli[pricing_yvars[pos+n_auxil].cont]=conta_vincoli;
				pricing_yvars[pos+n_auxil].cont++;
#endif
				ind++;
				pos++;
			}			
		}
#ifdef PRICING_ARCHI
		conta_vincoli++;
#endif
	}

	status=CPXaddrows(env,lp,0,rcnt,nzcnt,rhs,sense,rmatbeg,rmatind,rmatval,NULL,constr_name);
	if(status) {
		stop_error("CPXaddrows\n");
	}
	free(rhs);
	free(sense);
	free(rmatbeg);
	free(rmatind);
	free(rmatval);
	for(i=0;i<rcnt;i++) {
		free(constr_name[i]);
	}
	free(constr_name);

	
	//adding flow conservation constraints
	rcnt=n_of_aircraft_types*n_of_flights;
	nzcnt=0;
	rhs=(double*) calloc(rcnt,sizeof(double));
	if(rhs==NULL) alloc_error("rhs\n");
	sense=(char*) calloc(rcnt,sizeof(char));
	if(sense==NULL) alloc_error("sense\n");
	constr_name=(char**) calloc(rcnt,sizeof(char*));
	if(constr_name==NULL) alloc_error("constr_name\n");
	for(i=0;i<rcnt;i++) {
		constr_name[i]=(char*)calloc(12,sizeof(char));
		if(constr_name[i]==NULL) alloc_error("constr_name[i]\n");
	}

	for(i=0;i<rcnt;i++) {
		rhs[i]=0.0;
		sense[i]='E';
	}

	nzcnt=0;
	for(k=0;k<n_of_aircraft_types;k++) {
		company=aircrafts[k].company;
		for(i=0;i<n_of_flights;i++) {
			dep_airport=flight_array[i]->dep;
			//if the node is a possible origin consider the starting arcs
			for(j=0;j<N_MAIN_AIRPORTS;j++) {
				if(main_airports[j]!=dep_airport) continue;
				if(main_airports[j]!=aircrafts[k].dep_airport) continue;
				for(l=0;l<starting_arcs[j]->n_of_succ;l++) {
					if(starting_arcs[j]->ind_succ[l]!=i) continue;
					nzcnt++;
				}
			}

			//consider the successors
			for(j=0;j<successori_a[i]->n_of_succ;j++) {
				nzcnt++;
			}
						
			//consider the predecessors
			for(j=0;j<predecessori_a[i]->n_of_pred;j++) {
				nzcnt++;
			}

			arr_airport=flight_array[i]->arr;
			//if the node is a possible terminal consider the ending arcs
			for(j=0;j<N_MAIN_AIRPORTS;j++) {
				if(main_airports[j]!=arr_airport) continue;
				if(company==0 && (arr_airport==7 || arr_airport==3)) continue;
				for(l=0;l<ending_arcs[j]->n_of_succ;l++) {
					if(ending_arcs[j]->ind_succ[l]!=i) continue;
					nzcnt++;
				}
			}
		}
	}

	
	rmatbeg=(int*) calloc(rcnt,sizeof(int));
	if(rmatbeg==NULL) alloc_error("rmatbeg flow constraints\n");
	rmatind=(int*) calloc(nzcnt,sizeof(int));
	if(rmatind==NULL) alloc_error("rmatind flow constraints\n");
	rmatval=(double*) calloc(nzcnt,sizeof(double));
	if(rmatval==NULL) alloc_error("rmatval flow constraints\n");

	index=0;
	ind=0;
	count1=0;
	count2=0;
	count3=0;
	for(k=0;k<n_of_aircraft_types;k++) {
		company=aircrafts[k].company;
		for(i=0;i<n_of_flights;i++) {
			pos=count1;
			rmatbeg[index]=ind;
			dep_airport=flight_array[i]->dep;
			sprintf(constr_name[index],"c%d_%s_%d",flight_array[i]->code,
				airports[aircrafts[k].dep_airport],aircrafts[k].company);
			//if the node is a possible origin consider the starting arcs
			for(j=0;j<N_MAIN_AIRPORTS;j++) {
				if(main_airports[j]!=dep_airport) {
					for(l=0;l<starting_arcs[j]->n_of_succ;l++) {
						pos++;
					}
					continue;
				}
				if(main_airports[j]!=aircrafts[k].dep_airport) {
					for(l=0;l<starting_arcs[j]->n_of_succ;l++) {
						pos++;
					}
					continue;
				}
				for(l=0;l<starting_arcs[j]->n_of_succ;l++) {
					if(starting_arcs[j]->ind_succ[l]!=i) {
						pos++;
						continue;
					}
					rmatval[ind]=-1.0;
					rmatind[ind]=n_auxil+pos;
#ifdef PRICING_ARCHI
					pricing_yvars[pos+n_auxil].coeff_vincoli[pricing_yvars[pos+n_auxil].cont]=-1.0;
					pricing_yvars[pos+n_auxil].indici_vincoli[pricing_yvars[pos+n_auxil].cont]=conta_vincoli;
					pricing_yvars[pos+n_auxil].cont++;
#endif
					pos++;
					ind++;
				}
			}
			pos=count2+n_of_starting_arcs*n_of_aircraft_types;

			//consider the successors
			for(j=0;j<successori_a[i]->n_of_succ;j++) {
				rmatval[ind]=1.0;
				rmatind[ind]=n_auxil+pos+successori_a[i]->arcs[j];
#ifdef PRICING_ARCHI
				pricing_yvars[n_auxil+pos+successori_a[i]->arcs[j]].coeff_vincoli[pricing_yvars[n_auxil+pos+successori_a[i]->arcs[j]].cont]=1.0;
				pricing_yvars[n_auxil+pos+successori_a[i]->arcs[j]].indici_vincoli[pricing_yvars[n_auxil+pos+successori_a[i]->arcs[j]].cont]=conta_vincoli;
				pricing_yvars[n_auxil+pos+successori_a[i]->arcs[j]].cont++;
#endif
				ind++;				
			}
						
			pos=count2+n_of_starting_arcs*n_of_aircraft_types;
			//consider the predecessors
			for(j=0;j<predecessori_a[i]->n_of_pred;j++) {
				rmatval[ind]=-1.0;
				rmatind[ind]=n_auxil+pos+predecessori_a[i]->arcs[j];
#ifdef PRICING_ARCHI
				pricing_yvars[n_auxil+pos+predecessori_a[i]->arcs[j]].coeff_vincoli[pricing_yvars[n_auxil+pos+predecessori_a[i]->arcs[j]].cont]=-1.0;
				pricing_yvars[n_auxil+pos+predecessori_a[i]->arcs[j]].indici_vincoli[pricing_yvars[n_auxil+pos+predecessori_a[i]->arcs[j]].cont]=conta_vincoli;
				pricing_yvars[n_auxil+pos+predecessori_a[i]->arcs[j]].cont++;
#endif
				ind++;				
			}

			pos=count3+(n_of_starting_arcs+n_of_arcs_a)*n_of_aircraft_types;
			arr_airport=flight_array[i]->arr;
			//if the node is a possible terminal consider the ending arcs
			for(j=0;j<N_MAIN_AIRPORTS;j++) {
				if(main_airports[j]!=arr_airport) {
					for(l=0;l<ending_arcs[j]->n_of_succ;l++) {
						pos++;
					}
					continue;
				}
				if(company==0 && (arr_airport==7 || arr_airport==3)) {
					for(l=0;l<ending_arcs[j]->n_of_succ;l++) {
						pos++;
					}
					continue;
				}
				for(l=0;l<ending_arcs[j]->n_of_succ;l++) {
					if(ending_arcs[j]->ind_succ[l]!=i) {
						pos++;
						continue;
					}
					rmatval[ind]=1.0;
					rmatind[ind]=n_auxil+pos;
#ifdef PRICING_ARCHI
					pricing_yvars[pos+n_auxil].coeff_vincoli[pricing_yvars[pos+n_auxil].cont]=1.0;
					pricing_yvars[pos+n_auxil].indici_vincoli[pricing_yvars[pos+n_auxil].cont]=conta_vincoli;
					pricing_yvars[pos+n_auxil].cont++;
#endif
					pos++;
					ind++;					
				}
			}
			index++;
#ifdef PRICING_ARCHI
			conta_vincoli++;
#endif
		}
		count1=count1+n_of_starting_arcs;
		count2=count2+n_of_arcs_a;
		count3=count3+n_of_ending_arcs;
	}

	status=CPXaddrows(env,lp,0,rcnt,nzcnt,rhs,sense,rmatbeg,rmatind,rmatval,NULL,constr_name);
	if(status) {
		stop_error("CPXaddrows\n");
	}

	free(rhs);
	free(sense);
	free(rmatbeg);
	free(rmatind);
	free(rmatval);
	for(i=0;i<rcnt;i++) {
		free(constr_name[i]);
	}
	free(constr_name);



	//adding constraint on the maximum number of aircrafts starting and ending at T
	rcnt=1;
	nzcnt=0;
	for(k=0;k<n_of_aircraft_types;k++) {
		if(aircrafts[k].dep_airport!=1) continue; 
		for(j=0;j<N_MAIN_AIRPORTS;j++) {
			if(main_airports[j]!=1) continue;
			for(i=0;i<ending_arcs[j]->n_of_succ;i++) {
				nzcnt++;
			}
		}
	}
#ifdef ITER_MIN_CAMBI
	//var aux nuova: serve se uso equality constraint
	nzcnt++; 
	nzcnt++;
#endif

	rhs=(double*) calloc(rcnt,sizeof(double));
	if(rhs==NULL) alloc_error("rhs\n");
	sense=(char*) calloc(rcnt,sizeof(char));
	if(sense==NULL) alloc_error("sense\n");
	rmatbeg=(int*) calloc(rcnt,sizeof(int));
	if(rmatbeg==NULL) alloc_error("rmatbeg maxtt constraints\n");
	rmatind=(int*) calloc(nzcnt,sizeof(int));
	if(rmatind==NULL) alloc_error("rmatind maxtt constraints\n");
	rmatval=(double*) calloc(nzcnt,sizeof(double));
	if(rmatval==NULL) alloc_error("rmatval maxtt constraints\n");
	for(i=0;i<rcnt;i++) {
		rhs[i]=(double)MAX_TT;
		sense[i]='L';
#ifdef ITER_MIN_CAMBI
		sense[i]='E';
#endif

		rmatbeg[i]=i;
	}

	pos=0;
	ind=n_auxil+(n_of_starting_arcs+n_of_arcs_a)*n_of_aircraft_types;
	for(k=0;k<n_of_aircraft_types;k++) {
		for(j=0;j<N_MAIN_AIRPORTS;j++) {
			if(main_airports[j]!=1) {
				for(i=0;i<ending_arcs[j]->n_of_succ;i++) {
					ind++;
				}
				continue;
			}		
			if(aircrafts[k].dep_airport!=1) {
				for(i=0;i<ending_arcs[j]->n_of_succ;i++) {
					ind++;
				}
				continue; 
			}
			for(i=0;i<ending_arcs[j]->n_of_succ;i++) {
				rmatval[pos]=1.0;
				rmatind[pos]=ind;
#ifdef PRICING_ARCHI
				pricing_yvars[ind].coeff_vincoli[pricing_yvars[ind].cont]=1.0;
				pricing_yvars[ind].indici_vincoli[pricing_yvars[ind].cont]=conta_vincoli;
				pricing_yvars[ind].cont++;
#endif
				ind++;
				pos++;
				
			}
		}
	}
#ifdef ITER_MIN_CAMBI
	//per la var aux nuova: serve se uso equality constraint
	rmatval[pos]=1.0;
	rmatind[pos]=n_auxil-2;
	pos++;
	rmatval[pos]=1.0;
	rmatind[pos]=n_auxil-1;
#endif

#ifdef PRICING_ARCHI
	conta_vincoli++;
#endif


	status=CPXaddrows(env,lp,0,rcnt,nzcnt,rhs,sense,rmatbeg,rmatind,rmatval,NULL,NULL);
	if(status) {
		stop_error("CPXaddrows\n");
	}


	free(rhs);
	free(sense);
	free(rmatbeg);
	free(rmatind);
	free(rmatval);

	//adding constraint on the maximum number of aircrafts starting and ending at L
	rcnt=1;
	nzcnt=0;
	for(k=0;k<n_of_aircraft_types;k++) {
		if(aircrafts[k].dep_airport!=1) continue; 
		for(j=0;j<N_MAIN_AIRPORTS;j++) {
			if(main_airports[j]!=0) continue;
			for(i=0;i<ending_arcs[j]->n_of_succ;i++) {
				nzcnt++;
			}
		}
	}

	rhs=(double*) calloc(rcnt,sizeof(double));
	if(rhs==NULL) alloc_error("rhs\n");
	sense=(char*) calloc(rcnt,sizeof(char));
	if(sense==NULL) alloc_error("sense\n");
	rmatbeg=(int*) calloc(rcnt,sizeof(int));
	if(rmatbeg==NULL) alloc_error("rmatbeg maxtt constraints\n");
	rmatind=(int*) calloc(nzcnt,sizeof(int));
	if(rmatind==NULL) alloc_error("rmatind maxtt constraints\n");
	rmatval=(double*) calloc(nzcnt,sizeof(double));
	if(rmatval==NULL) alloc_error("rmatval maxtt constraints\n");
	for(i=0;i<rcnt;i++) {
		rhs[i]=(double)MAX_LL;
		sense[i]='L';
		rmatbeg[i]=i;
	}

	pos=0;
	ind=n_auxil+(n_of_starting_arcs+n_of_arcs_a)*n_of_aircraft_types;
	for(k=0;k<n_of_aircraft_types;k++) {
		for(j=0;j<N_MAIN_AIRPORTS;j++) {
			if(main_airports[j]!=0) {
				for(i=0;i<ending_arcs[j]->n_of_succ;i++) {
					ind++;
				}
				continue;
			}		
			if(aircrafts[k].dep_airport!=0) {
				for(i=0;i<ending_arcs[j]->n_of_succ;i++) {
					ind++;
				}
				continue; 
			}
			for(i=0;i<ending_arcs[j]->n_of_succ;i++) {
				rmatval[pos]=1.0;
				rmatind[pos]=ind;
#ifdef PRICING_ARCHI
				pricing_yvars[ind].coeff_vincoli[pricing_yvars[ind].cont]=1.0;
				pricing_yvars[ind].indici_vincoli[pricing_yvars[ind].cont]=conta_vincoli;
				pricing_yvars[ind].cont++;
#endif
				ind++;
				pos++;
				
			}
		}
	}

#ifdef PRICING_ARCHI
	conta_vincoli++;
#endif

	status=CPXaddrows(env,lp,0,rcnt,nzcnt,rhs,sense,rmatbeg,rmatind,rmatval,NULL,NULL);
	if(status) {
		stop_error("CPXaddrows\n");
	}


	free(rhs);
	free(sense);
	free(rmatbeg);
	free(rmatind);
	free(rmatval);


	//adding constraints linking y and x vars
	rcnt=N_COMPANIES*n_of_flights;
#ifdef RETIME_ERR
	//consider only the real flights 
	rcnt=N_COMPANIES*(n_of_flights/RETIME_INTERV);
#endif
	nzcnt=0;
	rhs=(double*) calloc(rcnt,sizeof(double));
	if(rhs==NULL) alloc_error("rhs\n");
	sense=(char*) calloc(rcnt,sizeof(char));
	if(sense==NULL) alloc_error("sense\n");
	/*constr_name=(char**) calloc(rcnt,sizeof(char*));
	if(constr_name==NULL) alloc_error("constr_name\n");
	for(i=0;i<rcnt;i++) {
		constr_name[i]=(char*)calloc(12,sizeof(char));
		if(constr_name[i]==NULL) alloc_error("constr_name[i]\n");
	}*/

	for(i=0;i<rcnt;i++) {
		rhs[i]=0.0;
		sense[i]='E';
	}

	nzcnt=0;
	for(h=0;h<N_COMPANIES;h++) {
		for(i=0;i<n_of_flights;i++) {
			for(k=0;k<n_of_aircraft_types;k++) {
				company=aircrafts[k].company;
				if(company!=h) continue;			
				//consider the successors
				for(j=0;j<successori_a[i]->n_of_succ;j++) {
					nzcnt++;
				}
				arr_airport=flight_array[i]->arr;
				//if the node is a possible terminal consider the ending arcs
				for(j=0;j<N_MAIN_AIRPORTS;j++) {
					if(main_airports[j]!=arr_airport) continue;
					if(company==0 && (arr_airport==7 || arr_airport==3)) {
						continue;
					}
					for(l=0;l<ending_arcs[j]->n_of_succ;l++) {
						if(ending_arcs[j]->ind_succ[l]!=i) {
							continue;
						}
						nzcnt++;
					}
				}
			}
		}
	}

	rmatbeg=(int*) calloc(rcnt,sizeof(int));
	if(rmatbeg==NULL) alloc_error("rmatbeg link_xy constraints\n");
	rmatind=(int*) calloc(nzcnt,sizeof(int));
	if(rmatind==NULL) alloc_error("rmatind link_xy constraints\n");
	rmatval=(double*) calloc(nzcnt,sizeof(double));
	if(rmatval==NULL) alloc_error("rmatval link_xy constraints\n");
	constr_name=(char**) calloc(rcnt,sizeof(char*));
	if(constr_name==NULL) alloc_error("constr_name\n");
	for(i=0;i<rcnt;i++) {
		constr_name[i]=(char*)calloc(12,sizeof(char));
		if(constr_name[i]==NULL) alloc_error("constr_name[i]\n");
	}

	index=0;
	ind=0;
	for(h=0;h<N_COMPANIES;h++) {
		for(i=0;i<n_of_flights;i++) {
			//printf("flight %d\n",flight_array[i]->code);
#ifdef RETIME_ERR
			if(!flight_array[i]->is_earliest_copy) continue;
			//printf("flight %d\n",flight_array[i]->code);
#endif
			rmatbeg[index]=ind;
			sprintf(constr_name[index],"L%d_%d",flight_array[i]->code,h);

			pos1=n_of_starting_arcs*n_of_aircraft_types;

			pos2=(n_of_starting_arcs+n_of_arcs_a)*n_of_aircraft_types;
			
										
			for(k=0;k<n_of_aircraft_types;k++) {
				//printf("type %d\n",k);
				
				company=aircrafts[k].company;
				if(company!=h) continue;			
						
				//consider the successors
				for(j=0;j<successori_a[i]->n_of_succ;j++) {
					rmatval[ind]=-1.0;
					rmatind[ind]=n_auxil+pos1+successori_a[i]->arcs[j]+n_of_arcs_a*k;
#ifdef PRICING_ARCHI
					pricing_yvars[n_auxil+pos1+successori_a[i]->arcs[j]+n_of_arcs_a*k].coeff_vincoli[pricing_yvars[n_auxil+pos1+successori_a[i]->arcs[j]+n_of_arcs_a*k].cont]=-1.0;
					pricing_yvars[n_auxil+pos1+successori_a[i]->arcs[j]+n_of_arcs_a*k].indici_vincoli[pricing_yvars[n_auxil+pos1+successori_a[i]->arcs[j]+n_of_arcs_a*k].cont]=conta_vincoli;
					pricing_yvars[n_auxil+pos1+successori_a[i]->arcs[j]+n_of_arcs_a*k].cont++;
#endif
					//printf("succ %d\n",rmatind[ind]);
					ind++;				
				}
											
				arr_airport=flight_array[i]->arr;
				//if the node is a possible terminal consider the ending arcs
				count1=0;
				for(j=0;j<N_MAIN_AIRPORTS;j++) {
					if(main_airports[j]!=arr_airport) {
						for(l=0;l<ending_arcs[j]->n_of_succ;l++) {
							count1++;
						}
						continue;
					}
					if(company==0 && (arr_airport==7 || arr_airport==3)) {
						for(l=0;l<ending_arcs[j]->n_of_succ;l++) {
							count1++;
						}
						continue;
					}
					for(l=0;l<ending_arcs[j]->n_of_succ;l++) {
						if(ending_arcs[j]->ind_succ[l]!=i) {
							continue;
						}
						rmatval[ind]=-1.0;
						rmatind[ind]=n_auxil+pos2+n_of_ending_arcs*k+l+count1;
#ifdef PRICING_ARCHI
					pricing_yvars[n_auxil+pos2+n_of_ending_arcs*k+l+count1].coeff_vincoli[pricing_yvars[n_auxil+pos2+n_of_ending_arcs*k+l+count1].cont]=-1.0;
					pricing_yvars[n_auxil+pos2+n_of_ending_arcs*k+l+count1].indici_vincoli[pricing_yvars[n_auxil+pos2+n_of_ending_arcs*k+l+count1].cont]=conta_vincoli;
					pricing_yvars[n_auxil+pos2+n_of_ending_arcs*k+l+count1].cont++;
#endif	
					//printf("end %d\n",rmatind[ind]);
						ind++;	
					}
				}
			}
#ifdef RETIME_ERR
			//insert the other copies of the earliest copy flight in the same constraint
			for(c=i+1;c<n_of_flights;c++) {
				//find the copies
				if(!are_copies(c,i)) continue;
				//printf("copy flight %d\n",flight_array[c]->code);
				pos1=n_of_starting_arcs*n_of_aircraft_types;

				pos2=(n_of_starting_arcs+n_of_arcs_a)*n_of_aircraft_types;
			
										
				for(k=0;k<n_of_aircraft_types;k++) {
					//printf("type %d\n",k);
					company=aircrafts[k].company;
					if(company!=h) continue;			
					
					//consider the successors
					for(j=0;j<successori_a[c]->n_of_succ;j++) {
						rmatval[ind]=-1.0;
						rmatind[ind]=n_auxil+pos1+successori_a[c]->arcs[j]+n_of_arcs_a*k;
						//printf("succ %d\n",rmatind[ind]);
						ind++;				
					}
																
					arr_airport=flight_array[c]->arr;
					//if the node is a possible terminal consider the ending arcs
					count1=0;
					for(j=0;j<N_MAIN_AIRPORTS;j++) {
						if(main_airports[j]!=arr_airport) {
							for(l=0;l<ending_arcs[j]->n_of_succ;l++) {
								count1++;
							}
							continue;
						}
						if(company==0 && (arr_airport==7 || arr_airport==3)) {
							for(l=0;l<ending_arcs[j]->n_of_succ;l++) {
								count1++;
							}
							continue;
						}
						for(l=0;l<ending_arcs[j]->n_of_succ;l++) {
							if(ending_arcs[j]->ind_succ[l]!=c) {
								continue;
							}
							rmatval[ind]=-1.0;
							rmatind[ind]=n_auxil+pos2+n_of_ending_arcs*k+l+count1;
							//printf("end %d\n",rmatind[ind]);
							ind++;	
						}
					}
				}
			}
#endif
			index++;
#ifdef PRICING_ARCHI
			conta_vincoli++;
#endif
		}
	}

	status=CPXaddrows(env,lp,0,rcnt,nzcnt,rhs,sense,rmatbeg,rmatind,rmatval,NULL,constr_name);
	if(status) {
		stop_error("CPXaddrows\n");
	}


	free(rhs);
	free(sense);
	free(rmatbeg);
	free(rmatind);
	free(rmatval);
	for(i=0;i<rcnt;i++) {
		free(constr_name[i]);
	}
	free(constr_name);


	/*CPXwriteprob(env,lp,"prova.lp",NULL);
	exit(0);*/



	//creating z vars
	ccnt=n_of_arcs;
	obj=(double*) calloc(ccnt,sizeof(double));
	if(obj==NULL) alloc_error("obj\n");
	lb=(double*) calloc(ccnt,sizeof(double));
	if(lb==NULL) alloc_error("lb\n");
	ub=(double*) calloc(ccnt,sizeof(double));
	if(ub==NULL) alloc_error("ub\n");
	col_name=(char**)calloc(ccnt,sizeof(char*));
	for(j=0;j<n_of_arcs;j++) {
		obj[j]=(double) z_cost;
		lb[j]=0.0;
		ub[j]=1.0;
		col_name[j]=(char*)calloc(12,sizeof(char));
	}

	ind=0;
	for(i=0;i<n_of_flights;i++) {
		for(k=0;k<successori[i]->n_of_succ;k++) {
			if(flight_array[successori[i]->ind_succ[k]]->dep_t-flight_array[i]->arr_t<30) {
				ub[ind]=0.0;
			}
			if(flight_array[i]->arr!=0 && flight_array[i]->arr!=1) {
				ub[ind]=0.0;
			}
#ifdef PRICING_ARCHI_BOUND
			//metto a zero l'ub anche delle z che non sono tra voli entrambi originali
			if(flight_array[i]->is_original_flight==0 || flight_array[successori[i]->ind_succ[k]]->is_original_flight==0) {
				ub[ind]=0.0;
			}
#endif
			sprintf(col_name[ind],"z%d_%d",
					flight_array[i]->code, flight_array[successori[i]->ind_succ[k]]->code);
			ind++;
		}
	}
	

	status=CPXnewcols(env,lp,ccnt,obj,lb,ub,NULL,col_name);
	if(status) {
		stop_error("CPXnewcols\n");
	}

	free(obj);
	free(lb);
	free(ub);
	for(i=0;i<ccnt;i++) {
		free(col_name[i]);
	}
	free(col_name);

	n_of_z=ccnt;

	offset=offset+n_of_z;

	/*CPXwriteprob(env,lp,"prova.lp",NULL);
	exit(0);*/

	//adding constraints linking xyz
	rcnt=n_of_arcs;
	nzcnt=0;
	rhs=(double*) calloc(rcnt,sizeof(double));
	if(rhs==NULL) alloc_error("rhs\n");
	sense=(char*) calloc(rcnt,sizeof(char));
	if(sense==NULL) alloc_error("sense\n");
	constr_name=(char**) calloc(rcnt,sizeof(char*));
	if(constr_name==NULL) alloc_error("constr_name\n");
	for(i=0;i<rcnt;i++) {
		constr_name[i]=(char*)calloc(12,sizeof(char));
		if(constr_name[i]==NULL) alloc_error("constr_name[i]\n");
	}

	for(i=0;i<rcnt;i++) {
		rhs[i]=0.0;
		sense[i]='L';
	}

	nzcnt=0;
	for(i=0;i<n_of_flights;i++) {
		for(j=0;j<successori_a[i]->n_of_succ;j++) {
			succ=successori_a[i]->ind_succ[j];
#ifndef MAGGIO
			if(flight_array[succ]->dep_t-flight_array[i]->arr_t>3*60) {
				continue;
			}
#else
			if(flight_array[succ]->dep_t-flight_array[i]->arr_t>4*60) {
				continue;
			}
#endif
			for(k=0;k<n_of_aircraft_types;k++) {
				nzcnt++; //for the y
			}
			nzcnt++; //for the z
		}
	}

	
	rmatbeg=(int*) calloc(rcnt,sizeof(int));
	if(rmatbeg==NULL) alloc_error("rmatbeg link_xyz constraints\n");
	rmatind=(int*) calloc(nzcnt,sizeof(int));
	if(rmatind==NULL) alloc_error("rmatind link_xyz constraints\n");
	rmatval=(double*) calloc(nzcnt,sizeof(double));
	if(rmatval==NULL) alloc_error("rmatval link_xyz constraints\n");
	constr_name=(char**) calloc(rcnt,sizeof(char*));
	if(constr_name==NULL) alloc_error("constr_name\n");
	for(i=0;i<rcnt;i++) {
		constr_name[i]=(char*)calloc(20,sizeof(char));
		if(constr_name[i]==NULL) alloc_error("constr_name[i]\n");
	}

	ind=0;
	index=0;
	pos1=n_of_starting_arcs*n_of_aircraft_types;
	for(i=0;i<n_of_flights;i++) {
		for(j=0;j<successori_a[i]->n_of_succ;j++) {
			succ=successori_a[i]->ind_succ[j];
#ifndef MAGGIO
			if(flight_array[succ]->dep_t-flight_array[i]->arr_t>3*60) continue;
#else
			if(flight_array[succ]->dep_t-flight_array[i]->arr_t>4*60) continue;
#endif
			rmatbeg[index]=ind;
			sprintf(constr_name[index],"L%d_%d",flight_array[i]->code,flight_array[succ]->code);
			for(k=0;k<n_of_aircraft_types;k++) {
				rmatval[ind]=-1.0;
				rmatind[ind]=n_auxil+pos1+successori_a[i]->arcs[j]+n_of_arcs_a*k;
#ifdef PRICING_ARCHI
				pricing_yvars[n_auxil+pos1+successori_a[i]->arcs[j]+n_of_arcs_a*k].coeff_vincoli[pricing_yvars[n_auxil+pos1+successori_a[i]->arcs[j]+n_of_arcs_a*k].cont]=-1.0;
				pricing_yvars[n_auxil+pos1+successori_a[i]->arcs[j]+n_of_arcs_a*k].indici_vincoli[pricing_yvars[n_auxil+pos1+successori_a[i]->arcs[j]+n_of_arcs_a*k].cont]=conta_vincoli;
				pricing_yvars[n_auxil+pos1+successori_a[i]->arcs[j]+n_of_arcs_a*k].cont++;
#endif
				ind++;
			}
			rmatval[ind]=-1.0;
			rmatind[ind]=n_auxil+n_of_y+index;
			ind++;
			index++;
#ifdef PRICING_ARCHI
			conta_vincoli++;
#endif
		}
	}

	status=CPXaddrows(env,lp,0,rcnt,nzcnt,rhs,sense,rmatbeg,rmatind,rmatval,NULL,constr_name);
	if(status) {
		stop_error("CPXaddrows\n");
	}


	free(rhs);
	free(sense);
	free(rmatbeg);
	free(rmatind);
	free(rmatval);
	for(i=0;i<rcnt;i++) {
		free(constr_name[i]);
	}
	free(constr_name);





	//adding constraints linking x y for the short arcs 
	rcnt=0;
	n_of_short_arcs=0;
	for(i=0;i<n_of_flights;i++) {
		for(j=0;j<successori_a[i]->n_of_succ;j++) {
			succ=successori_a[i]->ind_succ[j];
			if(flight_array[succ]->dep_t-flight_array[i]->arr_t<30) {
				n_of_short_arcs++;
			}
		}
	}
	
	rcnt=n_of_short_arcs*N_COMPANIES;
	nzcnt=0;
	rhs=(double*) calloc(rcnt,sizeof(double));
	if(rhs==NULL) alloc_error("rhs\n");
	sense=(char*) calloc(rcnt,sizeof(char));
	if(sense==NULL) alloc_error("sense\n");
	constr_name=(char**) calloc(rcnt,sizeof(char*));
	if(constr_name==NULL) alloc_error("constr_name\n");
	for(i=0;i<rcnt;i++) {
		constr_name[i]=(char*)calloc(12,sizeof(char));
		if(constr_name[i]==NULL) alloc_error("constr_name[i]\n");
	}

	for(i=0;i<rcnt;i++) {
		rhs[i]=0.0;
		sense[i]='G';
	}

	nzcnt=0;
	for(h=0;h<N_COMPANIES;h++) {
		for(i=0;i<n_of_flights;i++) {
			for(j=0;j<successori_a[i]->n_of_succ;j++) {
				succ=successori_a[i]->ind_succ[j];
				if(flight_array[succ]->dep_t-flight_array[i]->arr_t>=30) {
					continue;
				}
				for(k=0;k<n_of_aircraft_types;k++) {
					if(aircrafts[k].company!=h) continue;
					nzcnt++; //for the y
				}
			}
		}
	}

	
	rmatbeg=(int*) calloc(rcnt,sizeof(int));
	if(rmatbeg==NULL) alloc_error("rmatbeg link_xyz constraints\n");
	rmatind=(int*) calloc(nzcnt,sizeof(int));
	if(rmatind==NULL) alloc_error("rmatind link_xyz constraints\n");
	rmatval=(double*) calloc(nzcnt,sizeof(double));
	if(rmatval==NULL) alloc_error("rmatval link_xyz constraints\n");
	constr_name=(char**) calloc(rcnt,sizeof(char*));
	if(constr_name==NULL) alloc_error("constr_name\n");
	for(i=0;i<rcnt;i++) {
		constr_name[i]=(char*)calloc(20,sizeof(char));
		if(constr_name[i]==NULL) alloc_error("constr_name[i]\n");
	}

	ind=0;
	index=0;
	pos1=n_of_starting_arcs*n_of_aircraft_types;
	for(h=0;h<N_COMPANIES;h++) {
		for(i=0;i<n_of_flights;i++) {
			for(j=0;j<successori_a[i]->n_of_succ;j++) {
				succ=successori_a[i]->ind_succ[j];
				if(flight_array[succ]->dep_t-flight_array[i]->arr_t>=30) {
					continue;
				}
				rmatbeg[index]=ind;
				sprintf(constr_name[index],"SH%d_%d_%d",flight_array[i]->code,flight_array[succ]->code,h);
				for(k=0;k<n_of_aircraft_types;k++) {
					if(aircrafts[k].company!=h) {
						continue;
					}
					rmatval[ind]=-1.0;
					rmatind[ind]=n_auxil+pos1+successori_a[i]->arcs[j]+n_of_arcs_a*k;
#ifdef PRICING_ARCHI
					pricing_yvars[n_auxil+pos1+successori_a[i]->arcs[j]+n_of_arcs_a*k].coeff_vincoli[pricing_yvars[n_auxil+pos1+successori_a[i]->arcs[j]+n_of_arcs_a*k].cont]=-1.0;
					pricing_yvars[n_auxil+pos1+successori_a[i]->arcs[j]+n_of_arcs_a*k].indici_vincoli[pricing_yvars[n_auxil+pos1+successori_a[i]->arcs[j]+n_of_arcs_a*k].cont]=conta_vincoli;
					pricing_yvars[n_auxil+pos1+successori_a[i]->arcs[j]+n_of_arcs_a*k].cont++;
#endif
					ind++;
				}
				index++;
#ifdef PRICING_ARCHI
				conta_vincoli++;
#endif
			}
		}
	}

	status=CPXaddrows(env,lp,0,rcnt,nzcnt,rhs,sense,rmatbeg,rmatind,rmatval,NULL,constr_name);
	if(status) {
		stop_error("CPXaddrows\n");
	}


	free(rhs);
	free(sense);
	free(rmatbeg);
	free(rmatind);
	free(rmatval);
	for(i=0;i<rcnt;i++) {
		free(constr_name[i]);
	}
	free(constr_name);


#ifdef MIN_NUM_Z
	//adding constraint on the minimum number of changes
	rcnt=1;
	nzcnt=n_of_z;
	rhs=(double*) calloc(rcnt,sizeof(double));
	if(rhs==NULL) alloc_error("rhs\n");
	sense=(char*) calloc(rcnt,sizeof(char));
	if(sense==NULL) alloc_error("sense\n");
	
	rhs[0]=4.0;
	sense[0]='G';
	

	rmatbeg=(int*) calloc(rcnt,sizeof(int));
	if(rmatbeg==NULL) alloc_error("rmatbeg min_change constraint\n");
	rmatind=(int*) calloc(nzcnt,sizeof(int));
	if(rmatind==NULL) alloc_error("rmatind min_change constraint\n");
	rmatval=(double*) calloc(nzcnt,sizeof(double));
	if(rmatval==NULL) alloc_error("rmatval min_change constraint\n");

	rmatbeg[0]=0;
	for(i=0;i<n_of_z;i++) {
		rmatind[i]=n_auxil+n_of_y+i;
		rmatval[i]=1.0;
	}

	status=CPXaddrows(env,lp,0,rcnt,nzcnt,rhs,sense,rmatbeg,rmatind,rmatval,NULL,NULL);
	if(status) {
		stop_error("CPXaddrows\n");
	}


	free(rhs);
	free(sense);
	free(rmatbeg);
	free(rmatind);
	free(rmatval);
#endif

	/*CPXwriteprob(env,lp,"prova.lp",NULL);
	exit(0);*/


	n_of_constraints=n_of_crew_types+n_of_flights;

#ifdef RETIME
	n_of_constraints=n_of_crew_types+n_of_flights/RETIME_INTERV;
#endif

	n_of_constraints=n_of_constraints+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1+
		N_COMPANIES*n_of_flights+n_of_arcs+n_of_short_arcs*N_COMPANIES;

#ifdef RETIME_ERR
	n_of_constraints=n_of_crew_types+n_of_flights/RETIME_INTERV;

	n_of_constraints=n_of_constraints+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1+
		N_COMPANIES*(n_of_flights/RETIME_INTERV)+n_of_arcs+n_of_short_arcs*N_COMPANIES;

#endif

#ifdef MIN_NUM_Z
	n_of_constraints=n_of_constraints+1;
#endif

	dual_vars=(double*) calloc(n_of_constraints,sizeof(double));
	if(dual_vars==NULL) alloc_error("dual_vars\n");

	beta=(double*) calloc(n_of_crew_types,sizeof(double));
	if(beta==NULL) alloc_error("beta\n");

#ifndef RETIME
	alpha=(double*) calloc(n_of_flights,sizeof(double));
	if(alpha==NULL) alloc_error("alpha\n");
#else
	alpha=(double*) calloc(n_of_flights/RETIME_INTERV,sizeof(double));
	if(alpha==NULL) alloc_error("alpha\n");
#endif
	
	gamma_=(double**) calloc(N_COMPANIES*n_of_flights,sizeof(double*));
	if(gamma_==NULL) alloc_error("gamma\n");
	for(i=0;i<N_COMPANIES;i++) {
		gamma_[i]=(double*) calloc(n_of_flights,sizeof(double));
		if(gamma_[i]==NULL) alloc_error("gamma[i]\n");
	}


#ifdef RETIME_ERR	
	gamma_=(double**) calloc(N_COMPANIES*(n_of_flights/RETIME_INTERV),sizeof(double*));
	if(gamma_==NULL) alloc_error("gamma\n");
	for(i=0;i<N_COMPANIES;i++) {
		gamma_[i]=(double*) calloc(n_of_flights,sizeof(double));
		if(gamma_[i]==NULL) alloc_error("gamma[i]\n");
	}
#endif


	eta=(double**) calloc(n_of_flights,sizeof(double*));
	if(eta==NULL) alloc_error("eta\n");
	for(i=0;i<n_of_flights;i++) {
		for(j=0;j<successori[i]->n_of_succ;j++) {
			eta[i]=(double*) calloc(successori[i]->n_of_succ,sizeof(double));
			if(eta[i]==NULL) alloc_error("eta[i]\n");
		}
	}

	delta=(double***) calloc(N_COMPANIES,sizeof(double**));
	if(delta==NULL) alloc_error("delta\n");
	for(i=0;i<N_COMPANIES;i++) {
		delta[i]=(double**) calloc(n_of_flights,sizeof(double*));
		if(delta[i]==NULL) alloc_error("delta[i]\n");
		for(j=0;j<n_of_flights;j++) {
			delta[i][j]=(double*) calloc(successori[j]->n_of_succ,sizeof(double));
			if(delta[i][j]==NULL) alloc_error("delta[i][j]\n");
		}
	}

	crew_day_pool=(crew_day**) calloc(MAX_CREW_POOL,sizeof(crew_day*));
	if(crew_day_pool==NULL) alloc_error("crew_day_pool\n");


	indices_short_arcs=(int**) calloc(n_of_flights,sizeof(int*));
	if(indices_short_arcs==NULL) alloc_error("indices_short_arcs\n");
	for(i=0;i<n_of_flights;i++) {
		indices_short_arcs[i]=(int*) calloc(successori[i]->n_of_succ,sizeof(int));
		if(indices_short_arcs[i]==NULL) alloc_error("indices_short_arcs[i]\n");
	}
	ind=0;
	for(i=0;i<n_of_flights;i++) {
		for(j=0;j<successori[i]->n_of_succ;j++) {
			if(flight_array[successori[i]->ind_succ[j]]->dep_t-flight_array[i]->arr_t>=30) continue;
			indices_short_arcs[i][j]=ind;
			//printf("%d %d short arc %d\n",flight_array[i]->code,flight_array[successori[i]->ind_succ[j]]->code,ind);
			ind++;
		}
	}


	printf("number of auxil %d\n",n_auxil);
	printf("number of y %d\n",n_of_y);
	printf("number of z %d\n",n_of_z);
	printf("number of constraints %d\n",n_of_constraints);
}

//returns the objective value of the col gen
double solve_master()
{
	
	int status;
	
	
	
	int lpstat;
	double objval;
	int i;
	int cont;
	int k;
	int ind;
	int j;

	int counter;
	
#ifdef ALL_DP1
	time_t start_col,end_col;
#endif

#ifdef BOUND_COMPLETAMENTO
	double prev_objval;
	int conta_iter_noimpr;
	double improv;
#endif
	

	
	
	risolto=FALSE;
	
	counter=0;

#ifdef BOUND_COMPLETAMENTO
	prev_objval=(double) INF;
	conta_iter_noimpr=0;
	improv=0.0;
	profitti_ridotti=(double*) calloc(n_of_crew_types,sizeof(double));
	if(profitti_ridotti==NULL) alloc_error("profitti_ridotti\n");
#endif

	
	while(!risolto) {
		time(&start_cplex);
		//status=CPXprimopt(env,lp);
		status=CPXbaropt(env,lp);
		if(status) {
			stop_error("CPXprimopt\n");
		}
		time(&end_cplex);
		durata_cplex=durata_cplex+difftime(end_cplex,start_cplex);
		printf("durata cplex finora %f secondi\n",durata_cplex);
		counter++;
		printf("iter %d\n",counter);

		if(x!=NULL) {
			free(x);
			x=NULL;
		}
		x=(double*) calloc(offset+n_of_cols,sizeof(double));
		if(x==NULL) alloc_error("x\n");
		
		status=CPXsolution(env,lp,&lpstat,&objval,x,dual_vars,NULL,NULL);
		if(status) {
			printf("status %d lpstat %d\n",status,lpstat);
			CPXwriteprob(env,lp,"inf.lp",NULL);
			stop_error("CPXsolution\n");
		}

		printf("objective %f lpstat %d\n",objval,lpstat);
		printf("number of created columns %d\n",n_of_cols);

#ifdef BOUND_COMPLETAMENTO_NO
		improv=((prev_objval-objval)/prev_objval)*100;
		printf("improv %f\n",improv);
		if(improv<0.01) {
			conta_iter_noimpr++;
			printf("conta iter noimpr %d\n",conta_iter_noimpr);
		}
		else {
			conta_iter_noimpr=0;
			printf("conta iter noimpr %d\n",conta_iter_noimpr);
		}
		if(conta_iter_noimpr>0 && conta_iter_noimpr % 10==0) {
		//if(prev_objval-objval<1.0 && n_of_cols>1000 && durata_cplex>1800.0) {
			cont=0;
			risolto=TRUE;
			printf("10 iter senza miglioramento: stop\n");
			printf("objval prima %f\n",objval);
			//printf("difference between two consecutive lower bounds %f\n", prev_objval-objval);
			for(k=0;k<n_of_crew_types;k++) {
				objval=objval-profitti_ridotti[k]*crews[k].n_of_crews;
			}
			printf("objval dopo %f\n",objval);
			break;
		}
		prev_objval=objval;
#endif

#ifdef BOUND_COMPLETAMENTO
		if(counter==1) {
			prev_objval=objval;
		}
		else if((counter-1) % 10==0) {
			improv=((prev_objval-objval)/prev_objval)*100;
			printf("improv %f\n",improv);
			prev_objval=objval;
			if(improv<0.01) {
				risolto=TRUE;
				printf("10 iter senza miglioramento: stop\n");
				printf("objval prima %f\n",objval);
				for(k=0;k<n_of_crew_types;k++) {
					objval=objval-profitti_ridotti[k]*crews[k].n_of_crews;
				}
				printf("objval dopo %f\n",objval);
				break;
			}
		}
		
#endif

		/*store the dual variables*/
		for(i=0;i<n_of_crew_types;i++) {
			beta[i]=dual_vars[i];
		}
#ifndef RETIME
		for(i=0;i<n_of_flights;i++) {
			alpha[i]=dual_vars[n_of_crew_types+i];
			//printf("alpha[%d] %f\n",flight_array[i]->code,alpha[i]);
		}
		ind=0;
		for(i=0;i<N_COMPANIES;i++) {
			for(k=0;k<n_of_flights;k++) {
				gamma_[i][k]=dual_vars[n_of_crew_types+n_of_flights+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1+ind];
				/*printf("gamma[%d][%d] %f\n",i,
					flight_array[k]->code,
					gamma_[i][k]);*/
				ind++;
			}
		}
		ind=0;
		for(i=0;i<n_of_flights;i++) {
			for(j=0;j<successori[i]->n_of_succ;j++) {
				eta[i][j]=dual_vars[n_of_crew_types+n_of_flights+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1+N_COMPANIES*n_of_flights+ind];
				ind++;
			}
		}

		ind=0;
		for(k=0;k<N_COMPANIES;k++) {
			for(i=0;i<n_of_flights;i++) {
				for(j=0;j<successori[i]->n_of_succ;j++) {
					delta[k][i][j]=0;
					if(flight_array[successori[i]->ind_succ[j]]->dep_t-flight_array[i]->arr_t>=30) continue;
					delta[k][i][j]=dual_vars[n_of_crew_types+n_of_flights+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1+N_COMPANIES*n_of_flights+n_of_arcs+ind];
				    ind++;
				}
			}
		}
#endif

#ifdef RETIME
		for(i=0;i<n_of_flights/RETIME_INTERV;i++) {
			alpha[i]=dual_vars[n_of_crew_types+i];
			//printf("alpha[%d] %f\n",flight_array[i]->code,alpha[i]);
		}
		ind=0;
		for(i=0;i<N_COMPANIES;i++) {
			for(k=0;k<n_of_flights;k++) {
				gamma_[i][k]=dual_vars[n_of_crew_types+n_of_flights/RETIME_INTERV+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1+ind];
				/*printf("gamma[%d][%d] %f\n",i,
					flight_array[k]->code,
					gamma_[i][k]);*/
				ind++;
			}
		}
		ind=0;
		for(i=0;i<n_of_flights;i++) {
			for(j=0;j<successori[i]->n_of_succ;j++) {
				eta[i][j]=dual_vars[n_of_crew_types+n_of_flights/RETIME_INTERV+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1+N_COMPANIES*(n_of_flights)+ind];
				ind++;
			}
		}

		ind=0;
		for(k=0;k<N_COMPANIES;k++) {
			for(i=0;i<n_of_flights;i++) {
				for(j=0;j<successori[i]->n_of_succ;j++) {
					delta[k][i][j]=0;
					if(flight_array[successori[i]->ind_succ[j]]->dep_t-flight_array[i]->arr_t>=30) continue;
					delta[k][i][j]=dual_vars[n_of_crew_types+n_of_flights/RETIME_INTERV+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1+N_COMPANIES*(n_of_flights)+n_of_arcs+ind];
				    ind++;
				}
			}
		}
#endif

		risolto=TRUE;
		//chiamare pricing
#ifdef ALL_DP1
		time(&start_col);
		cont=solve_pricing_all_dp();
		time(&end_col);
		printf("durata generazione tutte colonne %f secondi\n",difftime(end_col,start_col));
		/*printf("created %d new columns\n",cont);
		exit(0);*/
#else
		cont=solve_pricing();
#endif	
       	printf("created %d new columns\n",cont);
       	//exit(0);
		//aggiornare risolto
#ifdef ALL_DP1
		cont=0; //per fermare la generazione di colonne (sono state generate tutte in un colpo)
#endif
		if(cont==0) {
			risolto=TRUE;
			//print columns
			/*for(i=0;i<n_of_cols;i++) {
				printf("col %d x%d type %d n_flights %d:\n",i,i+offset,
					crew_day_pool[i]->type,crew_day_pool[i]->n_flights);
				for(k=crew_day_pool[i]->n_flights-1;k>=0;k--) {
					printf("%s %d-%s %d, ",
						airports[flight_array[crew_day_pool[i]->flights[k]]->dep],
						flight_array[crew_day_pool[i]->flights[k]]->dep_t,
						airports[flight_array[crew_day_pool[i]->flights[k]]->arr],
						flight_array[crew_day_pool[i]->flights[k]]->arr_t);
				}
				printf("\n\n");
			}*/
			//print solution
			/*for(i=0;i<n_of_cols;i++) {
				if(x[i+offset]>EPSI) {
					printf("col %d x%d type %d n_flights %d:\n",i,i+offset,
						crew_day_pool[i]->type,crew_day_pool[i]->n_flights);
					for(k=crew_day_pool[i]->n_flights-1;k>=0;k--) {
						printf("%s %d-%s %d, ",
							airports[flight_array[crew_day_pool[i]->flights[k]]->dep],
							flight_array[crew_day_pool[i]->flights[k]]->dep_t,
							airports[flight_array[crew_day_pool[i]->flights[k]]->arr],
							flight_array[crew_day_pool[i]->flights[k]]->arr_t);
					}
					printf("\n\n");
				}
			}*/

			//print solution codes
			/*printf("solution\n\n\n");
			for(i=0;i<n_of_cols;i++) {
				if(x[i+offset]>EPSI) {
					printf("x%d n_flights %d:\n",i+offset,crew_day_pool[i]->n_flights);
					for(k=crew_day_pool[i]->n_flights-1;k>=0;k--) {
						printf("%d, ",flight_array[crew_day_pool[i]->flights[k]]->code);
					}
					printf("\n\n");
				}
			}*/
		}
		else {
			risolto=FALSE;
		}

		//free(x);
	}

#ifdef ALL_COLS_IN_GAP
	BestLB=round_(objval);
	find_int_sol(0);
	//exit(0);
	solve_additional_cols();
	exit(0);
#endif

#ifdef ALL_COLS_AND_Y_IN_GAP
	//printing the values of the variables
	/*printf("values of the variables at the root node\n");
	printf("auxiliary variables:\n");
	for(i=0;i<n_auxil;i++) {
		if(x[i]>EPSI) {
			printf("var[%d] %f\n",i,x[i]);
		}
	}
	printf("y variables:\n");
	for(i=n_auxil;i<n_auxil+n_of_y;i++) {
		if(x[i]>EPSI) {
			printf("y[%d] %f\n",i,x[i]);
		}
	}
	printf("z variables:\n");
	for(i=n_auxil+n_of_y;i<n_auxil+n_of_y+n_of_z;i++) {
		if(x[i]>EPSI) {
			printf("z[%d] %f\n",i,x[i]);
		}
	}
	exit(0);*/
	/*printf("x variables:\n");
	for(i=offset;i<offset+n_of_cols;i++) {
		if(x[i]>EPSI) {
			printf("x[%d] %f\n",i,x[i]);
		}
	}
	exit(0);*/

#ifdef BOUND_COMPLETAMENTO
	free(profitti_ridotti);
#endif

	BestLB=round_(objval);
	//exit(0);
	find_int_sol(0);
	exit(0);
	solve_additional_cols_and_y();
	exit(0);
#endif

#if 1
	//find_int_sol(1); //risolvo solo l'LP con tutte le colonne
	find_int_sol(0); //risolvo all'intero con tutte le colonne (o con le colonne inserite): anche heur
	
	exit(0);
#endif

	return objval;
}

//returns the number of new columns found
//columns are added to the master here
int solve_pricing()
{
	int i,k,j,li,nl;
	int new_cols;
	int succ;
	double best_profit;
	int best_node;
	int best_label;
	int ind, ind_l;
	int count;
	int tmp;
	double* obj_col;
	double* lb_col;
	double* ub_col;
	int* cmatbeg;
	int* cmatind;
	double* cmatval;
	int nzcnt_col;
	int pos;
	int status;
	int ora_fine_li, ora_fine_nl;
	int dur_li, dur_nl;
	int dur;
	int ora_fine;
	int attesa;
	double penal;
	int company;
	int posi;
	int ind_start,ind_end, index;
	double valore;

#ifdef MORE_COLS
	int ind_more_cols;
#endif

#ifdef RETIME
	int primo_A;
	int primo_S;
	int ultimo_A;
	int ultimo_S;
#endif

#ifdef RETIME
	primo_A=find_first_flight_A();
	primo_S=find_first_flight_S();
	ultimo_A=find_last_flight_A();
	ultimo_S=find_last_flight_S();
#endif


	//initializing labels
	labels=(label**) calloc(n_of_flights,sizeof(label*));
	if(labels==NULL) alloc_error("labels\n");
	for(i=0;i<n_of_flights;i++) {
		labels[i]=(label*) calloc(MAX_LABELS,sizeof(label));
		if(labels[i]==NULL) alloc_error("labels[i]\n");
	}

	//numero etichette occupate finora per ogni nodo
	ind_labels=(int*) calloc(n_of_flights,sizeof(int));
	if(ind_labels==NULL) alloc_error("ind_labels\n");

	//initializing the arrays for adding the column
	cmatbeg=(int*) calloc(1,sizeof(int));
	if(cmatbeg==NULL) alloc_error("cmatbeg\n");
	obj_col=(double*) calloc(1,sizeof(double));
	if(obj_col==NULL) alloc_error("obj_col\n");
	ub_col=(double*) calloc(1,sizeof(double));
	if(ub_col==NULL) alloc_error("ub_col\n");
	lb_col=(double*) calloc(1,sizeof(double));
	if(lb_col==NULL) alloc_error("lb_col\n");


	new_cols=0;

	for(k=0;k<n_of_crew_types;k++) {
		company=crews[k].company;
		for(i=0;i<n_of_flights;i++) {
			for(j=0;j<MAX_LABELS;j++) {
							
				labels[i][j].pred=-1;
				labels[i][j].pred_label=-1;
				//check aereporto di partenza per il tipo
				if(flight_array[i]->dep==crews[k].dep_airport) {
#ifndef RETIME
					labels[i][j].profit=alpha[i];
#else
					labels[i][j].profit=alpha[flight_array[i]->its_real_flight_index];
#endif
					//adding the profit of the link xy constraints
#ifndef RETIME
					labels[i][j].profit=labels[i][j].profit+gamma_[company][i];
#else
					//labels[i][j].profit=labels[i][j].profit+gamma_[company][flight_array[i]->its_real_flight_index];
labels[i][j].profit=labels[i][j].profit+gamma_[company][i];

#endif
					ind_labels[i]=1;
					labels[i][j].n_flights=1;
					labels[i][j].ind_table_duration=find_interval(i);
					labels[i][j].dep_time_first_flight=flight_array[i]->dep_t;

#ifdef RETIME
					if(are_copies(primo_A,i)) continue;
					if(are_copies(primo_S,i)) continue;
#endif

					if ( ( crews[k].dep_airport == 7 && flight_array[i]->dep_t > min_dep_time_A ) ||
					     ( crews[k].dep_airport == 3 && flight_array[i]->dep_t > min_dep_time_S ) )
					{
						labels[i][j].profit=-(double)INF;
						ind_labels[i]=0;
						labels[i][j].n_flights=0;
					}
					
			
				}
				else {
					labels[i][j].profit=-(double)INF;
					ind_labels[i]=0;
					labels[i][j].n_flights=0;
					
				}
			}
			
		}
		for(i=0;i<n_of_flights;i++) {
			
			//count=0;
			for(li=0;li<ind_labels[i];li++) {
				

				for(nl=0;nl<ind_labels[i];nl++) {
					if(li==nl) continue;
					if(labels[i][nl].pred==-2) continue;
					if(labels[i][nl].profit>labels[i][li].profit-EPSI) {
						if(labels[i][nl].n_flights<=labels[i][li].n_flights) {
							dur_li=duration_table[labels[i][li].ind_table_duration][labels[i][li].n_flights-1];
							ora_fine_li=labels[i][li].dep_time_first_flight-45+dur_li-30;
							if(ora_fine_li>24*60) {
								ora_fine_li=24*60;
							}
							dur_nl=duration_table[labels[i][nl].ind_table_duration][labels[i][nl].n_flights-1];
							ora_fine_nl=labels[i][nl].dep_time_first_flight-45+dur_nl-30;
							if(ora_fine_nl>24*60) {
								ora_fine_nl=24*60;
							}
							if(ora_fine_nl>=ora_fine_li) {
								//label li dominata: indicato con indice pred = -2
								
								labels[i][li].pred=-2;
							}
						}
					}
				}
				if(labels[i][li].pred==-2) continue;

#ifdef AL 
				//se il volo i va da A ad L e non ha predecessori, allora non etichettare a partire da lui (volo da solo)
				if(flight_array[i]->dep==7 && flight_array[i]->arr==0) {
					if(predecessori[i]->n_of_pred==0) {
						continue;
					}
					if(first_flight_A(i)) continue;
				}
#endif
				//count++;
				//printf("label %d profit %f nflight %d\n",li,labels[i][li].profit,labels[i][li].n_flights);
				for(j=0;j<successori[i]->n_of_succ;j++) {
					//avoiding to propagating to forbidden succ in the branch and price
					if(successori[i]->forb[j]) continue;

					succ=successori[i]->ind_succ[j];
				
					//check num voli
					if(labels[i][li].n_flights+1>crews[k].max_flights) continue;
					//check aereporto di arrivo per il tipo
					if(labels[i][li].n_flights+1==crews[k].max_flights) {
						if(flight_array[succ]->arr!=crews[k].arr_airport) continue;
					}
					
					//check durata
					dur=duration_table[labels[i][li].ind_table_duration][labels[i][li].n_flights];
					ora_fine=labels[i][li].dep_time_first_flight-45+dur-30;
					if(flight_array[succ]->arr_t>ora_fine) continue;
					
#ifdef AL
					//se il volo i va da A ad L ed e` il primo della giornata allora non etichettarlo (volo da solo)
					if(flight_array[succ]->dep==7 && flight_array[succ]->arr==0) {
						if(first_flight_A(succ)) continue;
					}
#endif
#ifdef LA

					//se il succ va da L ad A e non ha succ, allora non etichettarlo (volo da solo)
					if(flight_array[succ]->dep==0 && flight_array[succ]->arr==7) {
						if(successori[succ]->n_of_succ==0) continue;
						if(last_flight_A(succ)) continue;
					}
#endif
					
					if(ind_labels[succ]==MAX_LABELS) {
						printf("ind_labels[succ] %d succ %d\n",ind_labels[succ],succ);
						stop_error("max lenght of labels reached\n");
					}
					//propagating labels in a new slot (dominance will be checked at the top)
#ifndef RETIME
					labels[succ][ind_labels[succ]].profit=labels[i][li].profit+alpha[succ]+
						gamma_[company][succ]+eta[i][j]+delta[company][i][j];
#else
labels[succ][ind_labels[succ]].profit=labels[i][li].profit+alpha[flight_array[succ]->its_real_flight_index]+
						gamma_[company][succ]+eta[i][j]+delta[company][i][j];
					//labels[succ][ind_labels[succ]].profit=labels[i][li].profit+alpha[flight_array[succ]->its_real_flight_index]+gamma_[company][flight_array[succ]->its_real_flight_index]+eta[i][j]+delta[company][i][j];

#endif
#ifdef MULTI_OBJ
					//adding the cost of the arc
					attesa=flight_array[succ]->dep_t-flight_array[i]->arr_t;
					if(attesa<TIME1) {
						penal=PENAL1;
					}
					else if(attesa<TIME2) {
						penal=PENAL2;
					}
					else if(attesa<TIME3) {
						penal=PENAL3;
					}
					else if(attesa<=TIME4) {
						penal=PENAL4;
					}
					else {
						printf("attesa %d\n",attesa);
						stop_error("too long waiting\n");
					}
					labels[succ][ind_labels[succ]].profit=labels[succ][ind_labels[succ]].profit-((double)(attesa)/5.0)*penal;
#endif
					labels[succ][ind_labels[succ]].pred=i;
					labels[succ][ind_labels[succ]].pred_label=li;
					labels[succ][ind_labels[succ]].n_flights=labels[i][li].n_flights+1;
					labels[succ][ind_labels[succ]].dep_time_first_flight=labels[i][li].dep_time_first_flight;
					labels[succ][ind_labels[succ]].ind_table_duration=labels[i][li].ind_table_duration;
					
					/*if(flight_array[succ]->dep!=flight_array[labels[succ][ind_labels[succ]].pred]->arr) {
						stop_error("error different airports\n");
					}*/


					ind_labels[succ]++;

					
					
					
				}
			}
			//printf("node %d count %d\n",i,count);
		}

#ifdef MORE_COLS
	for(ind_more_cols=0; ind_more_cols<N_MORE_COLS; ind_more_cols++) {
		best_profit=0.0;
		best_node=-1;
		best_label=-1;
		for(i=0;i<n_of_flights;i++) {
			if(flight_array[i]->arr!=crews[k].arr_airport) continue;
			if(not_terminal[i]) continue;
			if ( ( crews[k].arr_airport == 7 && flight_array[i]->arr_t < max_arr_time_A ) ||
			     ( crews[k].arr_airport == 3 && flight_array[i]->arr_t < max_arr_time_S ) )
			{
#ifdef RETIME
				//se il volo arriva in A o S prima dell'ultimo ma e` una sua copia, puo` essere l'ultimo di un turno
				if( (crews[k].arr_airport == 7 && are_copies(i,ultimo_A)) || (crews[k].arr_airport == 3 && are_copies(i,ultimo_S)) );
				else
#endif
				continue;
			}
			for(li=0;li<ind_labels[i];li++) {

				if(labels[i][li].pred==-2) continue;
				if(labels[i][li].profit>best_profit) {
					best_profit=labels[i][li].profit;
					best_node=i;
					best_label=li;
				}
			}
		}
		if(best_node==-1) break; //esco dal ciclo sulle more cols

		//annullo il profitto del nodo corrente (per non ritrovare la stessa colonna al prossimo giro)
		labels[best_node][best_label].profit=0.0; 
		

		if(company==0) {
			valore=(double)x_cost_B;
		}
		else if(company==1) {
			valore=(double)x_cost_N;
		}
		else {
			valore=(double)x_cost_C;
		}
		if(k==6 || k==7 || k==8 || k==9 || k==10 || k==11 || k==12 || k==13) {
			if(company==0) {
				stop_error("route of Binter sleeping in S or A\n");
			}
			else if(company==1) {
				valore=valore+(double)x_cost_pernocta_N;
			}
			else {
				valore=valore+(double)x_cost_pernocta_C;
			}
		}
#ifdef BOUND_COMPLETAMENTO
		if(best_profit>valore-beta[k]+EPSI) {
			profitti_ridotti[k]=best_profit-valore+beta[k];
		}
		else {
			profitti_ridotti[k]=0.0;
		}
#endif
		if(best_profit>valore-beta[k]+EPSI) {
			//printf("best profit %f valore %f ",best_profit, valore);
			//printf("difference %f\n",best_profit-valore+beta[k]);

			
		//if(best_profit>1.0-beta[k]+EPSI) {

			//generating a new column
			risolto=FALSE;
			//reconstructing the route found
			ind=best_node;
			ind_l=best_label;
			count=0;
			while(labels[ind][ind_l].pred!=-1) {
				tmp=ind;
				ind=labels[ind][ind_l].pred;
				ind_l=labels[tmp][ind_l].pred_label;
				count++;
			}
			count++;
			//storing the new route in the routes pool
			//printf("new col profit %f\n",best_profit);
			crew_day_pool[n_of_cols]=(crew_day*) calloc(1,sizeof(crew_day));
			if(crew_day_pool[n_of_cols]==NULL) alloc_error("crew_day_pool[n_of_cols]\n");
			crew_day_pool[n_of_cols]->type=k;
			crew_day_pool[n_of_cols]->n_flights=count;
			crew_day_pool[n_of_cols]->flights=(int*) calloc(count,sizeof(int));
			if(crew_day_pool[n_of_cols]->flights==NULL) alloc_error("crew_day_pool[n_of_cols]->flights\n");
			ind=best_node;
			ind_l=best_label;
			count=0;
			while(labels[ind][ind_l].pred!=-1) {
				crew_day_pool[n_of_cols]->flights[count]=ind;
				tmp=ind;
				ind=labels[ind][ind_l].pred;
				ind_l=labels[tmp][ind_l].pred_label;
				count++;
			}
			crew_day_pool[n_of_cols]->flights[count]=ind;
			
			/*printf("company %d\n",company);
			for(i=0;i<crew_day_pool[n_of_cols]->n_flights;i++) {
				printf("%d %d %s %d %s %d\n",
					flight_array[crew_day_pool[n_of_cols]->flights[i]]->code,
					crews[crew_day_pool[n_of_cols]->type].company,
					airports[flight_array[crew_day_pool[n_of_cols]->flights[i]]->dep],
					flight_array[crew_day_pool[n_of_cols]->flights[i]]->dep_t,
					airports[flight_array[crew_day_pool[n_of_cols]->flights[i]]->arr],
					flight_array[crew_day_pool[n_of_cols]->flights[i]]->arr_t);

			}*/

			company=crews[crew_day_pool[n_of_cols]->type].company;
			
			//adding the new column to the master
			if(company==0) {
				obj_col[0]=(double)x_cost_B; //B
			}
			else if(company==1) {
				obj_col[0]=(double)x_cost_N;
			}
			else {
				obj_col[0]=(double)x_cost_C;
			}
			cmatbeg[0]=0;
			lb_col[0]=0.0;
			ub_col[0]=1.0;

			nzcnt_col=1+crew_day_pool[n_of_cols]->n_flights+crew_day_pool[n_of_cols]->n_flights+crew_day_pool[n_of_cols]->n_flights-1;
			
			//counting the number of short arcs in the column
			for(i=crew_day_pool[n_of_cols]->n_flights-1;i>0;i--) {
				ind_start=crew_day_pool[n_of_cols]->flights[i];
				ind_end=crew_day_pool[n_of_cols]->flights[i-1];
				for(j=0;j<successori[ind_start]->n_of_succ;j++) {
					succ=successori[ind_start]->ind_succ[j];
				    if(succ==ind_end) {
						if(flight_array[succ]->dep_t-flight_array[ind_start]->arr_t>=30) continue;
						nzcnt_col++;
					}
				}
			}
			
			crew_day_pool[n_of_cols]->forb=-1;
			//if the route of the crew is from S or A, or it is from A or S, higher cost
			if(crew_day_pool[n_of_cols]->type==6 || crew_day_pool[n_of_cols]->type==7 ||
				crew_day_pool[n_of_cols]->type==8 || crew_day_pool[n_of_cols]->type==9 ||
				crew_day_pool[n_of_cols]->type==19 || crew_day_pool[n_of_cols]->type==11 ||
				crew_day_pool[n_of_cols]->type==12 || crew_day_pool[n_of_cols]->type==13) {
				
				if(company==0) {
					stop_error("route of Binter from S or A\n");
				}
				else if(company==1) {
					obj_col[0]=obj_col[0]+(double)x_cost_pernocta_N;
				}
				else {
					obj_col[0]=obj_col[0]+(double)x_cost_pernocta_C;
				}
			}

#ifdef MULTI_OBJ
			for(i=crew_day_pool[n_of_cols]->n_flights-1;i>0;i--) {
				attesa=flight_array[crew_day_pool[n_of_cols]->flights[i-1]]->dep_t-
				flight_array[crew_day_pool[n_of_cols]->flights[i]]->arr_t;
				if(attesa<TIME1) {
					penal=PENAL1;
				}
				else if(attesa<TIME2) {
					penal=PENAL2;
				}
				else if(attesa<TIME3) {
					penal=PENAL3;
				}
				else if(attesa<=TIME4) {
					penal=PENAL4;
				}
				else {
					printf("attesa %d\n",attesa);
					stop_error("too long waiting\n");
				}
				obj_col[0]=obj_col[0]+((double)(attesa)/5.0)*penal;
					
			} 
#endif

			cmatind=(int*) calloc(nzcnt_col,sizeof(int));
			if(cmatind==NULL) alloc_error("cmatind\n");
			cmatval=(double*) calloc(nzcnt_col,sizeof(double));
			if(cmatval==NULL) alloc_error("cmatval\n");

			cmatind[0]=crew_day_pool[n_of_cols]->type;
			cmatval[0]=1.0;

			pos=1;
			for(i=0;i<crew_day_pool[n_of_cols]->n_flights;i++) {
#ifndef RETIME
				cmatind[pos]=n_of_crew_types+crew_day_pool[n_of_cols]->flights[i];
#else
/*printf("flight %d ind %d flight real index %d\n",flight_array[crew_day_pool[n_of_cols]->flights[i]]->code, crew_day_pool[n_of_cols]->flights[i], flight_array[crew_day_pool[n_of_cols]->flights[i]]->its_real_flight_index);*/
				cmatind[pos]=n_of_crew_types+flight_array[crew_day_pool[n_of_cols]->flights[i]]->its_real_flight_index;
#endif
				cmatval[pos]=1.0;
				pos++;
			}
			
			//inserting the column in the constraints linking xy
#ifndef RETIME
			posi=n_of_crew_types+n_of_flights+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1;
#else
			posi=n_of_crew_types+n_of_flights/RETIME_INTERV+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1;
#endif
			company=crews[crew_day_pool[n_of_cols]->type].company;
#ifndef RETIME
			posi=posi+n_of_flights*company;
#else
			//posi=posi+(n_of_flights/RETIME_INTERV)*company;
posi=posi+n_of_flights*company;
#endif
			for(i=0;i<crew_day_pool[n_of_cols]->n_flights;i++) {
#ifndef RETIME
				cmatind[pos]=posi+crew_day_pool[n_of_cols]->flights[i];
#else
cmatind[pos]=posi+crew_day_pool[n_of_cols]->flights[i];
				//cmatind[pos]=posi+flight_array[crew_day_pool[n_of_cols]->flights[i]]->its_real_flight_index;
#endif
				cmatval[pos]=1.0;
				pos++;
			}

			//inserting the column in the constraints linking xyz
#ifndef RETIME
			posi=n_of_crew_types+n_of_flights+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1+N_COMPANIES*n_of_flights;
#else
posi=n_of_crew_types+n_of_flights/RETIME_INTERV+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1+N_COMPANIES*(n_of_flights);
			//posi=n_of_crew_types+n_of_flights/RETIME_INTERV+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1+N_COMPANIES*(n_of_flights/RETIME_INTERV);
#endif
			for(i=crew_day_pool[n_of_cols]->n_flights-1;i>0;i--) {
				ind_start=crew_day_pool[n_of_cols]->flights[i];
				ind_end=crew_day_pool[n_of_cols]->flights[i-1];
				for(j=0;j<successori[ind_start]->n_of_succ;j++) {
					succ=successori[ind_start]->ind_succ[j];
				    if(succ==ind_end) {
						index=successori[ind_start]->arcs[j];
						break;
					}
				}
				cmatind[pos]=posi+index;
				cmatval[pos]=1.0;
				pos++;
			}

			//inserting the column in the constraints linking x y for short arcs
#ifndef RETIME
			posi=n_of_crew_types+n_of_flights+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1+N_COMPANIES*n_of_flights+n_of_arcs;
#else
posi=n_of_crew_types+n_of_flights/RETIME_INTERV+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1+N_COMPANIES*(n_of_flights)+n_of_arcs;
			//posi=n_of_crew_types+n_of_flights/RETIME_INTERV+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1+N_COMPANIES*(n_of_flights/RETIME_INTERV)+n_of_arcs;
#endif
			company=crews[crew_day_pool[n_of_cols]->type].company;
			posi=posi+n_of_short_arcs*company;
			for(i=crew_day_pool[n_of_cols]->n_flights-1;i>0;i--) {
				ind_start=crew_day_pool[n_of_cols]->flights[i];
				ind_end=crew_day_pool[n_of_cols]->flights[i-1];
				for(j=0;j<successori[ind_start]->n_of_succ;j++) {
					succ=successori[ind_start]->ind_succ[j];
				    if(succ==ind_end) {
						if(flight_array[succ]->dep_t-flight_array[ind_start]->arr_t>=30) continue;
						index=indices_short_arcs[ind_start][j];
						cmatind[pos]=posi+index;
						cmatval[pos]=1.0;
						pos++;
						break;
					}
				}
				
			}

			if(pos!=nzcnt_col) stop_error("pos different from nzcnt_col");
								
			status=CPXaddcols(env,lp,1,nzcnt_col,obj_col,cmatbeg,cmatind,cmatval,lb_col,ub_col,NULL);
			if(status) {
				stop_error("CPXaddcols\n");
			}

			free(cmatind);
			free(cmatval);

			n_of_cols++;
			new_cols++;
		}
	}





#else
		//determining the node with highest profit among those that have the right arrival airport
		best_profit=0.0;
		best_node=-1;
		best_label=-1;
		for(i=0;i<n_of_flights;i++) {
			if(flight_array[i]->arr!=crews[k].arr_airport) continue;
			if(not_terminal[i]) continue;
			if ( ( crews[k].arr_airport == 7 && flight_array[i]->arr_t < max_arr_time_A ) ||
			     ( crews[k].arr_airport == 3 && flight_array[i]->arr_t < max_arr_time_S ) )
			{
				continue;
			}
			for(li=0;li<ind_labels[i];li++) {

				if(labels[i][li].pred==-2) continue;
				if(labels[i][li].profit>best_profit) {
					best_profit=labels[i][li].profit;
					best_node=i;
					best_label=li;
				}
			}
		}
		if(best_node==-1) continue;

		if(company==0) {
			valore=(double)x_cost_B;
		}
		else if(company==1) {
			valore=(double)x_cost_N;
		}
		else {
			valore=(double)x_cost_C;
		}
		if(k==6 || k==7 || k==8 || k==9 || k==10 || k==11 || k==12 || k==13) {
			if(company==0) {
				stop_error("route of Binter sleeping in S or A\n");
			}
			else if(company==1) {
				valore=valore+(double)x_cost_pernocta_N;
			}
			else {
				valore=valore+(double)x_cost_pernocta_C;
			}
		}
		if(best_profit>valore-beta[k]+EPSI) {
			//printf("best profit %f valore %f ",best_profit, valore);
			//printf("difference %f\n",best_profit-valore+beta[k]);

			
		//if(best_profit>1.0-beta[k]+EPSI) {

			//generating a new column
			risolto=FALSE;
			//reconstructing the route found
			ind=best_node;
			ind_l=best_label;
			count=0;
			while(labels[ind][ind_l].pred!=-1) {
				tmp=ind;
				ind=labels[ind][ind_l].pred;
				ind_l=labels[tmp][ind_l].pred_label;
				count++;
			}
			count++;
			//storing the new route in the routes pool
			//printf("new col profit %f\n",best_profit);
			crew_day_pool[n_of_cols]=(crew_day*) calloc(1,sizeof(crew_day));
			if(crew_day_pool[n_of_cols]==NULL) alloc_error("crew_day_pool[n_of_cols]\n");
			crew_day_pool[n_of_cols]->type=k;
			crew_day_pool[n_of_cols]->n_flights=count;
			crew_day_pool[n_of_cols]->flights=(int*) calloc(count,sizeof(int));
			if(crew_day_pool[n_of_cols]->flights==NULL) alloc_error("crew_day_pool[n_of_cols]->flights\n");
			ind=best_node;
			ind_l=best_label;
			count=0;
			while(labels[ind][ind_l].pred!=-1) {
				crew_day_pool[n_of_cols]->flights[count]=ind;
				tmp=ind;
				ind=labels[ind][ind_l].pred;
				ind_l=labels[tmp][ind_l].pred_label;
				count++;
			}
			crew_day_pool[n_of_cols]->flights[count]=ind;
			
			/*printf("company %d\n",company);
			for(i=0;i<crew_day_pool[n_of_cols]->n_flights;i++) {
				printf("%d %d %s %d %s %d\n",
					flight_array[crew_day_pool[n_of_cols]->flights[i]]->code,
					crews[crew_day_pool[n_of_cols]->type].company,
					airports[flight_array[crew_day_pool[n_of_cols]->flights[i]]->dep],
					flight_array[crew_day_pool[n_of_cols]->flights[i]]->dep_t,
					airports[flight_array[crew_day_pool[n_of_cols]->flights[i]]->arr],
					flight_array[crew_day_pool[n_of_cols]->flights[i]]->arr_t);

			}*/

			company=crews[crew_day_pool[n_of_cols]->type].company;
			
			//adding the new column to the master
			if(company==0) {
				obj_col[0]=(double)x_cost_B; //B
			}
			else if(company==1) {
				obj_col[0]=(double)x_cost_N;
			}
			else {
				obj_col[0]=(double)x_cost_C;
			}
			cmatbeg[0]=0;
			lb_col[0]=0.0;
			ub_col[0]=1.0;

			nzcnt_col=1+crew_day_pool[n_of_cols]->n_flights+crew_day_pool[n_of_cols]->n_flights+crew_day_pool[n_of_cols]->n_flights-1;
			
			//counting the number of short arcs in the column
			for(i=crew_day_pool[n_of_cols]->n_flights-1;i>0;i--) {
				ind_start=crew_day_pool[n_of_cols]->flights[i];
				ind_end=crew_day_pool[n_of_cols]->flights[i-1];
				for(j=0;j<successori[ind_start]->n_of_succ;j++) {
					succ=successori[ind_start]->ind_succ[j];
				    if(succ==ind_end) {
						if(flight_array[succ]->dep_t-flight_array[ind_start]->arr_t>=30) continue;
						nzcnt_col++;
					}
				}
			}
			
			crew_day_pool[n_of_cols]->forb=-1;
			//if the route of the crew is from S or A, or it is from A or S, higher cost
			if(crew_day_pool[n_of_cols]->type==6 || crew_day_pool[n_of_cols]->type==7 ||
				crew_day_pool[n_of_cols]->type==8 || crew_day_pool[n_of_cols]->type==9 ||
				crew_day_pool[n_of_cols]->type==19 || crew_day_pool[n_of_cols]->type==11 ||
				crew_day_pool[n_of_cols]->type==12 || crew_day_pool[n_of_cols]->type==13) {
				
				if(company==0) {
					stop_error("route of Binter from S or A\n");
				}
				else if(company==1) {
					obj_col[0]=obj_col[0]+(double)x_cost_pernocta_N;
				}
				else {
					obj_col[0]=obj_col[0]+(double)x_cost_pernocta_C;
				}
			}

#ifdef MULTI_OBJ
			for(i=crew_day_pool[n_of_cols]->n_flights-1;i>0;i--) {
				attesa=flight_array[crew_day_pool[n_of_cols]->flights[i-1]]->dep_t-
				flight_array[crew_day_pool[n_of_cols]->flights[i]]->arr_t;
				if(attesa<TIME1) {
					penal=PENAL1;
				}
				else if(attesa<TIME2) {
					penal=PENAL2;
				}
				else if(attesa<TIME3) {
					penal=PENAL3;
				}
				else if(attesa<=TIME4) {
					penal=PENAL4;
				}
				else {
					printf("attesa %d\n",attesa);
					stop_error("too long waiting\n");
				}
				obj_col[0]=obj_col[0]+((double)(attesa)/5.0)*penal;
					
			} 
#endif

			cmatind=(int*) calloc(nzcnt_col,sizeof(int));
			if(cmatind==NULL) alloc_error("cmatind\n");
			cmatval=(double*) calloc(nzcnt_col,sizeof(double));
			if(cmatval==NULL) alloc_error("cmatval\n");

			cmatind[0]=crew_day_pool[n_of_cols]->type;
			cmatval[0]=1.0;

			pos=1;
			for(i=0;i<crew_day_pool[n_of_cols]->n_flights;i++) {
				cmatind[pos]=n_of_crew_types+crew_day_pool[n_of_cols]->flights[i];
				cmatval[pos]=1.0;
				pos++;
			}

			//inserting the column in the constraints linking xy
			posi=n_of_crew_types+n_of_flights+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1;
			company=crews[crew_day_pool[n_of_cols]->type].company;
			posi=posi+n_of_flights*company;
			for(i=0;i<crew_day_pool[n_of_cols]->n_flights;i++) {
				cmatind[pos]=posi+crew_day_pool[n_of_cols]->flights[i];
				cmatval[pos]=1.0;
				pos++;
			}

			//inserting the column in the constraints linking xyz
			posi=n_of_crew_types+n_of_flights+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1+N_COMPANIES*n_of_flights;
			for(i=crew_day_pool[n_of_cols]->n_flights-1;i>0;i--) {
				ind_start=crew_day_pool[n_of_cols]->flights[i];
				ind_end=crew_day_pool[n_of_cols]->flights[i-1];
				for(j=0;j<successori[ind_start]->n_of_succ;j++) {
					succ=successori[ind_start]->ind_succ[j];
				    if(succ==ind_end) {
						index=successori[ind_start]->arcs[j];
						break;
					}
				}
				cmatind[pos]=posi+index;
				cmatval[pos]=1.0;
				pos++;
			}

			//inserting the column in the constraints linking x y for short arcs
			posi=n_of_crew_types+n_of_flights+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1+N_COMPANIES*n_of_flights+n_of_arcs;
			company=crews[crew_day_pool[n_of_cols]->type].company;
			posi=posi+n_of_short_arcs*company;
			for(i=crew_day_pool[n_of_cols]->n_flights-1;i>0;i--) {
				ind_start=crew_day_pool[n_of_cols]->flights[i];
				ind_end=crew_day_pool[n_of_cols]->flights[i-1];
				for(j=0;j<successori[ind_start]->n_of_succ;j++) {
					succ=successori[ind_start]->ind_succ[j];
				    if(succ==ind_end) {
						if(flight_array[succ]->dep_t-flight_array[ind_start]->arr_t>=30) continue;
						index=indices_short_arcs[ind_start][j];
						cmatind[pos]=posi+index;
						cmatval[pos]=1.0;
						pos++;
						break;
					}
				}
				
			}

			if(pos!=nzcnt_col) stop_error("pos different from nzcnt_col");
								
			status=CPXaddcols(env,lp,1,nzcnt_col,obj_col,cmatbeg,cmatind,cmatval,lb_col,ub_col,NULL);
			if(status) {
				stop_error("CPXaddcols\n");
			}

			free(cmatind);
			free(cmatval);

			n_of_cols++;
			new_cols++;
		}
#endif
	}

		

	/*CPXwriteprob(env,lp,"prova.lp",NULL);
	exit(0);*/

	for(i=0;i<n_of_flights;i++) {
		free(labels[i]);
	}
	free(labels);
	free(ind_labels);
	free(cmatbeg);
	free(obj_col);
	free(lb_col);
	free(ub_col);
	return new_cols;
}


int find_interval(int flight)
{
	int k;

	for(k=0;k<n_of_intervals;k++) {
		if(flight_array[flight]->dep_t>=intervals[k].start && flight_array[flight]->dep_t<=intervals[k].end) {
			return k;
		}
	}
#ifdef RETIME
	if(flight_array[flight]->dep_t<intervals[0].start) return 0;
	if(flight_array[flight]->arr_t>intervals[n_of_intervals-1].end) return n_of_intervals-1;
#endif
	return -1;
}







void branch_and_price (int level, double LB_father)
{
	 int ind;   //the arc on which we branch
     double LB; //LP-relaxation of the node
	 int count; //number of successors to be considered in the first child
	 int* forbidden; //for storing the current situation of the current flight
	 int i;
	 int int_sol_found;
	 int k;
	 int c;
	 int pos,index;
	 int j, dep_airport, arr_airport;
	 char* ctype;
	 int status;
	 int start_node, end_node, number_nodes, node,l;
	 double objval;
	 int* indices;
	 

	 if(ceil(LB_father)>=best_sol) {
		 printf("LB %f >= best sol %d\n",LB_father,best_sol);
		 return;
	 }

	 printf("node %d level %d LB_father %f\n",nodes,level,LB_father);

	 nodes++;

	 printf("solving master...\n");
	 LB=solve_master(); 
	 printf("LB %f\n",LB);

	 if(LB<best_LB) {
		 best_LB=LB;
	 }
	 //CPXwriteprob(env,lp,"prova.lp",NULL);
	 /*exit(0);*/

	 //return;

	 for(c=0;c<n_auxil;c++) {
		 if(x[c]<EPSI) continue;
		 //checking if auxiliary variables are in the solution
		 if(c<n_auxil) {
			 printf("auxiliary variable in solution\n");
			 return;
		 } 
	 } 

	 //if integer sol found, update best_sol
	 int_sol_found=integer_sol_found();
	 if(int_sol_found) {
		 if(LB<(double)best_sol) {
			 best_sol=round_(LB);

			 /*ctype=(char*) calloc(offset+n_of_cols,sizeof(char));
			 if(ctype==NULL) alloc_error("ctype\n");
			 for(i=0;i<offset+n_of_cols;i++) {
				 ctype[i]='B';
			 }
			 status=CPXcopyctype(env,lp,ctype);
			 if(status) {
				 stop_error("CPXcopyctype\n");
			 }*/


			 CPXwriteprob(env,lp,"int.lp",NULL);
			 printf("int sol found %d\n",best_sol);

			 printf("solution\n\n\n");
			 /*for(i=0;i<n_of_cols;i++) {
				 if(x[i+offset]>EPSI) {
					 printf("x%d %f n_flights %d:\n",i+offset,x[i+offset],crew_day_pool[i]->n_flights);
					 for(k=crew_day_pool[i]->n_flights-1;k>=0;k--) {
						 printf("%d, ",flight_array[crew_day_pool[i]->flights[k]]->code);
					 } 
					 printf("\n\n");
				 } 
			 }*/
			 printf("%s\n",our_day);
			 printf("%d\n",best_sol);
			 printf("%f\n",LB);
			 
			 //emptying the y_sol
			 for(i=0;i<n_of_aircraft_types;i++) {
				for(j=0;j<n_of_flights;j++) {
					for(l=0;l<n_of_flights;l++) {
						y_sol[i][j][l]=0;
					} 
				}
			 }
			 pos=n_auxil+n_of_starting_arcs*n_of_aircraft_types;
			 for(j=0;j<n_of_aircraft_types;j++) {
				 for(i=0;i<n_of_flights;i++) {	
					 for(k=0;k<successori_a[i]->n_of_succ;k++) {
						 if(x[pos]<EPSI) {
							 pos++;
							 continue;
						 }
						 /*printf("y_%d_%d_%s_%d_%d %f\n",
							 flight_array[i]->code,
							 flight_array[successori_a[i]->ind_succ[k]]->code,
							 airports[aircrafts[j].dep_airport],
							 aircrafts[j].company,j, x[index]);*/
						 if(aircrafts[j].company==0) {
							 /*printf("B;%d;%d\n",flight_array[i]->code,
							 flight_array[successori_a[i]->ind_succ[k]]->code);*/
							 y_sol[j][i][successori_a[i]->ind_succ[k]]=1;
						 }
						 else if(aircrafts[j].company==1) {
							 /*printf("N;%d;%d\n",flight_array[i]->code,
							 flight_array[successori_a[i]->ind_succ[k]]->code);*/
							 y_sol[j][i][successori_a[i]->ind_succ[k]]=1;
						 }
						 else {
							 /*printf("C;%d;%d\n",flight_array[i]->code,
							 flight_array[successori_a[i]->ind_succ[k]]->code);*/
							 y_sol[j][i][successori_a[i]->ind_succ[k]]=1;
						 }
						 pos++;
					 }
				 }
			 }
			 //printf("\n\n");
			 
			 //printing the solution for y and z
			 //printf("yz\n");
			 index=n_auxil;
			 for(j=0;j<n_of_aircraft_types;j++) {
				 for(k=0;k<N_MAIN_AIRPORTS;k++) {
					 dep_airport=starting_arcs[k]->airport;
					 for(i=0;i<starting_arcs[k]->n_of_succ;i++) {
						 if(x[index]<EPSI) {
							 index++;
							 continue;
						 }
						 /*printf("ys_%d_%s_%d_%d %f\n",
							 flight_array[starting_arcs[k]->ind_succ[i]]->code,
							 airports[dep_airport],aircrafts[j].company,j,x[index]);*/
						 for(l=0;l<n_of_flights;l++) {
							 y_sol[j][l][starting_arcs[k]->ind_succ[i]]=2;
						 }
						 index++;
					 }
				 }
			 }

			 /*for(j=0;j<n_of_aircraft_types;j++) {
				 for(i=0;i<n_of_flights;i++) {	
					 for(k=0;k<successori_a[i]->n_of_succ;k++) {
						 if(x[index]<EPSI) {
							 index++;
							 continue;
						 }
						 printf("y_%d_%d_%s_%d_%d %f\n",
							 flight_array[i]->code,
							 flight_array[successori_a[i]->ind_succ[k]]->code,
							 airports[aircrafts[j].dep_airport],
							 aircrafts[j].company,j, x[index]);
						 index++;
					 }
				 }
			 }
			 for(j=0;j<n_of_aircraft_types;j++) {
				 for(k=0;k<N_MAIN_AIRPORTS;k++) {
					 arr_airport=ending_arcs[k]->airport;
					 for(i=0;i<ending_arcs[k]->n_of_succ;i++) {
						 if(x[index]<EPSI) {
							 index++;
							 continue;
						 }
						 printf("ye_%d_%s_%d_%d %f\n",
							 flight_array[ending_arcs[k]->ind_succ[i]]->code,
							 airports[aircrafts[j].dep_airport],
							 aircrafts[j].company,j, x[index]);
						 index++;
					 }
				 }
			 }
			 if(index!=n_auxil+n_of_y) {
				 printf("index %d non e' %d\n",index,n_auxil+n_of_y);
			 }
			 for(i=0;i<n_of_flights;i++) {
				 for(k=0;k<successori[i]->n_of_succ;k++) {
					 if(x[index]<EPSI) {
						 index++;
						 continue;
					 }
					 printf("z_%d_%d %f\n",flight_array[i]->code,
						 flight_array[successori[i]->ind_succ[k]]->code, x[index]);
					 index++;
				 }
			 }
			 if(index!=n_auxil+n_of_y+n_of_z) {
				 printf("index %d non e'  %d\n",index,n_auxil+n_of_y+n_of_z);
			 }*/

			 for(i=0;i<n_of_aircraft_types;i++) {
				for(j=0;j<n_of_flights;j++) {
					for(l=0;l<n_of_flights;l++) {
						best_y_sol[i][j][l]=y_sol[i][j][l];
					} 
				}
			 }

			 //printing in the format for the picture
			 number_nodes=0;
			 for(j=0;j<n_of_aircraft_types;j++) {
				 for(l=0;l<aircrafts[j].n_of_aircrafts;l++) {
					 start_node=FALSE;
					 for(i=0;i<n_of_flights;i++) {
						 count=0;
						  //looking for a node without entering arcs
						 for(k=0;k<n_of_flights;k++) {
							 if(y_sol[j][k][i]==-1) break;
							 if(y_sol[j][k][i]==1) break;
							 if(y_sol[j][k][i]==2) {
								 count++;
							 }
						 }
						 if(count==n_of_flights) {
							 if(aircrafts[j].company==0) {
								 printf("B;%d",flight_array[i]->code);
							 }
							 else if(aircrafts[j].company==1) {
								 printf("N;%d",flight_array[i]->code);
							 }
							 else if(aircrafts[j].company==2) {
								 printf("C;%d",flight_array[i]->code);
							 }
							 start_node=TRUE;
							 y_sol[j][0][i]=-1;
							 node=i;
							 number_nodes++;
							 break;
						 }					
					 }
					 if(start_node==FALSE) {
						 break;
					 }
					 end_node=FALSE;
					 while(!end_node) {
						 count=0;
						 for(k=0;k<n_of_flights;k++) {
							 if(y_sol[j][node][k]==1) {
								 printf(";%d",flight_array[k]->code);
								 
								 y_sol[j][node][k]=0;
								 node=k;
								 number_nodes++;
								 break;
							 }
							 count++;
						 }
						 if(count==n_of_flights) {
							 printf("\n");
							 end_node=TRUE;
							 if(number_nodes!=n_of_flights) {
								 start_node=FALSE;
							 }
						 }
					 }
				 }
			 }

			 printf("\n---\n");

			 //storing the best solution
			 pos=0;
			 for(i=0;i<n_of_cols;i++) {
				 if(x[i+offset]>EPSI) {
					 best_solution[pos]->n_flights=crew_day_pool[i]->n_flights;
					 best_solution[pos]->type=crew_day_pool[i]->type;
					 index=0;
					 for(k=crew_day_pool[i]->n_flights-1;k>=0;k--) {
						 best_solution[pos]->flights[index]=crew_day_pool[i]->flights[k];
						 printf("%d;",flight_array[crew_day_pool[i]->flights[k]]->code);
						 /*printf("(%d)%s %d-%s %d, ",flight_array[crew_day_pool[i]->flights[k]]->code,
							 airports[flight_array[crew_day_pool[i]->flights[k]]->dep],
							 flight_array[crew_day_pool[i]->flights[k]]->dep_t,
							 airports[flight_array[crew_day_pool[i]->flights[k]]->arr],
							 flight_array[crew_day_pool[i]->flights[k]]->arr_t);*/
						 index++;
					 } 
					 printf("\n");
					 pos++;
				 } 
			 }
			 n_crew_duties=pos;

			 

			 /*free(ctype);
			 ctype=(char*) calloc(offset+n_of_cols,sizeof(char));
			 if(ctype==NULL) alloc_error("ctype\n");
			 for(i=0;i<offset+n_of_cols;i++) {
				 ctype[i]='C';
			 }
			 status=CPXcopyctype(env,lp,ctype);
			 if(status) {
				 stop_error("CPXcopyctype\n");
			 }

			 free(ctype);*/

#if 0

			 exit(0);
#endif
		 }
		 return;
	 }

     if(ceil(LB)>=best_sol) {
		 printf("LB %f >= best sol %d\n",LB,best_sol);
		 return;
	 }
	 

	 //select the node to branch on
     ind=branch_node();
	 if(ind==-1) {
		 CPXwriteprob(env,lp,"int_forse.lp",NULL);
		 ctype=(char*) calloc(offset+n_of_cols,sizeof(char));
		 if(ctype==NULL) alloc_error("ctype\n");
		 indices=(int*) calloc(offset+n_of_cols,sizeof(int));
		 if(indices==NULL) alloc_error("indices\n");
		 for(i=0;i<offset+n_of_cols;i++) {
			 indices[i]=i;
			 ctype[i]='B';
		 }
		 /*status=CPXcopyctype(env,lp,ctype);
		 if(status) {
			 stop_error("CPXcopyctype\n");
		 }*/
		 status=CPXchgctype(env,lp,offset+n_of_cols,indices,ctype);
		 if(status) {
			 stop_error("CPXchgctype\n");
		 }
		 
		 status=CPXmipopt(env,lp);
		 if(status) {
			 stop_error("CPXmipoopt\n");
		 }
		 status=CPXgetstat(env,lp);
		 if(status==101 || status==102) {
			 printf("status %d\n",status);
		 
			 if(x!=NULL) {
				 free(x);
				 x=NULL;
			 }
			 x=(double*) calloc(offset+n_of_cols,sizeof(double));
			 if(x==NULL) alloc_error("x\n");

			 status=CPXgetmipx(env,lp,x,0,CPXgetnumcols(env,lp)-1);
			 if(status) {
				 stop_error("CPXgetmipx\n");
			 }
			 status=CPXgetmipobjval(env,lp,&objval);
			 if(status) {
				 stop_error("CPXgetmipobjval\n");
			 }
			 printf("objval %f\n",objval);

			 best_sol=round_(objval);
			 printf("int sol found %d\n",best_sol);

			 printf("solution\n\n\n");
			 /*for(i=0;i<n_of_cols;i++) {
				 if(x[i+offset]>EPSI) {
					 printf("x%d %f n_flights %d:\n",i+offset,x[i+offset],crew_day_pool[i]->n_flights);
					 for(k=crew_day_pool[i]->n_flights-1;k>=0;k--) {
						 printf("%d, ",flight_array[crew_day_pool[i]->flights[k]]->code);
					 } 
					 printf("\n\n");
				 } 
			 }*/
			 printf("%s\n",our_day);
			 printf("%d\n",best_sol);
			 printf("%f\n",LB);
			 
			 
			 //emptying the y_sol
			 for(i=0;i<n_of_aircraft_types;i++) {
				for(j=0;j<n_of_flights;j++) {
					for(l=0;l<n_of_flights;l++) {
						y_sol[i][j][l]=0;
					} 
				}
			 }
			 pos=n_auxil+n_of_starting_arcs*n_of_aircraft_types;
			 for(j=0;j<n_of_aircraft_types;j++) {
				 for(i=0;i<n_of_flights;i++) {	
					 for(k=0;k<successori_a[i]->n_of_succ;k++) {
						 if(x[pos]<EPSI) {
							 pos++;
							 continue;
						 }
						 /*printf("y_%d_%d_%s_%d_%d %f\n",
							 flight_array[i]->code,
							 flight_array[successori_a[i]->ind_succ[k]]->code,
							 airports[aircrafts[j].dep_airport],
							 aircrafts[j].company,j, x[index]);*/
						 if(aircrafts[j].company==0) {
							 /*printf("B;%d;%d\n",flight_array[i]->code,
							 flight_array[successori_a[i]->ind_succ[k]]->code);*/
							 y_sol[j][i][successori_a[i]->ind_succ[k]]=1;
						 }
						 else if(aircrafts[j].company==1) {
							 /*printf("N;%d;%d\n",flight_array[i]->code,
							 flight_array[successori_a[i]->ind_succ[k]]->code);*/
							 y_sol[j][i][successori_a[i]->ind_succ[k]]=1;
						 }
						 else {
							 /*printf("C;%d;%d\n",flight_array[i]->code,
							 flight_array[successori_a[i]->ind_succ[k]]->code);*/
							 y_sol[j][i][successori_a[i]->ind_succ[k]]=1;
						 }
						 pos++;
					 }
				 }
			 }
			 //printf("\n\n");
			 
			 //printing the solution for y and z
			 //printf("yz\n");
			 index=n_auxil;
			 for(j=0;j<n_of_aircraft_types;j++) {
				 for(k=0;k<N_MAIN_AIRPORTS;k++) {
					 dep_airport=starting_arcs[k]->airport;
					 for(i=0;i<starting_arcs[k]->n_of_succ;i++) {
						 if(x[index]<EPSI) {
							 index++;
							 continue;
						 }
						 /*printf("ys_%d_%s_%d_%d %f\n",
							 flight_array[starting_arcs[k]->ind_succ[i]]->code,
							 airports[dep_airport],aircrafts[j].company,j,x[index]);*/
						 for(l=0;l<n_of_flights;l++) {
							 y_sol[j][l][starting_arcs[k]->ind_succ[i]]=2;
						 }
						 index++;
					 }
				 }
			 }

			 /*for(j=0;j<n_of_aircraft_types;j++) {
				 for(i=0;i<n_of_flights;i++) {	
					 for(k=0;k<successori_a[i]->n_of_succ;k++) {
						 if(x[index]<EPSI) {
							 index++;
							 continue;
						 }
						 printf("y_%d_%d_%s_%d_%d %f\n",
							 flight_array[i]->code,
							 flight_array[successori_a[i]->ind_succ[k]]->code,
							 airports[aircrafts[j].dep_airport],
							 aircrafts[j].company,j, x[index]);
						 index++;
					 }
				 }
			 }
			 for(j=0;j<n_of_aircraft_types;j++) {
				 for(k=0;k<N_MAIN_AIRPORTS;k++) {
					 arr_airport=ending_arcs[k]->airport;
					 for(i=0;i<ending_arcs[k]->n_of_succ;i++) {
						 if(x[index]<EPSI) {
							 index++;
							 continue;
						 }
						 printf("ye_%d_%s_%d_%d %f\n",
							 flight_array[ending_arcs[k]->ind_succ[i]]->code,
							 airports[aircrafts[j].dep_airport],
							 aircrafts[j].company,j, x[index]);
						 index++;
					 }
				 }
			 }
			 if(index!=n_auxil+n_of_y) {
				 printf("index %d non e' %d\n",index,n_auxil+n_of_y);
			 }
			 for(i=0;i<n_of_flights;i++) {
				 for(k=0;k<successori[i]->n_of_succ;k++) {
					 if(x[index]<EPSI) {
						 index++;
						 continue;
					 }
					 printf("z_%d_%d %f\n",flight_array[i]->code,
						 flight_array[successori[i]->ind_succ[k]]->code, x[index]);
					 index++;
				 }
			 }
			 if(index!=n_auxil+n_of_y+n_of_z) {
				 printf("index %d non e'  %d\n",index,n_auxil+n_of_y+n_of_z);
			 }*/

			 for(i=0;i<n_of_aircraft_types;i++) {
				for(j=0;j<n_of_flights;j++) {
					for(l=0;l<n_of_flights;l++) {
						best_y_sol[i][j][l]=y_sol[i][j][l];
					} 
				}
			 }

			 //printing in the format for the picture
			 number_nodes=0;
			 for(j=0;j<n_of_aircraft_types;j++) {
				 for(l=0;l<aircrafts[j].n_of_aircrafts;l++) {
					 start_node=FALSE;
					 for(i=0;i<n_of_flights;i++) {
						 count=0;
						  //looking for a node without entering arcs
						 for(k=0;k<n_of_flights;k++) {
							 if(y_sol[j][k][i]==-1) break;
							 if(y_sol[j][k][i]==1) break;
							 if(y_sol[j][k][i]==2) {
								 count++;
							 }
						 }
						 if(count==n_of_flights) {
							 if(aircrafts[j].company==0) {
								 printf("B;%d",flight_array[i]->code);
							 }
							 else if(aircrafts[j].company==1) {
								 printf("N;%d",flight_array[i]->code);
							 }
							 else if(aircrafts[j].company==2) {
								 printf("C;%d",flight_array[i]->code);
							 }
							 start_node=TRUE;
							 y_sol[j][0][i]=-1;
							 node=i;
							 number_nodes++;
							 break;
						 }					
					 }
					 if(start_node==FALSE) {
						 break;
					 }
					 end_node=FALSE;
					 while(!end_node) {
						 count=0;
						 for(k=0;k<n_of_flights;k++) {
							 if(y_sol[j][node][k]==1) {
								 printf(";%d",flight_array[k]->code);
								 
								 y_sol[j][node][k]=0;
								 node=k;
								 number_nodes++;
								 break;
							 }
							 count++;
						 }
						 if(count==n_of_flights) {
							 printf("\n");
							 end_node=TRUE;
							 if(number_nodes!=n_of_flights) {
								 start_node=FALSE;
							 }
						 }
					 }
				 }
			 }
			 printf("\n---\n");

			 //storing the best solution
			 pos=0;
			 for(i=0;i<n_of_cols;i++) {
				 if(x[i+offset]>EPSI) {
					 best_solution[pos]->n_flights=crew_day_pool[i]->n_flights;
					 best_solution[pos]->type=crew_day_pool[i]->type;
					 index=0;
					 for(k=crew_day_pool[i]->n_flights-1;k>=0;k--) {
						 best_solution[pos]->flights[index]=crew_day_pool[i]->flights[k];
						 printf("%d;",flight_array[crew_day_pool[i]->flights[k]]->code);
						 /*printf("(%d)%s %d-%s %d, ",flight_array[crew_day_pool[i]->flights[k]]->code,
							 airports[flight_array[crew_day_pool[i]->flights[k]]->dep],
							 flight_array[crew_day_pool[i]->flights[k]]->dep_t,
							 airports[flight_array[crew_day_pool[i]->flights[k]]->arr],
							 flight_array[crew_day_pool[i]->flights[k]]->arr_t);*/
						 index++;
					 } 
					 printf("\n");
					 pos++;
				 } 
			 }
			 n_crew_duties=pos;

			 
			 if(x!=NULL) {
				 free(x);
				 x=NULL;
			 }

#if 0
			exit(0);
#endif


			 if(objval>=LB-EPSI) {
				 printf("objval %f = LB %f\n",objval,LB);
				 
				 

				 for(i=0;i<offset+n_of_cols;i++) {
					 ctype[i]='C';
					 indices[i]=i;
				 }
			 
				 status=CPXchgctype(env,lp,offset+n_of_cols,indices,ctype);
				 if(status) {
					 stop_error("CPXchgctype\n");
				 }
				 
				 status = CPXchgprobtype (env, lp, CPXPROB_LP);
				 if(status) {
					 stop_error("CPXchgprobtype\n");
				 }

				 free(ctype);
				 free(indices);
				 return;
			 }
			 else {
				 printf("objval %f diverso da LB %f\n",objval,LB);
				 free(ctype);
				 stop_error("no node to branch on\n");
			 }
		 }
		 else {
			 printf("status %d\n",status);
			 free(ctype);
			 stop_error("no node to branch on");
		 }
		 //free(ctype);	 
	 }

	 printf("branching on node %d\n",flight_array[ind]->code);

	 //select the set of arcs to forbid in the first child
	 count=branch_arcs(ind);

	 printf("forbid arcs 0-%d\n",count);

	 //store the current graph
	 forbidden=(int*) calloc(successori[ind]->n_of_succ,sizeof(int));
	 if(forbidden==NULL) alloc_error("forbidden\n");
	 for(i=0;i<successori[ind]->n_of_succ;i++) {
		 forbidden[i]=successori[ind]->forb[i];
	 }

	 

	 //set the forbidden succ in the new graph
	 for(i=0;i<=count;i++) {
		 successori[ind]->forb[i]=TRUE;
	 }

	 //forbid the node ind as terminal
	 not_terminal[ind]=TRUE;

	 printf("node %d not terminal\n",flight_array[ind]->code);

	 //set the UB of the columns passing through the forbidden succ to 0
	 

	 printf("forbidding columns...\n");
	 forbid_cols(ind,level,TRUE);
     
	 //recursive call to depth first: first child
	 branch_and_price(level+1,LB); 


	 printf("unforb arcs\n");
	 //setting back the old graph
	 for(i=0;i<successori[ind]->n_of_succ;i++) {
		 successori[ind]->forb[i]=forbidden[i];
	 }

	 
	 printf("unforbidding columns...\n");
	 //setting back the UB of the columns
	 unforbid_cols(level);

	 //set back the node as possible terminal
	 not_terminal[ind]=FALSE;

	 printf("node %d can be terminal\n",flight_array[ind]->code);
	 
	 
	 printf("back to level %d...\n",level);

	 if(ceil(LB)>=best_sol) {
		 printf("LB %f >= best sol %d\n",LB,best_sol);
		 return;
	 }
 
	 
	 //set the forbidden succ in the new graph
	 for(i=count+1;i<successori[ind]->n_of_succ;i++) {
		 successori[ind]->forb[i]=TRUE;
	 }

	 printf("node %d forbid arcs %d-%d\n",flight_array[ind]->code,
		 count+1,successori[ind]->n_of_succ);

	 

	 //set the UB of the columns passing through the forbidden succ to 0
	 printf("forbidding columns...\n");
	 forbid_cols(ind,level,FALSE);
	
	 //recursive call to depth first: second child 
	 branch_and_price(level+1,LB); 
  
	 //setting back the old graph
	 printf("unforb arcs\n");
	 for(i=0;i<successori[ind]->n_of_succ;i++) {
		 successori[ind]->forb[i]=forbidden[i];
	 }

	 
	 printf("unforbidding columns...\n");
	 //setting back the UB of the columns
	 unforbid_cols(level);

	 printf("back to level %d...\n",level);

	 free(forbidden);
	 
	 

}

int branch_node()
{
	int i,j;
	int c;
	int ind_start, ind_end; //estremi dell'arco
	int succ;
	int found;
	int count;
	int most_fract;
	int sel_node;

	
	for(i=0;i<n_of_flights;i++) {
		for(j=0;j<successori[i]->n_of_succ;j++) {
			successori[i]->x_values[j]=0.0;
		}
	}
	//construct the values of the flows along the arcs, starting from the path vars x
	for(c=0;c<n_of_cols;c++) {
		if(x[c+offset]<EPSI) continue;
		if(x[c+offset]>1.0-EPSI) continue;
		for(i=crew_day_pool[c]->n_flights-1;i>0;i--) {
			ind_start=crew_day_pool[c]->flights[i];
			ind_end=crew_day_pool[c]->flights[i-1];
			found=FALSE;
			for(j=0;j<successori[ind_start]->n_of_succ;j++) {
				succ=successori[ind_start]->ind_succ[j];
				if(succ==ind_end) {
					successori[ind_start]->x_values[j]=successori[ind_start]->x_values[j]+x[c+offset];
					if(successori[ind_start]->x_values[j]>1.0+EPSI) {
						printf("error\n");
					}
					found=TRUE;
					break;
				}
			} 
			if(!found) {
				stop_error("succ not found\n");
			}
		}
		
	}

	//select the arc for branching
	sel_node=-1;
	most_fract=0;
	for(i=0;i<n_of_flights;i++) {
		count=0;
		for(j=0;j<successori[i]->n_of_succ;j++) {
			if(successori[i]->x_values[j]>EPSI && successori[i]->x_values[j]<1.0-EPSI) {
				count++;
			}
		}
		if(count>most_fract) {
			most_fract=count;
			sel_node=i;
		}
	}

	/*if(sel_node==-1) {
		for(c=0;c<offset+n_of_cols;c++) {
			if(x[c]<EPSI) continue;
			if(c<n_auxil) {
				stop_error("auxiliary var in solution\n");
			}	 
			//skip the y vars
			if(c<offset) {
				continue;
			}
			if(x[c]>=1.0-EPSI) {
			}
			else {
				found=FALSE;
				printf("var frazionaria x[%d] %f type %d\n",c,x[c],
					crew_day_pool[c-offset]->type);
				for(i=crew_day_pool[c-offset]->n_flights-1;i>0;i--) {
					ind_start=crew_day_pool[c-offset]->flights[i];
					ind_end=crew_day_pool[c-offset]->flights[i-1];
					printf("volo %d volo %d\n",flight_array[ind_start]->code,
						flight_array[ind_end]->code);
					for(j=0;j<successori[ind_start]->n_of_succ;j++) {
						printf("succ %f ",successori[ind_start]->x_values[j]);
					}
					printf("\n");
				}
			}
		}
	}*/

	return sel_node;
}



void init_branch_and_price()
{
	int max_crew_sol;
	int i,j;

	nodes=0;
	level=0;
	best_sol=INF;

	//best_sol=36030;

	printf("imposing best sol = %d\n",best_sol);

	not_terminal=(int*) calloc(n_of_flights,sizeof(int));
	if(not_terminal==NULL) alloc_error("not_terminal\n");

	//init best solution
	max_crew_sol=0;
	for(i=0;i<n_of_crew_types;i++) {
		max_crew_sol=max_crew_sol+crews[i].n_of_crews;
	}
	best_solution=(crew_day**) calloc(max_crew_sol,sizeof(crew_day*));
	if(best_solution==NULL) alloc_error("best_solution\n"); 
	for(i=0;i<max_crew_sol;i++) {
		best_solution[i]=(crew_day*) calloc(1,sizeof(crew_day));
		if(best_solution[i]==NULL) alloc_error("best_solution[i]\n");
		best_solution[i]->flights=(int*) calloc(8,sizeof(int));
		if(best_solution[i]->flights==NULL) alloc_error("best_solution[i]->flights\n");
	}

	//initializing the matrix for the y solutions
	y_sol=(int***) calloc(n_of_aircraft_types,sizeof(int**));
	if(y_sol==NULL) alloc_error("y_sol\n");
	for(i=0;i<n_of_aircraft_types;i++) {
		y_sol[i]=(int**) calloc(n_of_flights,sizeof(int*));
		if(y_sol[i]==NULL) alloc_error("y_sol[i]\n");
		for(j=0;j<n_of_flights;j++) {
			y_sol[i][j]=(int*) calloc(n_of_flights,sizeof(int));
			if(y_sol[i][j]==NULL) alloc_error("y_sol[i][j]\n");
		}
	}

	//initializing the matrix for the best y solution
	best_y_sol=(int***) calloc(n_of_aircraft_types,sizeof(int**));
	if(best_y_sol==NULL) alloc_error("best_y_sol\n");
	for(i=0;i<n_of_aircraft_types;i++) {
		best_y_sol[i]=(int**) calloc(n_of_flights,sizeof(int*));
		if(best_y_sol[i]==NULL) alloc_error("best_y_sol[i]\n");
		for(j=0;j<n_of_flights;j++) {
			best_y_sol[i][j]=(int*) calloc(n_of_flights,sizeof(int));
			if(best_y_sol[i][j]==NULL) alloc_error("best_y_sol[i][j]\n");
		}
	}

	best_LB=(double)INF;

	min_dep_time_A = INF;
	min_dep_time_S = INF;
	max_arr_time_A = 0;
	max_arr_time_S = 0;
	for( i=0; i<n_of_flights; i++ )
	{
		if( flight_array[i]->dep == 7 && flight_array[i]->dep_t < min_dep_time_A ) 
			min_dep_time_A = flight_array[i]->dep_t;
		if( flight_array[i]->dep == 3 && flight_array[i]->dep_t < min_dep_time_S ) 
			min_dep_time_S = flight_array[i]->dep_t;
		if( flight_array[i]->arr== 7 && flight_array[i]->arr_t > max_arr_time_A ) 
			max_arr_time_A = flight_array[i]->arr_t;
		if( flight_array[i]->arr == 3 && flight_array[i]->arr_t > max_arr_time_S ) 
			max_arr_time_S = flight_array[i]->arr_t;
	}

#if 0

	printf("solving master...\n");
	 LB=solve_master(); 
	 printf("LB %f\n",LB);
	 exit(0);
#endif


	branch_and_price(level,0);

	printf("best sol %d\n",best_sol);

	print_best_solution();

	free(not_terminal);

	for(i=0;i<max_crew_sol;i++) {
		free(best_solution[i]->flights);
		free(best_solution[i]);
	}
	free(best_solution);

	for(i=0;i<n_of_aircraft_types;i++) {
		for(j=0;j<n_of_flights;j++) {
			free(y_sol[i][j]);
		}
		free(y_sol[i]);
	}
	free(y_sol);

	for(i=0;i<n_of_aircraft_types;i++) {
		for(j=0;j<n_of_flights;j++) {
			free(best_y_sol[i][j]);
		}
		free(best_y_sol[i]);
	}
	free(best_y_sol);

}

int branch_arcs(int ind_node)
{
	int i;
	double tot_value;
	double half_value;
	double acc_value;

	tot_value=0.0;
	half_value=0.0;

	for(i=0;i<successori[ind_node]->n_of_succ;i++) {
		tot_value=tot_value+successori[ind_node]->x_values[i];
	}

	printf("tot value %f tot_num_succ %d\n",tot_value,successori[ind_node]->n_of_succ);

	half_value=tot_value/2;

	acc_value=0.0;
	for(i=0;i<successori[ind_node]->n_of_succ;i++) {
		acc_value=acc_value+successori[ind_node]->x_values[i];
		printf("succ %d x_value %f, ",flight_array[successori[ind_node]->ind_succ[i]]->code,
			successori[ind_node]->x_values[i]);
		if(acc_value>=half_value) break;
	}

	printf("\n");

	if(acc_value>=tot_value-EPSI) {
		i=i-1;
	}

	return i;
}

//checks integrality of the x vars only
int integer_sol_found()
{
	int i;
	int found;

	found=TRUE;
	for(i=0;i<offset+n_of_cols;i++) {
		if(x[i]<EPSI) continue;
		if(i<n_auxil) {
			stop_error("auxiliary var in solution\n");
		}
		//skip the y vars
		if(i<offset) {
			continue;
		}
		if(x[i]>=1.0-EPSI) {
		}
		else {
			found=FALSE;
			printf("var frazionaria x[%d] %f\n",i,x[i]);
			break;
		}
	}

	return found;
}

void forbid_cols(int ind_node, int level, int flag_not_terminal_ind)
{
	int i,j;
	int c;
	int succ;
	int ind_start, ind_end;
	int status;
	int indices[1];
	char lu[1];
	double bd[1];
	double ub[1];

	for(i=0;i<successori[ind_node]->n_of_succ;i++) {
		succ=successori[ind_node]->ind_succ[i];
		if(!successori[ind_node]->forb[i]) continue;
		for(c=0;c<n_of_cols;c++) {
			for(j=crew_day_pool[c]->n_flights-1;j>0;j--) {
				ind_start=crew_day_pool[c]->flights[j];
			    ind_end=crew_day_pool[c]->flights[j-1];
				if(ind_start==ind_node && ind_end==succ) {
					//if the column was already forbidden at a previous level, do not
					//change the level because otherwise it becomes unforbidden
					//too early in the backtracking
					status=CPXgetub(env,lp,ub,c+offset,c+offset);
					if(status) {
						stop_error("CPXgetub\n");
					}
					if(ub[0]==0.0) continue;
					//set the UB to 0
					crew_day_pool[c]->forb=level;
					indices[0]=c+offset;
					lu[0]='U';
					bd[0]=0.0;
					status=CPXchgbds(env,lp,1,indices,lu,bd);
					if(status) {
						stop_error("CPXchgbds\n");
					}
					printf("col %d arc %d-%d\n",c+offset,
						flight_array[ind_start]->code,
						flight_array[ind_end]->code);
					break;
				}
			}
		}
	}

	if(flag_not_terminal_ind) {
		//forbid columns that terminate in ind
		for(c=0;c<n_of_cols;c++) {
			ind_end=crew_day_pool[c]->flights[0];
			if(ind_end==ind_node) {
				status=CPXgetub(env,lp,ub,c+offset,c+offset);
				if(status) {
					stop_error("CPXgetub\n");
				}
				if(ub[0]==0.0) continue;
				//set the UB to 0
				crew_day_pool[c]->forb=level;
				indices[0]=c+offset;
				lu[0]='U';
				bd[0]=0.0;
				status=CPXchgbds(env,lp,1,indices,lu,bd);
				if(status) {
					stop_error("CPXchgbds\n");
				}
				printf("col %d terminal %d\n",c+offset,
					flight_array[ind_end]->code);
			}
		}
	}
}

void unforbid_cols(int level) 
{
	int i;
	int status;
	int indices[1];
	char lu[1];
	double bd[1];

	for(i=0;i<n_of_cols;i++) {
		if(crew_day_pool[i]->forb==level) {
			//set the UB to 1
			indices[0]=i+offset;
			lu[0]='U';
			bd[0]=1.0;
			status=CPXchgbds(env,lp,1,indices,lu,bd);
			if(status) {
				stop_error("CPXchgbds\n");
			}
			crew_day_pool[i]->forb=-1;
			printf("col %d, ",i+offset);
		 }
	 }
	printf("\n");
}

int round_(double x) {
	if(x>=0) {
		return (long) (x+0.5);
	}
	else {
		return (long) (x-0.5);
	}
} 

void print_best_solution() {

	int i,j,k,l;
	int count;
	int number_nodes;
	int start_node;
	int node;
	int end_node;
	FILE* sol_file;

	sol_file=fopen(name_solution_file, "w");
	if(sol_file==NULL) stop_error("main cannot open solution file\n");

	fprintf(sol_file,"%s\n",our_day);
	fprintf(sol_file,"%d\n",best_sol);
	fprintf(sol_file,"%f\n",best_LB);

	//printing in the format for the picture the best solution y
	number_nodes=0;
	for(j=0;j<n_of_aircraft_types;j++) {
		for(l=0;l<aircrafts[j].n_of_aircrafts;l++) {
			start_node=FALSE;
			for(i=0;i<n_of_flights;i++) {
				count=0;
				//looking for a node without entering arcs
				for(k=0;k<n_of_flights;k++) {
					if(best_y_sol[j][k][i]==-1) break;
					if(best_y_sol[j][k][i]==1) break;
					if(best_y_sol[j][k][i]==2) {
						count++;
					} 
				} 
				if(count==n_of_flights) {
					if(aircrafts[j].company==0) {
						fprintf(sol_file,"B;%d",flight_array[i]->code);
					} 
					else if(aircrafts[j].company==1) {
						fprintf(sol_file,"N;%d",flight_array[i]->code);
					}
					else if(aircrafts[j].company==2) {
						fprintf(sol_file,"C;%d",flight_array[i]->code);
					}
					start_node=TRUE;
					best_y_sol[j][0][i]=-1;
					node=i;
					number_nodes++;
					break;
				}					
			}
			if(start_node==FALSE) {
				break;
			}
			end_node=FALSE;
			while(!end_node) {
				count=0;
				for(k=0;k<n_of_flights;k++) {
					if(best_y_sol[j][node][k]==1) {
						fprintf(sol_file,";%d",flight_array[k]->code);
						
						best_y_sol[j][node][k]=0;
						node=k;
						number_nodes++;
						break;
					}
				    count++;
				}
				if(count==n_of_flights) {
					fprintf(sol_file,"\n");
					end_node=TRUE;
					if(number_nodes!=n_of_flights) {
						start_node=FALSE;
					}
				} 
			}
		} 
	} 
	fprintf(sol_file,"\n---\n");

	//printing the best solution x
	for(i=0;i<n_crew_duties;i++) {
		for(k=best_solution[i]->n_flights-1;k>=0;k--) {
			fprintf(sol_file,"%d;",flight_array[best_solution[i]->flights[k]]->code);
		}
		fprintf(sol_file,"\n");
	}  
	 

	fclose(sol_file);
}

//checking if flight is the first leaving from A
int first_flight_A(int flight) {
	
	int i;
	int dep_time;

	dep_time=flight_array[flight]->dep_t;

	for(i=0;i<n_of_flights;i++) {
		if(i==flight) continue;
		if(flight_array[i]->dep!=7) continue;
		if(flight_array[i]->dep_t<dep_time) {
#ifdef RETIME
			if(are_copies(i,flight)) return TRUE;
#endif
			return FALSE;
		}
	}

	return TRUE;
}

//checking if flight is the last arriving in A
int last_flight_A(int flight) {
	
	int i;
	int arr_time;

	arr_time=flight_array[flight]->arr_t;

	for(i=0;i<n_of_flights;i++) {
		if(i==flight) continue;
		if(flight_array[i]->arr!=7) continue;
		if(flight_array[i]->arr_t>arr_time) {
#ifdef RETIME
			if(are_copies(i,flight)) return TRUE;
#endif
			return FALSE;
		}
	}

	return TRUE;
}


void find_int_sol(int solo_lp)
{
	char* ctype;
	int* indices;
	int i,k;
	int status;
	double objval;
	int ind_var;
	int pos, index;
	int company;
	int time_limit;
	int j,l;
	int dep_airport;
	int number_nodes;
	int start_node;
	int count;
	int node;
	int end_node;
	time_t start_IP, end_IP;
	double* values;
	int* indices2;
	
	time_t start_LP,end_LP;
	double durata_LP;
	int lpstat;
	
#ifdef LOG_FILE_CPLEX_ALL_DP1
	CPXFILEptr fp;
#endif
	

	time(&start_IP);

	ctype=(char*) calloc(offset+n_of_cols,sizeof(char));
	if(ctype==NULL) alloc_error("ctype\n");
	indices=(int*) calloc(offset+n_of_cols,sizeof(int));
	if(indices==NULL) alloc_error("indices\n");
	for(i=0;i<offset+n_of_cols;i++) {
		indices[i]=i;
		ctype[i]='B';
	}
	if(!solo_lp) {
		status=CPXchgctype(env,lp,offset+n_of_cols,indices,ctype);
		if(status) {
			stop_error("CPXchgctype\n");
		}
	}

	values=(double*) calloc(n_of_z,sizeof(double));
	if(values==NULL) alloc_error("values\n");
	indices2=(int*) calloc(n_of_z,sizeof(int));
	if(indices2==NULL) alloc_error("indices2\n");
	for(i=0;i<n_of_z;i++) {
		indices2[i]=offset-n_of_z+i;
		values[i]=10.0;
	}

	/*status = CPXchgobj (env, lp, n_of_z, indices2, values);
	if(status) {
		stop_error("CPXchgobj\n");
	}*/
	free(values);
	free(indices2);


//	CPXwriteprob(env,lp,"heur.lp",NULL);
	//exit(0);

	/*if((int)durata_cplex<TIME_LIMIT) {
		time_limit=TIME_LIMIT+(TIME_LIMIT-(int)durata_cplex);
	}
	else {
		time_limit=TIME_LIMIT;
	}*/

	time_limit=TIME_LIMIT;

	if(!solo_lp) {
		status = CPXsetdblparam(env, CPX_PARAM_TILIM, (double)time_limit);  
		if(status) {
			printf("error in setdblparam\n");
			stop_error("");
		}
	}

	if(solo_lp) {
		time(&start_LP);
		
		status=CPXbaropt(env,lp);
		if(status) {
			stop_error("CPXprimopt\n");
		}
		time(&end_LP);
		durata_LP=difftime(end_LP,start_LP);
		printf("durata LP %f secondi\n",durata_LP);
	
	
		if(x!=NULL) {
			free(x);
			x=NULL;
		}
		x=(double*) calloc(offset+n_of_cols,sizeof(double));
		if(x==NULL) alloc_error("x\n");
	
		status=CPXsolution(env,lp,&lpstat,&objval,x,NULL,NULL,NULL);
		if(status) {
			printf("status %d lpstat %d\n",status,lpstat);
			stop_error("CPXsolution\n");
		}
		printf("objective %f lpstat %d\n",objval,lpstat);
		printf("number of created columns %d\n",n_of_cols);

		return;
	}


	//qui
#ifdef LOG_FILE_CPLEX_ALL_DP1
	fp = CPXfopen ("mylog.log", "w");
	status = CPXsetlogfile (env, fp);
	if(status) {
		stop_error("CPXsetlogfile\n");
	}
#endif



	 
	status=CPXmipopt(env,lp);
	if(status) {
		stop_error("CPXmipoopt\n");
	} 
	status=CPXgetstat(env,lp);
	if(status==101 || status==102 || status==107) {
		printf("status %d\n",status);
	 
		if(x!=NULL) {
			free(x);
			x=NULL;
		}
		x=(double*) calloc(offset+n_of_cols,sizeof(double));
		if(x==NULL) alloc_error("x\n");

		status=CPXgetmipx(env,lp,x,0,CPXgetnumcols(env,lp)-1);
		if(status) {
			stop_error("CPXgetmipx\n");
		}
		status=CPXgetmipobjval(env,lp,&objval);
		if(status) {
			stop_error("CPXgetmipobjval\n");
		} 
		printf("objval %f\n",objval);

		best_sol=round_(objval);
		printf("int sol found %d\n",best_sol);

		printf("solution\n\n\n");
			 
		printf("%s\n",our_day);
		printf("%d\n",best_sol);
		
		 
		//emptying the y_sol
		for(i=0;i<n_of_aircraft_types;i++) {
			for(j=0;j<n_of_flights;j++) {
				for(l=0;l<n_of_flights;l++) {
					y_sol[i][j][l]=0;
				} 
			}
		} 
		pos=n_auxil+n_of_starting_arcs*n_of_aircraft_types;
		for(j=0;j<n_of_aircraft_types;j++) {
			for(i=0;i<n_of_flights;i++) {	
				for(k=0;k<successori_a[i]->n_of_succ;k++) {
					if(x[pos]<EPSI) {
						pos++;
						continue;
					} 
					 /*printf("y_%d_%d_%s_%d_%d %f\n",
						 flight_array[i]->code,
						 flight_array[successori_a[i]->ind_succ[k]]->code,
						 airports[aircrafts[j].dep_airport],
						 aircrafts[j].company,j, x[index]);*/
					if(aircrafts[j].company==0) {
						 /*printf("B;%d;%d\n",flight_array[i]->code,
						 flight_array[successori_a[i]->ind_succ[k]]->code);*/
						y_sol[j][i][successori_a[i]->ind_succ[k]]=1;
					}
					else if(aircrafts[j].company==1) {
						 /*printf("N;%d;%d\n",flight_array[i]->code,
						 flight_array[successori_a[i]->ind_succ[k]]->code);*/
						y_sol[j][i][successori_a[i]->ind_succ[k]]=1;
					}
					else {
						 /*printf("C;%d;%d\n",flight_array[i]->code,
						 flight_array[successori_a[i]->ind_succ[k]]->code);*/
					    y_sol[j][i][successori_a[i]->ind_succ[k]]=1;
					}
					pos++;
				}
			}
		}
		//printf("\n\n");
		 
		//printing the solution for y and z
		//printf("yz\n");
		index=n_auxil;
		for(j=0;j<n_of_aircraft_types;j++) {
			for(k=0;k<N_MAIN_AIRPORTS;k++) {
				dep_airport=starting_arcs[k]->airport;
				for(i=0;i<starting_arcs[k]->n_of_succ;i++) {
					if(x[index]<EPSI) {
						index++;
						continue;
					}
					 /*printf("ys_%d_%s_%d_%d %f\n",
						 flight_array[starting_arcs[k]->ind_succ[i]]->code,
						 airports[dep_airport],aircrafts[j].company,j,x[index]);*/
					for(l=0;l<n_of_flights;l++) {
						y_sol[j][l][starting_arcs[k]->ind_succ[i]]=2;
					}
					index++;
				}
			}
		}

		 

		for(i=0;i<n_of_aircraft_types;i++) {
			for(j=0;j<n_of_flights;j++) {
				for(l=0;l<n_of_flights;l++) {
					best_y_sol[i][j][l]=y_sol[i][j][l];
				}  
			}
		}

    	//printing in the format for the picture
		number_nodes=0;
		for(j=0;j<n_of_aircraft_types;j++) {
			for(l=0;l<aircrafts[j].n_of_aircrafts;l++) {
				start_node=FALSE;
				for(i=0;i<n_of_flights;i++) {
					count=0;
					//looking for a node without entering arcs
					for(k=0;k<n_of_flights;k++) {
						if(y_sol[j][k][i]==-1) break;
						if(y_sol[j][k][i]==1) break;
						if(y_sol[j][k][i]==2) {
							count++;
						}
					}
					if(count==n_of_flights) {
						if(aircrafts[j].company==0) {
							printf("B;%d",flight_array[i]->code);
						}
						else if(aircrafts[j].company==1) {
							printf("N;%d",flight_array[i]->code);
						}
						else if(aircrafts[j].company==2) {
							printf("C;%d",flight_array[i]->code);
						}
						start_node=TRUE;
						y_sol[j][0][i]=-1;
						node=i;
						number_nodes++;
						break;
					}					
				}
				if(start_node==FALSE) {
					break;
				}
				end_node=FALSE;
				while(!end_node) {
					count=0;
					for(k=0;k<n_of_flights;k++) {
						if(y_sol[j][node][k]==1) {
							printf(";%d",flight_array[k]->code);
							
							y_sol[j][node][k]=0;
							node=k;
							number_nodes++;
							break;
						}
					    count++;
					}
					if(count==n_of_flights) {
						printf("\n");
						end_node=TRUE;
						if(number_nodes!=n_of_flights) {
							start_node=FALSE;
						}
					}
				}
			}
		}

		printf("\n---\n");

		//storing the best solution
		pos=0;
		for(i=0;i<n_of_cols;i++) {
			if(x[i+offset]>EPSI) {
				best_solution[pos]->n_flights=crew_day_pool[i]->n_flights;
				best_solution[pos]->type=crew_day_pool[i]->type;
				index=0;
				for(k=crew_day_pool[i]->n_flights-1;k>=0;k--) {
					best_solution[pos]->flights[index]=crew_day_pool[i]->flights[k];
					printf("%d;",flight_array[crew_day_pool[i]->flights[k]]->code);
					 /*printf("(%d)%s %d-%s %d, ",flight_array[crew_day_pool[i]->flights[k]]->code,
						 airports[flight_array[crew_day_pool[i]->flights[k]]->dep],
						 flight_array[crew_day_pool[i]->flights[k]]->dep_t,
						 airports[flight_array[crew_day_pool[i]->flights[k]]->arr],
						 flight_array[crew_day_pool[i]->flights[k]]->arr_t);*/
					index++;
				} 
				printf("\n");
				pos++;
			} 
		}
		n_crew_duties=pos;

		
		if(x!=NULL) {
			free(x);
			x=NULL;
		}

	}
	
	else {
		printf("status %d\n",status);
		free(ctype);
		stop_error("no int sol found");
	}
    free(ctype);
    time(&end_IP);
    printf("durata IP %f secondi\n",difftime(end_IP,start_IP));

#ifdef ALL_COLS_IN_GAP
	BestUB=objval;
#endif

#ifdef ALL_COLS_AND_Y_IN_GAP
	BestUB=objval;
#endif
	
#ifdef LOG_FILE_CPLEX_ALL_DP1
    CPXfclose(fp);
#endif
}

#ifdef ALL_DP1
int solve_pricing_all_dp()
{
	int i,k,j,li,nl;
	int new_cols;
	int succ;
	double best_profit;
	int best_node;
	int best_label;
	int ind, ind_l;
	int count;
	int tmp;
	double* obj_col;
	double* lb_col;
	double* ub_col;
	int* cmatbeg;
	int* cmatind;
	double* cmatval;
	int nzcnt_col;
	int pos;
	int status;
	int ora_fine_li, ora_fine_nl;
	int dur_li, dur_nl;
	int dur;
	int ora_fine;
	int attesa;
	double penal;
	int company;
	int posi;
	int ind_start,ind_end, index;
	double valore;
	int in;


	


	//initializing labels
	labels=(label**) calloc(n_of_flights,sizeof(label*));
	if(labels==NULL) alloc_error("labels\n");
	for(i=0;i<n_of_flights;i++) {
		labels[i]=(label*) calloc(MAX_LABELS,sizeof(label));
		if(labels[i]==NULL) alloc_error("labels[i]\n");
	}

	//numero etichette occupate finora per ogni nodo
	ind_labels=(int*) calloc(n_of_flights,sizeof(int));
	if(ind_labels==NULL) alloc_error("ind_labels\n");

	//initializing the arrays for adding the column
	cmatbeg=(int*) calloc(1,sizeof(int));
	if(cmatbeg==NULL) alloc_error("cmatbeg\n");
	obj_col=(double*) calloc(1,sizeof(double));
	if(obj_col==NULL) alloc_error("obj_col\n");
	ub_col=(double*) calloc(1,sizeof(double));
	if(ub_col==NULL) alloc_error("ub_col\n");
	lb_col=(double*) calloc(1,sizeof(double));
	if(lb_col==NULL) alloc_error("lb_col\n");



	new_cols=0;

	for(k=0;k<n_of_crew_types;k++) {
		company=crews[k].company;
		for(i=0;i<n_of_flights;i++) {
			for(j=0;j<MAX_LABELS;j++) {
							
				labels[i][j].pred=-1;
				labels[i][j].pred_label=-1;
				//check aereporto di partenza per il tipo
				if(flight_array[i]->dep==crews[k].dep_airport) {
					labels[i][j].profit=alpha[i];
					//adding the profit of the link xy constraints
					labels[i][j].profit=labels[i][j].profit+gamma_[company][i];
					ind_labels[i]=1;
					labels[i][j].n_flights=1;
					labels[i][j].ind_table_duration=find_interval(i);
					labels[i][j].dep_time_first_flight=flight_array[i]->dep_t;

					if ( ( crews[k].dep_airport == 7 && flight_array[i]->dep_t > min_dep_time_A ) ||
					     ( crews[k].dep_airport == 3 && flight_array[i]->dep_t > min_dep_time_S ) )
					{
						labels[i][j].profit=-(double)INF;
						ind_labels[i]=0;
						labels[i][j].n_flights=0;
					}
					
			
				}
				else {
					labels[i][j].profit=-(double)INF;
					ind_labels[i]=0;
					labels[i][j].n_flights=0;
					
				}
			}
			
		}
		for(i=0;i<n_of_flights;i++) {
			
			//count=0;
			for(li=0;li<ind_labels[i];li++) {
				
#ifdef AL 
				//se il volo i va da A ad L e non ha predecessori, allora non etichettare a partire da lui (volo da solo)
				if(flight_array[i]->dep==7 && flight_array[i]->arr==0) {
					if(predecessori[i]->n_of_pred==0) {
						continue;
					}
					if(first_flight_A(i)) continue;
				}
#endif
				//count++;
				//printf("label %d profit %f nflight %d\n",li,labels[i][li].profit,labels[i][li].n_flights);
				for(j=0;j<successori[i]->n_of_succ;j++) {
					//avoiding to propagating to forbidden succ in the branch and price
					if(successori[i]->forb[j]) continue;

					succ=successori[i]->ind_succ[j];
				
					//check num voli
					if(labels[i][li].n_flights+1>crews[k].max_flights) continue;
					//check aereporto di arrivo per il tipo
					if(labels[i][li].n_flights+1==crews[k].max_flights) {
						if(flight_array[succ]->arr!=crews[k].arr_airport) continue;
					}
					
					//check durata
					dur=duration_table[labels[i][li].ind_table_duration][labels[i][li].n_flights];
					ora_fine=labels[i][li].dep_time_first_flight-45+dur-30;
					if(flight_array[succ]->arr_t>ora_fine) continue;
					
#ifdef AL
					//se il volo i va da A ad L ed e` il primo della giornata allora non etichettarlo (volo da solo)
					if(flight_array[succ]->dep==7 && flight_array[succ]->arr==0) {
						if(first_flight_A(succ)) continue;
					}
#endif
#ifdef LA

					//se il succ va da L ad A e non ha succ, allora non etichettarlo (volo da solo)
					if(flight_array[succ]->dep==0 && flight_array[succ]->arr==7) {
						if(successori[succ]->n_of_succ==0) continue;
						if(last_flight_A(succ)) continue;
					}
#endif
					
					if(ind_labels[succ]==MAX_LABELS) {
						printf("ind_labels[succ] %d succ %d\n",ind_labels[succ],succ);
						stop_error("max lenght of labels reached\n");
					}
					//propagating labels in a new slot (dominance will be checked at the top)
					labels[succ][ind_labels[succ]].profit=labels[i][li].profit+alpha[succ]+
						gamma_[company][succ]+eta[i][j]+delta[company][i][j];
#ifdef MULTI_OBJ
					//adding the cost of the arc
					attesa=flight_array[succ]->dep_t-flight_array[i]->arr_t;
					if(attesa<TIME1) {
						penal=PENAL1;
					}
					else if(attesa<TIME2) {
						penal=PENAL2;
					}
					else if(attesa<TIME3) {
						penal=PENAL3;
					}
					else if(attesa<=TIME4) {
						penal=PENAL4;
					}
					else {
						printf("attesa %d\n",attesa);
						stop_error("too long waiting\n");
					}
					labels[succ][ind_labels[succ]].profit=labels[succ][ind_labels[succ]].profit-((double)(attesa)/5.0)*penal;
#endif
					labels[succ][ind_labels[succ]].pred=i;
					labels[succ][ind_labels[succ]].pred_label=li;
					labels[succ][ind_labels[succ]].n_flights=labels[i][li].n_flights+1;
					labels[succ][ind_labels[succ]].dep_time_first_flight=labels[i][li].dep_time_first_flight;
					labels[succ][ind_labels[succ]].ind_table_duration=labels[i][li].ind_table_duration;
					
					/*if(flight_array[succ]->dep!=flight_array[labels[succ][ind_labels[succ]].pred]->arr) {
						stop_error("error different airports\n");
					}*/


					ind_labels[succ]++;

					
					
					
				}
			}
			//printf("node %d count %d\n",i,count);
		}
		//determining the node with highest profit among those that have the right arrival airport
		best_profit=0.0;
		best_node=-1;
		best_label=-1;
		for(in=0;in<n_of_flights;in++) {
			if(flight_array[in]->arr!=crews[k].arr_airport) continue;
			if(not_terminal[in]) continue;
			if ( ( crews[k].arr_airport == 7 && flight_array[in]->arr_t < max_arr_time_A ) ||
			     ( crews[k].arr_airport == 3 && flight_array[in]->arr_t < max_arr_time_S ) )
			{
				continue;
			}
			for(li=0;li<ind_labels[in];li++) {

				best_profit=labels[in][li].profit;
				best_node=in;
				best_label=li;


				if(best_node==-1) {
					printf("error best node = -1\n");
					stop_error("");
				} 

				if(company==0) {
					valore=(double)x_cost_B;
				}
				else if(company==1) {
					valore=(double)x_cost_N;
				}
				else {
					valore=(double)x_cost_C;
				}
				if(k==6 || k==7 || k==8 || k==9 || k==10 || k==11 || k==12 || k==13) {
					if(company==0) {
						stop_error("route of Binter sleeping in S or A\n");
					}
					else if(company==1) {
						valore=valore+(double)x_cost_pernocta_N;
					}
					else {
						valore=valore+(double)x_cost_pernocta_C;
					}
				}
				if(best_profit>valore-beta[k]+EPSI) {
					//printf("best profit %f valore %f ",best_profit, valore);
					//printf("new cols %d\n",new_cols);
					//printf("difference %f\n",best_profit-valore+beta[k]);

					
				//if(best_profit>1.0-beta[k]+EPSI) {

					//generating a new column
					risolto=FALSE;
					//reconstructing the route found
					ind=best_node;
					ind_l=best_label;
					count=0;
					while(labels[ind][ind_l].pred!=-1) {
						tmp=ind;
						ind=labels[ind][ind_l].pred;
						ind_l=labels[tmp][ind_l].pred_label;
						count++;
					}
					count++;
					//storing the new route in the routes pool
					//printf("new col profit %f\n",best_profit);
					crew_day_pool[n_of_cols]=(crew_day*) calloc(1,sizeof(crew_day));
					if(crew_day_pool[n_of_cols]==NULL) alloc_error("crew_day_pool[n_of_cols]\n");
					crew_day_pool[n_of_cols]->type=k;
					crew_day_pool[n_of_cols]->n_flights=count;
					crew_day_pool[n_of_cols]->flights=(int*) calloc(count,sizeof(int));
					if(crew_day_pool[n_of_cols]->flights==NULL) alloc_error("crew_day_pool[n_of_cols]->flights\n");
					ind=best_node;
					ind_l=best_label;
					count=0;
					while(labels[ind][ind_l].pred!=-1) {
						crew_day_pool[n_of_cols]->flights[count]=ind;
						tmp=ind;
						ind=labels[ind][ind_l].pred;
						ind_l=labels[tmp][ind_l].pred_label;
						count++;
					}
					crew_day_pool[n_of_cols]->flights[count]=ind;
					
					/*printf("company %d\n",company);
					for(i=0;i<crew_day_pool[n_of_cols]->n_flights;i++) {
						printf("%d %d %s %d %s %d\n",
							flight_array[crew_day_pool[n_of_cols]->flights[i]]->code,
							crews[crew_day_pool[n_of_cols]->type].company,
							airports[flight_array[crew_day_pool[n_of_cols]->flights[i]]->dep],
							flight_array[crew_day_pool[n_of_cols]->flights[i]]->dep_t,
							airports[flight_array[crew_day_pool[n_of_cols]->flights[i]]->arr],
							flight_array[crew_day_pool[n_of_cols]->flights[i]]->arr_t);

					}*/

					company=crews[crew_day_pool[n_of_cols]->type].company;
					
					//adding the new column to the master
					if(company==0) {
						obj_col[0]=(double)x_cost_B; //B
					}
					else if(company==1) {
						obj_col[0]=(double)x_cost_N;
					}
					else {
						obj_col[0]=(double)x_cost_C;
					}
					cmatbeg[0]=0;
					lb_col[0]=0.0;
					ub_col[0]=1.0;

					nzcnt_col=1+crew_day_pool[n_of_cols]->n_flights+crew_day_pool[n_of_cols]->n_flights+crew_day_pool[n_of_cols]->n_flights-1;
					
					//counting the number of short arcs in the column
					for(i=crew_day_pool[n_of_cols]->n_flights-1;i>0;i--) {
						ind_start=crew_day_pool[n_of_cols]->flights[i];
						ind_end=crew_day_pool[n_of_cols]->flights[i-1];
						for(j=0;j<successori[ind_start]->n_of_succ;j++) {
							succ=successori[ind_start]->ind_succ[j];
							if(succ==ind_end) {
								if(flight_array[succ]->dep_t-flight_array[ind_start]->arr_t>=30) continue;
								nzcnt_col++;
							}
						}
					}
					
					crew_day_pool[n_of_cols]->forb=-1;
					//if the route of the crew is from S or A, or it is from A or S, higher cost
					if(crew_day_pool[n_of_cols]->type==6 || crew_day_pool[n_of_cols]->type==7 ||
						crew_day_pool[n_of_cols]->type==8 || crew_day_pool[n_of_cols]->type==9 ||
						crew_day_pool[n_of_cols]->type==19 || crew_day_pool[n_of_cols]->type==11 ||
						crew_day_pool[n_of_cols]->type==12 || crew_day_pool[n_of_cols]->type==13) {
						
						if(company==0) {
							stop_error("route of Binter from S or A\n");
						}
						else if(company==1) {
							obj_col[0]=obj_col[0]+(double)x_cost_pernocta_N;
						}
						else {
							obj_col[0]=obj_col[0]+(double)x_cost_pernocta_C;
						}
					}

#ifdef MULTI_OBJ
					for(i=crew_day_pool[n_of_cols]->n_flights-1;i>0;i--) {
						attesa=flight_array[crew_day_pool[n_of_cols]->flights[i-1]]->dep_t-
						flight_array[crew_day_pool[n_of_cols]->flights[i]]->arr_t;
						if(attesa<TIME1) {
							penal=PENAL1;
						}
						else if(attesa<TIME2) {
							penal=PENAL2;
						}
						else if(attesa<TIME3) {
							penal=PENAL3;
						}
						else if(attesa<=TIME4) {
							penal=PENAL4;
						}
						else {
							printf("attesa %d\n",attesa);
							stop_error("too long waiting\n");
						}
						obj_col[0]=obj_col[0]+((double)(attesa)/5.0)*penal;
							
					} 
#endif

					cmatind=(int*) calloc(nzcnt_col,sizeof(int));
					if(cmatind==NULL) alloc_error("cmatind\n");
					cmatval=(double*) calloc(nzcnt_col,sizeof(double));
					if(cmatval==NULL) alloc_error("cmatval\n");

					cmatind[0]=crew_day_pool[n_of_cols]->type;
					cmatval[0]=1.0;

					pos=1;
					for(i=0;i<crew_day_pool[n_of_cols]->n_flights;i++) {
						cmatind[pos]=n_of_crew_types+crew_day_pool[n_of_cols]->flights[i];
						cmatval[pos]=1.0;
						pos++;
					}

					//inserting the column in the constraints linking xy
					posi=n_of_crew_types+n_of_flights+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1;
					company=crews[crew_day_pool[n_of_cols]->type].company;
					posi=posi+n_of_flights*company;
					for(i=0;i<crew_day_pool[n_of_cols]->n_flights;i++) {
						cmatind[pos]=posi+crew_day_pool[n_of_cols]->flights[i];
						cmatval[pos]=1.0;
						pos++;
					}

					//inserting the column in the constraints linking xyz
					posi=n_of_crew_types+n_of_flights+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1+N_COMPANIES*n_of_flights;
					for(i=crew_day_pool[n_of_cols]->n_flights-1;i>0;i--) {
						ind_start=crew_day_pool[n_of_cols]->flights[i];
						ind_end=crew_day_pool[n_of_cols]->flights[i-1];
						for(j=0;j<successori[ind_start]->n_of_succ;j++) {
							succ=successori[ind_start]->ind_succ[j];
							if(succ==ind_end) {
								index=successori[ind_start]->arcs[j];
								break;
							}
						}
						cmatind[pos]=posi+index;
						cmatval[pos]=1.0;
						pos++;
					}

					//inserting the column in the constraints linking x y for short arcs
					posi=n_of_crew_types+n_of_flights+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1+N_COMPANIES*n_of_flights+n_of_arcs;
					company=crews[crew_day_pool[n_of_cols]->type].company;
					posi=posi+n_of_short_arcs*company;
					for(i=crew_day_pool[n_of_cols]->n_flights-1;i>0;i--) {
						ind_start=crew_day_pool[n_of_cols]->flights[i];
						ind_end=crew_day_pool[n_of_cols]->flights[i-1];
						for(j=0;j<successori[ind_start]->n_of_succ;j++) {
							succ=successori[ind_start]->ind_succ[j];
							if(succ==ind_end) {
								if(flight_array[succ]->dep_t-flight_array[ind_start]->arr_t>=30) continue;
								index=indices_short_arcs[ind_start][j];
								cmatind[pos]=posi+index;
								cmatval[pos]=1.0;
								pos++;
								break;
							}
						}
						
					}

					if(pos!=nzcnt_col) stop_error("pos different from nzcnt_col");
										
					status=CPXaddcols(env,lp,1,nzcnt_col,obj_col,cmatbeg,cmatind,cmatval,lb_col,ub_col,NULL);
					if(status) {
						stop_error("CPXaddcols\n");
					}

					free(cmatind);
					free(cmatval);

					n_of_cols++;
					new_cols++;
				}
				

			}
		}
		

		
	}

/*	printf("colonne totali %d\n",new_cols);
	exit(0);	*/

	/*CPXwriteprob(env,lp,"prova.lp",NULL);
	exit(0);*/

	for(i=0;i<n_of_flights;i++) {
		free(labels[i]);
	}
	free(labels);
	free(ind_labels);
	free(cmatbeg);
	free(obj_col);
	free(lb_col);
	free(ub_col);
	return new_cols;
}
#endif



#ifdef ALL_COLS_IN_GAP
int solve_pricing_all_cols_in_gap()
{
	int i,k,j,li;
	int new_cols;
	int succ;
	double best_profit;
	int best_node;
	int best_label;
	int ind, ind_l;
	int count;
	int tmp;
	double* obj_col;
	double* lb_col;
	double* ub_col;
	int* cmatbeg;
	int* cmatind;
	double* cmatval;
	int nzcnt_col;
	int pos;
	int status;


	int dur;
	int ora_fine;
	int attesa;
	double penal;
	int company;
	int posi;
	int ind_start,ind_end, index;
	double valore;
	int in;
	double gap;


	gap=BestUB-BestLB;


	//initializing labels
	labels=(label**) calloc(n_of_flights,sizeof(label*));
	if(labels==NULL) alloc_error("labels\n");
	for(i=0;i<n_of_flights;i++) {
		labels[i]=(label*) calloc(MAX_LABELS,sizeof(label));
		if(labels[i]==NULL) alloc_error("labels[i]\n");
	}

	//numero etichette occupate finora per ogni nodo
	ind_labels=(int*) calloc(n_of_flights,sizeof(int));
	if(ind_labels==NULL) alloc_error("ind_labels\n");

	//initializing the arrays for adding the column
	cmatbeg=(int*) calloc(1,sizeof(int));
	if(cmatbeg==NULL) alloc_error("cmatbeg\n");
	obj_col=(double*) calloc(1,sizeof(double));
	if(obj_col==NULL) alloc_error("obj_col\n");
	ub_col=(double*) calloc(1,sizeof(double));
	if(ub_col==NULL) alloc_error("ub_col\n");
	lb_col=(double*) calloc(1,sizeof(double));
	if(lb_col==NULL) alloc_error("lb_col\n");



	new_cols=0;

	for(k=0;k<n_of_crew_types;k++) {
		company=crews[k].company;
		for(i=0;i<n_of_flights;i++) {
			for(j=0;j<MAX_LABELS;j++) {
							
				labels[i][j].pred=-1;
				labels[i][j].pred_label=-1;
				//check aereporto di partenza per il tipo
				if(flight_array[i]->dep==crews[k].dep_airport) {
					labels[i][j].profit=alpha[i];
					//adding the profit of the link xy constraints
					labels[i][j].profit=labels[i][j].profit+gamma_[company][i];
					ind_labels[i]=1;
					labels[i][j].n_flights=1;
					labels[i][j].ind_table_duration=find_interval(i);
					labels[i][j].dep_time_first_flight=flight_array[i]->dep_t;

					if ( ( crews[k].dep_airport == 7 && flight_array[i]->dep_t > min_dep_time_A ) ||
					     ( crews[k].dep_airport == 3 && flight_array[i]->dep_t > min_dep_time_S ) )
					{
						labels[i][j].profit=-(double)INF;
						ind_labels[i]=0;
						labels[i][j].n_flights=0;
					}
					
			
				}
				else {
					labels[i][j].profit=-(double)INF;
					ind_labels[i]=0;
					labels[i][j].n_flights=0;
					
				}
			}
			
		}
		for(i=0;i<n_of_flights;i++) {
			
			//count=0;
			for(li=0;li<ind_labels[i];li++) {
				
#ifdef AL 
				//se il volo i va da A ad L e non ha predecessori, allora non etichettare a partire da lui (volo da solo)
				if(flight_array[i]->dep==7 && flight_array[i]->arr==0) {
					if(predecessori[i]->n_of_pred==0) {
						continue;
					}
					if(first_flight_A(i)) continue;
				}
#endif
				//count++;
				//printf("label %d profit %f nflight %d\n",li,labels[i][li].profit,labels[i][li].n_flights);
				for(j=0;j<successori[i]->n_of_succ;j++) {
					//avoiding to propagating to forbidden succ in the branch and price
					if(successori[i]->forb[j]) continue;

					succ=successori[i]->ind_succ[j];
				
					//check num voli
					if(labels[i][li].n_flights+1>crews[k].max_flights) continue;
					//check aereporto di arrivo per il tipo
					if(labels[i][li].n_flights+1==crews[k].max_flights) {
						if(flight_array[succ]->arr!=crews[k].arr_airport) continue;
					}
					
					//check durata
					dur=duration_table[labels[i][li].ind_table_duration][labels[i][li].n_flights];
					ora_fine=labels[i][li].dep_time_first_flight-45+dur-30;
					if(flight_array[succ]->arr_t>ora_fine) continue;
					
#ifdef AL
					//se il volo i va da A ad L ed e` il primo della giornata allora non etichettarlo (volo da solo)
					if(flight_array[succ]->dep==7 && flight_array[succ]->arr==0) {
						if(first_flight_A(succ)) continue;
					}
#endif
#ifdef LA 

					//se il succ va da L ad A e non ha succ, allora non etichettarlo (volo da solo)
					if(flight_array[succ]->dep==0 && flight_array[succ]->arr==7) {
						if(successori[succ]->n_of_succ==0) continue;
						if(last_flight_A(succ)) continue;
					}
#endif
					
					if(ind_labels[succ]==MAX_LABELS) {
						printf("ind_labels[succ] %d succ %d\n",ind_labels[succ],succ);
						stop_error("max lenght of labels reached\n");
					}
					//propagating labels in a new slot (dominance will be checked at the top)
					labels[succ][ind_labels[succ]].profit=labels[i][li].profit+alpha[succ]+
						gamma_[company][succ]+eta[i][j]+delta[company][i][j];
#ifdef MULTI_OBJ
					//adding the cost of the arc
					attesa=flight_array[succ]->dep_t-flight_array[i]->arr_t;
					if(attesa<TIME1) {
						penal=PENAL1;
					}
					else if(attesa<TIME2) {
						penal=PENAL2;
					}
					else if(attesa<TIME3) {
						penal=PENAL3;
					}
					else if(attesa<=TIME4) {
						penal=PENAL4;
					}
					else {
						printf("attesa %d\n",attesa);
						stop_error("too long waiting\n");
					}
					labels[succ][ind_labels[succ]].profit=labels[succ][ind_labels[succ]].profit-((double)(attesa)/5.0)*penal;
#endif
					labels[succ][ind_labels[succ]].pred=i;
					labels[succ][ind_labels[succ]].pred_label=li;
					labels[succ][ind_labels[succ]].n_flights=labels[i][li].n_flights+1;
					labels[succ][ind_labels[succ]].dep_time_first_flight=labels[i][li].dep_time_first_flight;
					labels[succ][ind_labels[succ]].ind_table_duration=labels[i][li].ind_table_duration;
					
					/*if(flight_array[succ]->dep!=flight_array[labels[succ][ind_labels[succ]].pred]->arr) {
						stop_error("error different airports\n");
					}*/


					ind_labels[succ]++;

					
					
					
				}
			}
			//printf("node %d count %d\n",i,count);
		}
		//determining the node with highest profit among those that have the right arrival airport
		best_profit=0.0;
		best_node=-1;
		best_label=-1;
		for(in=0;in<n_of_flights;in++) {
			if(flight_array[in]->arr!=crews[k].arr_airport) continue;
			if(not_terminal[in]) continue;
			if ( ( crews[k].arr_airport == 7 && flight_array[in]->arr_t < max_arr_time_A ) ||
			     ( crews[k].arr_airport == 3 && flight_array[in]->arr_t < max_arr_time_S ) )
			{
				continue;
			}
			for(li=0;li<ind_labels[in];li++) {

				best_profit=labels[in][li].profit;
				best_node=in;
				best_label=li;


				if(best_node==-1) {
					printf("error best node = -1\n");
					stop_error("");
				} 

				if(company==0) {
					valore=(double)x_cost_B;
				}
				else if(company==1) {
					valore=(double)x_cost_N;
				}
				else {
					valore=(double)x_cost_C;
				}
				if(k==6 || k==7 || k==8 || k==9 || k==10 || k==11 || k==12 || k==13) {
					if(company==0) {
						stop_error("route of Binter sleeping in S or A\n");
					}
					else if(company==1) {
						valore=valore+(double)x_cost_pernocta_N;
					}
					else {
						valore=valore+(double)x_cost_pernocta_C;
					}
				}
				//if(best_profit>valore-beta[k]+EPSI) {
				if(best_profit>valore-beta[k]+EPSI-gap) {
					//printf("best profit %f valore %f ",best_profit, valore);
					//printf("difference %f\n",best_profit-valore+beta[k]);

					//generating a new column
					risolto=FALSE;
					//reconstructing the route found
					ind=best_node;
					ind_l=best_label;
					count=0;
					while(labels[ind][ind_l].pred!=-1) {
						tmp=ind;
						ind=labels[ind][ind_l].pred;
						ind_l=labels[tmp][ind_l].pred_label;
						count++;
					}
					count++;
					//storing the new route in the routes pool
					//printf("new col profit %f\n",best_profit);
					crew_day_pool[n_of_cols]=(crew_day*) calloc(1,sizeof(crew_day));
					if(crew_day_pool[n_of_cols]==NULL) alloc_error("crew_day_pool[n_of_cols]\n");
					crew_day_pool[n_of_cols]->type=k;
					crew_day_pool[n_of_cols]->n_flights=count;
					crew_day_pool[n_of_cols]->flights=(int*) calloc(count,sizeof(int));
					if(crew_day_pool[n_of_cols]->flights==NULL) alloc_error("crew_day_pool[n_of_cols]->flights\n");
					ind=best_node;
					ind_l=best_label;
					count=0;
					while(labels[ind][ind_l].pred!=-1) {
						crew_day_pool[n_of_cols]->flights[count]=ind;
						tmp=ind;
						ind=labels[ind][ind_l].pred;
						ind_l=labels[tmp][ind_l].pred_label;
						count++;
					}
					crew_day_pool[n_of_cols]->flights[count]=ind;
					
					/*printf("company %d\n",company);
					for(i=0;i<crew_day_pool[n_of_cols]->n_flights;i++) {
						printf("%d %d %s %d %s %d\n",
							flight_array[crew_day_pool[n_of_cols]->flights[i]]->code,
							crews[crew_day_pool[n_of_cols]->type].company,
							airports[flight_array[crew_day_pool[n_of_cols]->flights[i]]->dep],
							flight_array[crew_day_pool[n_of_cols]->flights[i]]->dep_t,
							airports[flight_array[crew_day_pool[n_of_cols]->flights[i]]->arr],
							flight_array[crew_day_pool[n_of_cols]->flights[i]]->arr_t);

					}*/

					company=crews[crew_day_pool[n_of_cols]->type].company;
					
					//adding the new column to the master
					if(company==0) {
						obj_col[0]=(double)x_cost_B; //B
					}
					else if(company==1) {
						obj_col[0]=(double)x_cost_N;
					}
					else {
						obj_col[0]=(double)x_cost_C;
					}
					cmatbeg[0]=0;
					lb_col[0]=0.0;
					ub_col[0]=1.0;

					nzcnt_col=1+crew_day_pool[n_of_cols]->n_flights+crew_day_pool[n_of_cols]->n_flights+crew_day_pool[n_of_cols]->n_flights-1;
					
					//counting the number of short arcs in the column
					for(i=crew_day_pool[n_of_cols]->n_flights-1;i>0;i--) {
						ind_start=crew_day_pool[n_of_cols]->flights[i];
						ind_end=crew_day_pool[n_of_cols]->flights[i-1];
						for(j=0;j<successori[ind_start]->n_of_succ;j++) {
							succ=successori[ind_start]->ind_succ[j];
							if(succ==ind_end) {
								if(flight_array[succ]->dep_t-flight_array[ind_start]->arr_t>=30) continue;
								nzcnt_col++;
							}
						}
					}
					
					crew_day_pool[n_of_cols]->forb=-1;
					//if the route of the crew is from S or A, or it is from A or S, higher cost
					if(crew_day_pool[n_of_cols]->type==6 || crew_day_pool[n_of_cols]->type==7 ||
						crew_day_pool[n_of_cols]->type==8 || crew_day_pool[n_of_cols]->type==9 ||
						crew_day_pool[n_of_cols]->type==19 || crew_day_pool[n_of_cols]->type==11 ||
						crew_day_pool[n_of_cols]->type==12 || crew_day_pool[n_of_cols]->type==13) {
						
						if(company==0) {
							stop_error("route of Binter from S or A\n");
						}
						else if(company==1) {
							obj_col[0]=obj_col[0]+(double)x_cost_pernocta_N;
						}
						else {
							obj_col[0]=obj_col[0]+(double)x_cost_pernocta_C;
						}
					}

#ifdef MULTI_OBJ
					for(i=crew_day_pool[n_of_cols]->n_flights-1;i>0;i--) {
						attesa=flight_array[crew_day_pool[n_of_cols]->flights[i-1]]->dep_t-
						flight_array[crew_day_pool[n_of_cols]->flights[i]]->arr_t;
						if(attesa<TIME1) {
							penal=PENAL1;
						}
						else if(attesa<TIME2) {
							penal=PENAL2;
						}
						else if(attesa<TIME3) {
							penal=PENAL3;
						}
						else if(attesa<=TIME4) {
							penal=PENAL4;
						}
						else {
							printf("attesa %d\n",attesa);
							stop_error("too long waiting\n");
						}
						obj_col[0]=obj_col[0]+((double)(attesa)/5.0)*penal;
							
					} 
#endif

					cmatind=(int*) calloc(nzcnt_col,sizeof(int));
					if(cmatind==NULL) alloc_error("cmatind\n");
					cmatval=(double*) calloc(nzcnt_col,sizeof(double));
					if(cmatval==NULL) alloc_error("cmatval\n");

					cmatind[0]=crew_day_pool[n_of_cols]->type;
					cmatval[0]=1.0;

					pos=1;
					for(i=0;i<crew_day_pool[n_of_cols]->n_flights;i++) {
						cmatind[pos]=n_of_crew_types+crew_day_pool[n_of_cols]->flights[i];
						cmatval[pos]=1.0;
						pos++;
					}

					//inserting the column in the constraints linking xy
					posi=n_of_crew_types+n_of_flights+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1;
					company=crews[crew_day_pool[n_of_cols]->type].company;
					posi=posi+n_of_flights*company;
					for(i=0;i<crew_day_pool[n_of_cols]->n_flights;i++) {
						cmatind[pos]=posi+crew_day_pool[n_of_cols]->flights[i];
						cmatval[pos]=1.0;
						pos++;
					}

					//inserting the column in the constraints linking xyz
					posi=n_of_crew_types+n_of_flights+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1+N_COMPANIES*n_of_flights;
					for(i=crew_day_pool[n_of_cols]->n_flights-1;i>0;i--) {
						ind_start=crew_day_pool[n_of_cols]->flights[i];
						ind_end=crew_day_pool[n_of_cols]->flights[i-1];
						for(j=0;j<successori[ind_start]->n_of_succ;j++) {
							succ=successori[ind_start]->ind_succ[j];
							if(succ==ind_end) {
								index=successori[ind_start]->arcs[j];
								break;
							}
						}
						cmatind[pos]=posi+index;
						cmatval[pos]=1.0;
						pos++;
					}

					//inserting the column in the constraints linking x y for short arcs
					posi=n_of_crew_types+n_of_flights+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1+N_COMPANIES*n_of_flights+n_of_arcs;
					company=crews[crew_day_pool[n_of_cols]->type].company;
					posi=posi+n_of_short_arcs*company;
					for(i=crew_day_pool[n_of_cols]->n_flights-1;i>0;i--) {
						ind_start=crew_day_pool[n_of_cols]->flights[i];
						ind_end=crew_day_pool[n_of_cols]->flights[i-1];
						for(j=0;j<successori[ind_start]->n_of_succ;j++) {
							succ=successori[ind_start]->ind_succ[j];
							if(succ==ind_end) {
								if(flight_array[succ]->dep_t-flight_array[ind_start]->arr_t>=30) continue;
								index=indices_short_arcs[ind_start][j];
								cmatind[pos]=posi+index;
								cmatval[pos]=1.0;
								pos++;
								break;
							}
						}
						
					}

					if(pos!=nzcnt_col) stop_error("pos different from nzcnt_col");
										
					status=CPXaddcols(env,lp,1,nzcnt_col,obj_col,cmatbeg,cmatind,cmatval,lb_col,ub_col,NULL);
					if(status) {
						stop_error("CPXaddcols\n");
					}

					free(cmatind);
					free(cmatval);

					n_of_cols++;
					new_cols++;
				}
				

			}
		}
		

		
	}

		

	/*CPXwriteprob(env,lp,"prova.lp",NULL);
	exit(0);*/

	for(i=0;i<n_of_flights;i++) {
		free(labels[i]);
	}
	free(labels);
	free(ind_labels);
	free(cmatbeg);
	free(obj_col);
	free(lb_col);
	free(ub_col);
	return new_cols;
}
#endif

#ifdef ALL_COLS_IN_GAP
void solve_additional_cols()
{
	char* ctype;
	int* indices;
	int i,k;
	int status;
	double objval;
	int pos, index;
	int j,l;
	int dep_airport;
	int number_nodes;
	int start_node;
	int count;
	int node;
	int end_node;
	time_t start_IP, end_IP;
	int lpstat;
	int cont;
	int ind;
	int* delstat;


	time(&start_IP);

	//recupero le duali
	/*store the dual variables*/
	for(i=0;i<n_of_crew_types;i++) {
		beta[i]=dual_vars[i];
	}
	for(i=0;i<n_of_flights;i++) {
		alpha[i]=dual_vars[n_of_crew_types+i];
		//printf("alpha[%d] %f\n",flight_array[i]->code,alpha[i]);
	}
	ind=0;
	for(i=0;i<N_COMPANIES;i++) {
		for(k=0;k<n_of_flights;k++) {
			gamma_[i][k]=dual_vars[n_of_crew_types+n_of_flights+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1+ind];
			/*printf("gamma[%d][%d] %f\n",i,
				flight_array[k]->code,
				gamma_[i][k]);*/
			ind++;
		}
	}
	ind=0;
	for(i=0;i<n_of_flights;i++) {
		for(j=0;j<successori[i]->n_of_succ;j++) {
			eta[i][j]=dual_vars[n_of_crew_types+n_of_flights+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1+N_COMPANIES*n_of_flights+ind];
			ind++;
		}
	}

	ind=0;
	for(k=0;k<N_COMPANIES;k++) {
		for(i=0;i<n_of_flights;i++) {
			for(j=0;j<successori[i]->n_of_succ;j++) {
				delta[k][i][j]=0;
				if(flight_array[successori[i]->ind_succ[j]]->dep_t-flight_array[i]->arr_t>=30) continue;
				delta[k][i][j]=dual_vars[n_of_crew_types+n_of_flights+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1+N_COMPANIES*n_of_flights+n_of_arcs+ind];
				ind++;
			}
		}
	}

	delstat=(int*) calloc(CPXgetnumcols(env,lp), sizeof(int));
	if(delstat==NULL) alloc_error("delstat\n");
	for(i=offset;i<CPXgetnumcols(env,lp);i++) {
		delstat[i]=1;
	}
	status = CPXdelsetcols (env, lp, delstat);
	if(status) {
		stop_error("CPXdelsetcols\n");
	}
	free(delstat);
	n_of_cols=0;

	//creo le colonne necessarie in base al gap tra BestUB e BestLB
	cont=solve_pricing_all_cols_in_gap();
	
	printf("Columns in gap %d\n",cont);

	//ricreo il problema come intero con le nuove colonne
	/*ctype=(char*) calloc(offset+n_of_cols,sizeof(char));
	if(ctype==NULL) alloc_error("ctype\n");
	indices=(int*) calloc(offset+n_of_cols,sizeof(int));
	if(indices==NULL) alloc_error("indices\n");
	for(i=0;i<offset+n_of_cols;i++) {
		indices[i]=i;
		ctype[i]='B';
	}
	
	status=CPXchgctype(env,lp,offset+n_of_cols,indices,ctype);*/
	//solo x e z intere
	ctype=(char*) calloc(n_of_z+n_of_cols,sizeof(char));
	if(ctype==NULL) alloc_error("ctype\n");
	indices=(int*) calloc(n_of_z+n_of_cols,sizeof(int));
	if(indices==NULL) alloc_error("indices\n");
	for(i=0;i<n_of_z+n_of_cols;i++) {
		indices[i]=i+n_auxil+n_of_y;
		ctype[i]='B';
	}
	
	status=CPXchgctype(env,lp,n_of_z+n_of_cols,indices,ctype);
	if(status) {
		stop_error("CPXchgctype\n");
	}

	free(ctype);
	free(indices);

	status = CPXsetdblparam(env, CPX_PARAM_TILIM, (double)INF);  
	if(status) {
		printf("error in setdblparam\n");
		stop_error("");
	}
	 
	status=CPXmipopt(env,lp);
	if(status) {
		stop_error("CPXmipoopt\n");
	} 
	status=CPXgetstat(env,lp);
	if(status==101 || status==102 || status==107) {
		printf("status %d\n",status);
	 
		if(x!=NULL) {
			free(x);
			x=NULL;
		}
		x=(double*) calloc(offset+n_of_cols,sizeof(double));
		if(x==NULL) alloc_error("x\n");

		status=CPXgetmipx(env,lp,x,0,CPXgetnumcols(env,lp)-1);
		if(status) {
			stop_error("CPXgetmipx\n");
		}
		status=CPXgetmipobjval(env,lp,&objval);
		if(status) {
			stop_error("CPXgetmipobjval\n");
		} 
		printf("objval %f\n",objval);

		best_sol=round_(objval);
		printf("int sol found %d\n",best_sol);

		printf("solution\n\n\n");
			 
		printf("%s\n",our_day);
		printf("%d\n",best_sol);
		
		 
		//emptying the y_sol
		for(i=0;i<n_of_aircraft_types;i++) {
			for(j=0;j<n_of_flights;j++) {
				for(l=0;l<n_of_flights;l++) {
					y_sol[i][j][l]=0;
				} 
			}
		} 
		pos=n_auxil+n_of_starting_arcs*n_of_aircraft_types;
		for(j=0;j<n_of_aircraft_types;j++) {
			for(i=0;i<n_of_flights;i++) {	
				for(k=0;k<successori_a[i]->n_of_succ;k++) {
					if(x[pos]<EPSI) {
						pos++;
						continue;
					} 
					 /*printf("y_%d_%d_%s_%d_%d %f\n",
						 flight_array[i]->code,
						 flight_array[successori_a[i]->ind_succ[k]]->code,
						 airports[aircrafts[j].dep_airport],
						 aircrafts[j].company,j, x[index]);*/
					if(aircrafts[j].company==0) {
						 /*printf("B;%d;%d\n",flight_array[i]->code,
						 flight_array[successori_a[i]->ind_succ[k]]->code);*/
						y_sol[j][i][successori_a[i]->ind_succ[k]]=1;
					}
					else if(aircrafts[j].company==1) {
						 /*printf("N;%d;%d\n",flight_array[i]->code,
						 flight_array[successori_a[i]->ind_succ[k]]->code);*/
						y_sol[j][i][successori_a[i]->ind_succ[k]]=1;
					}
					else {
						 /*printf("C;%d;%d\n",flight_array[i]->code,
						 flight_array[successori_a[i]->ind_succ[k]]->code);*/
					    y_sol[j][i][successori_a[i]->ind_succ[k]]=1;
					}
					pos++;
				}
			}
		}
		//printf("\n\n");
		 
		//printing the solution for y and z
		//printf("yz\n");
		index=n_auxil;
		for(j=0;j<n_of_aircraft_types;j++) {
			for(k=0;k<N_MAIN_AIRPORTS;k++) {
				dep_airport=starting_arcs[k]->airport;
				for(i=0;i<starting_arcs[k]->n_of_succ;i++) {
					if(x[index]<EPSI) {
						index++;
						continue;
					}
					 /*printf("ys_%d_%s_%d_%d %f\n",
						 flight_array[starting_arcs[k]->ind_succ[i]]->code,
						 airports[dep_airport],aircrafts[j].company,j,x[index]);*/
					for(l=0;l<n_of_flights;l++) {
						y_sol[j][l][starting_arcs[k]->ind_succ[i]]=2;
					}
					index++;
				}
			}
		}

		 

		for(i=0;i<n_of_aircraft_types;i++) {
			for(j=0;j<n_of_flights;j++) {
				for(l=0;l<n_of_flights;l++) {
					best_y_sol[i][j][l]=y_sol[i][j][l];
				}  
			}
		}

    	//printing in the format for the picture
		number_nodes=0;
		for(j=0;j<n_of_aircraft_types;j++) {
			for(l=0;l<aircrafts[j].n_of_aircrafts;l++) {
				start_node=FALSE;
				for(i=0;i<n_of_flights;i++) {
					count=0;
					//looking for a node without entering arcs
					for(k=0;k<n_of_flights;k++) {
						if(y_sol[j][k][i]==-1) break;
						if(y_sol[j][k][i]==1) break;
						if(y_sol[j][k][i]==2) {
							count++;
						}
					}
					if(count==n_of_flights) {
						if(aircrafts[j].company==0) {
							printf("B;%d",flight_array[i]->code);
						}
						else if(aircrafts[j].company==1) {
							printf("N;%d",flight_array[i]->code);
						}
						else if(aircrafts[j].company==2) {
							printf("C;%d",flight_array[i]->code);
						}
						start_node=TRUE;
						y_sol[j][0][i]=-1;
						node=i;
						number_nodes++;
						break;
					}					
				}
				if(start_node==FALSE) {
					break;
				}
				end_node=FALSE;
				while(!end_node) {
					count=0;
					for(k=0;k<n_of_flights;k++) {
						if(y_sol[j][node][k]==1) {
							printf(";%d",flight_array[k]->code);
							
							y_sol[j][node][k]=0;
							node=k;
							number_nodes++;
							break;
						}
					    count++;
					}
					if(count==n_of_flights) {
						printf("\n");
						end_node=TRUE;
						if(number_nodes!=n_of_flights) {
							start_node=FALSE;
						}
					}
				}
			}
		}

		printf("\n---\n");

		//storing the best solution
		pos=0;
		for(i=0;i<n_of_cols;i++) {
			if(x[i+offset]>EPSI) {
				best_solution[pos]->n_flights=crew_day_pool[i]->n_flights;
				best_solution[pos]->type=crew_day_pool[i]->type;
				index=0;
				for(k=crew_day_pool[i]->n_flights-1;k>=0;k--) {
					best_solution[pos]->flights[index]=crew_day_pool[i]->flights[k];
					printf("%d;",flight_array[crew_day_pool[i]->flights[k]]->code);
					 /*printf("(%d)%s %d-%s %d, ",flight_array[crew_day_pool[i]->flights[k]]->code,
						 airports[flight_array[crew_day_pool[i]->flights[k]]->dep],
						 flight_array[crew_day_pool[i]->flights[k]]->dep_t,
						 airports[flight_array[crew_day_pool[i]->flights[k]]->arr],
						 flight_array[crew_day_pool[i]->flights[k]]->arr_t);*/
					index++;
				} 
				printf("\n");
				pos++;
			} 
		}
		n_crew_duties=pos;

		
		if(x!=NULL) {
			free(x);
			x=NULL;
		}

	}
	
	else {
		printf("status %d\n",status);
		stop_error("no int sol found");
	}
    
    time(&end_IP);
    printf("durata IP %f secondi\n",difftime(end_IP,start_IP));
}
#endif






#ifdef ALL_COLS_AND_Y_IN_GAP
int solve_pricing_all_cols_in_gap()
{
	int i,k,j,li;
	int new_cols;
	int succ;
	double best_profit;
	int best_node;
	int best_label;
	int ind, ind_l;
	int count;
	int tmp;
	double* obj_col;
	double* lb_col;
	double* ub_col;
	int* cmatbeg;
	int* cmatind;
	double* cmatval;
	int nzcnt_col;
	int pos;
	int status;


	int dur;
	int ora_fine;
	int attesa;
	double penal;
	int company;
	int posi;
	int ind_start,ind_end, index;
	double valore;
	int in;
	double gap;
#ifdef RETIME
	int primo_A;
	int primo_S;
	int ultimo_A;
	int ultimo_S;
#endif


	gap=BestUB-BestLB;


	//initializing labels
	labels=(label**) calloc(n_of_flights,sizeof(label*));
	if(labels==NULL) alloc_error("labels\n");
	for(i=0;i<n_of_flights;i++) {
		labels[i]=(label*) calloc(MAX_LABELS,sizeof(label));
		if(labels[i]==NULL) alloc_error("labels[i]\n");
	}

	//numero etichette occupate finora per ogni nodo
	ind_labels=(int*) calloc(n_of_flights,sizeof(int));
	if(ind_labels==NULL) alloc_error("ind_labels\n");

	//initializing the arrays for adding the column
	cmatbeg=(int*) calloc(1,sizeof(int));
	if(cmatbeg==NULL) alloc_error("cmatbeg\n");
	obj_col=(double*) calloc(1,sizeof(double));
	if(obj_col==NULL) alloc_error("obj_col\n");
	ub_col=(double*) calloc(1,sizeof(double));
	if(ub_col==NULL) alloc_error("ub_col\n");
	lb_col=(double*) calloc(1,sizeof(double));
	if(lb_col==NULL) alloc_error("lb_col\n");



	new_cols=0;

#ifdef RETIME
	primo_A=find_first_flight_A();
	primo_S=find_first_flight_S();
	ultimo_A=find_last_flight_A();
	ultimo_S=find_last_flight_S();
#endif

	for(k=0;k<n_of_crew_types;k++) {
		company=crews[k].company;
		for(i=0;i<n_of_flights;i++) {
			for(j=0;j<MAX_LABELS;j++) {
							
				labels[i][j].pred=-1;
				labels[i][j].pred_label=-1;
				//check aereporto di partenza per il tipo
				if(flight_array[i]->dep==crews[k].dep_airport) {
#ifndef RETIME
					labels[i][j].profit=alpha[i];
#else
					labels[i][j].profit=alpha[flight_array[i]->its_real_flight_index];
#endif
					//adding the profit of the link xy constraints
#ifndef RETIME
					labels[i][j].profit=labels[i][j].profit+gamma_[company][i];
#else
labels[i][j].profit=labels[i][j].profit+gamma_[company][i];
					//labels[i][j].profit=labels[i][j].profit+gamma_[company][flight_array[i]->its_real_flight_index];
#endif
					ind_labels[i]=1;
					labels[i][j].n_flights=1;
					labels[i][j].ind_table_duration=find_interval(i);
					labels[i][j].dep_time_first_flight=flight_array[i]->dep_t;
#ifdef RETIME
					if(are_copies(primo_A,i)) continue;
					if(are_copies(primo_S,i)) continue;
#endif

					if ( ( crews[k].dep_airport == 7 && flight_array[i]->dep_t > min_dep_time_A ) ||
					     ( crews[k].dep_airport == 3 && flight_array[i]->dep_t > min_dep_time_S ) )
					{
						//non si possono avere turni da A o da S a meno che sia il primo volo
						labels[i][j].profit=-(double)INF;
						ind_labels[i]=0;
						labels[i][j].n_flights=0;
					}
					
			
				}
				else {
					labels[i][j].profit=-(double)INF;
					ind_labels[i]=0;
					labels[i][j].n_flights=0;
					
				}
			}
			
		}
		for(i=0;i<n_of_flights;i++) {
			
			//count=0;
			for(li=0;li<ind_labels[i];li++) {
				
#ifdef AL 
				//se il volo i va da A ad L e non ha predecessori, allora non etichettare a partire da lui (volo da solo)
				if(flight_array[i]->dep==7 && flight_array[i]->arr==0) {
					if(predecessori[i]->n_of_pred==0) {
						continue;
					}
					if(first_flight_A(i)) continue;
				}
#endif
				//count++;
				//printf("label %d profit %f nflight %d\n",li,labels[i][li].profit,labels[i][li].n_flights);
				for(j=0;j<successori[i]->n_of_succ;j++) {
					//avoiding to propagating to forbidden succ in the branch and price
					if(successori[i]->forb[j]) continue;

					succ=successori[i]->ind_succ[j];
				
					//check num voli
					if(labels[i][li].n_flights+1>crews[k].max_flights) continue;
					//check aereporto di arrivo per il tipo
					if(labels[i][li].n_flights+1==crews[k].max_flights) {
						if(flight_array[succ]->arr!=crews[k].arr_airport) continue;
					}
					
					//check durata
					dur=duration_table[labels[i][li].ind_table_duration][labels[i][li].n_flights];
					ora_fine=labels[i][li].dep_time_first_flight-45+dur-30;
					if(flight_array[succ]->arr_t>ora_fine) continue;
					
#ifdef AL
					//se il volo i va da A ad L ed e` il primo della giornata allora non etichettarlo (volo da solo)
					if(flight_array[succ]->dep==7 && flight_array[succ]->arr==0) {
						if(first_flight_A(succ)) continue;
					}
#endif
#ifdef LA

					//se il succ va da L ad A e non ha succ, allora non etichettarlo (volo da solo)
					if(flight_array[succ]->dep==0 && flight_array[succ]->arr==7) {
						if(successori[succ]->n_of_succ==0) continue;
						if(last_flight_A(succ)) continue;
					}
#endif
					
					if(ind_labels[succ]==MAX_LABELS) {
						printf("ind_labels[succ] %d succ %d\n",ind_labels[succ],succ);
						stop_error("max lenght of labels reached\n");
					}
					//propagating labels in a new slot (dominance will be checked at the top)
#ifndef RETIME
					labels[succ][ind_labels[succ]].profit=labels[i][li].profit+alpha[succ]+
						gamma_[company][succ]+eta[i][j]+delta[company][i][j];
#else
labels[succ][ind_labels[succ]].profit=labels[i][li].profit+alpha[flight_array[succ]->its_real_flight_index]+gamma_[company][succ]+eta[i][j]+delta[company][i][j];
					//labels[succ][ind_labels[succ]].profit=labels[i][li].profit+alpha[flight_array[succ]->its_real_flight_index]+gamma_[company][flight_array[succ]->its_real_flight_index]+eta[i][j]+delta[company][i][j];

#endif
#ifdef MULTI_OBJ
					//adding the cost of the arc
					attesa=flight_array[succ]->dep_t-flight_array[i]->arr_t;
					if(attesa<TIME1) {
						penal=PENAL1;
					}
					else if(attesa<TIME2) {
						penal=PENAL2;
					}
					else if(attesa<TIME3) {
						penal=PENAL3;
					}
					else if(attesa<=TIME4) {
						penal=PENAL4;
					}
					else {
						printf("attesa %d\n",attesa);
						stop_error("too long waiting\n");
					}
					labels[succ][ind_labels[succ]].profit=labels[succ][ind_labels[succ]].profit-((double)(attesa)/5.0)*penal;
#endif
					labels[succ][ind_labels[succ]].pred=i;
					labels[succ][ind_labels[succ]].pred_label=li;
					labels[succ][ind_labels[succ]].n_flights=labels[i][li].n_flights+1;
					labels[succ][ind_labels[succ]].dep_time_first_flight=labels[i][li].dep_time_first_flight;
					labels[succ][ind_labels[succ]].ind_table_duration=labels[i][li].ind_table_duration;
					
					/*if(flight_array[succ]->dep!=flight_array[labels[succ][ind_labels[succ]].pred]->arr) {
						stop_error("error different airports\n");
					}*/


					ind_labels[succ]++;

					
					
					
				}
			}
			//printf("node %d count %d\n",i,count);
		}
		//determining the node with highest profit among those that have the right arrival airport
		best_profit=0.0;
		best_node=-1;
		best_label=-1;
		for(in=0;in<n_of_flights;in++) {
			if(flight_array[in]->arr!=crews[k].arr_airport) continue;
			if(not_terminal[in]) continue;
			if ( ( crews[k].arr_airport == 7 && flight_array[in]->arr_t < max_arr_time_A ) ||
			     ( crews[k].arr_airport == 3 && flight_array[in]->arr_t < max_arr_time_S ) )
			{
#ifdef RETIME
				//se il volo arriva in A o S prima dell'ultimo ma e` una sua copia, puo` essere l'ultimo di un turno
				if( (crews[k].arr_airport == 7 && are_copies(in,ultimo_A)) || (crews[k].arr_airport == 3 && are_copies(in,ultimo_S)) );
				else
#endif  	
				continue;
			}

			for(li=0;li<ind_labels[in];li++) {

				best_profit=labels[in][li].profit;
				best_node=in;
				best_label=li;


				if(best_node==-1) {
					printf("error best node = -1\n");
					stop_error("");
				} 

				if(company==0) {
					valore=(double)x_cost_B;
				}
				else if(company==1) {
					valore=(double)x_cost_N;
				}
				else {
					valore=(double)x_cost_C;
				}
				if(k==6 || k==7 || k==8 || k==9 || k==10 || k==11 || k==12 || k==13) {
					if(company==0) {
						stop_error("route of Binter sleeping in S or A\n");
					}
					else if(company==1) {
						valore=valore+(double)x_cost_pernocta_N;
					}
					else {
						valore=valore+(double)x_cost_pernocta_C;
					}
				}
				//if(best_profit>valore-beta[k]+EPSI) {
				if(best_profit>valore-beta[k]+EPSI-gap) {
					//printf("best profit %f valore %f ",best_profit, valore);
					//printf("difference %f\n",best_profit-valore+beta[k]);

					//generating a new column
					risolto=FALSE;
					//reconstructing the route found
					ind=best_node;
					ind_l=best_label;
					count=0;
					while(labels[ind][ind_l].pred!=-1) {
						tmp=ind;
						ind=labels[ind][ind_l].pred;
						ind_l=labels[tmp][ind_l].pred_label;
						count++;
					}
					count++;
					//storing the new route in the routes pool
					//printf("new col profit %f\n",best_profit);
					crew_day_pool[n_of_cols]=(crew_day*) calloc(1,sizeof(crew_day));
					if(crew_day_pool[n_of_cols]==NULL) alloc_error("crew_day_pool[n_of_cols]\n");
					crew_day_pool[n_of_cols]->type=k;
					crew_day_pool[n_of_cols]->n_flights=count;
					crew_day_pool[n_of_cols]->flights=(int*) calloc(count,sizeof(int));
					if(crew_day_pool[n_of_cols]->flights==NULL) alloc_error("crew_day_pool[n_of_cols]->flights\n");
					ind=best_node;
					ind_l=best_label;
					count=0;
					while(labels[ind][ind_l].pred!=-1) {
						crew_day_pool[n_of_cols]->flights[count]=ind;
						tmp=ind;
						ind=labels[ind][ind_l].pred;
						ind_l=labels[tmp][ind_l].pred_label;
						count++;
					}
					crew_day_pool[n_of_cols]->flights[count]=ind;
					
					/*printf("company %d\n",company);
					for(i=0;i<crew_day_pool[n_of_cols]->n_flights;i++) {
						printf("%d %d %s %d %s %d\n",
							flight_array[crew_day_pool[n_of_cols]->flights[i]]->code,
							crews[crew_day_pool[n_of_cols]->type].company,
							airports[flight_array[crew_day_pool[n_of_cols]->flights[i]]->dep],
							flight_array[crew_day_pool[n_of_cols]->flights[i]]->dep_t,
							airports[flight_array[crew_day_pool[n_of_cols]->flights[i]]->arr],
							flight_array[crew_day_pool[n_of_cols]->flights[i]]->arr_t);

					}*/

					company=crews[crew_day_pool[n_of_cols]->type].company;
					
					//adding the new column to the master
					if(company==0) {
						obj_col[0]=(double)x_cost_B; //B
					}
					else if(company==1) {
						obj_col[0]=(double)x_cost_N;
					}
					else {
						obj_col[0]=(double)x_cost_C;
					}
					cmatbeg[0]=0;
					lb_col[0]=0.0;
					ub_col[0]=1.0;

					nzcnt_col=1+crew_day_pool[n_of_cols]->n_flights+crew_day_pool[n_of_cols]->n_flights+crew_day_pool[n_of_cols]->n_flights-1;
					
					//counting the number of short arcs in the column
					for(i=crew_day_pool[n_of_cols]->n_flights-1;i>0;i--) {
						ind_start=crew_day_pool[n_of_cols]->flights[i];
						ind_end=crew_day_pool[n_of_cols]->flights[i-1];
						for(j=0;j<successori[ind_start]->n_of_succ;j++) {
							succ=successori[ind_start]->ind_succ[j];
							if(succ==ind_end) {
								if(flight_array[succ]->dep_t-flight_array[ind_start]->arr_t>=30) continue;
								nzcnt_col++;
							}
						}
					}
					
					crew_day_pool[n_of_cols]->forb=-1;
					//if the route of the crew is from S or A, or it is from A or S, higher cost
					if(crew_day_pool[n_of_cols]->type==6 || crew_day_pool[n_of_cols]->type==7 ||
						crew_day_pool[n_of_cols]->type==8 || crew_day_pool[n_of_cols]->type==9 ||
						crew_day_pool[n_of_cols]->type==19 || crew_day_pool[n_of_cols]->type==11 ||
						crew_day_pool[n_of_cols]->type==12 || crew_day_pool[n_of_cols]->type==13) {
						
						if(company==0) {
							stop_error("route of Binter from S or A\n");
						}
						else if(company==1) {
							obj_col[0]=obj_col[0]+(double)x_cost_pernocta_N;
						}
						else {
							obj_col[0]=obj_col[0]+(double)x_cost_pernocta_C;
						}
					}

#ifdef MULTI_OBJ
					for(i=crew_day_pool[n_of_cols]->n_flights-1;i>0;i--) {
						attesa=flight_array[crew_day_pool[n_of_cols]->flights[i-1]]->dep_t-
						flight_array[crew_day_pool[n_of_cols]->flights[i]]->arr_t;
						if(attesa<TIME1) {
							penal=PENAL1;
						}
						else if(attesa<TIME2) {
							penal=PENAL2;
						}
						else if(attesa<TIME3) {
							penal=PENAL3;
						}
						else if(attesa<=TIME4) {
							penal=PENAL4;
						}
						else {
							printf("attesa %d\n",attesa);
							stop_error("too long waiting\n");
						}
						obj_col[0]=obj_col[0]+((double)(attesa)/5.0)*penal;
							
					} 
#endif

					cmatind=(int*) calloc(nzcnt_col,sizeof(int));
					if(cmatind==NULL) alloc_error("cmatind\n");
					cmatval=(double*) calloc(nzcnt_col,sizeof(double));
					if(cmatval==NULL) alloc_error("cmatval\n");

					cmatind[0]=crew_day_pool[n_of_cols]->type;
					cmatval[0]=1.0;

					pos=1;
					for(i=0;i<crew_day_pool[n_of_cols]->n_flights;i++) {
#ifndef RETIME
						cmatind[pos]=n_of_crew_types+crew_day_pool[n_of_cols]->flights[i];
#else
						cmatind[pos]=n_of_crew_types+flight_array[crew_day_pool[n_of_cols]->flights[i]]->its_real_flight_index;
#endif						
						cmatval[pos]=1.0;
						pos++;
					}

					//inserting the column in the constraints linking xy
#ifndef RETIME
					posi=n_of_crew_types+n_of_flights+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1;
#else
					posi=n_of_crew_types+n_of_flights/RETIME_INTERV+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1;
#endif
					company=crews[crew_day_pool[n_of_cols]->type].company;
#ifndef RETIME
					posi=posi+n_of_flights*company;
#else
posi=posi+n_of_flights*company;
					//posi=posi+(n_of_flights/RETIME_INTERV)*company;
#endif
					for(i=0;i<crew_day_pool[n_of_cols]->n_flights;i++) {
#ifndef RETIME
						cmatind[pos]=posi+crew_day_pool[n_of_cols]->flights[i];
#else
cmatind[pos]=posi+crew_day_pool[n_of_cols]->flights[i];
						//cmatind[pos]=posi+flight_array[crew_day_pool[n_of_cols]->flights[i]]->its_real_flight_index;
#endif
						cmatval[pos]=1.0;
						pos++;
					}

					//inserting the column in the constraints linking xyz
#ifndef RETIME
					posi=n_of_crew_types+n_of_flights+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1+N_COMPANIES*n_of_flights;
#else
posi=n_of_crew_types+n_of_flights/RETIME_INTERV+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1+N_COMPANIES*(n_of_flights);
					//posi=n_of_crew_types+n_of_flights/RETIME_INTERV+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1+N_COMPANIES*(n_of_flights/RETIME_INTERV);
#endif
					
					for(i=crew_day_pool[n_of_cols]->n_flights-1;i>0;i--) {
						ind_start=crew_day_pool[n_of_cols]->flights[i];
						ind_end=crew_day_pool[n_of_cols]->flights[i-1];
						for(j=0;j<successori[ind_start]->n_of_succ;j++) {
							succ=successori[ind_start]->ind_succ[j];
							if(succ==ind_end) {
								index=successori[ind_start]->arcs[j];
								break;
							}
						}
						cmatind[pos]=posi+index;
						cmatval[pos]=1.0;
						pos++;
					}

					//inserting the column in the constraints linking x y for short arcs
#ifndef RETIME
					posi=n_of_crew_types+n_of_flights+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1+N_COMPANIES*n_of_flights+n_of_arcs;
#else
posi=n_of_crew_types+n_of_flights/RETIME_INTERV+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1+N_COMPANIES*(n_of_flights)+n_of_arcs;
					//posi=n_of_crew_types+n_of_flights/RETIME_INTERV+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1+N_COMPANIES*(n_of_flights/RETIME_INTERV)+n_of_arcs;
#endif
					company=crews[crew_day_pool[n_of_cols]->type].company;
					posi=posi+n_of_short_arcs*company;
					for(i=crew_day_pool[n_of_cols]->n_flights-1;i>0;i--) {
						ind_start=crew_day_pool[n_of_cols]->flights[i];
						ind_end=crew_day_pool[n_of_cols]->flights[i-1];
						for(j=0;j<successori[ind_start]->n_of_succ;j++) {
							succ=successori[ind_start]->ind_succ[j];
							if(succ==ind_end) {
								if(flight_array[succ]->dep_t-flight_array[ind_start]->arr_t>=30) continue;
								index=indices_short_arcs[ind_start][j];
								cmatind[pos]=posi+index;
								cmatval[pos]=1.0;
								pos++;
								break;
							}
						}
						
					}

					if(pos!=nzcnt_col) stop_error("pos different from nzcnt_col");
										
					status=CPXaddcols(env,lp,1,nzcnt_col,obj_col,cmatbeg,cmatind,cmatval,lb_col,ub_col,NULL);
					if(status) {
						stop_error("CPXaddcols\n");
					}

					free(cmatind);
					free(cmatval);

					n_of_cols++;
					new_cols++;
				}
				

			}
		}
		

		
	}

		

	/*CPXwriteprob(env,lp,"prova.lp",NULL);
	exit(0);*/

	for(i=0;i<n_of_flights;i++) {
		free(labels[i]);
	}
	free(labels);
	free(ind_labels);
	free(cmatbeg);
	free(obj_col);
	free(lb_col);
	free(ub_col);
	return new_cols;
}
#endif

#ifdef ALL_COLS_AND_Y_IN_GAP
void solve_additional_cols_and_y()
{
	char* ctype;
	int* indices;
	int i,k;
	int status;
	double objval;
	int pos, index;
	int j,l;
	int dep_airport;
	int number_nodes;
	int start_node;
	int count;
	int node;
	int end_node;
	time_t start_IP, end_IP;
	int lpstat;
	int cont;
	int ind;
	int* delstat;
	double* dj;
	double gap;
	int indices_bd[1];
	char lu[1];
	double bd[1];
	int num_y_UB_0;
	double* ub;
	int num_z_UB_0;
	int* ind_var_del;
	int num_aux_UB_0;
	time_t start_UB,end_UB;
#ifdef LOG_FILE_CPLEX
	CPXFILEptr fp;
#endif
	time_t start_col,end_col;
	int num_y_UB_G;
	


	//risolvo nuovamente l'LP per ottenere i costi ridotti delle var y
	status = CPXchgprobtype (env, lp, CPXPROB_LP);
	if(status) {
		stop_error("CPXchgprobtype\n");
	}

	time(&start_cplex);
	//status=CPXprimopt(env,lp);
	status=CPXbaropt(env,lp);
	if(status) {
		stop_error("CPXbaropt\n");
	}
	time(&end_cplex);
	durata_cplex=durata_cplex+difftime(end_cplex,start_cplex);
	printf("durata cplex finora %f secondi\n",durata_cplex);
	
	if(x!=NULL) {
		free(x);
		x=NULL;
	}
	x=(double*) calloc(offset+n_of_cols,sizeof(double));
	if(x==NULL) alloc_error("x\n");
	dj=(double*) calloc(offset+n_of_cols,sizeof(double));
	if(dj==NULL) alloc_error("dj\n");
	
	status=CPXsolution(env,lp,&lpstat,&objval,x,dual_vars,NULL,dj);
	if(status) {
		printf("status %d lpstat %d\n",status,lpstat);
		stop_error("CPXsolution\n");
	}

	printf("objective %f lpstat %d\n",objval,lpstat);
	printf("number of created columns %d\n",n_of_cols);



	time(&start_IP);

	//recupero le duali
	/*store the dual variables*/
	for(i=0;i<n_of_crew_types;i++) {
		beta[i]=dual_vars[i];
	}
#ifndef RETIME
	for(i=0;i<n_of_flights;i++) {
		alpha[i]=dual_vars[n_of_crew_types+i];
		//printf("alpha[%d] %f\n",flight_array[i]->code,alpha[i]);
	}
	ind=0;
	for(i=0;i<N_COMPANIES;i++) {
		for(k=0;k<n_of_flights;k++) {
			gamma_[i][k]=dual_vars[n_of_crew_types+n_of_flights+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1+ind];
			/*printf("gamma[%d][%d] %f\n",i,
				flight_array[k]->code,
				gamma_[i][k]);*/
			ind++;
		}
	}
	ind=0;
	for(i=0;i<n_of_flights;i++) {
		for(j=0;j<successori[i]->n_of_succ;j++) {
			eta[i][j]=dual_vars[n_of_crew_types+n_of_flights+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1+N_COMPANIES*n_of_flights+ind];
			ind++;
		}
	}

	ind=0;
	for(k=0;k<N_COMPANIES;k++) {
		for(i=0;i<n_of_flights;i++) {
			for(j=0;j<successori[i]->n_of_succ;j++) {
				delta[k][i][j]=0;
				if(flight_array[successori[i]->ind_succ[j]]->dep_t-flight_array[i]->arr_t>=30) continue;
				delta[k][i][j]=dual_vars[n_of_crew_types+n_of_flights+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1+N_COMPANIES*n_of_flights+n_of_arcs+ind];
				ind++;
			}
		}
	}
#endif

#ifdef RETIME
		for(i=0;i<n_of_flights/RETIME_INTERV;i++) {
			alpha[i]=dual_vars[n_of_crew_types+i];
			//printf("alpha[%d] %f\n",flight_array[i]->code,alpha[i]);
		}
		ind=0;
		for(i=0;i<N_COMPANIES;i++) {
			for(k=0;k<n_of_flights;k++) {
				gamma_[i][k]=dual_vars[n_of_crew_types+n_of_flights+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1+ind];
				/*printf("gamma[%d][%d] %f\n",i,
					flight_array[k]->code,
					gamma_[i][k]);*/
				ind++;
			}
		}
		ind=0;
		for(i=0;i<n_of_flights;i++) {
			for(j=0;j<successori[i]->n_of_succ;j++) {
				eta[i][j]=dual_vars[n_of_crew_types+n_of_flights/RETIME_INTERV+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1+N_COMPANIES*(n_of_flights)+ind];
				ind++;
			}
		}

		ind=0;
		for(k=0;k<N_COMPANIES;k++) {
			for(i=0;i<n_of_flights;i++) {
				for(j=0;j<successori[i]->n_of_succ;j++) {
					delta[k][i][j]=0;
					if(flight_array[successori[i]->ind_succ[j]]->dep_t-flight_array[i]->arr_t>=30) continue;
					delta[k][i][j]=dual_vars[n_of_crew_types+n_of_flights/RETIME_INTERV+n_of_aircraft_types+n_of_aircraft_types*n_of_flights+1+1+N_COMPANIES*(n_of_flights)+n_of_arcs+ind];
				    ind++;
				}
			}
		}
#endif

	time(&start_UB);

	ind_var_del=(int*) calloc(offset,sizeof(int));
	if(ind_var_del==NULL) alloc_error("ind_var_del\n");

	ub=(double*) calloc(n_of_y,sizeof(double));
	if(ub==NULL) alloc_error("ub\n");
	pos=n_auxil;
	num_y_UB_0=0;
	status = CPXgetub (env, lp, ub, pos, pos+n_of_y-1);
	for(i=0;i<n_of_y;i++) {
		if(ub[i]<EPSI) {
			ind_var_del[pos+i]=1;
			num_y_UB_0++;
		}
	}
	printf("num y UB=0 prima %d	\n",num_y_UB_0);
	free(ub);

	ub=(double*) calloc(n_of_z,sizeof(double));
	if(ub==NULL) alloc_error("ub\n");
	pos=n_auxil+n_of_y;
	num_z_UB_0=0;
	status = CPXgetub (env, lp, ub, pos, pos+n_of_z-1);
	for(i=0;i<n_of_z;i++) {
		if(ub[i]<EPSI) {
			ind_var_del[pos+i]=1;
			num_z_UB_0++;
		}
	}
	printf("num z UB=0 prima %d	\n",num_z_UB_0);
	free(ub);

	

	//azzero l'UB delle var y che hanno profitto ridotto in base al gap
	num_y_UB_0=0;
	gap=BestUB-BestLB;

//printf("\n\nGAP %f\n\n",gap);
//exit(0);

	ind=0;
	pos=n_auxil;
	for(j=0;j<n_of_aircraft_types;j++) {
		for(k=0;k<N_MAIN_AIRPORTS;k++) {
			for(i=0;i<starting_arcs[k]->n_of_succ;i++) {
				if(dj[pos+ind]>EPSI+gap) {
					indices_bd[0]=pos+ind;
					lu[0]='U';
					bd[0]=0.0;
					num_y_UB_0++;
					status=CPXchgbds(env,lp,1,indices_bd,lu,bd);
					if(status) {
						stop_error("CPXchgbds\n");
					}
					ind_var_del[pos+ind]=1;
				}
								
				ind++;
			}
		}
	}

	for(j=0;j<n_of_aircraft_types;j++) {
		for(i=0;i<n_of_arcs_a;i++) {		
			if(dj[pos+ind]>EPSI+gap) {
				indices_bd[0]=pos+ind;
				lu[0]='U';
				bd[0]=0.0;
				num_y_UB_0++;
				status=CPXchgbds(env,lp,1,indices_bd,lu,bd);
				if(status) {
					stop_error("CPXchgbds\n");
				}
				ind_var_del[pos+ind]=1;
			}
			ind++;
		}
	}

	for(j=0;j<n_of_aircraft_types;j++) {
		for(k=0;k<N_MAIN_AIRPORTS;k++) {
			for(i=0;i<ending_arcs[k]->n_of_succ;i++) {
				if(dj[pos+ind]>EPSI+gap) {
					indices_bd[0]=pos+ind;
					lu[0]='U';
					bd[0]=0.0;
					num_y_UB_0++;
					status=CPXchgbds(env,lp,1,indices_bd,lu,bd);
					if(status) {
						stop_error("CPXchgbds\n");
					}
					ind_var_del[pos+ind]=1;
				}
								
				ind++;
			}
		}
	}

	
	printf("num y UB a zero %d\n",num_y_UB_0);

	//azzero l'UB delle var z che hanno profitto ridotto in base al gap
	num_z_UB_0=0;
	pos=n_auxil+n_of_y;
	ind=0;
	for(i=0;i<n_of_flights;i++) {
		for(k=0;k<successori[i]->n_of_succ;k++) {
			if(dj[pos+ind]>EPSI+gap) {
				indices_bd[0]=pos+ind;
				lu[0]='U';
				bd[0]=0.0;
				num_z_UB_0++;
				status=CPXchgbds(env,lp,1,indices_bd,lu,bd);
				if(status) {
					stop_error("CPXchgbds\n");
				}
				ind_var_del[pos+ind]=1;
			}
			ind++;
		}
	}
	printf("num z UB a zero %d\n",num_z_UB_0);

	//azzero l'UB delle var aux che hanno profitto ridotto in base al gap 
	num_aux_UB_0=0;
	for(i=0;i<n_auxil;i++) {
		if(dj[i]>EPSI+gap) {
			indices_bd[0]=i;
			lu[0]='U';
			bd[0]=0.0;
			num_aux_UB_0++;
			status=CPXchgbds(env,lp,1,indices_bd,lu,bd);
			if(status) {
				stop_error("CPXchgbds\n");
			}
			ind_var_del[i]=1;
		}
	}
	printf("num aux UB a zero %d\n",num_aux_UB_0);

	//counting the number of y vars in the gap
	ub=(double*) calloc(n_of_y,sizeof(double));
	if(ub==NULL) alloc_error("ub\n");
	pos=n_auxil;
	num_y_UB_0=0;
	status = CPXgetub (env, lp, ub, pos, pos+n_of_y-1);
	for(i=0;i<n_of_y;i++) {
		if(ub[i]>EPSI) {
			num_y_UB_G++;
		}
	}
	printf("num y nel gap %d\n",num_y_UB_G);
	free(ub);

	
	
	

	free(dj);
	time(&end_UB);
	printf("durata modifica UB %f secondi\n",difftime(end_UB,start_UB));
	//exit(0);

	

	//elimino le colonne e poi rigenero solo quelle che servono
	delstat=(int*) calloc(CPXgetnumcols(env,lp), sizeof(int));
	if(delstat==NULL) alloc_error("delstat\n");
	for(i=offset;i<CPXgetnumcols(env,lp);i++) {
		delstat[i]=1;
	}
	status = CPXdelsetcols (env, lp, delstat);
	if(status) {
		stop_error("CPXdelsetcols\n");
	}
	free(delstat);
	n_of_cols=0;

	time(&start_col);
	//creo le colonne necessarie in base al gap tra BestUB e BestLB
	cont=solve_pricing_all_cols_in_gap();
	time(&end_col);
	printf("durata generazione colonne nel gap %f secondi\n",difftime(end_col,start_col));
	
	printf("Columns in gap %d\n",cont);
	//exit(0);

	//ricreo il problema come intero con le nuove colonne
	/*ctype=(char*) calloc(offset+n_of_cols,sizeof(char));
	if(ctype==NULL) alloc_error("ctype\n");
	indices=(int*) calloc(offset+n_of_cols,sizeof(int));
	if(indices==NULL) alloc_error("indices\n");
	for(i=0;i<offset+n_of_cols;i++) {
		indices[i]=i;
		ctype[i]='B';
	}
	
	status=CPXchgctype(env,lp,offset+n_of_cols,indices,ctype);*/
	//solo x e z intere
	ctype=(char*) calloc(n_of_z+n_of_cols,sizeof(char));
	if(ctype==NULL) alloc_error("ctype\n");
	indices=(int*) calloc(n_of_z+n_of_cols,sizeof(int));
	if(indices==NULL) alloc_error("indices\n");
	for(i=0;i<n_of_z+n_of_cols;i++) {
		indices[i]=i+n_auxil+n_of_y;
		ctype[i]='B';
	}
	
	status=CPXchgctype(env,lp,n_of_z+n_of_cols,indices,ctype);
	if(status) {
		stop_error("CPXchgctype\n");
	}

	free(ctype);
	free(indices);

	status = CPXsetdblparam(env, CPX_PARAM_TILIM, (double)INF);  
	//status = CPXsetdblparam(env, CPX_PARAM_TILIM, 7200.0);  
	status = CPXsetdblparam(env, CPX_PARAM_TILIM, (double)TIME_LIMIT_BIG);
	if(status) {
		printf("error in setdblparam\n");
		stop_error("");
	}

	//elimino le y,z e aux
	/*delstat=(int*) calloc(CPXgetnumcols(env,lp), sizeof(int));
	if(delstat==NULL) alloc_error("delstat\n");
	for(i=0;i<offset;i++) {
		if(ind_var_del[i]) {
			delstat[i]=1;
		}
	}
	status = CPXdelsetcols (env, lp, delstat);
	if(status) {
		stop_error("CPXdelsetcols\n");
	}
	free(delstat);*/
	free(ind_var_del);

#ifdef LOG_FILE_CPLEX
	fp = CPXfopen ("mylog.log", "w");
	status = CPXsetlogfile (env, fp);
	if(status) {
		stop_error("CPXsetlogfile\n");
	}
#endif
	
	/*CPXwriteprob(env,lp,"allcolsingap.lp",NULL);
	exit(0);*/
	 
	status=CPXmipopt(env,lp);
	if(status) {
		stop_error("CPXmipoopt\n");
	} 
	status=CPXgetstat(env,lp);
	if(status==101 || status==102 || status==107) {
		printf("status %d\n",status);

		/*CPXwriteprob(env,lp,"prova.lp",NULL);
		exit(0);*/
	 
		if(x!=NULL) {
			free(x);
			x=NULL;
		}
		x=(double*) calloc(offset+n_of_cols,sizeof(double));
		if(x==NULL) alloc_error("x\n");

		status=CPXgetmipx(env,lp,x,0,CPXgetnumcols(env,lp)-1);
		if(status) {
			stop_error("CPXgetmipx\n");
		}
		status=CPXgetmipobjval(env,lp,&objval);
		if(status) {
			stop_error("CPXgetmipobjval\n");
		} 
		printf("objval %f\n",objval);

		best_sol=round_(objval);
		printf("int sol found %d\n",best_sol);

		printf("solution\n\n\n");
			 
		printf("%s\n",our_day);
		printf("%d\n",best_sol);
		
		 
		//emptying the y_sol
		for(i=0;i<n_of_aircraft_types;i++) {
			for(j=0;j<n_of_flights;j++) {
				for(l=0;l<n_of_flights;l++) {
					y_sol[i][j][l]=0;
				} 
			}
		} 
		pos=n_auxil+n_of_starting_arcs*n_of_aircraft_types;
		for(j=0;j<n_of_aircraft_types;j++) {
			for(i=0;i<n_of_flights;i++) {	
				for(k=0;k<successori_a[i]->n_of_succ;k++) {
					if(x[pos]<EPSI) {
						pos++;
						continue;
					} 
					 /*printf("y_%d_%d_%s_%d_%d %f\n",
						 flight_array[i]->code,
						 flight_array[successori_a[i]->ind_succ[k]]->code,
						 airports[aircrafts[j].dep_airport],
						 aircrafts[j].company,j, x[index]);*/
					if(aircrafts[j].company==0) {
						 /*printf("B;%d;%d\n",flight_array[i]->code,
						 flight_array[successori_a[i]->ind_succ[k]]->code);*/
						y_sol[j][i][successori_a[i]->ind_succ[k]]=1;
					}
					else if(aircrafts[j].company==1) {
						 /*printf("N;%d;%d\n",flight_array[i]->code,
						 flight_array[successori_a[i]->ind_succ[k]]->code);*/
						y_sol[j][i][successori_a[i]->ind_succ[k]]=1;
					}
					else {
						 /*printf("C;%d;%d\n",flight_array[i]->code,
						 flight_array[successori_a[i]->ind_succ[k]]->code);*/
					    y_sol[j][i][successori_a[i]->ind_succ[k]]=1;
					}
					pos++;
				}
			}
		}
		//printf("\n\n");
		 
		//printing the solution for y and z
		//printf("yz\n");
		index=n_auxil;
		for(j=0;j<n_of_aircraft_types;j++) {
			for(k=0;k<N_MAIN_AIRPORTS;k++) {
				dep_airport=starting_arcs[k]->airport;
				for(i=0;i<starting_arcs[k]->n_of_succ;i++) {
					if(x[index]<EPSI) {
						index++;
						continue;
					}
					 /*printf("ys_%d_%s_%d_%d %f\n",
						 flight_array[starting_arcs[k]->ind_succ[i]]->code,
						 airports[dep_airport],aircrafts[j].company,j,x[index]);*/
					for(l=0;l<n_of_flights;l++) {
						y_sol[j][l][starting_arcs[k]->ind_succ[i]]=2;
					}
					index++;
				}
			}
		}

		 

		for(i=0;i<n_of_aircraft_types;i++) {
			for(j=0;j<n_of_flights;j++) {
				for(l=0;l<n_of_flights;l++) {
					best_y_sol[i][j][l]=y_sol[i][j][l];
				}  
			}
		}

    	//printing in the format for the picture
		number_nodes=0;
		for(j=0;j<n_of_aircraft_types;j++) {
			for(l=0;l<aircrafts[j].n_of_aircrafts;l++) {
				start_node=FALSE;
				for(i=0;i<n_of_flights;i++) {
					count=0;
					//looking for a node without entering arcs
					for(k=0;k<n_of_flights;k++) {
						if(y_sol[j][k][i]==-1) break;
						if(y_sol[j][k][i]==1) break;
						if(y_sol[j][k][i]==2) {
							count++;
						}
					}
					if(count==n_of_flights) {
						if(aircrafts[j].company==0) {
							printf("B;%d",flight_array[i]->code);
						}
						else if(aircrafts[j].company==1) {
							printf("N;%d",flight_array[i]->code);
						}
						else if(aircrafts[j].company==2) {
							printf("C;%d",flight_array[i]->code);
						}
						start_node=TRUE;
						y_sol[j][0][i]=-1;
						node=i;
						number_nodes++;
						break;
					}					
				}
				if(start_node==FALSE) {
					break;
				}
				end_node=FALSE;
				while(!end_node) {
					count=0;
					for(k=0;k<n_of_flights;k++) {
						if(y_sol[j][node][k]==1) {
							printf(";%d",flight_array[k]->code);
							
							y_sol[j][node][k]=0;
							node=k;
							number_nodes++;
							break;
						}
					    count++;
					}
					if(count==n_of_flights) {
						printf("\n");
						end_node=TRUE;
						if(number_nodes!=n_of_flights) {
							start_node=FALSE;
						}
					}
				}
			}
		}

		printf("\n---\n");

		//storing the best solution
		pos=0;
		for(i=0;i<n_of_cols;i++) {
			if(x[i+offset]>EPSI) {
				best_solution[pos]->n_flights=crew_day_pool[i]->n_flights;
				best_solution[pos]->type=crew_day_pool[i]->type;
				index=0;
				for(k=crew_day_pool[i]->n_flights-1;k>=0;k--) {
					best_solution[pos]->flights[index]=crew_day_pool[i]->flights[k];
					printf("%d;",flight_array[crew_day_pool[i]->flights[k]]->code);
					 /*printf("(%d)%s %d-%s %d, ",flight_array[crew_day_pool[i]->flights[k]]->code,
						 airports[flight_array[crew_day_pool[i]->flights[k]]->dep],
						 flight_array[crew_day_pool[i]->flights[k]]->dep_t,
						 airports[flight_array[crew_day_pool[i]->flights[k]]->arr],
						 flight_array[crew_day_pool[i]->flights[k]]->arr_t);*/
					index++;
				} 
				printf("\n");
				pos++;
			} 
		}
		n_crew_duties=pos;

		
		if(x!=NULL) {
			free(x);
			x=NULL;
		}

	}
	
	else {
		printf("status %d\n",status);
		stop_error("no int sol found");
	}
    
    time(&end_IP);
    printf("durata IP %f secondi\n",difftime(end_IP,start_IP));
    
#ifdef LOG_FILE_CPLEX
    CPXfclose(fp);
#endif
}
#endif

#ifdef EVAL_MANUAL_SOL
int find_index_flight(int flight_code)
{
	int i;

	for(i=0;i<n_of_flights;i++) {
		if(flight_array[i]->code==flight_code) {
			return i;
		}
	}

	return -1;
}
double eval_sol(char* name_sol_file)
//attenzione: nei test del paper dove c'e` //qui volo_2 c'era volo_2...
{
	FILE* f_man;
	char buf[DIM];
	char app[DIM];
	int iapp;
	int company;
	int index;
	int dep_airport;
	int arr_airport;
	double cost;
	int** aerei;
	int ind_aereo, ind_volo;
	int i,j;
	int volo_1,volo_2;
	int attesa;
	double penal;
	int* companies;
	int aereo_1,aereo_2;
	int num_aerei, num_piloti, num_cambi;
	double costo_attese;
	int num_attese_minore_TIME1, num_attese_minore_TIME2, num_attese_minore_TIME3, num_attese_minore_TIME4;
	double aircraft_cost;
	int l;
	
	f_man=fopen(name_sol_file, "r");
	if(f_man==NULL) stop_error("main cannot open manual sol file\n");

	aerei=(int**) calloc(100,sizeof(int*));
	if(aerei==NULL) alloc_error("aerei\n");
	for(i=0;i<100;i++) {
		aerei[i]=(int*) calloc(100,sizeof(int));
		if(aerei[i]==NULL) alloc_error("aerei[i]\n");
		for(j=0;j<100;j++) {
			aerei[i][j]=-1;
		}
	}

	companies=(int*) calloc(100,sizeof(int));
	if(companies==NULL) alloc_error("companies\n");

	cost=0.0;
	num_aerei=0;
	num_piloti=0;
	num_cambi=0;
	costo_attese=0.0;
	aircraft_cost=0.0;
	num_attese_minore_TIME1=0;
	num_attese_minore_TIME2=0;
	num_attese_minore_TIME3=0;
	num_attese_minore_TIME4=0;
	//skip the date
	fgets(buf,DIM,f_man);
	//skip the two values
	fgets(buf,DIM,f_man);
	fgets(buf,DIM,f_man);

	fscanf(f_man,"%s ",app);
	
	if(strcmp(app,"B")==0) {
		company=0;
	}
	else if(strcmp(app,"N")==0) {
		company=1;
	}
	else if(strcmp(app,"C")==0) {
		company=2;
	}

	ind_aereo=0;
	while(fscanf(f_man,"%s ",app)!=EOF) {
		
		iapp=atoi(app);
		index=find_index_flight(iapp);

/*printf("\nflight %d code %d %s %d %s %d\n", index, flight_array[index]->code, airports[flight_array[index]->dep],
			flight_array[index]->dep_t, airports[flight_array[index]->arr],
			flight_array[index]->arr_t);*/

		dep_airport=flight_array[index]->dep;
		ind_volo=0;
		aerei[ind_aereo][ind_volo]=index;
		ind_volo++;
		companies[ind_aereo]=company;
		if(dep_airport==0) {
			cost=cost+y_cost_L;
			num_aerei++;
		}
		else if(dep_airport==1) {
			cost=cost+y_cost_T;
			num_aerei++;
		}
		else {
			cost=cost+y_cost_L;
			num_aerei++;
		}
		while(fscanf(f_man," %s",app)!=EOF) {
			
			if(strcmp(app,"B")==0) {
				company=0;
				ind_aereo++;
				companies[ind_aereo]=company;
				if(dep_airport==1 && arr_airport==1) {
					cost=cost+y_cost_TT;
					printf("TT\n");
				}
				if(dep_airport==0 && arr_airport==0) {
					cost=cost+y_cost_LL;
					printf("LL\n");
				}
				break;
			}
			else if(strcmp(app,"N")==0) {
				company=1;
				ind_aereo++;
				companies[ind_aereo]=company;
				if(dep_airport==1 && arr_airport==1) {
					cost=cost+y_cost_TT;
					printf("TT\n");
				}
				if(dep_airport==0 && arr_airport==0) {
					cost=cost+y_cost_LL;
					printf("LL\n");
				}
				break;
			}
			else if(strcmp(app,"C")==0) {
				company=2;
				ind_aereo++;
				companies[ind_aereo]=company;
				if(dep_airport==1 && arr_airport==1) {
					cost=cost+y_cost_TT;
					printf("TT\n");
				}
				if(dep_airport==0 && arr_airport==0) {
					cost=cost+y_cost_LL;
					printf("LL\n");
				}
				break;
			}
			else if(strcmp(app,"---")==0) {
				//questo non c'era nei test del paper
				if(dep_airport==1 && arr_airport==1) {
					cost=cost+y_cost_TT;
					printf("TT\n");
				}
				if(dep_airport==0 && arr_airport==0) {
					cost=cost+y_cost_LL;
					printf("LL\n");
				}//fino qui
				break;
			}
			iapp=atoi(app);
			index=find_index_flight(iapp);
/*printf("flight %d code %d %s %d %s %d\n", index, flight_array[index]->code, airports[flight_array[index]->dep],
			flight_array[index]->dep_t, airports[flight_array[index]->arr],
			flight_array[index]->arr_t);*/
			arr_airport=flight_array[index]->arr;
			aerei[ind_aereo][ind_volo]=index;
			ind_volo++;
			
		}
		if(strcmp(app,"---")==0) {

			break;
		}
		
	} 
	aircraft_cost=cost;

	printf("aircraft cost %f\n",aircraft_cost);

	//skip the ---
	//fgets(buf,DIM,f_man);
	
	//reading the part of the crew
	while(fscanf(f_man,"%s ",app)!=EOF) {
		iapp=atoi(app);
		index=find_index_flight(iapp);
		volo_1=index;
		printf("volo iniziale %d\n",flight_array[volo_1]->code);


/*printf("flight %d code %d %s %d %s %d\n", volo_1, flight_array[volo_1]->code, airports[flight_array[volo_1]->dep],
			flight_array[volo_1]->dep_t, airports[flight_array[volo_1]->arr],
			flight_array[volo_1]->arr_t);
printf("copies:\n");
for(l=0;l<RETIME_INTERV-1;l++) {
	printf("code %d\n",flight_array[volo_1]->copies[l]);
}*/

		for(ind_aereo=0;ind_aereo<100;ind_aereo++) {
			for(ind_volo=0;ind_volo<100;ind_volo++) {
				if(aerei[ind_aereo][ind_volo]==-1) break;
				if(aerei[ind_aereo][ind_volo]==volo_1) {
					aereo_1=ind_aereo;
					break;
				}
			}
			if(aerei[ind_aereo][ind_volo]==volo_1) {
				break;
			}
		}
		while(fscanf(f_man,"%s ",app)!=EOF) {
			if(strcmp(app,".")==0) {
//printf("\n");
				for(ind_aereo=0;ind_aereo<100;ind_aereo++) {
					for(ind_volo=0;ind_volo<100;ind_volo++) {
						if(aerei[ind_aereo][ind_volo]==-1) break;
						if(aerei[ind_aereo][ind_volo]==volo_1) {//qui volo_2
							company=companies[ind_aereo];
							break;
						}
					}
					if(aerei[ind_aereo][ind_volo]==volo_1) {//qui volo_2
						break;
					}
				}
				if(company==0) {
					cost=cost+x_cost_B;
					num_piloti++;
					printf("pilota B costo %d\n",x_cost_B);
				}
				else if(company==1) {
					cost=cost+x_cost_N;
					num_piloti++;
					printf("pilota N costo %d\n",x_cost_N);
				}
				else if(company==2) {
					cost=cost+x_cost_C;
					num_piloti++;
					printf("pilota C costo %d\n",x_cost_C);
				}

				break;
			}
			iapp=atoi(app);
			index=find_index_flight(iapp);
			volo_2=index;
/*printf("flight %d code %d %s %d %s %d\n", volo_2, flight_array[volo_2]->code, airports[flight_array[volo_2]->dep],
			flight_array[volo_2]->dep_t, airports[flight_array[volo_2]->arr],
			flight_array[volo_2]->arr_t);
printf("copies:\n");
for(l=0;l<RETIME_INTERV-1;l++) {
	printf("code %d\n",flight_array[volo_1]->copies[l]);
}*/

			for(ind_aereo=0;ind_aereo<100;ind_aereo++) {
				for(ind_volo=0;ind_volo<100;ind_volo++) {
					if(aerei[ind_aereo][ind_volo]==-1) break;
					if(aerei[ind_aereo][ind_volo]==volo_2) {
						aereo_2=ind_aereo;
						break;
					}
				}
				if(aerei[ind_aereo][ind_volo]==volo_2) {
					break;
				}
			}
			if(aereo_1!=aereo_2) {
				cost=cost+z_cost;
				num_cambi++;
			}
			//calcola il tempo pesato di attesa
			attesa=flight_array[volo_2]->dep_t-flight_array[volo_1]->arr_t;
			if(attesa<TIME1) {
				penal=PENAL1;
				num_attese_minore_TIME1=num_attese_minore_TIME1+1;
			}
			else if(attesa<TIME2) {
				penal=PENAL2;
				num_attese_minore_TIME2=num_attese_minore_TIME2+1;
			}
			else if(attesa<TIME3) {
				penal=PENAL3;
				num_attese_minore_TIME3=num_attese_minore_TIME3+1;
			}
			else if(attesa<=TIME4) {
				penal=PENAL4;
				num_attese_minore_TIME4=num_attese_minore_TIME4+1;
			}
			else {
				printf("attesa %d\n",attesa);
				printf("voli %d %d\n",flight_array[volo_1]->code, flight_array[volo_2]->code);
				stop_error("too long waiting\n");
			}
			printf("%d-%d: attesa %d penal %f\n",flight_array[volo_1]->code,
				flight_array[volo_2]->code,attesa, penal);
			cost=cost+((double)(attesa)/5.0)*penal;
			costo_attese=costo_attese+((double)(attesa)/5.0)*penal;
			volo_1=volo_2;
			aereo_1=aereo_2;
		}
	}

	printf("crew cost %f\n",cost-aircraft_cost-costo_attese-num_cambi*z_cost);
	printf("costo cambi %d\n",num_cambi*z_cost);

	fclose(f_man);

	for(i=0;i<100;i++) {
		free(aerei[i]);
	}
	free(aerei);
	free(companies);

	printf("num aerei %d\n",num_aerei);
	printf("num_piloti %d\n",num_piloti);
	printf("num cambi %d\n",num_cambi);
	printf("costo totale %f\n",cost);
	printf("costo attese %f\n",costo_attese);
	printf("num attese: min_TIME1=%d min_TIME2=%d, min_TIME3=%d, min_TIME4=%d\n",num_attese_minore_TIME1,num_attese_minore_TIME2,
	num_attese_minore_TIME3,num_attese_minore_TIME4);

	return cost;

}
#endif

#ifdef RETIME
int are_copies(int flight_a,int flight_b) 
{
	int c;
	for(c=0;c<RETIME_INTERV-1;c++) {
		if(flight_array[flight_a]->code==flight_array[flight_b]->copies[c]) return TRUE; 
	}
	return FALSE;
}

int find_first_flight_A()
{
	int i;
	for(i=0;i<n_of_flights;i++) {
		if(flight_array[i]->dep_t==min_dep_time_A && flight_array[i]->dep==7) return i;
	}
	return -1;
}
int find_first_flight_S()
{
	int i;
	for(i=0;i<n_of_flights;i++) {
		if(flight_array[i]->dep_t==min_dep_time_S && flight_array[i]->dep==3) return i;
	}
	return -1;
}
int find_last_flight_A()
{
	int i;
	for(i=0;i<n_of_flights;i++) {
		if(flight_array[i]->arr_t==max_arr_time_A && flight_array[i]->arr==7) return i;
	}
	return -1;
}
int find_last_flight_S()
{
	int i;
	for(i=0;i<n_of_flights;i++) {
		if(flight_array[i]->arr_t==max_arr_time_S && flight_array[i]->arr==3) return i;
	}
	return -1;
}
#endif

#ifdef PRICING_ARCHI
int pricing_arcs(double* duals)
{
	int i,j;
	int status;
	double* ub;
	double reduced_cost;
	int ind;
	double* obj;
	int* indices;
	char* lu;
	double* bd;
	int count;

	ind=n_auxil;
	ub=(double*) calloc(1,sizeof(double));
	if(ub==NULL) alloc_error("ub\n");
	obj=(double*) calloc(1,sizeof(double));
	if(obj==NULL) alloc_error("obj\n");
	indices=(int*) calloc(1,sizeof(int));
	if(indices==NULL) alloc_error("indices\n");
	lu=(char*) calloc(1,sizeof(char));
	if(lu==NULL) alloc_error("lu\n");
	bd=(double*) calloc(1,sizeof(double));
	if(bd==NULL) alloc_error("bd\n");
	count=0;
	
	for(i=0;i<n_of_starting_arcs+n_of_arcs+n_of_ending_arcs;i++) {
		status=CPXgetub(env,lp,ub,ind,ind);
		if(status) stop_error("cannot get ub\n");
		if(ub[0]>0.0) {
			ind++;
			continue;
		}
		status=CPXgetobj(env,lp,obj,ind,ind);
		if(status) stop_error("cannot get obj\n");
		reduced_cost=0.0;
		for(j=0;j<pricing_yvars[ind].cont;j++) {
			reduced_cost=reduced_cost+obj[0]-pricing_yvars[ind].coeff_vincoli[j]*duals[pricing_yvars[ind].indici_vincoli[j]];
		}
		if(reduced_cost<0.0) {
			//y var con costo ridotto negativo
			indices[0]=ind;
			lu[0]='U';
			bd[0]=1.0;
			status=CPXchgbds(env,lp,1,indices,lu,bd);
			if(status) stop_error("cannot change ub\n");
			count++;
		}
		ind++;
	}
	return count;
	free(ub);
	free(obj);
	free(indices);
	free(lu);
	free(bd);
}
#endif
