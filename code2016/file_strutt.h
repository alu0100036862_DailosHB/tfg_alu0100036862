#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <malloc.h>
#include <time.h>
#include <math.h>
#include <ilcplex/cplex.h>

//#define RETIME //per effettuare il retiming dei voli
//#define RETIME_INTERV 3 //significa che con RETIME_INTERV 3 vengono creati 3 voli dove prima ne avevamo 1 (+-5 min)
//#define RETIME_5 //per avere in totale 5 copie (+-10 min): va insieme a RETIME_INTERV 5
//#define ONLY_PENAL_COST //per avere in funzione obiettivo solo le penalita` della robustezza e i cambi
//#define BOUND_COMPLETAMENTO //per terminare la generazione di colonne senza arrivare in fondo, ma avendo un bound valido (per accelerare la risoluzione)
//#define PRICING_ARCHI //per avere il pricing delle variabili di arco nel caso del retiming
//#define PRICING_ARCHI_BOUND //setta il bound delle y e z a zero tranne che per i voli originali


#define DIM 150
#define N_OF_AIRPORTS 13
#define TRUE 1
#define FALSE 0
#define MAX_LABELS 10000
//#define MAX_LABELS 100000
#define INF 100000000
//#define MAX_CREW_POOL 1000000 //caso all_dp1
#define MAX_CREW_POOL 100000
#define EPSI 0.000001
#define MULTI_OBJ
#define N_MAIN_AIRPORTS 4 //T, L, S, A
#define N_COMPANIES 3 //0=B 1=N 2=C
#define INCR_COST 100 //factor for which multply the cost read in the config_file (otherwise the cost of the waiting is too large)

//#define TIME_LIMIT 180000
//#define TIME_LIMIT 60
//#define TIME_LIMIT 120 //caso 6 aprile; usato anche per tutti i confronti con 2colgen
//#define TIME_LIMIT 600
#define TIME_LIMIT 1800
//#define TIME_LIMIT 18000
#define TIME_LIMIT_BIG 18000

#define COST_JJ //for using the costs of the Omega paper by JJ
//#define COST_0_CHANGES //for having a 0 cost on the z vars

//#define ALL_DP1 //si generano tutte le colonne e si risolve con Cplex intero
//#define ALL_COLS_IN_GAP //si generano tutte le colonne con costo ridotto minore del gap tra UB e LB
#define ALL_COLS_AND_Y_IN_GAP //si generano tutte le colonne e le var y con costo ridotto minore del gap tra UB e LB
//#define EVAL_MANUAL_SOL //valuta il costo della soluzione manuale secondo i parametri usati
#define MIN_NUM_Z //impone un lower bound sul numero di cambi
//#define CALCOLA_MIN_CAMBI //per calcolare il lower bound sul numero di cambi
//#define ITER_MIN_CAMBI 
#define LOG_FILE_CPLEX //per avere il file di log di cplex in all_cols_and_y_in_gap
//#define LOG_FILE_CPLEX_ALL_DP1 //per avere il file di log di cplex in all_dp1

//#define MAGGIO //si permettono 4 ore di sosta

#define MORE_COLS //per generare piu` colonne per ogni company ad ogni iterazione del column generation
#define N_MORE_COLS 10 //numero di colonne da generare ad ogni iterazione per ogni company nel column generation

#define AL //per le istanze che hanno il primo volo del mattino da A a L
//#define LA //per le istanze che hanno l'ultimo volo della sera da L ad A

//AL: 1/09 7/04
//LA: 2/09 1/04
//AL + LA 3/09 4/09 5/09 6/09 7/09 2/04 3/04 4/04 5/04 6/04 

typedef struct {
	int dep;
	int arr;
	int dep_t;
	int arr_t;
	int code;
#ifdef RETIME
	int* copies;
	int is_earliest_copy;
	int is_latest_copy;
	int its_real_flight_index;
	int is_original_flight;
#endif
} flight;

typedef struct {
	int n_of_succ;
	int* ind_succ;
	//valori degli archi in base alle variabili x
	double* x_values;
	//0-1 for telling if the succ is forbidden (used in the branch and price)
	int* forb;
	int* arcs; //used in the model for the y vars
} succ;


typedef struct {
	int n_of_pred;
	int* ind_pred;
	int* arcs;//used in the model for the y vars
} pred;


typedef struct {
	int pred; 
	int pred_label;
	double profit;
	int n_flights;
	int ind_table_duration;
	int dep_time_first_flight;
} label;

typedef struct {
	int dep_airport;
	int arr_airport;
	int max_flights;
	int n_of_crews;
	int company; //B=0, N=1, C=2
} crew_type;

typedef struct {
	int n_flights;
	int* flights;
	int type;
	int forb;
} crew_day;

typedef struct {
	int start;
	int end;
} interval;


typedef struct {
	int dep_airport;
	int company;
	int n_of_aircrafts;
} aircraft_type;

typedef struct {
	int n_of_succ;
	int* ind_succ;
	int airport;
	int flag; //0 = starting arc, 1 = ending arc
} arc;

#ifdef PRICING_ARCHI
typedef struct {
	int cont;
	int* indici_vincoli;
	double* coeff_vincoli;
} info_y;
#endif

//var globali
int n_of_flights;
flight** flight_array;
char** airports;
int n_of_crew_types; //one crew type for each airport where the crew must sleep
crew_type* crews;
succ** successori;
int n_of_cols;
CPXENVptr env;
CPXLPptr lp;
double LB;
double* beta;
double* alpha;
int n_of_constraints;
double* dual_vars;
double* alpha;
double* beta;
label** labels;
int* ind_labels;
int risolto;
crew_day** crew_day_pool;
int n_of_intervals;
interval* intervals;
int** duration_table;
int best_sol;
int nodes;
int level;
double* x;
int* not_terminal;
crew_day** best_solution;
int n_crew_duties;
char* name_input_file;
char* name_duration_file;
char* name_solution_file;
int PENAL1, PENAL2, PENAL3, PENAL4;
int TIME1, TIME2, TIME3, TIME4;
int MAX_TT, MAX_LL;
char our_day[10];
	


//full model vars
int offset; //offset wrt the initial position of the x_p columns
int n_auxil; //number of auxiliary variables for the crews
int n_of_aircraft_types;
aircraft_type* aircrafts; 
arc** starting_arcs;
arc** ending_arcs; 
int* main_airports;
int n_of_arcs; //do not include starting and ending arcs
int n_of_starting_arcs;
int n_of_ending_arcs;
int n_of_y;
pred** predecessori;
pred** predecessori_a;
succ** successori_a;
int n_of_arcs_a;  //do not include starting and ending arcs; a stands for aircraft
double** gamma_;
int n_of_z;
double** eta;
time_t start_cplex, end_cplex;
double durata_cplex;
int*** y_sol;
int n_of_short_arcs;
double*** delta;
int** indices_short_arcs;
int*** best_y_sol;
double best_LB;
int z_cost;
int y_cost_T, y_cost_L;
int x_cost_B, x_cost_C, x_cost_N;
int x_cost_pernocta_N, x_cost_pernocta_C;
int y_cost_TT, y_cost_LL;
int y_cost_intern_N, y_cost_intern_C;


int min_dep_time_A;
int min_dep_time_S;
int max_arr_time_A;
int max_arr_time_S;

#ifdef ALL_COLS_IN_GAP
double BestUB, BestLB;
#endif

#ifdef ALL_COLS_AND_Y_IN_GAP
double BestUB, BestLB;
#endif

#ifdef BOUND_COMPLETAMENTO
double* profitti_ridotti;
#endif

#ifdef PRICING_ARCHI
int conta_vincoli;
info_y* pricing_yvars;
int numero_vincoli_y;
#endif

//funzioni
void alloc_error(char *s);
void stop_error(char *s);
void free_memory();
void set_airports();
void set_crews();
void read_input_file(char* name_input_file);
void read_duration_table(char* name_duration_file);
int find_airport(char* name_airport);
void build_crew_graph();
void init_master();
double solve_master();
int solve_pricing();
int find_interval(int flight);
void branch_and_price (int level, double LB_father);
int branch_node();
void init_branch_and_price();
int branch_arcs(int ind_node);
int integer_sol_found();
void forbid_cols(int ind_node, int level, int flag_not_terminal_ind);
void unforbid_cols(int level);
int round_(double x);

//functions for the full model
void set_aircrafts();
void build_aircraft_graph();
void build_starting_ending_arcs();
void set_main_airports();
void read_config_file(char* name_config_file);
void print_best_solution();
int first_flight_A(int flight);
int last_flight_A(int flight);

void find_int_sol(int solo_lp);

#ifdef ALL_DP1
int solve_pricing_all_dp();
#endif

#ifdef ALL_COLS_IN_GAP
int solve_pricing_all_cols_in_gap();
void solve_additional_cols();
#endif

#ifdef ALL_COLS_AND_Y_IN_GAP
int solve_pricing_all_cols_in_gap();
void solve_additional_cols_and_y();
#endif

#ifdef EVAL_MANUAL_SOL
int find_index_flight(int flight_code);
double eval_sol(char* name_sol_file);
#endif

#ifdef RETIME
int cmp_dep_times(int* n1,int* n2);
int are_copies(int flight_a,int flight_b);
int find_first_flight_A();
int find_first_flight_S();
int find_last_flight_A();
int find_last_flight_S();
#endif

#ifdef PRICING_ARCHI
int pricing_arcs(double* duals);
#endif
